package com.dataworks;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.dataworks.adapter.PhValueDetailAdapter;
import com.dataworks.adapter.SensorTagValueDetailsAdapter;
import com.dataworks.data.PHSensor;
import com.dataworks.data.SensorTag;
import com.dataworks.data.Values;
import com.dataworks.database.DataBaseHelper;
import com.dataworks.database.DataBaseSingleTon;
import com.dataworks.util.Global;
import com.dataworks.util.NestedListView;
import com.dataworks.util.Utils;

public class ValueDetailsFragment extends Fragment {

	public static ValueDetailsFragment detailsFragment;
	private Values values;
	private TextView done_values;

	private TextView location, dateTime;
	private TextView back;
	private EditText name_edit, notes_edit;
	DataBaseHelper db;

	public MainActivity mainActivity;

	private NestedListView sensorTag_values_listView;
	private NestedListView pH_values_listView;

	private SensorTagValueDetailsAdapter sensorTagAdapter;
	private PhValueDetailAdapter pHAdapter;

	private String dateformat = "dd-MM-yyyy hh:mm:ss aa";

	public static ValueDetailsFragment newInstance(MainActivity mainActivity) {
		// if (detailsFragment == null)
		detailsFragment = new ValueDetailsFragment(mainActivity);
		return detailsFragment;
	}

	public ValueDetailsFragment(MainActivity parent) {
		// TODO Auto-generated constructor stub

		this.mainActivity = parent;
	}

	public String fromWhere;

	public void setData(Values values, String fromWhere) {
		this.values = values;
		this.fromWhere = fromWhere;
	}

	public static ValueDetailsFragment getInstance() {

		return detailsFragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.value_detail_fragment, container,
				false);

		DataBaseSingleTon.sContext = getActivity();

		db = DataBaseSingleTon.getDB();

		location = (TextView) view.findViewById(R.id.location);
		dateTime = (TextView) view.findViewById(R.id.dateTime);

		name_edit = (EditText) view.findViewById(R.id.name_edit);
		notes_edit = (EditText) view.findViewById(R.id.notes_edit);
		done_values = (TextView) view.findViewById(R.id.done_values);

		sensorTag_values_listView = (NestedListView) view
				.findViewById(R.id.sensorTag_values_listView);
		pH_values_listView = (NestedListView) view
				.findViewById(R.id.pH_values_listView);

		back = (TextView) view.findViewById(R.id.back);

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		name_edit.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (hasFocus) {
					visibilityDone();
				}
			}
		});

		notes_edit.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (hasFocus) {
					visibilityDone();
				}
			}
		});

		done_values.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				InvisibilityDone();

				String name = name_edit.getText().toString();
				String notes = notes_edit.getText().toString();

				values.setmName(name);
				values.setmNotes(notes);

				// mainActivity.curDataSet.setValueList(values);

				updateNotesAndNameValues(values.getmTime(), name, notes);

				if (fromWhere.equalsIgnoreCase("from_values")) {
					ValuesFragment.getInstance().notifyDataSetChanged();
				} else {
					mainActivity.curDataSet.updateNameOrNotesOrBoth(values);
				}
				try {
					Global.hideSoftKeyboard(getActivity());
				} catch (Exception e) {

				}
			}
		});

		notes_edit.setOnEditorActionListener(new OnEditorActionListener() {

			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				// TODO Auto-generated method stub
				if (actionId == EditorInfo.IME_ACTION_DONE) {

					InvisibilityDone();

					// do something
					String name = name_edit.getText().toString();
					String notes = notes_edit.getText().toString();

					values.setmName(name);
					values.setmNotes(notes);

					updateNotesAndNameValues(values.getmTime(), name, notes);

					if (fromWhere.equalsIgnoreCase("from_values")) {
						ValuesFragment.getInstance().notifyDataSetChanged();
					} else {
						mainActivity.curDataSet.updateNameOrNotesOrBoth(values);
					}
					try {
						Global.hideSoftKeyboard(getActivity());
					} catch (Exception e) {

					}
				}
				return false;
			}
		});

		return view;
	}

	public void visibilityDone() {
		name_edit.setCursorVisible(true);
		notes_edit.setCursorVisible(true);
		done_values.setVisibility(View.VISIBLE);
	}

	public void InvisibilityDone() {
		done_values.setVisibility(View.GONE);

		name_edit.setCursorVisible(false);
		notes_edit.setCursorVisible(false);
	}

	public void updateNotesAndNameValues(String valueId, String mName,
			String mNotes) {

		db.open();
		db.updateNameAndNotesForValue(valueId, mName, mNotes);
		db.close();
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		location.setText("Unknown");

		name_edit.setText(values.getmName());
		notes_edit.setText(values.getmNotes());

		long milliSeconds = Long.parseLong(values.getmTime());

		String date = Utils.getDate(milliSeconds, dateformat);

		dateTime.setText(date);

		ArrayList<SensorTag> sensorTagsList = values.getSensorTagsList();
		ArrayList<PHSensor> pHSensorsArrayList = values.getpHSensorsList();

		if (sensorTagsList != null) {

			sensorTagAdapter = new SensorTagValueDetailsAdapter(getActivity(),
					R.layout.sensor_tag_value_details_inflate, sensorTagsList);

			sensorTag_values_listView.setAdapter(sensorTagAdapter);
		}

		if (pHSensorsArrayList != null) {

			pHAdapter = new PhValueDetailAdapter(getActivity(),
					R.layout.ph_values_details_inlate, pHSensorsArrayList);

			pH_values_listView.setAdapter(pHAdapter);
		}

	}
}
