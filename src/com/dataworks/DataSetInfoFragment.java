package com.dataworks;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.dataworks.data.DataSet;
import com.dataworks.data.PHSensor;
import com.dataworks.data.SensorTag;
import com.dataworks.data.Values;
import com.dataworks.database.DataBaseHelper;
import com.dataworks.database.DataBaseSingleTon;
import com.dataworks.util.AddClickListener;
import com.dataworks.util.Global;
import com.dataworks.util.Utils;

public class DataSetInfoFragment extends Fragment {

	private static DataSetInfoFragment dataSetsInfoFragment;
	private DataSet dataset;
	private TextView d_name, d_notes, creation_date, back, last_used_date,
			export_btn;
	private TextView view_dataset_values;
	private AddClickListener mClickListener;

	public boolean isTouched;
	String DataBase_Name = "dataworks";
	SQLiteDatabase myDatabase = null;
	String Table_Name = "export_values";
	File dataset_value;
	File dataset_information;
	Uri datasetInformUri;
	Uri dataValueURI;
	DataBaseHelper db;
	ToggleButton toggleLocation;
	String mDataSetId, mName, mNotes, mDate, mTime, mLocation;

	private String dateformat = "dd-MM-yyyy hh:mm:ss aa";

	boolean isSensorTagAvailable = false;
	boolean ispHAvailable = false;

	public MainActivity mainActivity;
	public String from_where;

	public static DataSetInfoFragment newInstance(MainActivity parent) {
		// if (dataSetsInfoFragment == null)
		dataSetsInfoFragment = new DataSetInfoFragment(parent);

		return dataSetsInfoFragment;
	}

	private DataSetInfoFragment(MainActivity parent) {
		this.mainActivity = parent;
	}

	public void setData(DataSet dataset, String from_where) {
		this.dataset = dataset;
		this.from_where = from_where;
	}

	private ArrayList<Values> valuesArray = new ArrayList<>();

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View dataSetsView = inflater.inflate(R.layout.dataset_information,
				container, false);

		back = (TextView) dataSetsView.findViewById(R.id.back);
		d_name = (TextView) dataSetsView.findViewById(R.id.d_name);
		d_notes = (TextView) dataSetsView.findViewById(R.id.d_notes);
		creation_date = (TextView) dataSetsView
				.findViewById(R.id.creation_date);
		last_used_date = (TextView) dataSetsView
				.findViewById(R.id.last_used_date);
		export_btn = (TextView) dataSetsView.findViewById(R.id.export_btn);

		toggleLocation = (ToggleButton) dataSetsView
				.findViewById(R.id.toggleBtn);

		view_dataset_values = (TextView) dataSetsView
				.findViewById(R.id.view_dataset_values);

		d_name.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				if (hasFocus) {
					changeExportToDone();
				}

			}
		});
		d_notes.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				if (hasFocus) {
					changeExportToDone();
				}

			}
		});

		toggleLocation
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							AlertDialog.Builder builder = new AlertDialog.Builder(
									getActivity());
							builder.setTitle("Location Services");
							builder.setMessage("Location services are currently unavailable. We hope to make them available in an upcoming version. Your interest in using them has been noted.");
							builder.setPositiveButton("Done",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int which) {
											Log.e("info", "OK");
											toggleLocation.setChecked(false);
										}
									});

							builder.show();
						}
					}
				});

		view_dataset_values.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				mClickListener.onViewValues(dataset.getmName(),
						dataset.getmId(), dataset, "View_Values");

			}
		});

		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});
		export_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!isTouched) {

					new ExportDatabaseCSVTask().execute();
				} else {

					changeDoneToExport();

					String name = d_name.getText().toString();
					String notes = d_notes.getText().toString();

					dataset.setmName(name);
					dataset.setmNotes(notes);

					if (Global.getInstance()
							.getPreferenceVal(getActivity(), "datasetId")
							.equalsIgnoreCase(dataset.getmId())) {

						Global.getInstance().storeIntoPreference(getActivity(),
								"datasetName", name);

					}

					updateNotesAndNameDataSet(dataset.getmId(), name, notes);

					if (!from_where.equalsIgnoreCase("from_main")) {
						DataSetsFragment.newInstance(mainActivity)
								.notifyDataChanged();

					}

					try {
						Global.hideSoftKeyboard(getActivity());
					} catch (Exception e) {

					}
				}

			}
		});

		myDatabase = getActivity().openOrCreateDatabase(DataBase_Name,
				getActivity().MODE_PRIVATE, null);
		return dataSetsView;

	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		if (activity instanceof AddClickListener) {
			mClickListener = (AddClickListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implement BluetoothClickListener");
		}
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		DataBaseSingleTon.sContext = getActivity();
		db = DataBaseSingleTon.getDB();

		d_name.setText(dataset.getmName());
		d_notes.setText(dataset.getmNotes());
		// creation_date.setText(dataset.getmDate() + " " + dataset.getmTime());

		// creation_date.setText(dataset.getmDate() + " "
		// + Utils.Convert24to12(dataset.getmTime()));

		creation_date.setText(dataset.getmDate() + " " + (dataset.getmTime()));

		Calendar cl = Calendar.getInstance();
		cl.setTimeInMillis(Long.parseLong(dataset.getmDateAndTime())); // here
																		// your
																		// time
																		// in
																		// miliseconds

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

		String date = dateFormat.format(cl.getTime());

		String[] dateAndTime = date.split(" ");

		String time = dateAndTime[1];
		String timeIn12HrFormat = Utils.Convert24to12(time);

		// String date = "" + cl.get(Calendar.DAY_OF_MONTH) + ":"
		// + cl.get(Calendar.MONTH) + ":" + cl.get(Calendar.YEAR);
		// String time = "" + cl.get(Calendar.HOUR_OF_DAY) + ":"
		// + cl.get(Calendar.MINUTE) + ":" + cl.get(Calendar.SECOND);

		// last_used_date.setText(date + " " + time);
		last_used_date.setText(dateAndTime[0] + " " + time);

	}

	public void changeExportToDone() {
		isTouched = true;
		export_btn.setText("Done");
		d_name.setCursorVisible(true);
		d_notes.setCursorVisible(true);
	}

	public void changeDoneToExport() {
		export_btn.setText("Export");

		d_name.setCursorVisible(false);
		d_notes.setCursorVisible(false);

		isTouched = false;
	}

	public void updateNotesAndNameDataSet(String dataSetId, String mName,
			String mNotes) {
		// db = new DataBaseHelper(getActivity());
		db.open();
		db.updateDataSetsNameAndNotes(dataSetId, mName, mNotes);
		db.close();
	}

	/***** get values to export ***/
	public ArrayList<Values> getValuesToExport() {

		db.open();
		ArrayList<Values> array = new ArrayList<Values>();

		ArrayList<SensorTag> sensorsList = null;
		ArrayList<PHSensor> pHSensorsList = null;

		Cursor dataSetValues = db.getDPValuesForDataSet(dataset.getmId());
		if (dataSetValues.moveToFirst()) {
			do {

				sensorsList = new ArrayList<SensorTag>();
				pHSensorsList = new ArrayList<PHSensor>();

				String mDataSetId = dataSetValues.getString(0);
				String time = dataSetValues.getString(1);
				String data_point_name = dataSetValues.getString(2);
				String data_point_notes = dataSetValues.getString(3);
				String data_point_location = dataSetValues.getString(4);
				String mId = dataSetValues.getString(5);

				Values values = new Values();
				values.setmDataSetId(mDataSetId);
				values.setmTime(time);
				values.setmName(data_point_name);
				values.setmNotes(data_point_notes);
				values.setmLocation(data_point_location);
				values.setmId(mId);
				Cursor data = db.getValuesForDataPoint(time);
				if (data.moveToFirst()) {
					do {
						String deviceAddress = data.getString(0);
						String deviceName = data.getString(1);

						if (deviceName.equalsIgnoreCase("SensorTag")) {
							String ir_temp = data.getString(2);
							String object_temp = data.getString(3);
							String humidity = data.getString(4);
							String pressure = data.getString(5);
							String mag_x = data.getString(6);
							String mag_y = data.getString(7);
							String mag_z = data.getString(8);

							String acc_x = data.getString(9);
							String acc_y = data.getString(10);
							String acc_z = data.getString(11);
							String gyro_x = data.getString(12);
							String gyro_y = data.getString(13);
							String gyro_z = data.getString(14);

							SensorTag sensorTag = new SensorTag();
							sensorTag.setDeviceAddress(deviceAddress);
							sensorTag.setaTempValue(ir_temp);
							sensorTag.setoTempValue(object_temp);
							sensorTag.setHumidity(humidity);
							sensorTag.setBaroValue(pressure);
							sensorTag.setMagnetometerValue_x(mag_x);
							sensorTag.setMagnetometerValue_y(mag_y);
							sensorTag.setMagnetometerValue_z(mag_z);
							sensorTag.setAccelerometerValue_x(acc_x);
							sensorTag.setAccelerometerValue_y(acc_y);
							sensorTag.setAccelerometerValue_z(acc_z);
							sensorTag.setGyroMeterValue_x(gyro_x);
							sensorTag.setGyroMeterValue_y(gyro_y);
							sensorTag.setGyroMeterValue_z(gyro_z);

							sensorsList.add(sensorTag);

							values.setSensorTagsList(sensorsList);
						} else if (deviceName.equalsIgnoreCase("pH-Meter")) {

							String pH_value = data.getString(15);
							String pH_temp = data.getString(16);
							String mV = data.getString(17);

							PHSensor pHsensor = new PHSensor();
							pHsensor.setDeviceAddress(deviceAddress);
							pHsensor.setpHValue(pH_value);
							pHsensor.setTemp_value(pH_temp);
							pHsensor.setmV(mV);

							pHSensorsList.add(pHsensor);

							values.setpHSensorsList(pHSensorsList);
						}

					} while (data.moveToNext());
				}
				data.close();
				array.add(values);

			} while (dataSetValues.moveToNext());
		}
		dataSetValues.close();
		db.close();
		return array;
	}

	/***** get dataset name of particular selected dataset *****/
	public void getDataSetName(String id) {

		db = new DataBaseHelper(getActivity());
		db.open();

		Cursor dataSetValues = db.getDataSetsForId(id);
		if (dataSetValues.moveToFirst()) {
			do {

				mName = dataSetValues.getString(0);
				mNotes = dataSetValues.getString(1);
				mDate = dataSetValues.getString(2);
				mTime = dataSetValues.getString(3);
				mLocation = dataSetValues.getString(4);

			} while (dataSetValues.moveToNext());
		}
		dataSetValues.close();
		db.close();

	}

	/********* to expoert the database to make csv file in required coloumns ***********/
	public class ExportDatabaseCSVTask extends AsyncTask<String, Void, Boolean> {
		private final ProgressDialog dialog = new ProgressDialog(getActivity());

		@Override
		protected void onPreExecute() {
			this.dialog.setMessage("Writing Files");
			this.dialog.show();
		}

		protected Boolean doInBackground(final String... args) {

			File exportDir = new File(
					Environment.getExternalStorageDirectory(), "");

			if (!exportDir.exists()) {
				exportDir.mkdirs();
			}

			/****** selecting the fields and titles and values will be returned *********/
			getDataSetName(dataset.getmId());

			File file = new File(exportDir, mName + "_datasetInformation.csv");
			File file_new = new File(exportDir, mName + "_data.csv");

			generateCsvFile(file.toString());

			generateCsvFileForValues(file_new.toString());

			return true;
		}

		protected void onPostExecute(final Boolean success) {
			if (this.dialog.isShowing()) {
				this.dialog.dismiss();
			}
			if (success) {

				dataset_information = new File(
						Environment.getExternalStorageDirectory(), mName
								+ "_datasetInformation.csv");

				dataset_value = new File(
						Environment.getExternalStorageDirectory(), mName
								+ "_data.csv");

				datasetInformUri = Uri.fromFile(dataset_information);

				dataValueURI = Uri.fromFile(dataset_value);

				ArrayList<Uri> uris = new ArrayList<Uri>();
				// convert from paths to Android friendly Parcelable Uri's

				ArrayList<String> filePaths = new ArrayList<String>();
				filePaths.add(dataset_value.toString());
				filePaths.add(dataset_information.toString());

				for (String files : filePaths) {
					File fileIn = new File(files);
					Uri u = Uri.fromFile(fileIn);
					uris.add(u);
				}

				Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);

				emailIntent.setType("text/plain");
				emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,
						new String[] { "" });
				emailIntent.putExtra(Intent.EXTRA_SUBJECT,
						"Export of Dataset: " + mName);

				emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM,
						uris);
				try {
					startActivity(Intent.createChooser(emailIntent,
							"Send email..."));
				} catch (android.content.ActivityNotFoundException ex) {
					Toast.makeText(getActivity(),
							"There are no email clients installed.",
							Toast.LENGTH_SHORT).show();
				}

				/******* Send Email and sms of attached files **********/

			} else {
				// Toast.makeText(ExportContactsActivity.this, "Export failed",
				// Toast.LENGTH_SHORT).show();
			}
		}

		/*** get all devices list that are connectced ****/
		public ArrayList<String> getAllDevicesList() {
			ArrayList<String> devicesList = new ArrayList<String>();

			db.open();

			Cursor devices = db.getDevicesList(dataset.getmId());

			if (devices.moveToFirst()) {
				do {

					String deviceAddress = devices.getString(0);
					devicesList.add(deviceAddress);
				} while (devices.moveToNext());
			}
			devices.close();
			db.close();

			Utils.removeDuplicates(devicesList);
			return devicesList;
		}

		/***** get device type based on address ****/
		public String getDeviceType(String deviceAddress) {

			String deviceType = null;

			db = new DataBaseHelper(getActivity());
			db.open();

			Cursor deviceTypes = db
					.getDeviceTypeUsingDeviceAddress(deviceAddress);

			if (deviceTypes.moveToFirst()) {
				do {

					deviceType = deviceTypes.getString(0);

				} while (deviceTypes.moveToNext());
			}
			deviceTypes.close();
			db.close();

			return deviceType;

		}

		/**** generate csv file for values of particualr dataset ***/
		private void generateCsvFileForValues(String sFileName) {

			ArrayList<Values> valuesArray = dataset.getValueList();

			ArrayList<String> devicesList = getAllDevicesList();

			try {
				FileWriter writer = new FileWriter(sFileName);

				writer.append("Datapoint Name");
				writer.append(',');
				writer.append("Notes");
				writer.append(',');
				writer.append("Location");
				writer.append(',');
				writer.append("Date & Time Recorded");

				for (int i = 0; i < devicesList.size(); i++) {

					String deviceAddress = devicesList.get(i);
					String deviceType = getDeviceType(deviceAddress);

					if (deviceType != null) {
						if (deviceType.equalsIgnoreCase("Sensor Tag")) {
							writer.append(',');
							writer.append("Temperature (C)");
							writer.append(',');
							writer.append("Pressure (mbar)");
							writer.append(',');
							writer.append("Magnetometer-X (�T)");
							writer.append(',');
							writer.append("Magnetometer-Y (�T)");
							writer.append(',');
							writer.append("Magnetometer-Z (�T)");
							writer.append(',');
							writer.append("IR Temperature (C)");
							writer.append(',');
							writer.append("Humidity (%)");
							writer.append(',');
							writer.append("Gyroscope-X (deg/S)");
							writer.append(',');
							writer.append("Gyroscope-Y (deg/S)");
							writer.append(',');
							writer.append("Gyroscope-Z (deg/S)");
							writer.append(',');
							writer.append("Accelerometer-X (G)");
							writer.append(',');
							writer.append("Accelerometer-Y (G)");
							writer.append(',');
							writer.append("Accelerometer-Z (G)");
						} else if (deviceType.equalsIgnoreCase("pH")) {
							writer.append(',');
							writer.append("Temperature (C)");
							writer.append(',');
							writer.append("pH");
							writer.append(',');
							writer.append("mV");

						}
					}
				}

				writer.append('\n');

				for (int i = 0; i < valuesArray.size(); i++) {

					Values values = valuesArray.get(i);

					long milliSeconds = Long.parseLong(values.getmTime());

					String date = Utils.getDate(milliSeconds, dateformat);

					writer.append(values.getmName());
					writer.append(',');
					writer.append(values.getmNotes());
					writer.append(',');
					writer.append(values.getmLocation());
					writer.append(',');
					writer.append(date);

					ArrayList<SensorTag> sensorTagsList = values
							.getSensorTagsList();
					ArrayList<PHSensor> pHSensorsList = values
							.getpHSensorsList();

					for (int j = 0; j < devicesList.size(); j++) {

						String deviceAddress = devicesList.get(j);
						String deviceType = getDeviceType(deviceAddress);

						if (deviceType != null) {
							if (deviceType.equalsIgnoreCase("Sensor Tag")) {
								if (sensorTagsList != null) {
									for (int k = 0; k < sensorTagsList.size(); k++) {

										String deviceAddressName = sensorTagsList
												.get(k).getDeviceAddress();

										if (deviceAddress
												.equalsIgnoreCase(deviceAddressName)) {
											writer.append(',');
											writer.append(sensorTagsList.get(k)
													.getaTempValue());
											writer.append(',');
											writer.append(sensorTagsList.get(k)
													.getBaroValue());
											writer.append(',');
											writer.append(sensorTagsList.get(k)
													.getMagnetometerValue_x());
											writer.append(',');
											writer.append(sensorTagsList.get(k)
													.getMagnetometerValue_y());
											writer.append(',');
											writer.append(sensorTagsList.get(k)
													.getMagnetometerValue_z());
											writer.append(',');
											writer.append(sensorTagsList.get(k)
													.getoTempValue());
											writer.append(',');
											writer.append(sensorTagsList.get(k)
													.getHumidity());
											writer.append(',');
											writer.append(sensorTagsList.get(k)
													.getGyroMeterValue_x());
											writer.append(',');
											writer.append(sensorTagsList.get(k)
													.getGyroMeterValue_y());
											writer.append(',');
											writer.append(sensorTagsList.get(k)
													.getGyroMeterValue_z());
											writer.append(',');
											writer.append(sensorTagsList.get(k)
													.getAccelerometerValue_x());
											writer.append(',');
											writer.append(sensorTagsList.get(k)
													.getAccelerometerValue_y());
											writer.append(',');
											writer.append(sensorTagsList.get(k)
													.getAccelerometerValue_z());

											break;

										} else {
											if (k == (sensorTagsList.size() - 1)) {
												writer.append(',');
												writer.append("");
												writer.append(',');
												writer.append("");
												writer.append(',');
												writer.append("");
												writer.append(',');
												writer.append("");
												writer.append(',');
												writer.append("");
												writer.append(',');
												writer.append("");
												writer.append(',');
												writer.append("");
												writer.append(',');
												writer.append("");
												writer.append(',');
												writer.append("");
												writer.append(',');
												writer.append("");
												writer.append(',');
												writer.append("");
												writer.append(',');
												writer.append("");
												writer.append(',');
												writer.append("");
											}

										}

									}
								} else {
									writer.append(',');
									writer.append("");
									writer.append(',');
									writer.append("");
									writer.append(',');
									writer.append("");
									writer.append(',');
									writer.append("");
									writer.append(',');
									writer.append("");
									writer.append(',');
									writer.append("");
									writer.append(',');
									writer.append("");
									writer.append(',');
									writer.append("");
									writer.append(',');
									writer.append("");
									writer.append(',');
									writer.append("");
									writer.append(',');
									writer.append("");
									writer.append(',');
									writer.append("");
									writer.append(',');
									writer.append("");
								}

							} else if (deviceType.equalsIgnoreCase("pH")) {

								if (pHSensorsList != null) {
									for (int k = 0; k < pHSensorsList.size(); k++) {

										String deviceAddressName = pHSensorsList
												.get(k).getDeviceAddress();

										if (deviceAddress
												.equalsIgnoreCase(deviceAddressName)) {
											writer.append(',');
											writer.append(pHSensorsList.get(k)
													.getTemp_value());
											writer.append(',');
											writer.append(pHSensorsList.get(k)
													.getpHValue());
											writer.append(',');
											writer.append(pHSensorsList.get(k)
													.getmV());

											break;

										} else {
											if (k == (pHSensorsList.size() - 1)) {
												writer.append(',');
												writer.append("");
												writer.append(',');
												writer.append("");
												writer.append(',');
												writer.append("");
											}
										}

									}
								} else {
									writer.append(',');
									writer.append("");
									writer.append(',');
									writer.append("");
									writer.append(',');
									writer.append("");
								}

							}
						}

					}

					writer.append('\n');
				}

				// generate whatever data you want

				writer.flush();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		/**** generates csv file dataset info and values info *****/
		private void generateCsvFile(String sFileName) {

			try {
				FileWriter writer = new FileWriter(sFileName);

				writer.append("Dataset Name");
				writer.append(',');
				writer.append(mName);
				writer.append(',');
				writer.append("");
				writer.append(',');
				writer.append("");
				writer.append(',');
				writer.append("");
				writer.append(',');
				writer.append("");
				writer.append('\n');

				writer.append("Dataset Notes");
				writer.append(',');
				writer.append(mNotes);
				writer.append(',');
				writer.append("");
				writer.append(',');
				writer.append("");
				writer.append(',');
				writer.append("");
				writer.append(',');
				writer.append("");
				writer.append('\n');

				writer.append("");
				writer.append(',');
				writer.append("");
				writer.append(',');
				writer.append("");
				writer.append(',');
				writer.append("");
				writer.append(',');
				writer.append("");
				writer.append(',');
				writer.append("");
				writer.append('\n');

				writer.append("Type");
				writer.append(',');
				writer.append("Device Name");
				writer.append(',');
				writer.append("UUID");
				writer.append(',');
				writer.append("Serial Number");
				writer.append(',');
				writer.append("Manufacturer");
				writer.append(',');
				writer.append("Model");
				writer.append('\n');

				ArrayList<DeviceData> devicesList = getDeviceAddressandName();

				for (int i = 0; i < devicesList.size(); i++) {

					String deviceType = devicesList.get(i).getDeviceType();

					if (deviceType.equalsIgnoreCase("Sensor Tag")) {

						String deviceAddress = devicesList.get(i)
								.getDeviceAddress();
						ArrayList<DeviceData> deviceDataArray = getDeviceInfoFromDB(deviceAddress);

						writer.append("Sensor Tag");
						writer.append(',');
						writer.append(deviceDataArray.get(0).getCustomName());
						writer.append(',');
						writer.append("N.A");
						writer.append(',');
						writer.append(deviceDataArray.get(0).getSerialNumber());
						writer.append(',');
						writer.append(deviceDataArray.get(0)
								.getManufacturerName());
						writer.append(',');
						writer.append(deviceDataArray.get(0).getModelNumber());
						writer.append('\n');

					} else if (deviceType.equalsIgnoreCase("pH")) {
						String deviceAddress = devicesList.get(i)
								.getDeviceAddress();
						ArrayList<DeviceData> deviceDataArray = getDeviceInfoFromDB(deviceAddress);

						writer.append("pH");
						writer.append(',');
						writer.append(deviceDataArray.get(0).getCustomName());
						writer.append(',');
						writer.append("N.A");
						writer.append(',');
						writer.append(deviceDataArray.get(0).getSerialNumber());
						writer.append(',');
						writer.append(deviceDataArray.get(0)
								.getManufacturerName());
						writer.append(',');
						writer.append(deviceDataArray.get(0).getModelNumber());
						writer.append('\n');
					}

				}

				// generate whatever data you want

				writer.flush();
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/***** get device info of partcular device based on address from database ***/
	public ArrayList<DeviceData> getDeviceInfoFromDB(String deviceAddress) {
		ArrayList<DeviceData> devicesList = new ArrayList<DeviceData>();

		db.open();

		Cursor deviceInfo = db.getDeviceInfo(deviceAddress);

		if (deviceInfo.moveToFirst()) {
			do {

				String deviceCustomName = deviceInfo.getString(0);
				String Rssi = deviceInfo.getString(1);
				String forwardRevision = deviceInfo.getString(2);
				String manuFacturerName = deviceInfo.getString(3);
				String hardWareRevision = deviceInfo.getString(4);
				String serialNumber = deviceInfo.getString(5);
				String modelNumber = deviceInfo.getString(6);
				String batteryLevel = deviceInfo.getString(7);
				String deviceType = deviceInfo.getString(8);

				DeviceData deviceData = new DeviceData();
				deviceData.setCustomName(deviceCustomName);
				deviceData.setDeviceType(deviceType);
				deviceData.setModelNumber(modelNumber);
				deviceData.setManufacturerName(manuFacturerName);
				deviceData.setSerialNumber(serialNumber);

				devicesList.add(deviceData);

			} while (deviceInfo.moveToNext());
		}
		deviceInfo.close();
		db.close();

		return devicesList;
	}

	/****** get device address and device name *****/
	public ArrayList<DeviceData> getDeviceAddressandName() {

		ArrayList<DeviceData> devicesList = new ArrayList<DeviceData>();

		db.open();

		Cursor deviceInfo = db.getDeviceInfoAddressAndNames();

		if (deviceInfo.moveToFirst()) {
			do {

				String deviceAddress = deviceInfo.getString(0);
				String deviceType = deviceInfo.getString(1);

				DeviceData deviceData = new DeviceData();
				deviceData.setDeviceAddress(deviceAddress);
				deviceData.setDeviceType(deviceType);

				devicesList.add(deviceData);

			} while (deviceInfo.moveToNext());
		}
		deviceInfo.close();
		db.close();
		return devicesList;
	}

	public class DeviceData {
		String deviceAddress;
		String deviceType;
		String serialNumber;
		String manufacturerName;
		String modelNumber;
		String customName;

		public String getSerialNumber() {
			return serialNumber;
		}

		public void setSerialNumber(String serialNumber) {
			this.serialNumber = serialNumber;
		}

		public String getManufacturerName() {
			return manufacturerName;
		}

		public void setManufacturerName(String manufacturerName) {
			this.manufacturerName = manufacturerName;
		}

		public String getModelNumber() {
			return modelNumber;
		}

		public void setModelNumber(String modelNumber) {
			this.modelNumber = modelNumber;
		}

		public String getCustomName() {
			return customName;
		}

		public void setCustomName(String customName) {
			this.customName = customName;
		}

		public String getDeviceAddress() {
			return deviceAddress;
		}

		public void setDeviceAddress(String deviceAddress) {
			this.deviceAddress = deviceAddress;
		}

		public String getDeviceType() {
			return deviceType;
		}

		public void setDeviceType(String deviceType) {
			this.deviceType = deviceType;
		}

	}
}
