package com.dataworks.adapter;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dataworks.data.PHSensor;
import com.dataworks.R;

public class PhValueDetailAdapter extends BaseAdapter {

	private int layoutResourceId;
	private Context mContext;
	private ArrayList<PHSensor> pHSensorsArrayList;

	LayoutInflater inflater;
	private DecimalFormat decimal_2 = new DecimalFormat("0.00");

	public PhValueDetailAdapter(Context mContext, int layoutResourceId,
			ArrayList<PHSensor> pHSensorsArrayList) {
		this.mContext = mContext;
		this.pHSensorsArrayList = pHSensorsArrayList;
		this.layoutResourceId = layoutResourceId;

		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return pHSensorsArrayList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return pHSensorsArrayList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.ph_Value = (TextView) convertView
					.findViewById(R.id.ph_Value);
			holder.ph_temp_value = (TextView) convertView
					.findViewById(R.id.ph_temp_value);
			convertView.setTag(holder);
		}

		else {

			holder = (ViewHolder) convertView.getTag();
		}

		holder.ph_Value.setText(pHSensorsArrayList.get(position).getpHValue());

		String temp_value = decimal_2.format(Float
				.parseFloat(pHSensorsArrayList.get(position).getTemp_value()));

		holder.ph_temp_value.setText(temp_value + " ºC");

		return convertView;
	}

	class ViewHolder {
		TextView ph_Value;
		TextView ph_temp_value;
	}

}
