package com.dataworks.adapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.dataworks.R;

import com.dataworks.data.DataSet;
import com.dataworks.data.PHSensor;
import com.dataworks.data.SensorTag;
import com.dataworks.data.Values;
import com.dataworks.database.DataBaseHelper;
import com.dataworks.database.DataBaseSingleTon;
import com.dataworks.util.AddClickListener;
import com.dataworks.util.Global;
import com.dataworks.util.Utils;

public class DataSetAdapter extends BaseAdapter {

	private int layoutResourceId;
	private Context mContext;
	private ArrayList<DataSet> datasetsArray;

	LayoutInflater inflater;
	private AddClickListener mClickListener;

	public boolean mButtonClick;

	private DataBaseHelper db;

	public DataSetAdapter(Context mContext, int layoutResourceId,
			ArrayList<DataSet> datasets) {
		this.mContext = mContext;
		this.datasetsArray = datasets;
		this.layoutResourceId = layoutResourceId;

		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		DataBaseSingleTon.sContext = mContext;

		db = DataBaseSingleTon.getDB();

		if (mContext instanceof AddClickListener) {
			mClickListener = (AddClickListener) mContext;
		} else {
			throw new IllegalArgumentException(
					"Activity must implement BluetoothClickListener");
		}

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return datasetsArray.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return datasetsArray.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();
			holder.dataSetName = (TextView) convertView
					.findViewById(R.id.dataset_name);
			holder.dateAndTime = (TextView) convertView
					.findViewById(R.id.date_time);
			holder.info_icon = (TextView) convertView
					.findViewById(R.id.info_icon);

			holder.info_icon.setTag(position);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		holder.dataSetName.setText(datasetsArray.get(position).getmName());
		// holder.dateAndTime.setText(datasetsArray.get(position).getmDate() +
		// " "
		// + datasetsArray.get(position).getmTime());
		Calendar cl = Calendar.getInstance();
		cl.setTimeInMillis(Long.parseLong(datasetsArray.get(position)
				.getmDateAndTime())); // here your time in miliseconds

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

		String date = dateFormat.format(cl.getTime());

		String[] dateAndTime = date.split(" ");

		String time = dateAndTime[1];
		String timeIn12HrFormat = Utils.Convert24to12(time);

		holder.dateAndTime.setText(dateAndTime[0] + "  " + timeIn12HrFormat);
		String m_id = datasetsArray.get(position).getmId();
		String mId = Global.getInstance().getPreferenceVal(mContext,
				"datasetId");
		if (m_id.equalsIgnoreCase(mId)) {
			convertView.setBackgroundColor(Color.parseColor("#E4E9F0"));
		} else {
			convertView.setBackground(mContext.getResources().getDrawable(
					R.drawable.list_selector));

		}
		holder.info_icon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!mButtonClick) {

					// DataSet dataset = datasetsArray.get(Integer.parseInt(v
					// .getTag().toString()));

					DataSet dataset = datasetsArray.get(position);

					String mId = dataset.getmId();

					String selectedDatasetId = Global.getInstance()
							.getPreferenceVal(mContext, "datasetId");
					if (selectedDatasetId.equalsIgnoreCase(mId)) {
						mClickListener.onDataSetInfo(dataset, "from_dataset");
					} else {
						new GetValuesForDataSet(mId, dataset).execute();
					}

					// mClickListener.onDataSetInfo(datasetsArray.get(Integer
					// .parseInt(v.getTag().toString())));

					System.out.println();
				}

			}
		});

		return convertView;
	}

	class ViewHolder {
		TextView dataSetName;
		TextView dateAndTime;
		TextView info_icon;

	}

	public void setButtonClick(boolean buttonClick) {
		mButtonClick = buttonClick;
	}

	private class GetValuesForDataSet extends
			AsyncTask<String, ArrayList<Values>, ArrayList<Values>> {

		private ProgressDialog progressDialog;

		private String dataSetId;

		private DataSet dataset;

		public GetValuesForDataSet(String mId, DataSet dataset) {
			// TODO Auto-generated constructor stub
			this.dataSetId = mId;

			this.dataset = dataset;

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			// if (number > 200) {
			progressDialog = new ProgressDialog(mContext);

			progressDialog.setMessage("Loading values...");
			progressDialog.setCancelable(false);
			progressDialog.show();
			// }

		}

		@Override
		protected ArrayList<Values> doInBackground(String... params) {
			// TODO Auto-generated method stub

			ArrayList<Values> valuesList = getValuesForDataSet(dataSetId);

			return valuesList;
		}

		@Override
		protected void onPostExecute(ArrayList<Values> valuesList) {
			// TODO Auto-generated method stub
			super.onPostExecute(valuesList);

			// if (number > 200) {
			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			// }

			// Collections.reverse(valuesList);

			dataset.setValuesDataList(valuesList);

			mClickListener.onDataSetInfo(dataset, "from_dataset");

		}

	}

	public ArrayList<Values> getValuesForDataSet(String id) {
		// db = new DataBaseHelper(getActivity());
		db.open();
		ArrayList<Values> array = new ArrayList<Values>();

		ArrayList<SensorTag> sensorsList = null;
		ArrayList<PHSensor> pHSensorsList = null;

		Cursor dataSetValues = db.getDPValuesForDataSet(id);
		if (dataSetValues.moveToFirst()) {
			do {

				sensorsList = new ArrayList<SensorTag>();
				pHSensorsList = new ArrayList<PHSensor>();

				String mDataSetId = dataSetValues.getString(0);
				String time = dataSetValues.getString(1);
				String data_point_name = dataSetValues.getString(2);
				String data_point_notes = dataSetValues.getString(3);
				String data_point_location = dataSetValues.getString(4);
				String mId = dataSetValues.getString(5);

				Values values = new Values();
				values.setmDataSetId(mDataSetId);
				values.setmTime(time);
				values.setmName(data_point_name);
				values.setmNotes(data_point_notes);
				values.setmLocation(data_point_location);
				values.setmId(mId);
				Cursor data = db.getValuesForDataPoint(time);
				if (data.moveToFirst()) {
					do {
						String deviceAddress = data.getString(0);
						String deviceName = data.getString(1);

						if (deviceName.equalsIgnoreCase("SensorTag")) {
							String ir_temp = data.getString(2);
							String object_temp = data.getString(3);
							String humidity = data.getString(4);
							String pressure = data.getString(5);
							String mag_x = data.getString(6);
							String mag_y = data.getString(7);
							String mag_z = data.getString(8);

							String acc_x = data.getString(9);
							String acc_y = data.getString(10);
							String acc_z = data.getString(11);
							String gyro_x = data.getString(12);
							String gyro_y = data.getString(13);
							String gyro_z = data.getString(14);

							SensorTag sensorTag = new SensorTag();
							sensorTag.setDeviceAddress(deviceAddress);
							sensorTag.setDeviceName(deviceName);
							sensorTag.setaTempValue(ir_temp);
							sensorTag.setoTempValue(object_temp);
							sensorTag.setHumidity(humidity);
							sensorTag.setBaroValue(pressure);
							sensorTag.setMagnetometerValue_x(mag_x);
							sensorTag.setMagnetometerValue_y(mag_y);
							sensorTag.setMagnetometerValue_z(mag_z);
							sensorTag.setAccelerometerValue_x(acc_x);
							sensorTag.setAccelerometerValue_y(acc_y);
							sensorTag.setAccelerometerValue_z(acc_z);
							sensorTag.setGyroMeterValue_x(gyro_x);
							sensorTag.setGyroMeterValue_y(gyro_y);
							sensorTag.setGyroMeterValue_z(gyro_z);

							sensorsList.add(sensorTag);

							values.setSensorTagsList(sensorsList);
						} else if (deviceName.equalsIgnoreCase("pH-Meter")) {

							String pH_value = data.getString(15);
							String pH_temp = data.getString(16);
							String pH_mV = data.getString(17);

							PHSensor pHsensor = new PHSensor();
							pHsensor.setDeviceAddress(deviceAddress);
							pHsensor.setDeviceName(deviceName);
							pHsensor.setpHValue(pH_value);
							pHsensor.setTemp_value(pH_temp);
							pHsensor.setmV(pH_mV);

							pHSensorsList.add(pHsensor);

							values.setpHSensorsList(pHSensorsList);
						}

					} while (data.moveToNext());
				}

				data.close();

				array.add(values);

			} while (dataSetValues.moveToNext());
		}
		dataSetValues.close();
		db.close();
		return array;

	}
}
