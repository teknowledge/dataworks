package com.dataworks.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dataworks.R;
import com.dataworks.data.PHSensor;
import com.dataworks.data.SensorTag;
import com.dataworks.data.Values;
import com.dataworks.util.Utils;

public class ValuesAdapter extends BaseAdapter {

	private int layoutResourceId;
	private Context mContext;
	private ArrayList<Values> valuesArray;

	LayoutInflater inflater;

	private String dateformat = "dd-MM-yyyy hh:mm:ss aa";

	public ValuesAdapter(Context mContext, int layoutResourceId,
			ArrayList<Values> values) {
		this.mContext = mContext;
		this.valuesArray = values;
		this.layoutResourceId = layoutResourceId;

		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return valuesArray.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return valuesArray.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();

			holder.dateAndTime = (TextView) convertView
					.findViewById(R.id.date_and_time);

			// holder.red_dot = (ImageView)
			// convertView.findViewById(R.id.red_dot);
			// holder.green_dot = (ImageView) convertView
			// .findViewById(R.id.green_dot);
			holder.dots_layout = (LinearLayout) convertView
					.findViewById(R.id.dots_layout);

			holder.dots_layout.setTag(position);

			convertView.setTag(holder);

		} else {

			holder = (ViewHolder) convertView.getTag();
		}
		if (valuesArray.get(position).getmName().equals("")) {

			long milliSeconds = Long.parseLong(valuesArray.get(position)
					.getmTime());

			String date = Utils.getDate(milliSeconds, dateformat);

			holder.dateAndTime.setText(date);

		} else {
			holder.dateAndTime.setText(valuesArray.get(position).getmName());
		}

		ArrayList<SensorTag> sensorTagsList = valuesArray.get(position)
				.getSensorTagsList();
		ArrayList<PHSensor> pHsensorsList = valuesArray.get(position)
				.getpHSensorsList();
		//
		// int tag = (int) holder.dots_layout.getTag();
		//
		// if (tag == position) {
		holder.dots_layout.removeAllViews();
		if (sensorTagsList != null) {
			int sensorTagsCount = sensorTagsList.size();
			if (sensorTagsCount > 0) {
				for (int i = 0; i < sensorTagsCount; i++) {
					ImageView imageView = new ImageView(mContext);
					imageView.setImageResource(R.drawable.red_dot);
					imageView.setPadding(0, 0, 5, 0);
					holder.dots_layout.addView(imageView);
				}
			}
		}
		if (pHsensorsList != null) {
			int pHSensorsCount = pHsensorsList.size();
			if (pHSensorsCount > 0) {
				for (int i = 0; i < pHSensorsCount; i++) {
					ImageView imageView = new ImageView(mContext);
					imageView.setImageResource(R.drawable.green_dot);
					imageView.setPadding(0, 0, 5, 0);
					holder.dots_layout.addView(imageView);
				}
			}
		}
		// }

		return convertView;
	}

	class ViewHolder {

		TextView dateAndTime;
		ImageView red_dot;
		ImageView green_dot;
		LinearLayout dots_layout;
	}

}
