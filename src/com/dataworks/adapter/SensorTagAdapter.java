package com.dataworks.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dataworks.R;
import com.dataworks.data.SensorTag;
import com.dataworks.util.AddClickListener;

public class SensorTagAdapter extends BaseAdapter {

	private int layoutResourceId;
	private Context mContext;
	private ArrayList<SensorTag> sensorsArray;

	LayoutInflater inflater;
	private AddClickListener mClickListener;
	private boolean isIntervalRecordingActivated;
	private boolean isIntervalRecordingStarted;
	private boolean isSwitchBlack;

	public SensorTagAdapter(Context mContext, int layoutResourceId,
			ArrayList<SensorTag> sensorsArray) {
		this.mContext = mContext;
		this.sensorsArray = sensorsArray;
		this.layoutResourceId = layoutResourceId;

		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		if (mContext instanceof AddClickListener) {
			mClickListener = (AddClickListener) mContext;
		} else {
			throw new IllegalArgumentException(
					"Activity must implement BluetoothClickListener");
		}

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return sensorsArray.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return sensorsArray.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);

			holder = new ViewHolder();

			holder.sensorTag_parent_layout = (LinearLayout) convertView
					.findViewById(R.id.sensorTag_parent_layout);

			holder.sensor_tag_device_name_layout = (LinearLayout) convertView
					.findViewById(R.id.sensor_tag_device_name_layout);

			holder.info_inflate_scale_layout = (RelativeLayout) convertView
					.findViewById(R.id.info_inflate_scale_layout);
			holder.info_scale_info_button = (ImageButton) convertView
					.findViewById(R.id.info_scale_info_button);
			holder.info_scale_calibrate_button = (ImageButton) convertView
					.findViewById(R.id.info_scale_calibrate_button);

			holder.bluetooth_device = (TextView) convertView
					.findViewById(R.id.bluetooth_device);
			holder.sensor_tag_values_layout = (LinearLayout) convertView
					.findViewById(R.id.sensor_tag_values_layout);
			holder.record_device = (ImageView) convertView
					.findViewById(R.id.record_device);
			holder.switch_device = (ImageView) convertView
					.findViewById(R.id.switch_device);
			holder.oTemp_drawable = (ImageView) convertView
					.findViewById(R.id.oTemp_drawable);
			holder.oTemp = (TextView) convertView.findViewById(R.id.oTemp);
			holder.aTemp_drawable = (ImageView) convertView
					.findViewById(R.id.aTemp_drawable);
			holder.aTemp = (TextView) convertView.findViewById(R.id.aTemp);
			holder.baro_drawable = (ImageView) convertView
					.findViewById(R.id.baro_drawable);
			holder.baro = (TextView) convertView.findViewById(R.id.baro);

			holder.hum_drawable = (ImageView) convertView
					.findViewById(R.id.hum_drawable);
			holder.hum = (TextView) convertView.findViewById(R.id.hum);
			holder.mag_x = (TextView) convertView.findViewById(R.id.mag_x);
			holder.mag_y = (TextView) convertView.findViewById(R.id.mag_y);
			holder.mag_z = (TextView) convertView.findViewById(R.id.mag_z);
			holder.acc_x = (TextView) convertView.findViewById(R.id.acc_x);
			holder.acc_y = (TextView) convertView.findViewById(R.id.acc_y);
			holder.acc_z = (TextView) convertView.findViewById(R.id.acc_z);
			holder.gyro_x = (TextView) convertView.findViewById(R.id.gyro_x);
			holder.gyro_y = (TextView) convertView.findViewById(R.id.gyro_y);
			holder.gyro_z = (TextView) convertView.findViewById(R.id.gyro_z);
			holder.disconnected_text_layout = (FrameLayout) convertView
					.findViewById(R.id.disconnected_text_layout);

			holder.progress_m_x = (ProgressBar) convertView
					.findViewById(R.id.progress_m_x);
			holder.progress_m_y = (ProgressBar) convertView
					.findViewById(R.id.progress_m_y);
			holder.progress_m_z = (ProgressBar) convertView
					.findViewById(R.id.progress_m_z);
			holder.progress_a_x = (ProgressBar) convertView
					.findViewById(R.id.progress_a_x);
			holder.progress_a_y = (ProgressBar) convertView
					.findViewById(R.id.progress_a_y);
			holder.progress_a_z = (ProgressBar) convertView
					.findViewById(R.id.progress_a_z);
			holder.progress_g_x = (ProgressBar) convertView
					.findViewById(R.id.progress_g_x);
			holder.progress_g_y = (ProgressBar) convertView
					.findViewById(R.id.progress_g_y);
			holder.progress_g_z = (ProgressBar) convertView
					.findViewById(R.id.progress_g_z);

			holder.sensorTag_parent_layout.setTag(sensorsArray.get(position)
					.getDeviceAddress());

			convertView.setTag(holder);

		} else {

			holder = (ViewHolder) convertView.getTag();
		}

		holder.info_inflate_scale_layout.setVisibility(View.GONE);

		holder.bluetooth_device.setText(sensorsArray.get(position)
				.getDeviceName());

		if (sensorsArray.get(position).getAccelerometerValue_x() != null) {
			holder.acc_x.setText(sensorsArray.get(position)
					.getAccelerometerValue_x() + "G");
		} else {
			holder.acc_x.setText("X: 0.0" + "G");
		}
		if (sensorsArray.get(position).getAccelerometerValue_y() != null) {
			holder.acc_y.setText(sensorsArray.get(position)
					.getAccelerometerValue_y() + "G");
		} else {
			holder.acc_y.setText("Y: 0.0" + "G");
		}
		if (sensorsArray.get(position).getAccelerometerValue_z() != null) {
			holder.acc_z.setText(sensorsArray.get(position)
					.getAccelerometerValue_z() + "G");
		} else {
			holder.acc_z.setText("Z: 0.0" + "G");
		}

		holder.progress_a_x.setProgress(sensorsArray.get(position)
				.getProgress_acc_x());
		holder.progress_a_y.setProgress(sensorsArray.get(position)
				.getProgress_acc_y());
		holder.progress_a_z.setProgress(sensorsArray.get(position)
				.getProgress_acc_z());

		if (sensorsArray.get(position).getMagnetometerValue_x() != null) {
			holder.mag_x.setText(sensorsArray.get(position)
					.getMagnetometerValue_x() + "µT");
		} else {
			holder.mag_x.setText("X: 0.0" + "µT");
		}

		if (sensorsArray.get(position).getMagnetometerValue_y() != null) {
			holder.mag_y.setText(sensorsArray.get(position)
					.getMagnetometerValue_y() + "µT");
		} else {
			holder.mag_y.setText("Y: 0.0" + "µT");
		}
		if (sensorsArray.get(position).getMagnetometerValue_z() != null) {
			holder.mag_z.setText(sensorsArray.get(position)
					.getMagnetometerValue_z() + "µT");
		} else {
			holder.mag_z.setText("Z: 0.0" + "µT");
		}

		holder.progress_m_x.setProgress(sensorsArray.get(position)
				.getProgress_mag_x());
		holder.progress_m_y.setProgress(sensorsArray.get(position)
				.getProgress_mag_y());
		holder.progress_m_z.setProgress(sensorsArray.get(position)
				.getProgress_mag_z());

		if (sensorsArray.get(position).getGyroMeterValue_x() != null) {
			holder.gyro_x.setText(sensorsArray.get(position)
					.getGyroMeterValue_x() + "º/s");
		} else {
			holder.gyro_x.setText("X: 0.0" + "º/s");
		}
		if (sensorsArray.get(position).getGyroMeterValue_y() != null) {
			holder.gyro_y.setText(sensorsArray.get(position)
					.getGyroMeterValue_y() + "º/s");
		} else {
			holder.gyro_y.setText("Y: 0.0" + "º/s");
		}

		if (sensorsArray.get(position).getGyroMeterValue_z() != null) {
			holder.gyro_z.setText(sensorsArray.get(position)
					.getGyroMeterValue_z() + "º/s");
		} else {
			holder.gyro_z.setText("Z: 0.0" + "º/s");
		}

		holder.progress_g_x.setProgress(sensorsArray.get(position)
				.getProgress_gyro_x());
		holder.progress_g_y.setProgress(sensorsArray.get(position)
				.getProgress_gyro_y());
		holder.progress_g_z.setProgress(sensorsArray.get(position)
				.getProgress_gyro_z());

		if (sensorsArray.get(position).getHumidity() != null) {
			holder.hum.setText(sensorsArray.get(position).getHumidity());
		} else {
			holder.hum.setText("00.00");
		}

		if (sensorsArray.get(position).getaTempValue() != null) {
			holder.aTemp.setText(sensorsArray.get(position).getaTempValue());
		} else {
			holder.aTemp.setText("00.00");
		}

		if (sensorsArray.get(position).getoTempValue() != null) {
			holder.oTemp.setText(sensorsArray.get(position).getoTempValue());
		} else {
			holder.oTemp.setText("00.00");
		}

		if (sensorsArray.get(position).getBaroValue() != null) {
			holder.baro.setText(sensorsArray.get(position).getBaroValue());
		} else {
			holder.baro.setText("00.00");
		}

		holder.sensorTag_parent_layout
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						openSensorTagMenuLayout(holder.info_inflate_scale_layout);
					}
				});

		holder.info_scale_info_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				closeSensorTagMenuLayout(holder.info_inflate_scale_layout);

				mClickListener.openDeviceInfoFragment(sensorsArray
						.get(position).getDeviceAddress());

			}
		});

		holder.info_scale_calibrate_button
				.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						closeSensorTagMenuLayout(holder.info_inflate_scale_layout);

						mClickListener
								.openSensorTagCalibrationFragment(sensorsArray
										.get(position).getDeviceAddress());

					}
				});

		boolean isDiscoonected = sensorsArray.get(position).ismDisconnected();

		if (isDiscoonected) {
			holder.sensor_tag_values_layout.setVisibility(View.GONE);
			holder.disconnected_text_layout.setVisibility(View.VISIBLE);
		} else {
			holder.sensor_tag_values_layout.setVisibility(View.VISIBLE);
			holder.disconnected_text_layout.setVisibility(View.GONE);
		}

		if (isIntervalRecordingActivated) {
			holder.record_device.setImageResource(R.drawable.start_red_button);
			holder.switch_device.setImageResource(R.drawable.switch_red_button);

		} else {
			holder.record_device.setImageResource(R.drawable.record_button);
			holder.switch_device.setImageResource(R.drawable.switch_button);
		}

		if (isIntervalRecordingStarted) {
			holder.record_device.setImageResource(R.drawable.stop_red_button);
			holder.switch_device
					.setImageResource(R.drawable.switch_black_button);
		}

		return convertView;
	}

	public void setInteralRecordingActivated(boolean isIRActivated) {
		isIntervalRecordingActivated = isIRActivated;
	}

	public void setInteralRecordingStarted(boolean isStarted) {
		isIntervalRecordingStarted = isStarted;
	}

	public class ViewHolder {

		LinearLayout sensorTag_parent_layout;

		LinearLayout sensor_tag_device_name_layout;
		TextView bluetooth_device;
		LinearLayout sensor_tag_values_layout;

		RelativeLayout info_inflate_scale_layout;

		ImageButton info_scale_info_button;
		ImageButton info_scale_calibrate_button;

		ImageView record_device;
		ImageView switch_device;
		ImageView oTemp_drawable;
		TextView oTemp;
		ImageView aTemp_drawable;
		TextView aTemp;
		ImageView baro_drawable;
		TextView baro;
		ImageView hum_drawable;
		TextView hum;

		TextView mag_x;
		TextView mag_y;
		TextView mag_z;
		TextView acc_x;
		TextView acc_y;
		TextView acc_z;
		TextView gyro_x;
		TextView gyro_y;
		TextView gyro_z;

		FrameLayout disconnected_text_layout;

		ProgressBar progress_m_x;
		ProgressBar progress_m_y;
		ProgressBar progress_m_z;

		ProgressBar progress_a_x;
		ProgressBar progress_a_y;
		ProgressBar progress_a_z;

		ProgressBar progress_g_x;
		ProgressBar progress_g_y;
		ProgressBar progress_g_z;

	}

	public void openSensorTagMenuLayout(
			final RelativeLayout info_inflate_scale_layout) {
		if (info_inflate_scale_layout.getVisibility() == View.VISIBLE) {
			Animation animation = AnimationUtils.loadAnimation(mContext,
					R.anim.slide_out_up);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					info_inflate_scale_layout.setVisibility(View.GONE);
				}
			});
			info_inflate_scale_layout.startAnimation(animation);
		} else {
			Animation animation = AnimationUtils.loadAnimation(mContext,
					R.anim.slide_in_down);
			info_inflate_scale_layout.startAnimation(animation);
			info_inflate_scale_layout.setVisibility(View.VISIBLE);
		}

	}

	public void closeSensorTagMenuLayout(
			final RelativeLayout info_inflate_scale_layout) {
		if (info_inflate_scale_layout.getVisibility() == View.VISIBLE) {
			Animation animation = AnimationUtils.loadAnimation(mContext,
					R.anim.slide_out_up);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					info_inflate_scale_layout.setVisibility(View.GONE);
				}
			});
			info_inflate_scale_layout.startAnimation(animation);
		}
	}
}
