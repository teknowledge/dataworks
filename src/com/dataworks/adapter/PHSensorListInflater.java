package com.dataworks.adapter;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.CompoundButton.OnCheckedChangeListener;

import com.dataworks.MainActivity;
import com.dataworks.R;
import com.dataworks.data.AlarmData;
import com.dataworks.data.PHSensor;
import com.dataworks.database.DevicesDataBaseManager;
import com.dataworks.util.AddClickListener;

public class PHSensorListInflater {

	private int layoutResourceId;
	private Context mContext;
	private ArrayList<PHSensor> pHSensorsArray;
	private AddClickListener mClickListener;

	private DevicesDataBaseManager mDatabaseManager;

	boolean isHigh_On = false;
	boolean isLow_On = false;
	private DecimalFormat decimal_2 = new DecimalFormat("0.00");

	LinearLayout mParentLayout;

	public PHSensorListInflater(Context mContext, LinearLayout parentLayout) {

		this.mContext = mContext;

		mParentLayout = parentLayout;

		mDatabaseManager = new DevicesDataBaseManager(mContext);

		if (mContext instanceof AddClickListener) {
			mClickListener = (AddClickListener) mContext;
		} else {
			throw new IllegalArgumentException(
					"Activity must implement BluetoothClickListener");
		}
	}

	public void notifyList(final ArrayList<PHSensor> pHSensorsArray,
			int layoutResourceId) {

		if (pHSensorsArray != null && pHSensorsArray.size() > 0) {

			int size = pHSensorsArray.size();

			for (int i = 0; i < size; i++) {

				final ViewHolder holder = new ViewHolder();
				PHSensor phSensor = pHSensorsArray.get(i);

				View convertView = LayoutInflater.from(mContext).inflate(
						layoutResourceId, null, false);

				holder.pH_sensor_parent_layout = (LinearLayout) convertView
						.findViewById(R.id.pH_sensor_parent_layout);
				holder.pH = (TextView) convertView.findViewById(R.id.pH);
				holder.pH_value = (TextView) convertView
						.findViewById(R.id.pH_value);
				holder.alarm_image = (ImageView) convertView
						.findViewById(R.id.alarm_image);
				holder.alarm_high = (TextView) convertView
						.findViewById(R.id.alarm_high);
				holder.alarm_low = (TextView) convertView
						.findViewById(R.id.alarm_low);
				holder.temperature_type = (TextView) convertView
						.findViewById(R.id.temperature_type);
				holder.mtc_temp_value = (TextView) convertView
						.findViewById(R.id.mtc_temp_value);
				holder.ph_mtc_layout = (LinearLayout) convertView
						.findViewById(R.id.ph_mtc_layout);
				holder.pH_info_caliber_layout = (RelativeLayout) convertView
						.findViewById(R.id.pH_info_caliber_layout);
				holder.pH_info = (ImageButton) convertView
						.findViewById(R.id.pH_info);
				holder.pH_caliber = (ImageButton) convertView
						.findViewById(R.id.pH_caliber);
				holder.pH_Alarm = (ImageButton) convertView
						.findViewById(R.id.pH_Alarm);
				holder.alarm_layout = (LinearLayout) convertView
						.findViewById(R.id.alarm_layout);
				holder.mV_text = (TextView) convertView
						.findViewById(R.id.mV_text);
				
				holder.ph_mv_text_layout = (LinearLayout)convertView.findViewById(R.id.ph_mv_text_layout);
				 holder.ph_error_text_layout = (FrameLayout)convertView.findViewById(R.id.ph_error_text_layout);
				 holder.ph_error_text =  (TextView)convertView.findViewById(R.id.ph_error_text);

				holder.pH_sensor_parent_layout.setTag(phSensor
						.getDeviceAddress());

				holder.alarm_layout.setTag(i);
				holder.mtc_temp_value.setTag(i);
				holder.pH_info.setTag(i);
				holder.pH_caliber.setTag(i);
				holder.pH_Alarm.setTag(i);
				holder.ph_mtc_layout.setTag(i);
				holder.pH_info_caliber_layout.setTag(i);
				holder.temperature_type.setTag(i);
				holder.alarm_image.setTag(i);
				holder.mV_text.setTag(i);
				holder.pH.setTag(i);
				holder.pH_value.setTag(i);
				holder.alarm_high.setTag(i);
				holder.alarm_low.setTag(i);

				holder.pH_info_caliber_layout.setVisibility(View.GONE);

				if (phSensor.getpHValue() != null) {
					holder.pH_value.setText(phSensor.getpHValue());
				} else {
					holder.pH_value.setText("00.00");
				}
				holder.pH.setText(phSensor.getDeviceName());

				holder.pH_sensor_parent_layout
						.setOnClickListener(new OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								openpHMenuLayout(holder.pH_info_caliber_layout);
							}
						});

				holder.pH_info.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						int position = (int) v.getTag();
						PHSensor phSensor = pHSensorsArray.get(position);
						closepHMenuLayout(holder.pH_info_caliber_layout);

						mClickListener.pHDeviceInfoFragment(phSensor
								.getDeviceAddress());
					}
				});

				// boolean isDiscoonected =
				// pHSensorsArray.get(position).ismDisconected();

				// if (isDiscoonected) {
				// holder.pH_value.setText("Instrument" +"\n"+"Disconnected");
				// holder.pH_value.setTextSize(20);
				// holder.pH_value.setTextColor(Color.parseColor("#ff0000"));
				//
				// } else {
				// //
				// holder.pH_value.setText(pHSensorsArray.get(position).getpHValue());
				// // holder.pH_value.setTextSize(30);
				// // holder.pH_value.setTextColor(Color.parseColor("#000000"));
				// }

				holder.pH_caliber.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						int position = (int) v.getTag();
						PHSensor phSensor = pHSensorsArray.get(position);
						closepHMenuLayout(holder.pH_info_caliber_layout);

						mClickListener.openpHSensorCalibrationFragment(phSensor
								.getDeviceAddress());
					}
				});

				holder.pH_Alarm.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						int position = (int) v.getTag();
						PHSensor phSensor = pHSensorsArray.get(position);
						closepHMenuLayout(holder.pH_info_caliber_layout);

						String deviceAddress = phSensor.getDeviceAddress();

						openAlarmDialog(mContext, deviceAddress,
								holder.alarm_image, holder.alarm_high,
								holder.alarm_low);

					}
				});

				holder.alarm_layout.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						int position = (int) v.getTag();
						PHSensor phSensor = pHSensorsArray.get(position);
						String deviceAddress = phSensor.getDeviceAddress();

						boolean isDeviceExists = mDatabaseManager
								.isDeviceExistsforAlarm(deviceAddress);

						if (isDeviceExists) {

							if (!((MainActivity) mContext).m.isPlaying()) {
								boolean isAlarmStopped = true;

								mDatabaseManager.updateAlarmBeepPlay(
										deviceAddress, false, isAlarmStopped);
							}

							AlarmData alarmData = mDatabaseManager
									.getAlarmDetailsForDevice(deviceAddress);

							if (alarmData.isAlarmStopped()) {
								boolean isAlarmStopped = false;
								boolean isBeepPlaying = false;

								holder.alarm_image
										.setImageResource(R.drawable.alarm);
								holder.alarm_high.setTextColor(Color.BLACK);
								holder.alarm_low.setTextColor(Color.BLACK);

								mDatabaseManager.updateAlarmBeepPlay(
										deviceAddress, isBeepPlaying,
										isAlarmStopped);

							} else {
								// boolean isAlarmStopped = true;
								// boolean isBeepPlaying = false;
								// isBeepPlaying = true;
								// if (!((MainActivity) mContext).m.isPlaying())
								// {
								// //isAlarmStopped = false;
								// // isBeepPlaying = false;
								//
								// mDatabaseManager.updateAlarmBeepPlay(deviceAddress,
								// isBeepPlaying, isAlarmStopped);
								//
								// }
								// mDatabaseManager.updateAlarmBeepPlay(deviceAddress,
								// isBeepPlaying, isAlarmStopped);

								((MainActivity) mContext)
										.stopBeep(deviceAddress);

							}

						}

					}
				});

				holder.ph_mtc_layout.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub

						int position = (int) v.getTag();
						String temp_type = holder.temperature_type.getText()
								.toString();
						if (temp_type.equalsIgnoreCase("MTC")) {

							((MainActivity) mContext)
									.tempPickerLayout(position);
						}
					}
				});

				mParentLayout.addView(convertView);
			}
		}
	}

	class ViewHolder {

		LinearLayout pH_sensor_parent_layout;
		TextView pH;
		TextView pH_value;
		ImageView alarm_image;
		TextView alarm_high;
		TextView alarm_low;
		TextView temperature_type;
		TextView mtc_temp_value;
		LinearLayout ph_mtc_layout;
		RelativeLayout pH_info_caliber_layout;
		ImageButton pH_info;
		ImageButton pH_caliber;
		ImageButton pH_Alarm;
		LinearLayout alarm_layout;
		TextView mV_text;
		
		LinearLayout ph_mv_text_layout;
		FrameLayout ph_error_text_layout;
		TextView ph_error_text; 
	}

	public void openpHMenuLayout(final RelativeLayout pH_info_caliber_layout) {

		// TODO Auto-generated method stub
		if (pH_info_caliber_layout.getVisibility() == View.VISIBLE) {
			Animation animation = AnimationUtils.loadAnimation(mContext,
					R.anim.slide_out_up);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					pH_info_caliber_layout.setVisibility(View.GONE);
				}
			});
			pH_info_caliber_layout.startAnimation(animation);
		} else {
			Animation animation = AnimationUtils.loadAnimation(mContext,
					R.anim.slide_in_down);
			pH_info_caliber_layout.startAnimation(animation);
			pH_info_caliber_layout.setVisibility(View.VISIBLE);
		}

	}

	public void closepHMenuLayout(final RelativeLayout pH_info_caliber_layout) {
		if (pH_info_caliber_layout.getVisibility() == View.VISIBLE) {
			Animation animation = AnimationUtils.loadAnimation(mContext,
					R.anim.slide_out_up);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub
					pH_info_caliber_layout.setVisibility(View.GONE);
				}
			});
			pH_info_caliber_layout.startAnimation(animation);
		}
	}

	public void openAlarmDialog(final Context mContext,
			final String deviceAddress, final ImageView alarm_image,
			final TextView alarm_high, final TextView alarm_low) {
		/****
		 * alarm dialog which is used to set high and low values for pH meter
		 */

		final Dialog d = new Dialog(mContext);

		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.ph_alarm);
		d.show();
		d.getWindow().setBackgroundDrawable(
				new ColorDrawable(Color.TRANSPARENT));
		d.setCanceledOnTouchOutside(false);

		TextView cancel = (TextView) d.findViewById(R.id.cancel);
		TextView done = (TextView) d.findViewById(R.id.done);

		final EditText high_value_ph = (EditText) d
				.findViewById(R.id.high_value_ph);
		final EditText low_value_ph = (EditText) d
				.findViewById(R.id.low_value_ph);
		ToggleButton high_ph_toggle = (ToggleButton) d
				.findViewById(R.id.high_ph_toggle);
		ToggleButton low_ph_toggle = (ToggleButton) d
				.findViewById(R.id.low_ph_toggle);

		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				d.dismiss();
			}
		});

		boolean isDeviceExists = mDatabaseManager
				.isDeviceExistsforAlarm(deviceAddress);

		if (!isDeviceExists) {
			high_ph_toggle.setChecked(false);
			high_value_ph.setEnabled(false);

			high_value_ph.setText("");

			low_ph_toggle.setChecked(false);
			low_value_ph.setEnabled(false);

			low_value_ph.setText("");
		} else {

			AlarmData alarmData = mDatabaseManager
					.getAlarmDetailsForDevice(deviceAddress);

			isHigh_On = alarmData.isHighOn();
			isLow_On = alarmData.isLowOn();

			high_ph_toggle.setChecked(isHigh_On);
			high_value_ph.setEnabled(isHigh_On);

			high_value_ph.setText(alarmData.getHighValue());

			low_ph_toggle.setChecked(isLow_On);
			low_value_ph.setEnabled(isLow_On);

			low_value_ph.setText(alarmData.getLowValue());
		}

		high_ph_toggle
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub

						isHigh_On = isChecked;

						high_value_ph.setEnabled(isChecked);

					}
				});

		low_ph_toggle.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub

				isLow_On = isChecked;

				low_value_ph.setEnabled(isChecked);

			}
		});

		/****
		 * to set the high and low values for the alarm
		 */

		done.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String ph_high_value = "0.00";
				String ph_low_value = "0.00";

				ph_high_value = high_value_ph.getText().toString();
				ph_low_value = low_value_ph.getText().toString();

				if (ph_high_value.equalsIgnoreCase("")) {
					ph_high_value = "0.00";
				} else {
					ph_high_value = decimal_2.format(Float
							.parseFloat(ph_high_value));
				}
				if (ph_low_value.equalsIgnoreCase("")) {
					ph_low_value = "0.00";
				} else {
					ph_low_value = decimal_2.format(Float
							.parseFloat(ph_low_value));
				}

				if (isHigh_On && !isLow_On) {

					alarm_high.setText(ph_high_value);
					alarm_high.setTextColor(Color.parseColor("#000000"));
					alarm_low.setText("- -");
					alarm_low.setTextColor(Color.parseColor("#000000"));

					alarm_image.setImageResource(R.drawable.alarm);

					boolean isDeviceExists = mDatabaseManager
							.isDeviceExistsforAlarm(deviceAddress);

					if (isDeviceExists) {
						mDatabaseManager.updateAlarmDetailsForDeviceAddress(
								deviceAddress, isHigh_On, isLow_On,
								ph_high_value, ph_low_value, false, false);
					} else {
						mDatabaseManager.insertAlarmDetailsForDeviceAddress(
								deviceAddress, isHigh_On, isLow_On,
								ph_high_value, ph_low_value, false, false);
					}

				} else if (isLow_On && !isHigh_On) {

					alarm_low.setText(ph_low_value);
					alarm_low.setTextColor(Color.parseColor("#000000"));
					alarm_high.setText("- -");
					alarm_high.setTextColor(Color.parseColor("#000000"));

					alarm_image.setImageResource(R.drawable.alarm);

					boolean isDeviceExists = mDatabaseManager
							.isDeviceExistsforAlarm(deviceAddress);

					if (isDeviceExists) {
						mDatabaseManager.updateAlarmDetailsForDeviceAddress(
								deviceAddress, isHigh_On, isLow_On,
								ph_high_value, ph_low_value, false, false);
					} else {
						mDatabaseManager.insertAlarmDetailsForDeviceAddress(
								deviceAddress, isHigh_On, isLow_On,
								ph_high_value, ph_low_value, false, false);
					}
				}

				else if (isLow_On && isHigh_On) {

					if (Float.parseFloat(ph_high_value) > Float
							.parseFloat(ph_low_value)) {

						alarm_high.setText(ph_high_value);
						alarm_low.setText(ph_low_value);

						alarm_image.setImageResource(R.drawable.alarm);

						alarm_high.setTextColor(Color.parseColor("#000000"));

						alarm_low.setTextColor(Color.parseColor("#000000"));

						boolean isDeviceExists = mDatabaseManager
								.isDeviceExistsforAlarm(deviceAddress);

						if (isDeviceExists) {
							mDatabaseManager
									.updateAlarmDetailsForDeviceAddress(
											deviceAddress, isHigh_On, isLow_On,
											ph_high_value, ph_low_value, false,
											false);
						} else {
							mDatabaseManager
									.insertAlarmDetailsForDeviceAddress(
											deviceAddress, isHigh_On, isLow_On,
											ph_high_value, ph_low_value, false,
											false);
						}

					} else {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								mContext);
						builder.setMessage(
								"High value should not be lower or equal to the Low value")
								.setTitle("Oops!")
								.setCancelable(false)
								.setPositiveButton("OK",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												// do things

											}
										});
						AlertDialog alert = builder.create();
						alert.show();
					}

				} else {
					alarm_high.setText("- -");
					alarm_low.setText("- -");

					alarm_image.setImageResource(R.drawable.alarm);

					alarm_high.setTextColor(Color.parseColor("#000000"));

					alarm_low.setTextColor(Color.parseColor("#000000"));

					boolean isDeviceExists = mDatabaseManager
							.isDeviceExistsforAlarm(deviceAddress);

					if (isDeviceExists) {
						mDatabaseManager.updateAlarmDetailsForDeviceAddress(
								deviceAddress, isHigh_On, isLow_On,
								ph_high_value, ph_low_value, false, false);
					} else {
						mDatabaseManager.insertAlarmDetailsForDeviceAddress(
								deviceAddress, isHigh_On, isLow_On,
								ph_high_value, ph_low_value, false, false);
					}
				}

				d.dismiss();

			}
		});

	}

}
