package com.dataworks.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dataworks.data.SensorTag;
import com.dataworks.R;

public class SensorTagValueDetailsAdapter extends BaseAdapter {

	private int layoutResourceId;
	private Context mContext;
	private ArrayList<SensorTag> sensorTagsList;

	LayoutInflater inflater;

	public SensorTagValueDetailsAdapter(Context mContext, int layoutResourceId,
			ArrayList<SensorTag> sensorTagsList) {
		this.mContext = mContext;
		this.sensorTagsList = sensorTagsList;
		this.layoutResourceId = layoutResourceId;

		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return sensorTagsList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return sensorTagsList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();

			holder.ir_amb = (TextView) convertView.findViewById(R.id.ir_amb);
			holder.amb_temp = (TextView) convertView
					.findViewById(R.id.amb_temp);
			holder.humidity_value = (TextView) convertView
					.findViewById(R.id.humidity_value);
			holder.pressure = (TextView) convertView
					.findViewById(R.id.pressure);
			holder.mag = (TextView) convertView.findViewById(R.id.mag);
			holder.accel = (TextView) convertView.findViewById(R.id.accel);
			holder.gyro = (TextView) convertView.findViewById(R.id.gyro);

			convertView.setTag(holder);

		} else {

			holder = (ViewHolder) convertView.getTag();
		}

		holder.ir_amb.setText(sensorTagsList.get(position).getoTempValue());
		holder.amb_temp.setText(sensorTagsList.get(position).getaTempValue());
		holder.humidity_value.setText(sensorTagsList.get(position)
				.getHumidity());
		holder.pressure.setText(sensorTagsList.get(position).getBaroValue());

		holder.mag.setText(sensorTagsList.get(position)
				.getMagnetometerValue_x()
				+ "\n\n"
				+ sensorTagsList.get(position).getMagnetometerValue_y()
				+ "\n\n"
				+ sensorTagsList.get(position).getMagnetometerValue_z());
		holder.accel.setText(sensorTagsList.get(position)
				.getAccelerometerValue_x()
				+ "\n\n"
				+ sensorTagsList.get(position).getAccelerometerValue_y()
				+ "\n\n"
				+ sensorTagsList.get(position).getAccelerometerValue_z());

		holder.gyro.setText(sensorTagsList.get(position).getGyroMeterValue_x()
				+ "\n\n" + sensorTagsList.get(position).getGyroMeterValue_y()
				+ "\n\n" + sensorTagsList.get(position).getGyroMeterValue_z());

		return convertView;
	}

	class ViewHolder {
		TextView ir_amb;
		TextView amb_temp;
		TextView humidity_value;
		TextView pressure;
		TextView mag;
		TextView accel;
		TextView gyro;

	}

}
