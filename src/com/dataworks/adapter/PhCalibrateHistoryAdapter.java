package com.dataworks.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dataworks.R;
import com.dataworks.data.PhCalibrationHistoryData;
import com.dataworks.util.AddClickListener;
import com.dataworks.util.Utils;

public class PhCalibrateHistoryAdapter extends BaseAdapter {

	private int layoutResourceId;
	private Context mContext;
	private ArrayList<PhCalibrationHistoryData> datasetsArray;

	LayoutInflater inflater;
	private AddClickListener mClickListener;

	public PhCalibrateHistoryAdapter(Context mContext, int layoutResourceId,
			ArrayList<PhCalibrationHistoryData> datasets) {
		this.mContext = mContext;
		this.datasetsArray = datasets;
		this.layoutResourceId = layoutResourceId;

		inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if (mContext instanceof AddClickListener) {
			mClickListener = (AddClickListener) mContext;
		} else {
			throw new IllegalArgumentException(
					"Activity must implement BluetoothClickListener");
		}

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return datasetsArray.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return datasetsArray.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder;

		if (convertView == null) {

			convertView = inflater.inflate(layoutResourceId, parent, false);
			holder = new ViewHolder();

			holder.date_time_textview = (TextView) convertView
					.findViewById(R.id.date_time_textview);
			holder.values_collected_count_textview = (TextView) convertView
					.findViewById(R.id.values_collected_count_textview);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();

		}

		holder.date_time_textview.setText(datasetsArray.get(position).getDate()
				+ " "
				+ Utils.Convert24to12(datasetsArray.get(position).getTime()));

		String noOfValues = datasetsArray.get(position).getNo_values();
		
		if(!noOfValues.equalsIgnoreCase("")&&!noOfValues.equalsIgnoreCase("0")){
			holder.values_collected_count_textview.setText(noOfValues+" values collected");
		}else{
			holder.values_collected_count_textview.setText("no values collected");
		}
		

		return convertView;
	}

	class ViewHolder {
		TextView date_time_textview;
		TextView values_collected_count_textview;

	}

}
