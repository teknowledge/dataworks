package com.dataworks.helper;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dataworks.MainActivity;
import com.dataworks.PhCaliberationFragment;
import com.dataworks.R;
import com.dataworks.data.AlarmData;
import com.dataworks.data.PHSensor;
import com.dataworks.database.DevicesDataBaseManager;
import com.dataworks.util.NestedListView;

public class PHThread {

	/**** 
	 *    get the pH values in the thread and update the values in UI thread 
	 * @param mainActivity
	 * @param pH_listView
	 * @param deviceAddress
	 * @param pH
	 * @param mV
	 * @param decimal_2
	 * @param pH_data
	 * @param msg
	 * @param pHSensorsList
	 */
	public PHThread(MainActivity mainActivity, LinearLayout pH_listView,
			String deviceAddress, float pH, float mV, DecimalFormat decimal_2,
			String pH_data, String msg, ArrayList<PHSensor> pHSensorsList) {

		loadPHData(mainActivity, pH_listView, deviceAddress, pH, mV, decimal_2,
				pH_data, msg, pHSensorsList);
	}

	float ph_compensated = 0;
	AlarmData alarmData = null;

	private void loadPHData(final MainActivity mainActivity,
			final LinearLayout pH_listView, final String deviceAddress,
			final float pH, final float mV, final DecimalFormat decimal_2,
			final String pH_data, final String msg,
			final ArrayList<PHSensor> pHSensorsList) {

		final Handler handler = new Handler() {

			@Override
			public void handleMessage(Message ms) {
				// TODO Auto-generated method stub
				super.handleMessage(ms);

				MainActivity activity = mainActivity;
				String address = deviceAddress;
				float phFloat = pH;
				float mvFloat = mV;
				DecimalFormat decimalFormat = decimal_2;

				String message = msg;
				ArrayList<PHSensor> phSensorsList = pHSensorsList;

				String phData = decimalFormat.format(ph_compensated);

				boolean type_display = activity.getmVType(deviceAddress);

				startUIThread(activity, pH_listView, phSensorsList, address,
						phData, message, ph_compensated, type_display,
						alarmData);
			}
		};

		Thread thread = new Thread() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				super.run();

				MainActivity activity = mainActivity;
				String address = deviceAddress;
				float phFloat = pH;
				float mvFloat = mV;
				DecimalFormat decimalFormat = decimal_2;
				String phData = pH_data;
				String message = msg;
				ArrayList<PHSensor> phSensorsList = pHSensorsList;

				String slope_value = activity.getSlopeForDevice(address);
				float slope = 0.0f;
				float intercept = 0.0f;

				if (slope_value != null) {
					slope = Float.parseFloat(slope_value);
				} else {
					slope = 0.0f;
				}

				String intercept_value = activity
						.getInterceptForDevice(address);

				if (intercept_value != null) {
					intercept = Float.parseFloat(intercept_value);
				} else {
					intercept = 0.0f;
				}

				if (slope == 0.0) {
					slope = 1f;
				}
				if (intercept == 0.0) {
					intercept = 0.0f;
				}
				if (slope == 0.0 && intercept == 0.0) {
					phFloat = activity.utils.caliberatedpH(mvFloat, slope,
							intercept);

				} else {
					phFloat = activity.utils.caliberatedpH(mvFloat, slope,
							intercept);
				}

				String pHDecimal = decimalFormat.format(phFloat);

				float mtc_atc_temperature = 0.0f;
				String temperature = activity.getMTC_ATC_Temp(address);

				if (temperature != null) {
					mtc_atc_temperature = Float.parseFloat(temperature);

				} else {
					mtc_atc_temperature = 0.0f;
				}

				ph_compensated = activity.utils.compensatedpH(phFloat,
						mtc_atc_temperature);

				phData = decimalFormat.format(ph_compensated);

				final DevicesDataBaseManager mDatabaseManager = new DevicesDataBaseManager(
						mainActivity);
				alarmData = mDatabaseManager
						.getAlarmDetailsForDevice(deviceAddress);

				// new PHThreadDataHandler(activity, phSensorsList, address,
				// phData, message, ph_compensated, type_display,
				// alarmData);

				handler.sendEmptyMessage(0);

			}
		};

		thread.start();
	}

	public void startUIThread(MainActivity activity, LinearLayout pH_listView,
			ArrayList<PHSensor> phSensorsList, String address, String phData,
			String message, float ph_compensated, boolean type_display,
			AlarmData alarmData) {

		for (int i = 0; i < phSensorsList.size(); i++) {
			if (address.equalsIgnoreCase(phSensorsList.get(i)
					.getDeviceAddress())) {

				reqpHPosition = i;
				PHSensor sensor = phSensorsList.get(i);
				if (sensor.ismDisconected()) {
					return;
				}

			}

		}

		handleData(activity, pH_listView, phSensorsList, address, phData,
				message, ph_compensated, type_display, alarmData);
	}

	int reqpHPosition = -1;

	private void handleData(final MainActivity mainActivity,
			final LinearLayout pH_listView,
			final ArrayList<PHSensor> pHSensorsList,
			final String deviceAddress, final String pH_data, final String msg,
			final float ph_compensated, final boolean type_display,
			final AlarmData alarmData) {

		final MainActivity activity = mainActivity;
		final ArrayList<PHSensor> phSensorsList = pHSensorsList;
		final String address = deviceAddress;
		final String phData = pH_data;
		final String message = msg;
		final float phCompenSated = ph_compensated;

		final Handler handler = new Handler() {

			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);

				if (reqpHPosition != -1 && (phSensorsList.size() > 0)) {
					PHSensor pHSensor = phSensorsList.get(reqpHPosition);
					pHSensor.setDeviceAddress(address);
					pHSensor.setpHValue(phData);
					pHSensor.setmV(message);

					phSensorsList.set(reqpHPosition, pHSensor);

					final View pHSensorView = pH_listView
							.getChildAt(reqpHPosition);

					if (pHSensorView != null) {

						TextView pHValue = (TextView) pHSensorView
								.findViewById(R.id.pH_value);
						pHValue.setText(phSensorsList.get(reqpHPosition)
								.getpHValue());

						TextView alarm_high = (TextView) pHSensorView
								.findViewById(R.id.alarm_high);
						TextView alarm_low = (TextView) pHSensorView
								.findViewById(R.id.alarm_low);
						ImageView alarm_image = (ImageView) pHSensorView
								.findViewById(R.id.alarm_image);
						TextView mV_text = (TextView) pHSensorView
								.findViewById(R.id.mV_text);

						LinearLayout ph_mv_text_layout = (LinearLayout) pHSensorView
								.findViewById(R.id.ph_mv_text_layout);
						FrameLayout ph_error_text_layout = (FrameLayout) pHSensorView
								.findViewById(R.id.ph_error_text_layout);
						TextView ph_error_text = (TextView) pHSensorView
								.findViewById(R.id.ph_error_text);

						activity.checkProbe(phCompenSated, pHValue, phData,
								message, address, mV_text, type_display,
								ph_mv_text_layout, ph_error_text_layout,
								ph_error_text);

						activity.checkAlarm(phCompenSated, alarm_high,
								alarm_low, alarm_image, activity, address,
								alarmData);

					}
				}

				if (PhCaliberationFragment.getInstance() != null) {

					PhCaliberationFragment.getInstance()
							.onCharacteristicUpdatedpH(ph_compensated, message,
									address);
				}
			}
		};

		Thread thread = new Thread() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				super.run();

				for (int i = 0; i < phSensorsList.size(); i++) {
					if (address.equalsIgnoreCase(phSensorsList.get(i)
							.getDeviceAddress())) {

						reqpHPosition = i;

						handler.sendEmptyMessage(0);
						break;
					}
				}
			}
		};

		thread.start();

	}

	public View getPHSensorView(int reqpHPosition, NestedListView pH_listView) {
		int wantedPosition = reqpHPosition; // Whatever position you're
		// looking for
		int firstPosition = pH_listView.getFirstVisiblePosition()
				- pH_listView.getHeaderViewsCount(); // This is the
		// same as child
		// #0
		int wantedChild = wantedPosition - firstPosition;
		// Say, first visible position is 8, you want position 10,
		// wantedChild will now be 2
		// So that means your view is child #2 in the ViewGroup:
		if (wantedChild < 0 || wantedChild >= pH_listView.getChildCount()) {
			// Log.w(TAG,
			// "Unable to get view for desired position, because it's not being displayed on screen.");
			return null;
		}
		// Could also check if wantedPosition is between
		// listView.getFirstVisiblePosition() and
		// listView.getLastVisiblePosition() instead.
		View wantedView = pH_listView.getChildAt(wantedChild);

		return wantedView;
	}
}
