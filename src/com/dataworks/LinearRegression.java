package com.dataworks;

import java.util.ArrayList;

import android.util.Log;

import com.dataworks.data.DataItem;
import com.dataworks.data.RegressionResult;

public class LinearRegression {

	public RegressionResult CalculateRegression(ArrayList<DataItem> dataItems) {
		
		float sumY = 0.0f;
		float sumX = 0.0f;
		float sumXY = 0.0f;
		float sumX2 = 0.0f;
		float sumY2 = 0.0f;

		RegressionResult result = new RegressionResult();

		int count = dataItems.size();
		Log.d("length of buffer", count + "");

		for (DataItem data : dataItems) {

			sumX = (float) (sumX + data.xValue);

			sumY = (float) (sumY + data.yValue);

			sumXY = (float) (sumXY + (data.xValue * data.yValue));

			sumX2 = (float) (sumX2 + (data.xValue * data.xValue));

			sumY2 = (float) (sumY2 + (data.yValue * data.yValue));

			Log.d("x,y", data.xValue + "  " + data.yValue + "");
		}

		Log.d("sumX:", sumX + "");

		result.slope = ((count * sumXY) - (sumX * sumY))
				/ ((count * sumX2) - (sumX * sumX));

		Log.d("slope:", result.slope + "");

		result.intercept = ((sumY - (result.slope * sumX)) / count);

		Log.d("intercept:", result.intercept + "");

		result.correlation = (((count * sumXY) - (sumX * sumY)) / (Math
				.sqrt(count * sumX2 - (sumX * sumX)) * Math.sqrt(count * sumY2
				- (sumY * sumY))));

		Log.d("correlation:", result.correlation + "");

		return result;
	}

}
