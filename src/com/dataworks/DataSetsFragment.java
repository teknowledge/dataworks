package com.dataworks;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.dataworks.adapter.DataSetAdapter;
import com.dataworks.data.DataSet;
import com.dataworks.data.PHSensor;
import com.dataworks.data.SensorTag;
import com.dataworks.data.Values;
import com.dataworks.database.DataBaseHelper;
import com.dataworks.database.DataBaseSingleTon;
import com.dataworks.util.AddClickListener;
import com.dataworks.util.Global;

public class DataSetsFragment extends Fragment {

	private static DataSetsFragment dataSetsFragment;

	public static DataSetsFragment newInstance(MainActivity parent) {
		if (dataSetsFragment == null)
			dataSetsFragment = new DataSetsFragment(parent);
		return dataSetsFragment;
	}

	private DataSetsFragment(MainActivity parent) {
		this.mainActivity = parent;
	}

	private TextView cancel;
	private ListView datasetsListView;
	private ImageButton add_btn;
	private AddClickListener mClickListener;
	DataBaseHelper db;

	private ArrayList<DataSet> mDataSetsArray = new ArrayList<DataSet>();
	private DataSetAdapter adapter;

	private boolean isButtonClick;

	public MainActivity mainActivity;

	Handler handler = new Handler();

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		DataBaseSingleTon.sContext = getActivity();

		db = DataBaseSingleTon.getDB();

		View dataSetsView = inflater.inflate(R.layout.datasets, container,
				false);

		cancel = (TextView) dataSetsView.findViewById(R.id.cancel);

		datasetsListView = (ListView) dataSetsView
				.findViewById(R.id.datasetsListView);

		add_btn = (ImageButton) dataSetsView.findViewById(R.id.add_btn);

		setupOnClick();

		mDataSetsArray = getDataSetsFromDb();

		adapter = new DataSetAdapter(getActivity(), R.layout.dataset_list_item,
				mDataSetsArray);

		datasetsListView.setAdapter(adapter);

		datasetsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				if (isButtonClick)
					return;

				isButtonClick = true;
				adapter.setButtonClick(isButtonClick);

				handler.postDelayed(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						isButtonClick = false;
					}
				}, 1000);

				DataSet dataset = (DataSet) adapter.getItem(position);
				String mID = dataset.getmId();

				String selectedDatasetId = Global.getInstance()
						.getPreferenceVal(getActivity(), "datasetId");
				if (selectedDatasetId.equalsIgnoreCase(mID)) {
					mainActivity.curDataSet = dataset;
				} else {
					new GetValuesForDataSet(mID, dataset).execute();
				}

				Global.getInstance().storeIntoPreference(getActivity(),
						"datasetName", dataset.getmName());

				Global.getInstance().storeIntoPreference(getActivity(),
						"datasetId", dataset.getmId());

				getActivity().onBackPressed();

			}
		});

		datasetsListView
				.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub

						if (isButtonClick)
							return false;

						isButtonClick = true;

						handler.post(new Runnable() {

							@Override
							public void run() {
								// TODO Auto-generated method stub
								isButtonClick = false;
							}
						});

						DataSet dataset = (DataSet) adapter.getItem(position);
						final String idValue = dataset.getmId();

						if (!idValue.equalsIgnoreCase(Global.getInstance()
								.getPreferenceVal(getActivity(), "datasetId"))) {
							AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
									getActivity());

							// set title
							alertDialogBuilder.setTitle("Delete");

							// set dialog message
							alertDialogBuilder
									.setMessage(
											"Are you sure you want to delete it?")

									.setCancelable(false)
									.setPositiveButton(
											"Yes",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int id) {
													// if this button is
													// clicked,
													// close
													// current activity

													deleteDataSet(idValue);
													notifyDataChanged();
												}
											})
									.setNegativeButton(
											"No",
											new DialogInterface.OnClickListener() {
												public void onClick(
														DialogInterface dialog,
														int id) {
													// if this button is
													// clicked,
													// just close
													// the dialog box and do
													// nothing
													dialog.cancel();
												}
											});

							// create alert dialog
							AlertDialog alertDialog = alertDialogBuilder
									.create();

							// show it
							alertDialog.show();
						}

						return true;
					}
				});

		return dataSetsView;

	}

	/**** delete dataset from database ***/
	public void deleteDataSet(String id) {
		db = new DataBaseHelper(getActivity());
		db.open();
		db.deleteDaset(id);
		db.close();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		if (activity instanceof AddClickListener) {
			mClickListener = (AddClickListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implement BluetoothClickListener");
		}
	}

	public void notifyDataChanged() {
		mDataSetsArray.clear();
		mDataSetsArray.addAll(getDataSetsFromDb());
		adapter.notifyDataSetChanged();
	}

	private void setupOnClick() {

		cancel.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		add_btn.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				mClickListener.onAddButtonClick();
			}
		});
	}

	/************* retrieving datasets from database and adding into the array **************/

	public ArrayList<DataSet> getDataSetsFromDb() {
		// db = new DataBaseHelper(getActivity());
		db.open();
		ArrayList<DataSet> array = new ArrayList<DataSet>();

		Cursor dataSets = db.getDataSets();
		if (dataSets.moveToFirst()) {
			do {

				String mName = dataSets.getString(0);
				String mNotes = dataSets.getString(1);
				String mDate = dataSets.getString(2);
				String mTime = dataSets.getString(3);
				String mLocation = dataSets.getString(4);
				String mId = dataSets.getString(5);
				String mdateAndTime = dataSets.getString(6);

				String selectedDatasetId = Global.getInstance()
						.getPreferenceVal(getActivity(), "datasetId");
				DataSet dataSet = null;
				if (selectedDatasetId.equalsIgnoreCase(mId)) {
					dataSet = mainActivity.curDataSet;
				} else {
					dataSet = new DataSet();
				}
				// DataSet dataSet = new DataSet();
				dataSet.setmName(mName);
				dataSet.setmNotes(mNotes);
				dataSet.setmDate(mDate);
				dataSet.setmTime(mTime);
				dataSet.setmLocation(mLocation);
				dataSet.setmId(mId);
				dataSet.setmDateAndTime(mdateAndTime);

				array.add(dataSet);

			} while (dataSets.moveToNext());
		}
		dataSets.close();
		db.close();
		return array;
	}

	/**** get values for dataset in the background ****/
	private class GetValuesForDataSet extends
			AsyncTask<String, ArrayList<Values>, ArrayList<Values>> {

		private ProgressDialog progressDialog;

		private String dataSetId;
		private DataSet mDataSet;

		public GetValuesForDataSet(String mId, DataSet mDataset) {
			// TODO Auto-generated constructor stub
			this.dataSetId = mId;
			this.mDataSet = mDataset;

		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			// if (number > 200) {
			progressDialog = new ProgressDialog(getActivity());

			progressDialog.setMessage("Loading values...");
			progressDialog.setCancelable(false);
			progressDialog.show();
			// }

		}

		@Override
		protected ArrayList<Values> doInBackground(String... params) {
			// TODO Auto-generated method stub

			ArrayList<Values> valuesList = getValuesForDataSet(dataSetId);

			return valuesList;
		}

		@Override
		protected void onPostExecute(ArrayList<Values> valuesList) {
			// TODO Auto-generated method stub
			super.onPostExecute(valuesList);

			// if (number > 200) {
			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			// }

			mainActivity.curDataSet = mDataSet;

			mainActivity.curDataSet.setValuesDataList(valuesList);

		}

	}

	/*** fetch the values for particular dataset from database *****/
	public ArrayList<Values> getValuesForDataSet(String id) {

		db.open();
		ArrayList<Values> array = new ArrayList<Values>();

		ArrayList<SensorTag> sensorsList = null;
		ArrayList<PHSensor> pHSensorsList = null;

		Cursor dataSetValues = db.getDPValuesForDataSet(id);
		if (dataSetValues.moveToFirst()) {
			do {

				sensorsList = new ArrayList<SensorTag>();
				pHSensorsList = new ArrayList<PHSensor>();

				String mDataSetId = dataSetValues.getString(0);
				String time = dataSetValues.getString(1);
				String data_point_name = dataSetValues.getString(2);
				String data_point_notes = dataSetValues.getString(3);
				String data_point_location = dataSetValues.getString(4);
				String mId = dataSetValues.getString(5);

				Values values = new Values();
				values.setmDataSetId(mDataSetId);
				values.setmTime(time);
				values.setmName(data_point_name);
				values.setmNotes(data_point_notes);
				values.setmLocation(data_point_location);
				values.setmId(mId);
				Cursor data = db.getValuesForDataPoint(time);
				if (data.moveToFirst()) {
					do {
						String deviceAddress = data.getString(0);
						String deviceName = data.getString(1);

						if (deviceName.equalsIgnoreCase("SensorTag")) {
							String ir_temp = data.getString(2);
							String object_temp = data.getString(3);
							String humidity = data.getString(4);
							String pressure = data.getString(5);
							String mag_x = data.getString(6);
							String mag_y = data.getString(7);
							String mag_z = data.getString(8);

							String acc_x = data.getString(9);
							String acc_y = data.getString(10);
							String acc_z = data.getString(11);
							String gyro_x = data.getString(12);
							String gyro_y = data.getString(13);
							String gyro_z = data.getString(14);

							SensorTag sensorTag = new SensorTag();
							sensorTag.setDeviceAddress(deviceAddress);
							sensorTag.setDeviceName(deviceName);
							sensorTag.setaTempValue(ir_temp);
							sensorTag.setoTempValue(object_temp);
							sensorTag.setHumidity(humidity);
							sensorTag.setBaroValue(pressure);
							sensorTag.setMagnetometerValue_x(mag_x);
							sensorTag.setMagnetometerValue_y(mag_y);
							sensorTag.setMagnetometerValue_z(mag_z);
							sensorTag.setAccelerometerValue_x(acc_x);
							sensorTag.setAccelerometerValue_y(acc_y);
							sensorTag.setAccelerometerValue_z(acc_z);
							sensorTag.setGyroMeterValue_x(gyro_x);
							sensorTag.setGyroMeterValue_y(gyro_y);
							sensorTag.setGyroMeterValue_z(gyro_z);

							sensorsList.add(sensorTag);

							values.setSensorTagsList(sensorsList);
						} else if (deviceName.equalsIgnoreCase("pH-Meter")) {

							String pH_value = data.getString(15);
							String pH_temp = data.getString(16);
							String pH_mV = data.getString(17);

							PHSensor pHsensor = new PHSensor();
							pHsensor.setDeviceAddress(deviceAddress);
							pHsensor.setDeviceName(deviceName);
							pHsensor.setpHValue(pH_value);
							pHsensor.setTemp_value(pH_temp);
							pHsensor.setmV(pH_mV);

							pHSensorsList.add(pHsensor);

							values.setpHSensorsList(pHSensorsList);
						}

					} while (data.moveToNext());
				}

				data.close();

				array.add(values);

			} while (dataSetValues.moveToNext());
		}
		dataSetValues.close();
		db.close();
		return array;

	}
}
