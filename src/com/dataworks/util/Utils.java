package com.dataworks.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class Utils {
	
	public float convertRawtoResistance(int raw) {
		float slope = 0.497269805f;// 0.321212834;

		float intercept = 18.71687232f;// 6.906275858;

		float resistance = slope * raw + intercept;

		return resistance;
	}

	public float convertResistancetoTemperature(float resistance) {
		float coeff = 3.845f;

		float temp;

		temp = (resistance - 1000) / coeff;

		return temp;
	}

	public float caliberatedpH(float mV, float slope, float intercept) {
		// Converts the mV value to a calibrated pH value
		// slope = is slope adjustment determined by the calibration of the
		// electrode
		// zero = is the zero point adjustment determined by the calibration of
		// the electrode
		// sl25 = is the ideal slope at 25 degrees C
		// mV = is the measure mV value supplied by the pH electrode.

		float sl25 = 59.16f;
		float pH;

		// pH = slope * (sl25 * 7 - (mV - zero)) / sl25;
		pH = 7 - ((mV - intercept) / (slope * sl25));
		// NSLog(@"pH = %f, mV = %f, zero = %f, slope = %f",pH, mV, zero,
		// slope);

		return pH;
	}
	
	public float compensatedpH(float pH, float temperature) {
		// Converts the measured pH value into a temperature compensated pH
		// value.
		// temperature = is the temperature in celcius

		float kelvin = temperature + 273.15f;
		float pH_t;

		pH_t = (float) (7 - kelvin / 298.15 * (7 - pH));

		return pH_t;
	}



	// convert time 24 format to 12 hours
	public static String Convert24to12(String time) {
		String convertedTime = "";
		try {
			SimpleDateFormat displayFormat = new SimpleDateFormat("hh:mm:ss a");
			SimpleDateFormat parseFormat = new SimpleDateFormat("HH:mm:ss");
			Date date = parseFormat.parse(time);
			convertedTime = displayFormat.format(date);
//			System.out.println("convertedTime : " + convertedTime);

		} catch (final ParseException e) {
			e.printStackTrace();
		}
		return convertedTime;
		// Output will be 10:23 PM
	}
	public static String splitToComponentTimes(int duration) {
		String totalTime = null;
		long longVal = (long) duration;
		int hours = (int) longVal / 3600;
		int remainder = (int) longVal - hours * 3600;
		int mins = remainder / 60;
		remainder = remainder - mins * 60;
		int secs = remainder;

		// int[] ints = { hours, mins, secs };

		totalTime = hours + " hrs " + mins + " mins " + secs + " secs ";
		return totalTime;
	}
	
	
	/**
	 * Return date in specified format.
	 * @param milliSeconds Date in milliseconds
	 * @param dateFormat Date format 
	 * @return String representing date in specified format
	 */
	public static String getDate(long milliSeconds, String dateFormat)
	{
	    // Create a DateFormatter object for displaying date in specified format.
	    SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

	    // Create a calendar object that will convert the date and time value in milliseconds to date. 
	     Calendar calendar = Calendar.getInstance();
	     calendar.setTimeInMillis(milliSeconds);
	     return formatter.format(calendar.getTime());
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void removeDuplicates(List list) {
		Set uniqueEntries = new HashSet();
		for (Iterator iter = list.iterator(); iter.hasNext();) {
			Object element = iter.next();
			if (!uniqueEntries.add(element)) // if current element is a
												// duplicate,
				iter.remove(); // remove it
		}
	}
}
