package com.dataworks.util;

public abstract class CustomTimerCallback {

	protected abstract void onTimeout(int checkedPosition);

	protected abstract void onTick(int i);
}
