package com.dataworks.util;

import android.bluetooth.BluetoothDevice;

public interface BluetoothClickListener {
	
	public void onDeviceButtonClick(BluetoothDevice mBtDevice);

}
