package com.dataworks.util;

public class Constants {

	public static final String FORWARD_REVISION_TAG = "0";
	public static final String MANUFACTURER_NAME_TAG = "1";
	public static final String HARDWARE_REVISION_TAG = "2";
	public static final String SERIAL_NUMBER_TAG = "3";
	public static final String MODEL_NUMBER_TAG = "4";
	public static final String BATTERY_LEVEL_TAG = "5";

	public static final String RSSI_TAG = "6";
	public static final String DEVICE_CUSTOM_NAME = "7";

	public static final String CALIBRATE_MAG_TAG = "8";
	public static final String CALIBRATE_GYRO_TAG = "9";

	public static final String IS_MAG_CALIBRATED_TAG = "10";
	public static final String IS_GYRO_CALIBRATED_TAG = "11";

	public static final String SLOPE_TAG = "12";
	public static final String INTERCEPT_TAG = "13";
	public static final String TEMPERATURE_TAG = "14";
	
	public static final String PH_MV_SHOW_TAG = "15";
}
