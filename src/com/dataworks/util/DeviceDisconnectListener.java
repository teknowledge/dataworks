package com.dataworks.util;

public interface DeviceDisconnectListener {

	public void onDeviceDisconnect(String deviceName, String deviceAddress);

	public void onDeviceDeselect(String deviceName, String deviceAddress);
}
