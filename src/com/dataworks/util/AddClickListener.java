package com.dataworks.util;

import com.dataworks.data.DataSet;
import com.dataworks.data.PhCalibrationHistoryData;
import com.dataworks.data.Values;

public interface AddClickListener {

	public void onAddButtonClick();

	public void onDetailClick(Values mValues, String from_where);

	public void onDataSetInfo(DataSet dataset, String fromWhere);

	public void onViewValues(String name, String id, DataSet dataset,
			String viewValues);

	public void onCalibrationHistory(String deviceAddress);

	public void onCalibrationHistoryItemClick(
			PhCalibrationHistoryData calibrationHistoryData);

	public void openDeviceInfoFragment(String deviceAddress);

	public void pHDeviceInfoFragment(String deviceAddress);

	public void openSensorTagCalibrationFragment(String deviceAddress);

	public void openpHSensorCalibrationFragment(String deviceAddress);

	public void onSensorDoneClick(String deviceAddress);

	public void onpHDoneClick(String deviceAddress);

}
