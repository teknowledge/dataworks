package com.dataworks;

import java.text.DecimalFormat;
import java.util.ArrayList;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dataworks.data.SensorTag;
import com.dataworks.R;
import com.dataworks.util.NestedListView;
import com.dataworks.util.Point3D;

public class SensorTagRecordingView {
	private ArrayList<SensorTag> sensorTagsList = null;
	private String deviceAddress = null;
	private NestedListView sensorTag_listView;
	private DecimalFormat decimal = new DecimalFormat("0.0;-0.0");

	float MAG3110_RANGE = 2000.0f;
	float IMU3000_RANGE = 500.0f;
	float KXTJ9_RANGE = 4.0f;

	public SensorTagRecordingView(String deviceAddress,
			NestedListView sensorTag_listView,
			ArrayList<SensorTag> sensorTagsList) {
		// TODO Auto-generated constructor stub

		this.sensorTagsList = sensorTagsList;
		this.sensorTag_listView = sensorTag_listView;
		this.deviceAddress = deviceAddress;

	}

	public View getSensorTagKeysView() {
		int reqPosition = -1;
		View wantedView = null;

		for (int i = 0; i < sensorTagsList.size(); i++) {
			if (deviceAddress.equalsIgnoreCase(sensorTagsList.get(i)
					.getDeviceAddress())) {

				reqPosition = i;
				break;
			}
		}

		if (reqPosition != -1) {

			wantedView = getSensorTagView(reqPosition);
		}

		return wantedView;
	}

	public void updateAccValues(Point3D v) {

		String acc_x = "X: " + decimal.format(v.x);
		String acc_y = "Y: " + decimal.format(v.y);
		String acc_z = "Z: " + decimal.format(v.z);

		// acc.setText(msg);

		float acc_x_value = Float.parseFloat(decimal.format(v.x));
		float acc_y_value = Float.parseFloat(decimal.format(v.y));
		float acc_z_value = Float.parseFloat(decimal.format(v.z));

		float progress_acc_x = (acc_x_value / KXTJ9_RANGE) + 0.5f;
		float progress_acc_y = (acc_y_value / KXTJ9_RANGE) + 0.5f;
		float progress_acc_z = (acc_z_value / KXTJ9_RANGE) + 0.5f;

		int progress_acc_x_value = ((int) (progress_acc_x * 100));

		int progress_acc_y_value = ((int) (progress_acc_y * 100));

		int progress_acc_z_value = ((int) (progress_acc_z * 100));

		int reqPosition = -1;

		for (int i = 0; i < sensorTagsList.size(); i++) {
			if (deviceAddress.equalsIgnoreCase(sensorTagsList.get(i)
					.getDeviceAddress())) {

				reqPosition = i;
				break;
			}
		}

		if (reqPosition != -1) {
			SensorTag sensorTag = sensorTagsList.get(reqPosition);
			sensorTag.setAccelerometerValue_x(acc_x);
			sensorTag.setAccelerometerValue_y(acc_y);
			sensorTag.setAccelerometerValue_z(acc_z);
			sensorTag.setProgress_acc_x(progress_acc_x_value);
			sensorTag.setProgress_acc_y(progress_acc_y_value);
			sensorTag.setProgress_acc_z(progress_acc_z_value);
			sensorTag.setDeviceAddress(deviceAddress);

			sensorTagsList.set(reqPosition, sensorTag);

			View wantedView = getSensorTagView(reqPosition);
			if (wantedView != null) {
				TextView accX = (TextView) wantedView.findViewById(R.id.acc_x);
				TextView accY = (TextView) wantedView.findViewById(R.id.acc_y);
				TextView accZ = (TextView) wantedView.findViewById(R.id.acc_z);

				accX.setText(sensorTagsList.get(reqPosition)
						.getAccelerometerValue_x() + "G");
				accY.setText(sensorTagsList.get(reqPosition)
						.getAccelerometerValue_y() + "G");
				accZ.setText(sensorTagsList.get(reqPosition)
						.getAccelerometerValue_z() + "G");

				ProgressBar progress_a_x = (ProgressBar) wantedView
						.findViewById(R.id.progress_a_x);
				ProgressBar progress_a_y = (ProgressBar) wantedView
						.findViewById(R.id.progress_a_y);
				ProgressBar progress_a_z = (ProgressBar) wantedView
						.findViewById(R.id.progress_a_z);

				progress_a_x.setProgress(sensorTagsList.get(reqPosition)
						.getProgress_acc_x());
				progress_a_y.setProgress(sensorTagsList.get(reqPosition)
						.getProgress_acc_y());
				progress_a_z.setProgress(sensorTagsList.get(reqPosition)
						.getProgress_acc_z());
			}

		}
	}

	public void updateMagValues(Point3D v) {
		String mag_x = "X: " + decimal.format(v.x);
		String mag_y = "Y: " + decimal.format(v.y);
		String mag_z = "Z: " + decimal.format(v.z);

		float mag_x_value = Float.parseFloat(decimal.format(v.x));
		float mag_y_value = Float.parseFloat(decimal.format(v.y));
		float mag_z_value = Float.parseFloat(decimal.format(v.z));

		float progress_mag_x = (mag_x_value / MAG3110_RANGE) + 0.5f;
		float progress_mag_y = (mag_y_value / MAG3110_RANGE) + 0.5f;
		float progress_mag_z = (mag_z_value / MAG3110_RANGE) + 0.5f;

		int progress_mag_x_value = ((int) (progress_mag_x * 100));
		int progress_mag_y_value = ((int) (progress_mag_y * 100));
		int progress_mag_z_value = ((int) (progress_mag_z * 100));

		int reqPosition = -1;

		for (int i = 0; i < sensorTagsList.size(); i++) {
			if (deviceAddress.equalsIgnoreCase(sensorTagsList.get(i)
					.getDeviceAddress())) {

				reqPosition = i;
				break;
			}
		}

		if (reqPosition != -1) {
			SensorTag sensorTag = sensorTagsList.get(reqPosition);
			sensorTag.setMagnetometerValue_x(mag_x);
			sensorTag.setMagnetometerValue_y(mag_y);
			sensorTag.setMagnetometerValue_z(mag_z);
			sensorTag.setProgress_mag_x(progress_mag_x_value);
			sensorTag.setProgress_mag_y(progress_mag_y_value);
			sensorTag.setProgress_mag_z(progress_mag_z_value);
			sensorTag.setDeviceAddress(deviceAddress);

			sensorTagsList.set(reqPosition, sensorTag);

			View wantedView = getSensorTagView(reqPosition);
			if (wantedView != null) {

				TextView magX = (TextView) wantedView.findViewById(R.id.mag_x);
				TextView magY = (TextView) wantedView.findViewById(R.id.mag_y);
				TextView magZ = (TextView) wantedView.findViewById(R.id.mag_z);
				magX.setText(sensorTagsList.get(reqPosition)
						.getMagnetometerValue_x() + "µT");
				magY.setText(sensorTagsList.get(reqPosition)
						.getMagnetometerValue_y() + "µT");
				magZ.setText(sensorTagsList.get(reqPosition)
						.getMagnetometerValue_z() + "µT");

				ProgressBar progress_m_x = (ProgressBar) wantedView
						.findViewById(R.id.progress_m_x);
				ProgressBar progress_m_y = (ProgressBar) wantedView
						.findViewById(R.id.progress_m_y);
				ProgressBar progress_m_z = (ProgressBar) wantedView
						.findViewById(R.id.progress_m_z);

				progress_m_x.setProgress(sensorTagsList.get(reqPosition)
						.getProgress_mag_x());
				progress_m_y.setProgress(sensorTagsList.get(reqPosition)
						.getProgress_mag_y());
				progress_m_z.setProgress(sensorTagsList.get(reqPosition)
						.getProgress_mag_z());
			}
		}
	}

	public void updateGyroValues(Point3D v) {
		String gyro_x = "X: " + decimal.format(v.x);
		String gyro_y = "Y: " + decimal.format(v.y);
		String gyro_z = "Z: " + decimal.format(v.z);

		float gyro_x_value = Float.parseFloat(decimal.format(v.x));
		float gyro_y_value = Float.parseFloat(decimal.format(v.y));
		float gyro_z_value = Float.parseFloat(decimal.format(v.z));

		float progress_gyro_x = (gyro_x_value / IMU3000_RANGE) + 0.5f;
		float progress_gyro_y = (gyro_y_value / IMU3000_RANGE) + 0.5f;
		float progress_gyro_z = (gyro_z_value / IMU3000_RANGE) + 0.5f;

		int progress_gyro_x_value = ((int) (progress_gyro_x * 100));
		int progress_gyro_y_value = ((int) (progress_gyro_y * 100));
		int progress_gyro_z_value = ((int) (progress_gyro_z * 100));

		int reqPosition = -1;

		for (int i = 0; i < sensorTagsList.size(); i++) {
			if (deviceAddress.equalsIgnoreCase(sensorTagsList.get(i)
					.getDeviceAddress())) {

				reqPosition = i;
				break;
			}
		}

		if (reqPosition != -1) {

			SensorTag sensorTag = sensorTagsList.get(reqPosition);
			sensorTag.setGyroMeterValue_x(gyro_x);
			sensorTag.setGyroMeterValue_y(gyro_y);
			sensorTag.setGyroMeterValue_z(gyro_z);
			sensorTag.setProgress_gyro_x(progress_gyro_x_value);
			sensorTag.setProgress_gyro_y(progress_gyro_y_value);
			sensorTag.setProgress_gyro_z(progress_gyro_z_value);
			sensorTag.setDeviceAddress(deviceAddress);

			sensorTagsList.set(reqPosition, sensorTag);

			View wantedView = getSensorTagView(reqPosition);
			if (wantedView != null) {

				TextView gyroX = (TextView) wantedView
						.findViewById(R.id.gyro_x);
				TextView gyroY = (TextView) wantedView
						.findViewById(R.id.gyro_y);
				TextView gyroZ = (TextView) wantedView
						.findViewById(R.id.gyro_z);

				gyroX.setText(sensorTagsList.get(reqPosition)
						.getGyroMeterValue_x() + "º/s");
				gyroY.setText(sensorTagsList.get(reqPosition)
						.getGyroMeterValue_y() + "º/s");
				gyroZ.setText(sensorTagsList.get(reqPosition)
						.getGyroMeterValue_z() + "º/s");

				ProgressBar progress_g_x = (ProgressBar) wantedView
						.findViewById(R.id.progress_g_x);
				ProgressBar progress_g_y = (ProgressBar) wantedView
						.findViewById(R.id.progress_g_y);
				ProgressBar progress_g_z = (ProgressBar) wantedView
						.findViewById(R.id.progress_g_z);

				progress_g_x.setProgress(sensorTagsList.get(reqPosition)
						.getProgress_gyro_x());
				progress_g_y.setProgress(sensorTagsList.get(reqPosition)
						.getProgress_gyro_y());
				progress_g_z.setProgress(sensorTagsList.get(reqPosition)
						.getProgress_gyro_z());
			}
		}
	}

	public void updateIRtemperature(Point3D v) {

		String aTemp_value = decimal.format(v.x);

		String oTemp_value = decimal.format(v.y);

		int reqPosition = -1;

		for (int i = 0; i < sensorTagsList.size(); i++) {
			if (deviceAddress.equalsIgnoreCase(sensorTagsList.get(i)
					.getDeviceAddress())) {

				reqPosition = i;
				break;
			}
		}
		if (reqPosition != -1) {
			SensorTag sensorTag = sensorTagsList.get(reqPosition);
			sensorTag.setaTempValue(aTemp_value);
			sensorTag.setoTempValue(oTemp_value);

			sensorTag.setDeviceAddress(deviceAddress);

			sensorTagsList.set(reqPosition, sensorTag);

			View wantedView = getSensorTagView(reqPosition);
			if (wantedView != null) {

				TextView aTemp = (TextView) wantedView.findViewById(R.id.aTemp);
				aTemp.setText(sensorTagsList.get(reqPosition).getaTempValue());

				TextView oTemp = (TextView) wantedView.findViewById(R.id.oTemp);
				oTemp.setText(sensorTagsList.get(reqPosition).getoTempValue());
			}
		}
	}

	public void updateHumidityValues(Point3D v) {
		String hum_value = decimal.format(v.x);

		int reqPosition = -1;

		for (int i = 0; i < sensorTagsList.size(); i++) {
			if (deviceAddress.equalsIgnoreCase(sensorTagsList.get(i)
					.getDeviceAddress())) {

				reqPosition = i;
				break;
			}
		}
		if (reqPosition != -1) {
			SensorTag sensorTag = sensorTagsList.get(reqPosition);
			sensorTag.setHumidity(hum_value);

			sensorTag.setDeviceAddress(deviceAddress);

			sensorTagsList.set(reqPosition, sensorTag);

			View wantedView = getSensorTagView(reqPosition);
			if (wantedView != null) {

				TextView hum = (TextView) wantedView.findViewById(R.id.hum);
				hum.setText(sensorTagsList.get(reqPosition).getHumidity());
			}
		}
	}

	public void updateBaroValue(Point3D v) {
		int baroValue = (int) (v.x / 100.0f);

		String baro_value = baroValue + "";

		int reqPosition = -1;

		for (int i = 0; i < sensorTagsList.size(); i++) {
			if (deviceAddress.equalsIgnoreCase(sensorTagsList.get(i)
					.getDeviceAddress())) {

				reqPosition = i;
				break;
			}
		}

		if (reqPosition != -1) {
			SensorTag sensorTag = sensorTagsList.get(reqPosition);
			sensorTag.setBaroValue(baro_value);

			sensorTag.setDeviceAddress(deviceAddress);

			sensorTagsList.set(reqPosition, sensorTag);

			View wantedView = getSensorTagView(reqPosition);
			if (wantedView != null) {

				TextView baro = (TextView) wantedView.findViewById(R.id.baro);
				baro.setText(sensorTagsList.get(reqPosition).getBaroValue());
			}
		}
	}

	public View getSensorTagView(int reqPosition) {
		int wantedPosition = reqPosition; // Whatever position you're
		// looking for
		int firstPosition = sensorTag_listView.getFirstVisiblePosition()
				- sensorTag_listView.getHeaderViewsCount(); // This is the
		// same as child
		// #0
		int wantedChild = wantedPosition - firstPosition;
		// Say, first visible position is 8, you want position 10,
		// wantedChild will now be 2
		// So that means your view is child #2 in the ViewGroup:
		if (wantedChild < 0
				|| wantedChild >= sensorTag_listView.getChildCount()) {
			// Log.w(TAG,
			// "Unable to get view for desired position, because it's not being displayed on screen.");
			return null;
		}
		// Could also check if wantedPosition is between
		// listView.getFirstVisiblePosition() and
		// listView.getLastVisiblePosition() instead.
		View wantedView = sensorTag_listView.getChildAt(wantedChild);

		return wantedView;
	}

}
