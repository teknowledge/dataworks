package com.dataworks;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.dataworks.R;
import com.dataworks.slidemenu.SlidingFragmentActivity;
import com.dataworks.slidemenu.SlidingMenu;



public class BaseFragmentActivity extends SlidingFragmentActivity {

	Fragment mFragment;
	public static BaseFragmentActivity mActivity;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mActivity = new BaseFragmentActivity();
		setBehindContentView(R.layout.menu_frame);

		if (savedInstanceState == null) {
			FragmentTransaction t = this.getSupportFragmentManager()
					.beginTransaction();
			mFragment = new MenuFragment();
			t.replace(R.id.menu_frame, mFragment);
			t.commit();
		} else {
			mFragment = (Fragment) this.getSupportFragmentManager()
					.findFragmentById(R.id.menu_frame);
		}
		
		// customize the SlidingMenu
				SlidingMenu sm = getSlidingMenu();
				sm.setShadowWidthRes(R.dimen.shadow_width);
				sm.setShadowDrawable(R.drawable.shadow);
				sm.setBehindOffsetRes(R.dimen.slidingmenu_offset);
				sm.setFadeDegree(0.35f);
				sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mActivity = new BaseFragmentActivity();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mActivity = new BaseFragmentActivity();
	}
}
