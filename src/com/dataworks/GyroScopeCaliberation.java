package com.dataworks;

import com.dataworks.util.Point3D;

/**
 * As a last-second hack i'm storing the gyroscope coefficients in a global.
 */
public enum GyroScopeCaliberation {
	INSTANCE;
	Point3D val = new Point3D(0.0, 0.0, 0.0);
}