
package com.dataworks;

//import static android.bluetooth.BluetoothGattCharacteristic.FORMAT_UINT8;

import static com.dataworks.SensorTagGatt.UUID_ACC_CONF;
import static com.dataworks.SensorTagGatt.UUID_ACC_DATA;
import static com.dataworks.SensorTagGatt.UUID_ACC_SERV;
import static com.dataworks.SensorTagGatt.UUID_GYR_CONF;
import static com.dataworks.SensorTagGatt.UUID_GYR_DATA;
import static com.dataworks.SensorTagGatt.UUID_GYR_SERV;
import static com.dataworks.SensorTagGatt.UUID_HUM_CONF;
import static com.dataworks.SensorTagGatt.UUID_HUM_DATA;
import static com.dataworks.SensorTagGatt.UUID_HUM_SERV;
import static com.dataworks.SensorTagGatt.UUID_IRT_CONF;
import static com.dataworks.SensorTagGatt.UUID_IRT_DATA;
import static com.dataworks.SensorTagGatt.UUID_IRT_SERV;
import static com.dataworks.SensorTagGatt.UUID_KEY_DATA;
import static com.dataworks.SensorTagGatt.UUID_KEY_SERV;
import static com.dataworks.SensorTagGatt.UUID_MAG_CONF;
import static com.dataworks.SensorTagGatt.UUID_MAG_DATA;
import static com.dataworks.SensorTagGatt.UUID_MAG_SERV;
import static com.dataworks.SensorTagGatt.UUID_OPT_CONF;
import static com.dataworks.SensorTagGatt.UUID_OPT_DATA;
import static com.dataworks.SensorTagGatt.UUID_OPT_SERV;

import static com.dataworks.SensorTagGatt.UUID_PH_SERV;
import static com.dataworks.SensorTagGatt.UUID_PH_PERI;
import static com.dataworks.SensorTagGatt.UUID_PH_MEASURE;
import static com.dataworks.SensorTagGatt.UUID_PH_TEMP_MEASURE;
import static java.lang.Math.pow;

import java.util.List;
import java.util.UUID;

import android.bluetooth.BluetoothGattCharacteristic;

import com.dataworks.util.Point3D;

// import android.util.Log;

/**
 * This enum encapsulates the differences amongst the sensors. The differences
 * include UUID values and how to interpret the
 * characteristic-containing-measurement.
 */
public enum Sensor {
	IR_TEMPERATURE(UUID_IRT_SERV, UUID_IRT_DATA, UUID_IRT_CONF) {
		@Override
		public Point3D convert(final byte[] value) {

			/*
			 * The IR Temperature sensor produces two measurements; Object ( AKA
			 * target or IR) Temperature, and Ambient ( AKA die ) temperature.
			 * Both need some conversion, and Object temperature is dependent on
			 * Ambient temperature. They are stored as [ObjLSB, ObjMSB, AmbLSB,
			 * AmbMSB] (4 bytes) Which means we need to shift the bytes around
			 * to get the correct values.
			 */

			double ambient = extractAmbientTemperature(value);
			double target = extractTargetTemperature(value, ambient);
			return new Point3D(ambient, target, 0);
		}

		private double extractAmbientTemperature(byte[] v) {
			int offset = 2;
			return shortUnsignedAtOffset(v, offset) / 128.0;
		}

		private double extractTargetTemperature(byte[] v, double ambient) {
			Integer twoByteValue = shortSignedAtOffset(v, 0);

			double Vobj2 = twoByteValue.doubleValue();
			Vobj2 *= 0.00000015625;

			double Tdie = ambient + 273.15;

			double S0 = 5.593E-14; // Calibration factor
			double a1 = 1.75E-3;
			double a2 = -1.678E-5;
			double b0 = -2.94E-5;
			double b1 = -5.7E-7;
			double b2 = 4.63E-9;
			double c2 = 13.4;
			double Tref = 298.15;
			double S = S0
					* (1 + a1 * (Tdie - Tref) + a2 * pow((Tdie - Tref), 2));
			double Vos = b0 + b1 * (Tdie - Tref) + b2 * pow((Tdie - Tref), 2);
			double fObj = (Vobj2 - Vos) + c2 * pow((Vobj2 - Vos), 2);
			double tObj = pow(pow(Tdie, 4) + (fObj / S), .25);

			return tObj - 273.15;
		}
	},

	ACCELEROMETER(UUID_ACC_SERV, UUID_ACC_DATA, UUID_ACC_CONF) {
		@Override
		public Point3D convert(final byte[] value) {
			/*
			 * The accelerometer has the range [-2g, 2g] with unit (1/64)g. To
			 * convert from unit (1/64)g to unit g we divide by 64. (g = 9.81
			 * m/s^2) The z value is multiplied with -1 to coincide with how we
			 * have arbitrarily defined the positive y direction. (illustrated
			 * by the apps accelerometer image)
			 */
			// DeviceActivity da = DeviceActivity.getInstance();
			//
			// if (da.isSensorTag2()) {
			// // Range 8G
			// final float SCALE = (float) 4096.0;
			//
			// int x = (value[0] << 8) + value[1];
			// int y = (value[2] << 8) + value[3];
			// int z = (value[4] << 8) + value[5];
			// return new Point3D(x / SCALE, y / SCALE, z / SCALE);
			// } else {
			Point3D v;
			Integer x = (int) value[0];
			// Integer y = (int) value[1];
			Integer y = (int) value[1] * -1;
			// Integer z = (int) value[2] * -1;
			Integer z = (int) value[2];

			// needed

			// if (da.firmwareRevision().contains("1.5"))
			// {
			// // Range 8G
			// final float SCALE = (float) 64.0;
			// v = new Point3D(x / SCALE, y / SCALE, z / SCALE);
			// } else {
			// // Range 2G
			// final float SCALE = (float) 16.0;
			// v = new Point3D(x / SCALE, y / SCALE, z / SCALE);
			// }

			// Range 8G
			final float SCALE = (float) 64.0;
			v = new Point3D(x / SCALE, y / SCALE, z / SCALE);

			return v;
		}

		// }
	},

	HUMIDITY(UUID_HUM_SERV, UUID_HUM_DATA, UUID_HUM_CONF) {
		@Override
		public Point3D convert(final byte[] value) {
			int a = shortUnsignedAtOffset(value, 2);
			// bits [1..0] are status bits and need to be cleared according
			// to the user guide, but the iOS code doesn't bother. It should
			// have minimal impact.
			a = a - (a % 4);

			return new Point3D((-6f) + 125f * (a / 65535f), 0, 0);
		}
	},

	MAGNETOMETER(UUID_MAG_SERV, UUID_MAG_DATA, UUID_MAG_CONF) {
		@Override
		public Point3D convert(final byte[] value) {
			Point3D mcal = MagnetometerCalibrationCoefficients.INSTANCE.val;
			// Multiply x and y with -1 so that the values correspond with the
			// image in the app
			float x = shortSignedAtOffset(value, 0) * (2000f / 65536f) * -1;
			float y = shortSignedAtOffset(value, 2) * (2000f / 65536f) * -1;
			float z = shortSignedAtOffset(value, 4) * (2000f / 65536f);

			return new Point3D(x - mcal.x, y - mcal.y, z - mcal.z);
		}
	},

	LUXOMETER(UUID_OPT_SERV, UUID_OPT_DATA, UUID_OPT_CONF) {
		@Override
		public Point3D convert(final byte[] value) {
			int mantissa;
			int exponent;
			Integer sfloat = shortUnsignedAtOffset(value, 0);

			mantissa = sfloat & 0x0FFF;
			exponent = (sfloat >> 12) & 0xFF;

			double output;
			double magnitude = pow(2.0f, exponent);
			output = (mantissa * magnitude);

			return new Point3D(output / 100.0f, 0, 0);
		}
	},

	GYROSCOPE(UUID_GYR_SERV, UUID_GYR_DATA, UUID_GYR_CONF, (byte) 7) {
		@Override
		public Point3D convert(final byte[] value) {

			Point3D mcal = GyroScopeCaliberation.INSTANCE.val;

			float y = shortSignedAtOffset(value, 0) * (500f / 65536f) * -1;
			float x = shortSignedAtOffset(value, 2) * (500f / 65536f);
			float z = shortSignedAtOffset(value, 4) * (500f / 65536f);

			return new Point3D(x - mcal.x, y - mcal.y, z - mcal.z);
		}
	},

	BAROMETER(SensorTagGatt.UUID_BAR_SERV, SensorTagGatt.UUID_BAR_DATA,
			SensorTagGatt.UUID_BAR_CONF) {
		@Override
		public Point3D convert(final byte[] value) {

			// if (DeviceActivity.getInstance().isSensorTag2()) {
			// int mantissa;
			// int exponent;
			// Integer sfloat = shortUnsignedAtOffset(value, 2);
			//
			// mantissa = sfloat & 0x0FFF;
			// exponent = (sfloat >> 12) & 0xFF;
			//
			// double output;
			// double magnitude = pow(2.0f, exponent);
			// output = (mantissa * magnitude);
			//
			// return new Point3D(output / 100.0f, 0, 0);
			// } else {

			List<Integer> barometerCalibrationCoefficients = BarometerCalibrationCoefficients.INSTANCE.barometerCalibrationCoefficients;
			if (barometerCalibrationCoefficients == null) {
				// Log.w("Sensor",
				// "Data notification arrived for barometer before it was calibrated.");
				return new Point3D(0, 0, 0);
			}

			final int[] c; // Calibration coefficients
			final Integer t_r; // Temperature raw value from sensor
			final Integer p_r; // Pressure raw value from sensor
			final Double S; // Interim value in calculation
			final Double O; // Interim value in calculation
			final Double p_a; // Pressure actual value in unit Pascal.

			c = new int[barometerCalibrationCoefficients.size()];
			for (int i = 0; i < barometerCalibrationCoefficients.size(); i++) {
				c[i] = barometerCalibrationCoefficients.get(i);
			}

			t_r = shortSignedAtOffset(value, 0);
			p_r = shortUnsignedAtOffset(value, 1);

			S = c[2] + c[3] * t_r / pow(2, 17)
					+ ((c[4] * t_r / pow(2, 15)) * t_r) / pow(2, 18);
			O = c[5] * pow(2, 14) + c[6] * t_r / pow(2, 2)
					+ ((c[7] * t_r / pow(2, 15)) * t_r) / pow(2, 2);
			
			p_a = (S * p_r + O) / pow(2, 15);
			

			return new Point3D(p_a, 0, 0);
		}
		// }
	},

	/******** convert pH Values ******/
	PH_MEASUREMENT(UUID_PH_SERV, UUID_PH_MEASURE, UUID_PH_PERI) {
		@Override
		public Point3D convert(final byte[] value) {

			// float pH = 1.0f;

			// final byte[] data = characteristic.getValue();
			// System.out.println("Value = ");

//			System.out.println(value.toString());

			int raw = binaryToInteger(value);

			float mV = convertRawtomV(raw);

//			float pH = caliberatedpH(mV, 1f, 0f);
//
//			float ph_compensated = compensatedpH(pH, 0f);

			return new Point3D(mV, 0, 0);
		}
	},
	/******** convert pH Values ******/
	PH_TEMP_MEASUREMENT(UUID_PH_SERV, UUID_PH_TEMP_MEASURE, UUID_PH_PERI) {
		@Override
		public Point3D convert(final byte[] value) {

			// float pH = 1.0f;

			// final byte[] data = characteristic.getValue();
			// System.out.println("Value = ");

//			System.out.println(value.toString());

			int raw = binaryToInteger(value);

			

			return new Point3D(raw, 0, 0);
		}
	},

	SIMPLE_KEYS(UUID_KEY_SERV, UUID_KEY_DATA, null) {
		@Override
		public SimpleKeysStatus convertKeys(final byte value) {
			/*
			 * Key mapping for SensorTagGatt: 0 - right key 1 - left key 2 -
			 * side key
			 */
			int keys = (int) value;
			// if (DeviceActivity.getInstance().isSensorTag2()) {
			// /*
			// * Key mapping for SensorTagGatt 2: 0 - left key 1 - right key 2
			// * - reed relay
			// */
			// int t = keys;
			// keys = t & 0x04;
			// // Swapped keys compared to first SensorTagGatt
			// if ((t & 1) == 1)
			// keys |= 2;
			// if ((t & 2) == 2)
			// keys |= 1;
			// }

			return SimpleKeysStatus.values()[keys & 7];
		}
	};

	/*********** conversions to get the values from pH sensor in pHs and mVs ********/
	public Integer binaryToInteger(byte[] data) {

		// char[] hexChars = new char[2];
		int result = 0;
		//
		// hexChars[0] = (char) data.toString().charAt(0) ;
		// hexChars[1] = (char) data.toString().charAt(1) ;
		//
		// result = (hexChars[1] <<8)| (hexChars[0] & 0xff);

		for (int i = 0; i < data.length; i++) {
			result = result | ((data[i] & 0xff) << i * 8);
		}

		return result;
	}

	public float convertRawtomV(int raw) {

		float slope = 0.25229283f;

		float intercept = -1040.5521f;

		float mV = slope * raw + intercept;

		return mV;
	}

	public float caliberatedpH(float mV, float slope, float intercept) {
		// Converts the mV value to a calibrated pH value
		// slope = is slope adjustment determined by the calibration of the
		// electrode
		// zero = is the zero point adjustment determined by the calibration of
		// the electrode
		// sl25 = is the ideal slope at 25 degrees C
		// mV = is the measure mV value supplied by the pH electrode.

		float sl25 = 59.16f;
		float pH;

		// pH = slope * (sl25 * 7 - (mV - zero)) / sl25;
		pH = 7 - ((mV - intercept) / (slope * sl25));
		// NSLog(@"pH = %f, mV = %f, zero = %f, slope = %f",pH, mV, zero,
		// slope);

		return pH;
	}

	public float compensatedpH(float pH, float temperature) {
		// Converts the measured pH value into a temperature compensated pH
		// value.
		// temperature = is the temperature in celcius

		float kelvin = temperature + 273.15f;
		float pH_t;

		pH_t = (float) (7 - kelvin / 298.15 * (7 - pH));

		return pH_t;
	}


	/**
	 * Gyroscope, Magnetometer, Barometer, IR temperature all store 16 bit two's
	 * complement values as LSB MSB, which cannot be directly parsed as
	 * getIntValue(FORMAT_SINT16, offset) because the bytes are stored as
	 * little-endian.
	 * 
	 * This function extracts these 16 bit two's complement values.
	 * */
	private static Integer shortSignedAtOffset(byte[] c, int offset) {
		Integer lowerByte = (int) c[offset] & 0xFF;
		Integer upperByte = (int) c[offset + 1]; // // Interpret MSB as signed
		return (upperByte << 8) + lowerByte;
	}

	private static Integer shortUnsignedAtOffset(byte[] c, int offset) {
		Integer lowerByte = (int) c[offset] & 0xFF;
		Integer upperByte = (int) c[offset + 1] & 0xFF; // // Interpret MSB as
														// signed
		return (upperByte << 8) + lowerByte;
	}

	public void onCharacteristicChanged(BluetoothGattCharacteristic c) {
		throw new UnsupportedOperationException(
				"Error: the individual enum classes are supposed to override this method.");
	}

	public SimpleKeysStatus convertKeys(byte value) {
		throw new UnsupportedOperationException(
				"Error: the individual enum classes are supposed to override this method.");
	}

	public Point3D convert(byte[] value) {
		throw new UnsupportedOperationException(
				"Error: the individual enum classes are supposed to override this method.");
	}

	private final UUID service, data, config;
	private byte enableCode; // See getEnableSensorCode for explanation.
	public static final byte DISABLE_SENSOR_CODE = 0;
	public static final byte ENABLE_SENSOR_CODE = 1;
	public static final byte CALIBRATE_SENSOR_CODE = 2;
//	public static final byte SENSOR_TAG_CODE = 1;
//	private byte enableCodeForSensorTag;

	/**
	 * Constructor called by the Gyroscope and Accelerometer because it more
	 * than a boolean enable code.
	 */
	private Sensor(UUID service, UUID data, UUID config, byte enableCode) {
		this.service = service;
		this.data = data;
		this.config = config;
		this.enableCode = enableCode;
	}

	/**
	 * Constructor called by all the sensors except Gyroscope
	 * */
	private Sensor(UUID service, UUID data, UUID config) {
		this.service = service;
		this.data = data;
		this.config = config;
		this.enableCode = ENABLE_SENSOR_CODE; // This is the sensor enable code
												// for all sensors except the
												// gyroscope
		
		//this.enableCodeForSensorTag = SENSOR_TAG_CODE;
	}

	/**
	 * @return the code which, when written to the configuration characteristic,
	 *         turns on the sensor.
	 * */
	public byte getEnableSensorCode() {
		return enableCode;
	}
//	public byte getEnableSensorCodeForSensorTag(){
//		return enableCodeForSensorTag;
//	}

	public UUID getService() {
		return service;
	}

	public UUID getData() {
		return data;
	}

	public UUID getConfig() {
		return config;
	}

	public static Sensor getFromDataUuid(UUID uuid) {
		for (Sensor s : Sensor.values()) {
			if (s.getData().equals(uuid)) {
				return s;
			}
		}
		throw new RuntimeException("unable to find UUID.");
	}

	public static final Sensor[] SENSOR_LIST = { IR_TEMPERATURE, ACCELEROMETER,
			MAGNETOMETER, LUXOMETER, GYROSCOPE, HUMIDITY, BAROMETER,
			PH_MEASUREMENT,PH_TEMP_MEASUREMENT, SIMPLE_KEYS };
}
