package com.dataworks;

import java.text.DecimalFormat;

import com.dataworks.data.PhCalibrationHistoryData;
import com.dataworks.R;
import com.dataworks.util.Utils;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class PhCalibrationHistoryDetails extends Fragment {

	public static PhCalibrationHistoryDetails pHCalibrationHistoryDetailsFrag;
	PhCalibrationHistoryData mCalibrationHistoryData;

	public static PhCalibrationHistoryDetails newInstance() {
		// if (detailsFragment == null)
		pHCalibrationHistoryDetailsFrag = new PhCalibrationHistoryDetails();
		return pHCalibrationHistoryDetailsFrag;
	}

	public void setpHCalibrationHistoryData(
			PhCalibrationHistoryData calibrationHistoryData) {
		mCalibrationHistoryData = calibrationHistoryData;
	}

	public static PhCalibrationHistoryDetails getInstance() {

		return pHCalibrationHistoryDetailsFrag;
	}

	View historydetailsView;

	private TextView date_time_text;
	private TextView slope_value_text;
	private TextView intercept_value_text;

	private TextView ph_four_temerature_value_text;
	private TextView ph_four_millivolts_value_text;
	private TextView ph_four_uncorrect_ph_value_text;

	private TextView ph_sev_temerature_value_text;
	private TextView ph_sev_millivolts_value_text;
	private TextView ph_sev_uncorrect_ph_value_text;

	private TextView ph_ten_temerature_value_text;
	private TextView ph_ten_millivolts_value_text;
	private TextView ph_ten_uncorrect_ph_value_text;

	private DecimalFormat decimal_2 = new DecimalFormat("0.00");

	private LinearLayout ph4_layout;
	private LinearLayout ph7_layout;
	private LinearLayout ph10_layout;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		historydetailsView = inflater.inflate(R.layout.calibration_details,
				container, false);

		date_time_text = (TextView) historydetailsView
				.findViewById(R.id.date_time_text);
		slope_value_text = (TextView) historydetailsView
				.findViewById(R.id.slope_value_text);
		intercept_value_text = (TextView) historydetailsView
				.findViewById(R.id.intercept_value_text);
		ph4_layout = (LinearLayout) historydetailsView
				.findViewById(R.id.ph4_layout);
		ph7_layout = (LinearLayout) historydetailsView
				.findViewById(R.id.ph7_layout);
		ph10_layout = (LinearLayout) historydetailsView
				.findViewById(R.id.ph10_layout);

		ph_four_temerature_value_text = (TextView) historydetailsView
				.findViewById(R.id.ph_four_temerature_value_text);

		ph_four_millivolts_value_text = (TextView) historydetailsView
				.findViewById(R.id.ph_four_millivolts_value_text);

		ph_four_uncorrect_ph_value_text = (TextView) historydetailsView
				.findViewById(R.id.ph_four_uncorrect_ph_value_text);

		ph_sev_temerature_value_text = (TextView) historydetailsView
				.findViewById(R.id.ph_sev_temerature_value_text);

		ph_sev_millivolts_value_text = (TextView) historydetailsView
				.findViewById(R.id.ph_sev_millivolts_value_text);

		ph_sev_uncorrect_ph_value_text = (TextView) historydetailsView
				.findViewById(R.id.ph_sev_uncorrect_ph_value_text);

		ph_ten_temerature_value_text = (TextView) historydetailsView
				.findViewById(R.id.ph_ten_temerature_value_text);

		ph_ten_millivolts_value_text = (TextView) historydetailsView
				.findViewById(R.id.ph_ten_millivolts_value_text);

		ph_ten_uncorrect_ph_value_text = (TextView) historydetailsView
				.findViewById(R.id.ph_ten_uncorrect_ph_value_text);

		return historydetailsView;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		date_time_text.setText(mCalibrationHistoryData.getDate() + " "
				+ Utils.Convert24to12(mCalibrationHistoryData.getTime()));

		String slope = decimal_2.format(Float
				.parseFloat(mCalibrationHistoryData.getSlope()));
		String intercept = decimal_2.format(Float
				.parseFloat(mCalibrationHistoryData.getIntercept()));

		slope_value_text.setText(slope);
		intercept_value_text.setText(intercept);

		if (!mCalibrationHistoryData.getPh4().equalsIgnoreCase("")) {
			ph4_layout.setVisibility(View.VISIBLE);

			String dataArray[] = mCalibrationHistoryData.getPh4().split(",");
			String temp = dataArray[0];
			String millV = dataArray[1];
			String unCorretectedPh = dataArray[2];

			String temperature = decimal_2.format(Float.parseFloat(temp));
			String milliVolts = decimal_2.format(Float.parseFloat(millV));
			String unCorretectedpH = decimal_2.format(Float
					.parseFloat(unCorretectedPh));

			ph_four_temerature_value_text.setText(temperature);
			ph_four_millivolts_value_text.setText(milliVolts);
			ph_four_uncorrect_ph_value_text.setText(unCorretectedpH);
		}
		if (!mCalibrationHistoryData.getPh7().equalsIgnoreCase("")) {
			ph7_layout.setVisibility(View.VISIBLE);

			String dataArray[] = mCalibrationHistoryData.getPh7().split(",");
			String temp = dataArray[0];
			String millV = dataArray[1];
			String unCorretectedPh = dataArray[2];

			String temperature = decimal_2.format(Float.parseFloat(temp));
			String milliVolts = decimal_2.format(Float.parseFloat(millV));
			String unCorretectedpH = decimal_2.format(Float
					.parseFloat(unCorretectedPh));

			ph_sev_temerature_value_text.setText(temperature);
			ph_sev_millivolts_value_text.setText(milliVolts);
			ph_sev_uncorrect_ph_value_text.setText(unCorretectedpH);
		}
		if (!mCalibrationHistoryData.getPh10().equalsIgnoreCase("")) {
			ph10_layout.setVisibility(View.VISIBLE);

			String dataArray[] = mCalibrationHistoryData.getPh10().split(",");
			String temp = dataArray[0];
			String millV = dataArray[1];
			String unCorretectedPh = dataArray[2];

			String temperature = decimal_2.format(Float.parseFloat(temp));
			String milliVolts = decimal_2.format(Float.parseFloat(millV));
			String unCorretectedpH = decimal_2.format(Float
					.parseFloat(unCorretectedPh));

			ph_ten_temerature_value_text.setText(temperature);
			ph_ten_millivolts_value_text.setText(milliVolts);
			ph_ten_uncorrect_ph_value_text.setText(unCorretectedpH);
		}

	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

}
