package com.dataworks;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.dataworks.database.DataBaseHelper;
import com.dataworks.util.AddClickListener;
import com.dataworks.util.Constants;
import com.dataworks.util.Global;
import com.dataworks.util.Point3D;

public class CaliberationFragment extends Fragment {

	private static CaliberationFragment dataSetsFragment;

	private TextView acc, mag, gyro, oTemp, aTemp, hum, baro;
	private Button calib_mag;
	private Button calibre_gyro;
	public static LinearLayout device_instreument_view;

	public static LinearLayout device_view;
	public static LinearLayout discoonected_text;
	private LinearLayout sensorDeviceValuesLayout;

	public static ImageView record_device, switch_device;
	private TextView bluetooth_device_text;
	private static final double PA_PER_METER = 12.0;
	private DecimalFormat decimal = new DecimalFormat("0.0;0.0");

	private ProgressBar progress_m_x, progress_m_y, progress_m_z, progress_a_x,
			progress_a_y, progress_a_z, progress_g_x, progress_g_y,
			progress_g_z;
	private DataBaseHelper db;
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

	private int delayInMSec;
	private int numberOfPoints;
	private boolean isRecordPressed, isStarted, isRecordSwitched;
	private int i;
	private TextView cancel_btn;
	private String dataSetId, date_value, time_value, oTemp_value, aTemp_value,
			hum_value, baro_value, acc_value, mag_value, gyro_value;

	private TextView mag_X, mag_Y, mag_Z, acc_X, acc_Y, acc_Z, gyro_X, gyro_Y,
			gyro_Z;

	private AddClickListener mClickListener;

	private boolean isDeviceConnected;
	public String mDeviceAddress;

	private String deviceCustomName;

	public static CaliberationFragment newInstance() {
		if (dataSetsFragment == null)
			dataSetsFragment = new CaliberationFragment();
		return dataSetsFragment;
	}

	public static CaliberationFragment getInstance() {

		return dataSetsFragment;
	}

	public void setDeviceConnectionStatus(String deviceAddress,
			boolean isDeviceConnected) {

		if (mDeviceAddress.equalsIgnoreCase(deviceAddress)) {
			this.isDeviceConnected = isDeviceConnected;
			checkDeviceConnection();
		}

	}

	public void setDeviceAddress(String deviceAddress) {

		mDeviceAddress = deviceAddress;

	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		if (activity instanceof AddClickListener) {
			mClickListener = (AddClickListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implement BluetoothClickListener");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.caliberation_fragment, container,
				false);

		cancel_btn = (TextView) view.findViewById(R.id.cancel_btn);
		bluetooth_device_text = (TextView) view
				.findViewById(R.id.bluetooth_device);
		device_instreument_view = (LinearLayout) view
				.findViewById(R.id.device_instreument_view);

		device_view = (LinearLayout) view.findViewById(R.id.device_view);
		discoonected_text = (LinearLayout) view
				.findViewById(R.id.discoonected_text);
		sensorDeviceValuesLayout = (LinearLayout) view
				.findViewById(R.id.callberation_device_values_layout);

		calib_mag = (Button) view.findViewById(R.id.calib_mag);
		calibre_gyro = (Button) view.findViewById(R.id.calibre_gyro);

		acc_X = (TextView) view.findViewById(R.id.acc_X);
		acc_Y = (TextView) view.findViewById(R.id.acc_Y);
		acc_Z = (TextView) view.findViewById(R.id.acc_Z);

		mag_X = (TextView) view.findViewById(R.id.mag_X);
		mag_Y = (TextView) view.findViewById(R.id.mag_Y);
		mag_Z = (TextView) view.findViewById(R.id.mag_Z);

		gyro_X = (TextView) view.findViewById(R.id.gyro_X);
		gyro_Y = (TextView) view.findViewById(R.id.gyro_Y);
		gyro_Z = (TextView) view.findViewById(R.id.gyro_Z);

		oTemp = (TextView) view.findViewById(R.id.oTemp);
		aTemp = (TextView) view.findViewById(R.id.aTemp);
		hum = (TextView) view.findViewById(R.id.hum);

		baro = (TextView) view.findViewById(R.id.baro);
		// mButton = (ImageView) findViewById(R.id.buttons);

		record_device = (ImageView) view.findViewById(R.id.record_device);
		switch_device = (ImageView) view.findViewById(R.id.switch_device);

		progress_m_x = (ProgressBar) view.findViewById(R.id.progress_m_x);
		progress_m_y = (ProgressBar) view.findViewById(R.id.progress_m_y);
		progress_m_z = (ProgressBar) view.findViewById(R.id.progress_m_z);
		progress_a_x = (ProgressBar) view.findViewById(R.id.progress_a_x);
		progress_a_y = (ProgressBar) view.findViewById(R.id.progress_a_y);
		progress_a_z = (ProgressBar) view.findViewById(R.id.progress_a_z);
		progress_g_x = (ProgressBar) view.findViewById(R.id.progress_g_x);
		progress_g_y = (ProgressBar) view.findViewById(R.id.progress_g_y);
		progress_g_z = (ProgressBar) view.findViewById(R.id.progress_g_z);

		calib_mag.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				insertCalibrationForSensorTag(mDeviceAddress,
						Constants.CALIBRATE_MAG_TAG, "1");
			}
		});

		calibre_gyro.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				insertCalibrationForSensorTag(mDeviceAddress,
						Constants.CALIBRATE_GYRO_TAG, "1");
			}
		});

		getDeviceInfoFromDB(mDeviceAddress);

		bluetooth_device_text.setText(deviceCustomName);

		cancel_btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		return view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		checkDeviceConnection();

	}

	/***** insert calibration for sensor Tag ***/
	public void insertCalibrationForSensorTag(String deviceAddress, String tag,
			String calibration) {
		db = new DataBaseHelper(getActivity());
		db.open();
		db.updateCalibrationSensorTag(deviceAddress, tag, calibration);
		db.close();
	}

	/***** check whether device is connected or disconnected ****/
	private void checkDeviceConnection() {

		if (sensorDeviceValuesLayout != null && discoonected_text != null) {
			if (!isDeviceConnected) {
				sensorDeviceValuesLayout.setVisibility(View.GONE);
				CaliberationFragment.discoonected_text
						.setVisibility(View.VISIBLE);
			} else {
				sensorDeviceValuesLayout.setVisibility(View.VISIBLE);
				CaliberationFragment.discoonected_text.setVisibility(View.GONE);
			}
		}
	}

	Handler handler = new Handler();
	Runnable runnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub

			if (i != numberOfPoints) {
				insertValuesForDataset();
				handler.postDelayed(runnable, delayInMSec);
				i++;
			}
			if (i == numberOfPoints) {
				isStarted = false;

				record_device.setImageResource(R.drawable.start_red_button);
				switch_device.setImageResource(R.drawable.switch_red_button);

			}
		}
	};

	/**
	 * Handle changes in sensor values
	 * */
	public void onCharacteristicChangedUpdate(String uuidStr, byte[] rawValue,
			String deviceAddress) {

		Point3D v;
		String msg;

		float MAG3110_RANGE = 2000.0f;
		float IMU3000_RANGE = 500.0f;
		float KXTJ9_RANGE = 4.0f;

		if (uuidStr.equals(SensorTagGatt.UUID_ACC_DATA.toString())) {
			v = Sensor.ACCELEROMETER.convert(rawValue);
			msg = "X: " + decimal.format(v.x) + "G" + "\n" + "Y: "
					+ decimal.format(v.y) + "G" + "\n" + "Z: "
					+ decimal.format(v.z) + "G";

			acc_value = "X: " + decimal.format(v.x) + "," + "Y: "
					+ decimal.format(v.y) + "," + "Z: " + decimal.format(v.z);

			if (mDeviceAddress.equalsIgnoreCase(deviceAddress)) {

				if (acc_X != null && acc_Y != null && acc_Z != null) {
					acc_X.setText("X: " + decimal.format(v.x) + "G");
					acc_Y.setText("Y: " + decimal.format(v.y) + "G");
					acc_Z.setText("Z: " + decimal.format(v.z) + "G");
				}

				float acc_x_value = Float.parseFloat(decimal.format(v.x));
				float acc_y_value = Float.parseFloat(decimal.format(v.y));
				float acc_z_value = Float.parseFloat(decimal.format(v.z));

				float progress_acc_x = (acc_x_value / KXTJ9_RANGE) + 0.5f;
				float progress_acc_y = (acc_y_value / KXTJ9_RANGE) + 0.5f;
				float progress_acc_z = (acc_z_value / KXTJ9_RANGE) + 0.5f;

				if (progress_a_x != null && progress_a_y != null
						&& progress_a_z != null) {
					progress_a_x.setProgress((int) (progress_acc_x * 100));
					progress_a_y.setProgress((int) (progress_acc_y * 100));
					progress_a_z.setProgress((int) (progress_acc_z * 100));
				}

			}

		}

		if (uuidStr.equals(SensorTagGatt.UUID_MAG_DATA.toString())) {
			v = Sensor.MAGNETOMETER.convert(rawValue);
			msg = "X: " + decimal.format(v.x) + "µT" + "\n" + "Y: "
					+ decimal.format(v.y) + "µT" + "\n" + "Z: "
					+ decimal.format(v.z) + "µT";

			mag_value = "X: " + decimal.format(v.x) + "," + "Y: "
					+ decimal.format(v.y) + "," + "Z: " + decimal.format(v.z);

			if (mDeviceAddress.equalsIgnoreCase(deviceAddress)) {

				if (mag_X != null && mag_Y != null && mag_Z != null) {
					mag_X.setText("X: " + decimal.format(v.x) + "µT");
					mag_Y.setText("Y: " + decimal.format(v.y) + "µT");
					mag_Z.setText("Z: " + decimal.format(v.z) + "µT");
				}

				float mag_x_value = Float.parseFloat(decimal.format(v.x));
				float mag_y_value = Float.parseFloat(decimal.format(v.y));
				float mag_z_value = Float.parseFloat(decimal.format(v.z));

				float progress_mag_x = (mag_x_value / MAG3110_RANGE) + 0.5f;
				float progress_mag_y = (mag_y_value / MAG3110_RANGE) + 0.5f;
				float progress_mag_z = (mag_z_value / MAG3110_RANGE) + 0.5f;

				if (progress_m_x != null && progress_m_y != null
						&& progress_m_z != null) {
					progress_m_x.setProgress((int) (progress_mag_x * 100));
					progress_m_y.setProgress((int) (progress_mag_y * 100));
					progress_m_z.setProgress((int) (progress_mag_z * 100));
				}

			}

		}

		if (uuidStr.equals(SensorTagGatt.UUID_OPT_DATA.toString())) {
			v = Sensor.LUXOMETER.convert(rawValue);
			msg = decimal.format(v.x);
			// mLuxValue.setText(msg);
		}

		if (uuidStr.equals(SensorTagGatt.UUID_GYR_DATA.toString())) {
			v = Sensor.GYROSCOPE.convert(rawValue);
			msg = "X: " + decimal.format(v.x) + "º/s" + "\n" + "Y: "
					+ decimal.format(v.y) + "º/s" + "\n" + "Z: "
					+ decimal.format(v.z) + "º/s";

			gyro_value = "X: " + decimal.format(v.x) + "," + "Y: "
					+ decimal.format(v.y) + "," + "Z: " + decimal.format(v.z);

			if (mDeviceAddress.equalsIgnoreCase(deviceAddress)) {

				if (gyro_X != null && gyro_Y != null && gyro_Z != null) {
					gyro_X.setText("X: " + decimal.format(v.x) + "º/s");
					gyro_Y.setText("Y: " + decimal.format(v.y) + "º/s");
					gyro_Z.setText("Z: " + decimal.format(v.z) + "º/s");
				}

				float gyro_x_value = Float.parseFloat(decimal.format(v.x));
				float gyro_y_value = Float.parseFloat(decimal.format(v.y));
				float gyro_z_value = Float.parseFloat(decimal.format(v.z));

				float progress_gyro_x = (gyro_x_value / IMU3000_RANGE) + 0.5f;
				float progress_gyro_y = (gyro_y_value / IMU3000_RANGE) + 0.5f;
				float progress_gyro_z = (gyro_z_value / IMU3000_RANGE) + 0.5f;

				if (progress_g_x != null && progress_g_y != null
						&& progress_g_z != null) {
					progress_g_x.setProgress((int) (progress_gyro_x * 100));
					progress_g_y.setProgress((int) (progress_gyro_y * 100));
					progress_g_z.setProgress((int) (progress_gyro_z * 100));
				}

			}

		}

		if (uuidStr.equals(SensorTagGatt.UUID_IRT_DATA.toString())) {
			v = Sensor.IR_TEMPERATURE.convert(rawValue);

			if (mDeviceAddress.equalsIgnoreCase(deviceAddress)) {
				msg = decimal.format(v.x);
				aTemp_value = decimal.format(v.x);

				String o_msg = decimal.format(v.y);

				oTemp_value = decimal.format(v.y);

				if (aTemp != null && oTemp != null) {
					aTemp.setText(msg);
					oTemp.setText(o_msg);

				}
			}
		}

		if (uuidStr.equals(SensorTagGatt.UUID_HUM_DATA.toString())) {
			v = Sensor.HUMIDITY.convert(rawValue);

			if (mDeviceAddress.equalsIgnoreCase(deviceAddress)) {
				msg = decimal.format(v.x);

				hum_value = decimal.format(v.x);

				if (hum != null) {
					hum.setText(msg);
				}
			}
		}

		if (uuidStr.equals(SensorTagGatt.UUID_BAR_DATA.toString())) {

			v = Sensor.BAROMETER.convert(rawValue);

			// double h = (v.x -
			// BarometerCalibrationCoefficients.INSTANCE.heightCalibration)
			// / PA_PER_METER;
			// double h = (v.x -
			// BarometerCalibrationCoefficients.INSTANCE.heightCalibration);

			// h = (double) Math.round(-h * 10.0) / 10.0;
			// msg = decimal.format(v.x / 100.0f) + "\n" + h;

			if (mDeviceAddress.equalsIgnoreCase(deviceAddress)) {
				// msg = decimal.format(v.x / 100.0f);
				// msg = String.valueOf(h);
				int baroValue = (int) (v.x / 100.0f);

				String baro_value = baroValue + "";
				// baro_value = msg;

				if (baro != null) {
					baro.setText(baro_value);
				}
			}
		}

		if (uuidStr.equals(SensorTagGatt.UUID_KEY_DATA.toString())) {
			int keys = rawValue[0];
			SimpleKeysStatus s;
			final int imgBtn;
			s = Sensor.SIMPLE_KEYS.convertKeys((byte) (keys & 3));

			switch (s) {
			case OFF_ON:
				// imgBtn = R.drawable.buttonsoffon;
				// setBusy(true);

				// if (!isRecordSwitched) {
				//
				// //openDialog();
				// } else {
				// if (isStarted) {
				// // do nothing disable the action for switch button
				// } else {
				// if (isRecordPressed) {
				//
				// record_device
				// .setImageResource(R.drawable.record_button);
				// switch_device
				// .setImageResource(R.drawable.switch_button);
				//
				// handler.removeCallbacks(runnable);
				// isRecordPressed = false;
				// } else {
				//
				// record_device
				// .setImageResource(R.drawable.start_red_background);
				// switch_device
				// .setImageResource(R.drawable.switch_red_button);
				//
				// isRecordPressed = true;
				//
				// }
				// }
				// }

				break;

			/*********** action for record button *********/
			case ON_OFF:
				// imgBtn = R.drawable.buttonsonoff;
				// setBusy(true);
				//
				// if (isRecordPressed && !isStarted) {
				//
				// isStarted = true;
				// i = 0;
				// handler.post(runnable);
				//
				// record_device.setImageResource(R.drawable.stop_red_button);
				// switch_device
				// .setImageResource(R.drawable.switch_black_button);
				//
				// } else if (isStarted)
				//
				// {
				// isStarted = false;
				//
				// record_device
				// .setImageResource(R.drawable.start_red_background);
				// switch_device
				// .setImageResource(R.drawable.switch_red_button);
				//
				// handler.removeCallbacks(runnable);
				// }
				// if (!isRecordPressed) {
				// insertValuesForDataset();
				// }

				break;
			case ON_ON:
				// imgBtn = R.drawable.buttonsonon;
				break;
			default:
				// imgBtn = R.drawable.buttonsoffoff;
				// setBusy(false);
				break;
			}

			// mButton.setImageResource(imgBtn);

			// if (mIsSensorTag2) {
			// // Only applicable for SensorTag2
			// final int imgRelay;
			//
			// if ((keys&4) == 4) {
			// imgRelay = R.drawable.reed_open;
			// } else {
			// imgRelay = R.drawable.reed_closed;
			// }
			// mRelay.setImageResource(imgRelay);
			// }
		}
	}

	public void insertValuesForDataset() {
		// dataSetId = Global.getInstance().getPreferenceVal(
		// getActivity().getApplicationContext(), "datasetId");
		//
		// Calendar cal = Calendar.getInstance();
		// // System.out.println(dateFormat.format(cal.getTime()));
		// String dateAndTime[] = dateFormat.format(cal.getTime()).split(" ");
		// String date = dateAndTime[0];
		// String time = dateAndTime[1];
		// db = new DataBaseHelper(getActivity());
		// db.open();
		// db.insertValues(dataSetId, "", "", date, time, "", aTemp_value,
		// oTemp_value, hum_value, baro_value, mag_value, acc_value,
		// gyro_value);
		// db.close();
	}

	/***** get device info from db or device custom name to display *****/
	public void getDeviceInfoFromDB(String deviceAddress) {
		db = new DataBaseHelper(getActivity());
		db.open();

		Cursor deviceInfo = db.getDeviceInfoAddress(deviceAddress);

		if (deviceInfo.moveToFirst()) {
			do {

				deviceCustomName = deviceInfo.getString(0);

			} while (deviceInfo.moveToNext());
		}
		deviceInfo.close();
		db.close();
	}
}
