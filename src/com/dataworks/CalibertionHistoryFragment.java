package com.dataworks;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.dataworks.adapter.PhCalibrateHistoryAdapter;
import com.dataworks.data.PhCalibrationHistoryData;
import com.dataworks.database.DataBaseHelper;
import com.dataworks.R;
import com.dataworks.util.AddClickListener;

public class CalibertionHistoryFragment extends Fragment {

	public static CalibertionHistoryFragment pHCalibrationHistoryFrag;

	public static CalibertionHistoryFragment newInstance() {
		// if (detailsFragment == null)
		pHCalibrationHistoryFrag = new CalibertionHistoryFragment();
		return pHCalibrationHistoryFrag;
	}

	public static CalibertionHistoryFragment getInstance() {

		return pHCalibrationHistoryFrag;
	}

	public String mDeviceAddress;

	public void setDeviceAddress(String deviceAddress) {
		this.mDeviceAddress = deviceAddress;
	}

	private ListView ch_listview;
	private TextView ch_back_text;

	DataBaseHelper db;

	PhCalibrateHistoryAdapter adapter;

	ArrayList<PhCalibrationHistoryData> historyItems = new ArrayList<PhCalibrationHistoryData>();

	private AddClickListener addClickListener;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View view = inflater.inflate(R.layout.calibration_history, container,
				false);

		ch_listview = (ListView) view.findViewById(R.id.ch_listview);
		ch_back_text = (TextView) view.findViewById(R.id.ch_back_text);

		return view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		historyItems = getValuespHCalibration();
		adapter = new PhCalibrateHistoryAdapter(getActivity(),
				R.layout.calibration_history_list_item, historyItems);
		ch_listview.setAdapter(adapter);

		ch_listview
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						// TODO Auto-generated method stub
						addClickListener
								.onCalibrationHistoryItemClick(historyItems
										.get(position));
					}

				});

		ch_listview.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub

				PhCalibrationHistoryData pHCalibrationHistorydata = (PhCalibrationHistoryData) adapter
						.getItem(position);

				final String mId = pHCalibrationHistorydata.getmId();

				if ((pHCalibrationHistorydata.getNo_values().equalsIgnoreCase(
						"") || pHCalibrationHistorydata.getNo_values()
						.equalsIgnoreCase("0"))) {

					AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
							getActivity());

					// set title
					alertDialogBuilder.setTitle("Delete");

					// set dialog message
					alertDialogBuilder
							.setMessage("Are you sure you want to delete it?")

							.setCancelable(false)
							.setPositiveButton("Yes",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											// if this button is
											// clicked,
											// close
											// current activity

											deleteCalibrationHistory(mId);
											notifyDataChanged();
										}
									})
							.setNegativeButton("No",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog, int id) {
											// if this button is
											// clicked,
											// just close
											// the dialog box and do
											// nothing
											dialog.cancel();
										}
									});

					// create alert dialog
					AlertDialog alertDialog = alertDialogBuilder.create();

					// show it
					alertDialog.show();
				}

				return true;
			}
		});
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (activity instanceof AddClickListener) {
			addClickListener = (AddClickListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implement AddClickListener");
		}
	}

	/**** get values for pH calibration ****/
	public ArrayList<PhCalibrationHistoryData> getValuespHCalibration() {
		db = new DataBaseHelper(getActivity());
		db.open();
		ArrayList<PhCalibrationHistoryData> array = new ArrayList<PhCalibrationHistoryData>();

		Cursor dataSetValues = db
				.getCalibrationHistoryForDeviceAddress(mDeviceAddress);
		if (dataSetValues.moveToFirst()) {
			do {

				String date = dataSetValues.getString(0);
				String time = dataSetValues.getString(1);
				String slope = dataSetValues.getString(2);
				String intercept = dataSetValues.getString(3);
				String ph4 = dataSetValues.getString(4);
				String ph7 = dataSetValues.getString(5);

				String ph10 = dataSetValues.getString(6);
				String no_values = dataSetValues.getString(7);
				String mID = dataSetValues.getString(8);

				PhCalibrationHistoryData data = new PhCalibrationHistoryData();
				data.setDate(date);
				data.setTime(time);
				data.setSlope(slope);
				data.setIntercept(intercept);
				data.setPh4(ph4);
				data.setPh7(ph7);
				data.setPh10(ph10);
				data.setNo_values(no_values);
				data.setmId(mID);

				array.add(data);

			} while (dataSetValues.moveToNext());
		}
		dataSetValues.close();
		db.close();
		return array;
	}

	/*** delete calibration history ***/
	private void deleteCalibrationHistory(String mId) {
		db = new DataBaseHelper(getActivity());
		db.open();
		db.deleteCalibrationHistoryRow(mId);
		db.close();
	}

	public void notifyDataChanged() {
		historyItems.clear();
		historyItems.addAll(getValuespHCalibration());
		adapter.notifyDataSetChanged();
	}
}
