package com.dataworks;

import java.util.ArrayList;
import java.util.Collections;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.dataworks.adapter.ValuesAdapter;
import com.dataworks.data.DataSet;
import com.dataworks.data.PHSensor;
import com.dataworks.data.SensorTag;
import com.dataworks.data.Values;
import com.dataworks.database.DataBaseHelper;
import com.dataworks.database.DataBaseSingleTon;
import com.dataworks.util.AddClickListener;

public class ValuesFragment extends Fragment {

	private static ValuesFragment valuesFragment;

	public MainActivity mainActivity;

	public DataSet dataset;

	public static ValuesFragment getInstance() {

		return valuesFragment;
	}

	String name, id;

	public ValuesFragment(String name, String id, DataSet dataset,
			MainActivity parent) {
		valuesFragment = this;

		this.name = name;
		this.id = id;
		this.dataset = dataset;
		this.mainActivity = parent;
	}

	int number = 0;

	private TextView cancel_values, title_text;
	private ListView valuesListView;
	DataBaseHelper db;
	private AddClickListener mClickListener;

	private ArrayList<Values> valuesArrayList = new ArrayList<Values>();
	private ValuesAdapter adapter;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View valuesFragment = inflater.inflate(R.layout.fragment_values,
				container, false);

		DataBaseSingleTon.sContext = getActivity();

		db = DataBaseSingleTon.getDB();

		cancel_values = (TextView) valuesFragment
				.findViewById(R.id.cancel_values);
		valuesListView = (ListView) valuesFragment
				.findViewById(R.id.valuesListView);
		title_text = (TextView) valuesFragment.findViewById(R.id.title_text);

		return valuesFragment;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		if (!name.equalsIgnoreCase("")) {
			title_text.setText(name);

		} else {
			title_text.setText("Values");
		}

		numberOfValues(id);

		valuesArrayList = dataset.getValueList();

		Collections.sort(valuesArrayList);

		loadValuesListView();

		onCancelClick();

		valuesListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Values values = (Values) adapter.getItem(position);

				mClickListener.onDetailClick(values, "from_values");

			}
		});
		valuesListView
				.setOnItemLongClickListener(new OnItemLongClickListener() {

					@Override
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub

						final Values values = (Values) adapter
								.getItem(position);

						// final String idValue = values.getmId();
						final String idValue = values.getmId();

						final String timeUnique = values.getmTime();

						AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
								getActivity());

						// set title
						alertDialogBuilder.setTitle("Delete");

						// set dialog message
						alertDialogBuilder
								.setMessage(
										"Are you sure you want to delete it?")

								.setCancelable(false)
								.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												// if this button is clicked,
												// close
												// current activity

												deleteDataSet(timeUnique);
												mainActivity.curDataSet
														.deleteValuePoint(values);

												notifyDataSetChanged();
											}
										})
								.setNegativeButton("No",
										new DialogInterface.OnClickListener() {
											public void onClick(
													DialogInterface dialog,
													int id) {
												// if this button is clicked,
												// just close
												// the dialog box and do nothing
												dialog.cancel();
											}
										});

						// create alert dialog
						AlertDialog alertDialog = alertDialogBuilder.create();

						// show it
						alertDialog.show();

						return true;
					}
				});

	}

	public void notifyDataSetChanged() {

		Log.d("ValuesFragment", "calling notifydatasetchanged");
		if (adapter != null) {

			valuesArrayList = dataset.getValueList();
			Collections.sort(valuesArrayList);
			adapter.notifyDataSetChanged();
		}

	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		if (activity instanceof AddClickListener) {
			mClickListener = (AddClickListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implement BluetoothClickListener");
		}
	}

	private void onCancelClick() {
		cancel_values.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();

			}
		});
	}

	public void deleteDataSet(String timeUnique) {

		db.open();
		db.deleteValues(timeUnique);
		db.close();
	}

	public int numberOfValues(String id) {

		db.open();
		number = db.ValuesCount(id);
		db.close();

		return number;
	}

	/**** get values for dataset ***/
	public ArrayList<Values> getValuesForDataSet() {

		db.open();
		ArrayList<Values> array = new ArrayList<Values>();

		ArrayList<SensorTag> sensorsList = null;
		ArrayList<PHSensor> pHSensorsList = null;

		Cursor dataSetValues = db.getDPValuesForDataSet(id);
		if (dataSetValues.moveToFirst()) {
			do {

				sensorsList = new ArrayList<SensorTag>();
				pHSensorsList = new ArrayList<PHSensor>();

				String mDataSetId = dataSetValues.getString(0);
				String time = dataSetValues.getString(1);
				String data_point_name = dataSetValues.getString(2);
				String data_point_notes = dataSetValues.getString(3);
				String data_point_location = dataSetValues.getString(4);
				String mId = dataSetValues.getString(5);

				Values values = new Values();
				values.setmDataSetId(mDataSetId);
				values.setmTime(time);
				values.setmName(data_point_name);
				values.setmNotes(data_point_notes);
				values.setmLocation(data_point_location);
				values.setmId(mId);
				Cursor data = db.getValuesForDataPoint(time);
				if (data.moveToFirst()) {
					do {
						String deviceAddress = data.getString(0);
						String deviceName = data.getString(1);

						if (deviceName.equalsIgnoreCase("SensorTag")) {
							String ir_temp = data.getString(2);
							String object_temp = data.getString(3);
							String humidity = data.getString(4);
							String pressure = data.getString(5);
							String mag_x = data.getString(6);
							String mag_y = data.getString(7);
							String mag_z = data.getString(8);

							String acc_x = data.getString(9);
							String acc_y = data.getString(10);
							String acc_z = data.getString(11);
							String gyro_x = data.getString(12);
							String gyro_y = data.getString(13);
							String gyro_z = data.getString(14);

							SensorTag sensorTag = new SensorTag();
							sensorTag.setaTempValue(ir_temp);
							sensorTag.setoTempValue(object_temp);
							sensorTag.setHumidity(humidity);
							sensorTag.setBaroValue(pressure);
							sensorTag.setMagnetometerValue_x(mag_x);
							sensorTag.setMagnetometerValue_y(mag_y);
							sensorTag.setMagnetometerValue_z(mag_z);
							sensorTag.setAccelerometerValue_x(acc_x);
							sensorTag.setAccelerometerValue_y(acc_y);
							sensorTag.setAccelerometerValue_z(acc_z);
							sensorTag.setGyroMeterValue_x(gyro_x);
							sensorTag.setGyroMeterValue_y(gyro_y);
							sensorTag.setGyroMeterValue_z(gyro_z);

							sensorsList.add(sensorTag);

							values.setSensorTagsList(sensorsList);
						} else if (deviceName.equalsIgnoreCase("pH-Meter")) {

							String pH_value = data.getString(15);
							String pH_temp = data.getString(16);

							PHSensor pHsensor = new PHSensor();
							pHsensor.setpHValue(pH_value);
							pHsensor.setTemp_value(pH_temp);

							pHSensorsList.add(pHsensor);

							values.setpHSensorsList(pHSensorsList);
						}

					} while (data.moveToNext());
				}

				data.close();

				array.add(values);

			} while (dataSetValues.moveToNext());
		}
		dataSetValues.close();
		db.close();
		return array;

	}

	public void loadValuesListView() {
		adapter = new ValuesAdapter(getActivity(),
				R.layout.values_list_iem_inflate, valuesArrayList);

		valuesListView.setAdapter(adapter);
	}

}
