package com.dataworks.database;

import com.dataworks.util.Constants;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper {

	// table for clients

	private int mOpenCounter;

	public static final String _ID = "_id";
	public static final String DATASET_NAME = "dataset_name";

	public static final String DATASET_NOTES = "dataset_notes";
	public static final String DATASET_DATE = "dataset_date";
	public static final String DATASET_TIME = "dataset_time";
	public static final String DATASET_LOCATION = "dataset_location";
	public static final String DATE_TIME_DATASET = "date_time_dataset";

	public static final String DATASET_TABLE = "datasets";

	// table to store pH calibration history values
	public static final String PH_CALBRAT_ID = "ph_calibrat_id";

	public static final String PH_DEVICE_ADDRESS = "ph_device_address";
	public static final String PH_CALIBRATE_DATE = "ph_calibrate_date";
	public static final String PH_CALIBRATE_TIME = "ph_calibrate_time";
	public static final String PH_CALIBRATE_SLOPE = "ph_calibrate_slope";
	public static final String PH_CALIBRATE_INTERCEPT = "ph_calibrate_intercept";
	public static final String PH_CALIBRATE_4 = "ph_calibrate_4";
	public static final String PH_CALIBRATE_7 = "ph_calibrate_7";
	public static final String PH_CALIBRATE_10 = "ph_calibrate_10";
	public static final String PH_CALIBRATE_NO_VALUES = "ph_calibrate_no_values";

	public static final String PH_CALIBRATE_HISTORY = "ph_calibrate_history";

	public static final String E_ID = "e_id";
	public static final String E_D_NAME = "e_d_name";
	public static final String E_D_NOTES = "e_d_notes";
	public static final String E_LOCATION = "e_location";
	public static final String E_DATE_TIME = "e_date_time";
	public static final String E_TEMP = "e_a_temp";
	public static final String E_PRESSURE = "e_pressure";
	public static final String E_MAG_X = "e_mag_x";
	public static final String E_MAG_Y = "e_mag_y";
	public static final String E_MAG_Z = "e_mag_z";
	public static final String E_O_TEMP = "e_o_temp";
	public static final String E_HUM = "e_hum";
	public static final String E_GYRO_X = "e_gyro_x";
	public static final String E_GYRO_Y = "e_gyro_y";
	public static final String E_GYRO_Z = "e_gyro_z";
	public static final String E_ACC_X = "e_acc_x";
	public static final String E_ACC_Y = "e_acc_y";
	public static final String E_ACC_Z = "e_acc_z";
	public static final String E_TEMP_PH = "e_temp_ph";
	public static final String E_PH = "e_ph";
	public static final String E_MV = "e_mv";

	public static final String EXPORT_VALUES = "export_values";

	/****
	 * Table for device Information for particular device based on address
	 */
	public static final String ID = "id";
	public static final String CUSTOM_NAME = "custom_name";
	public static final String FORWARD_REVISION = "forward_revision";
	public static final String MANUFACTURER_NAME = "manufacturer_name";
	public static final String HARDWARE_REVISION = "hardware_revision";
	public static final String SERIAL_NUMBER = "serial_number";
	public static final String MODEL_NUMBER = "model_number";
	public static final String BATTERY_LEVEL = "battery_level";
	public static final String DEVICE_ADDRESS = "device_address";
	public static final String DEVICE_NAME = "device_name";
	public static final String RSSI = "rssi";
	public static final String DEVICE_TYPE = "device_type";
	public static final String PH_VALUE_MV = "ph_value_mv";

	public static final String DEVICE_INFO = "device_info";

	/*****
	 * Table for caibration for sensorTag
	 */
	public static final String CALIBRATE_ID = "calibrate_id";
	public static final String CALIBRATE_MAG = "calibrate_mag";
	public static final String CALIBRATE_GYRO = "calibrate_gyro";
	public static final String IS_CALIBRATED_MAG = "is_calibrated_mag";
	public static final String IS_CALIBRATED_GYRO = "is_calibrated_gyro";
	public static final String CALIBRATE_DEVICE_ADDRESS = "calibrate_device_address";

	public static final String SENSORTAG_CALIBRATION = "sensorTag_calibration";

	/**** Table for calibration pH ****/
	public static final String PH_CALIBRATION_ID = "ph_calibration_id";
	public static final String PH_CALIB_DEVICE_ADDRESS = "ph_calib_device_address";
	public static final String SLOPE = "slope";
	public static final String INTERCEPT = "intercept";
	public static final String TEMPERATURE = "temperature";

	public static final String PH_CALIBRATION = "ph_calibration";

	/***
	 * Table for recording values
	 */

	public static final String RECORDING_ID = "recording_id";
	public static final String RECORDING_TIME = "recording_time";
	public static final String RECORDING_DATASET_ID = "recording_dataset_id";
	public static final String RECORDING_DATAPOINT_NAME = "recording_datapoint_name";
	public static final String RECORDING_DATAPOINT_NOTES = "recording_datapoint_notes";
	public static final String RECORDING_DATAPOINT_LOCATION = "recording_datapoint_location";

	public static final String RECORDING = "recording";

	/****
	 * Table for recording values for each sensorTag
	 */
	public static final String RECORDING_VALUES_ID = "recording_values_id";
	public static final String RECORD_VALUES_TIME = "record_values_time";
	public static final String RECORD_VALUES_DEVICE_ADDRESS = "record_values_device_address";
	public static final String RECORD_VALUES_DEVICE_NAME = "record_values_device_name";
	public static final String RECORD_VALUES_IR_AMB_TEMP = "record_values_amb_temp";
	public static final String RECORD_VALUES_IR_OBJ_TEMP = "record_values_obj_temp";
	public static final String RECORD_VALUES_HUMIDITY = "record_values_humidity";
	public static final String RECORD_VALUES_PRESSURE = "record_values_pressure";
	public static final String RECORD_VALUES_MAG_X = "record_values_mag_x";
	public static final String RECORD_VALUES_MAG_Y = "record_values_mag_y";
	public static final String RECORD_VALUES_MAG_Z = "record_values_mag_z";
	public static final String RECORD_VALUES_ACC_X = "record_values_acc_x";
	public static final String RECORD_VALUES_ACC_Y = "record_values_acc_y";
	public static final String RECORD_VALUES_ACC_Z = "record_values_acc_z";
	public static final String RECORD_VALUES_GYRO_X = "record_values_gyro_x";
	public static final String RECORD_VALUES_GYRO_Y = "record_values_gyro_y";
	public static final String RECORD_VALUES_GYRO_Z = "record_values_gyro_z";

	public static final String RECORD_VALUES_PH_VALUE = "record_values_ph_value";
	public static final String RECORD_VALUES_PH_TEMP = "record_values_ph_temp";
	public static final String RECORD_VALUES_PH_MV = "record_values_ph_mv";
	public static final String RECORDING_VALUES_DATASET_ID = "recording_values_dataset_id";

	public static final String RECORDING_VALUES = "recording_values";

	public static final String PH_ALARM_ID = "ph_alarm_id";
	public static final String PH_ALARM_DEVICE_ADDRESS = "ph_alarm_device_address";

	public static final String PH_HIGH_ENABLED = "ph_high_enabled";
	public static final String PH_LOW_ENABLED = "ph_low_enabled";
	public static final String PH_HIGH_VALUE = "ph_high_value";
	public static final String PH_LOW_VALUE = "ph_low_value";
	public static final String IS_BEEP_PLAYING = "is_beep_playing";
	public static final String IS_ALARM_STOPPED = "is_alarm_stopped";

	public static final String PH_ALARM = "ph_alarm";

	// ******* database name *********//
	private static final String DATABASE_NAME = "dataworks";

	// ******* database version *********//
	private static final int DATABASE_VERSION = 512;

	private static final String CREATE_DATASETS = "create table datasets(_id integer primary key autoincrement , "
			+ "dataset_name text not null,"
			+ "dataset_notes text not null,"
			+ "dataset_date text not null,"
			+ "dataset_time text not null,"
			+ "date_time_dataset long," + "dataset_location text not null);";

	private static final String CREATE_PH_CALIBRATE_HISTORY = "create table ph_calibrate_history(ph_calibrat_id integer primary key autoincrement , "
			+ "ph_device_address text not null,"
			+ "ph_calibrate_date text not null,"
			+ "ph_calibrate_time text not null,"
			+ "ph_calibrate_slope text not null,"
			+ "ph_calibrate_intercept text not null,"
			+ "ph_calibrate_4 text not null,"
			+ "ph_calibrate_7 text not null,"
			+ "ph_calibrate_10 text not null,"

			+ "ph_calibrate_no_values text not null);";

	private static final String CREATE_EXPORT_VALUES = "create table export_values(e_id integer primary key autoincrement , "
			+ "e_d_name text not null,"
			+ "e_d_notes text not null,"
			+ "e_location text not null,"
			+ "e_date_time text not null,"
			+ "e_a_temp text not null,"
			+ "e_pressure text not null,"
			+ "e_mag_x text not null,"
			+ "e_mag_y text not null,"
			+ "e_mag_z text not null,"
			+ "e_o_temp text not null,"
			+ "e_hum text not null,"
			+ "e_gyro_x text not null,"
			+ "e_gyro_y text not null,"
			+ "e_gyro_z text not null,"
			+ "e_acc_x text not null,"
			+ "e_acc_y text not null,"
			+ "e_acc_z text not null,"

			+ "e_temp_ph text not null,"
			+ "e_ph text not null,"
			+ "e_mv text not null);";

	private static final String CREATE_DEVICE_INFO = "create table device_info(id integer primary key autoincrement , "
			+ "custom_name text not null,"
			+ "forward_revision text not null,"
			+ "manufacturer_name text not null,"
			+ "hardware_revision text not null,"
			+ "serial_number text not null,"
			+ "model_number text not null,"
			+ "battery_level text not null,"
			+ "device_address text not null,"
			+ "device_name text not null,"
			+ "rssi text not null,"
			+ "device_type text not null," + "ph_value_mv text not null);";

	private static final String CREATE_SENSORTAG_CALIBRATION = "create table sensorTag_calibration(calibrate_id integer primary key autoincrement , "
			+ "calibrate_mag text not null,"
			+ "calibrate_gyro text not null,"
			+ "is_calibrated_mag text not null,"
			+ "is_calibrated_gyro text not null,"

			+ "calibrate_device_address text not null);";

	private static final String CREATE_PH_CALIBRATION = "create table ph_calibration(ph_calibration_id integer primary key autoincrement , "
			+ "ph_calib_device_address text not null,"
			+ "slope text not null,"
			+ "intercept text not null,"

			+ "temperature text not null);";

	private static final String CREATE_RECORDING_VALUES = "create table recording_values(recording_values_id integer primary key autoincrement , "
			+ "record_values_time text not null,"
			+ "record_values_device_address text not null,"
			+ "record_values_device_name text not null,"
			+ "record_values_amb_temp text not null,"
			+ "record_values_obj_temp text not null,"
			+ "record_values_humidity text not null,"
			+ "record_values_pressure text not null,"
			+ "record_values_mag_x text not null,"
			+ "record_values_mag_y text not null,"
			+ "record_values_mag_z text not null,"
			+ "record_values_acc_x text not null,"
			+ "record_values_acc_y text not null,"
			+ "record_values_acc_z text not null,"
			+ "record_values_gyro_x text not null,"
			+ "record_values_gyro_y text not null,"
			+ "record_values_gyro_z text not null,"

			+ "record_values_ph_value text not null,"

			+ "record_values_ph_temp text not null,"
			+ "record_values_ph_mv text not null,"

			+ "recording_values_dataset_id text not null);";

	private static final String CREATE_RECORDING = "create table recording(recording_id integer primary key autoincrement , "
			+ "recording_time text not null,"
			+ "recording_dataset_id text not null,"
			+ "recording_datapoint_name text not null,"
			+ "recording_datapoint_notes text not null,"
			+ "recording_datapoint_location text not null);";

	private static final String CREATE_PH_ALARM = "create table ph_alarm(ph_alarm_id integer primary key autoincrement , "
			+ "ph_alarm_device_address text not null,"
			+ "ph_high_enabled text not null,"
			+ "ph_low_enabled text not null,"
			+ "ph_high_value text not null,"
			+ "ph_low_value text not null,"
			+ "is_beep_playing text not null,"
			+ "is_alarm_stopped text not null);";

	private final Context context;

	private DBhelper DBHelper;
	private SQLiteDatabase db;

	public DataBaseHelper(Context ctx) {
		this.context = ctx;
		DBHelper = new DBhelper(context);

	}

	public boolean isDbOpen() {
		return db.isOpen();
	}

	private static class DBhelper extends SQLiteOpenHelper {
		DBhelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_DATASETS);
			// db.execSQL(CREATE_VALUES);
			db.execSQL(CREATE_EXPORT_VALUES);
			db.execSQL(CREATE_PH_CALIBRATE_HISTORY);
			db.execSQL(CREATE_DEVICE_INFO);
			db.execSQL(CREATE_SENSORTAG_CALIBRATION);
			db.execSQL(CREATE_PH_CALIBRATION);
			db.execSQL(CREATE_RECORDING);
			db.execSQL(CREATE_RECORDING_VALUES);
			db.execSQL(CREATE_PH_ALARM);

		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// Log.w(TAG, "Upgrading database from version " + oldVersion+
			// " to "+ newVersion + ", which will destroy all old data");
			db.execSQL("DROP TABLE IF EXISTS datasets");
			// db.execSQL("DROP TABLE IF EXISTS values_ds");
			db.execSQL("DROP TABLE IF EXISTS export_values");
			db.execSQL("DROP TABLE IF EXISTS ph_calibrate_history");
			db.execSQL("DROP TABLE IF EXISTS device_info");
			db.execSQL("DROP TABLE IF EXISTS sensorTag_calibration");
			db.execSQL("DROP TABLE IF EXISTS ph_calibration");
			db.execSQL("DROP TABLE IF EXISTS recording");
			db.execSQL("DROP TABLE IF EXISTS recording_values");
			db.execSQL("DROP TABLE IF EXISTS ph_alarm");

			onCreate(db);
		}
	}

	// ---opens the database---
	public synchronized DataBaseHelper open() throws SQLException {

		mOpenCounter++;
		if (mOpenCounter == 1) {
			// Opening new database
			db = DBHelper.getWritableDatabase();
			// System.out.println(db.getPath());
		}

		return this;
	}

	// ---closes the database---
	public synchronized void close() {
		mOpenCounter--;
		if (mOpenCounter == 0) {
			// Closing database
			DBHelper.close();

		}

	}

	/********** DATASETS table database methods ****************/
	// ----insert data into the datasets table----
	public long insertDataSets(String dataset_name, String dataset_notes,
			String dataset_date, String dataset_time, String dataset_location,
			long dateTime) {
		ContentValues initialValues = new ContentValues();

		initialValues.put(DATASET_NAME, dataset_name);
		initialValues.put(DATASET_NOTES, dataset_notes);
		initialValues.put(DATASET_DATE, dataset_date);
		initialValues.put(DATASET_TIME, dataset_time);
		initialValues.put(DATE_TIME_DATASET, dateTime);
		initialValues.put(DATASET_LOCATION, dataset_location);

		return db.insert(DATASET_TABLE, null, initialValues);
	}

	// ---retrieves all datasets from datasets table--

	public Cursor getDataSets() throws SQLException {

		return db.query(DATASET_TABLE, new String[] { DATASET_NAME,
				DATASET_NOTES, DATASET_DATE, DATASET_TIME, DATASET_LOCATION,
				_ID, DATE_TIME_DATASET }, null, null, null, null,
				DATE_TIME_DATASET + " DESC");

	}

	// ---retrieves all datasets from datasets table--

	public Cursor getDataSetsForId(String id) throws SQLException {

		return db.query(DATASET_TABLE, new String[] { DATASET_NAME,
				DATASET_NOTES, DATASET_DATE, DATASET_TIME, DATASET_LOCATION,
				_ID, DATE_TIME_DATASET }, _ID + "=" + id, null, null, null, _ID
				+ " DESC");

	}

	// -----delete dataset for id------
	public boolean deleteDaset(String rowId) {
		return db.delete(DATASET_TABLE, _ID + "=" + rowId, null) > 0;
	}

	// ----update client details for id-----
	public void updateDataSetsNameAndNotes(String dataSetId, String mName,
			String mNotes) {

		ContentValues initialValues = new ContentValues();
		initialValues.put(DATASET_NAME, mName);
		initialValues.put(DATASET_NOTES, mNotes);

		db.update(DATASET_TABLE, initialValues, _ID + "=" + dataSetId, null);
	}

	// ----update client details for id-----
	public void updateDataSetsTimeAndDate(String dataSetId, long dateAndTime) {

		ContentValues initialValues = new ContentValues();
		// initialValues.put(DATASET_DATE, Date);
		// initialValues.put(DATASET_TIME, Time);
		initialValues.put(DATE_TIME_DATASET, dateAndTime);

		db.update(DATASET_TABLE, initialValues, _ID + "=" + dataSetId, null);
	}

	// /********** Values table database methods ****************/
	// // ----insert data into the values table----
	// public long insertValues(String dataset_id, String v_name, String
	// v_notes,
	// String v_date, String v_time, String v_location, String v_amb_temp,
	// String v_obj_temp, String v_humidity, String v_pressure,
	// String v_mag, String v_acc, String v_gyro, String ph_value,
	// String ph_temp, String ph_mv) {
	// ContentValues initialValues = new ContentValues();
	//
	// initialValues.put(V_DATASET_ID, dataset_id);
	// initialValues.put(V_NAME, v_name);
	// initialValues.put(V_NOTES, v_notes);
	// initialValues.put(V_DATE, v_date);
	// initialValues.put(V_TIME, v_time);
	// initialValues.put(V_LOCATION, v_location);
	// initialValues.put(V_IR_AMB_TEMP, v_amb_temp);
	// initialValues.put(V_IR_OBJ_TEMP, v_obj_temp);
	// initialValues.put(V_HUMIDITY, v_humidity);
	// initialValues.put(V_PRESSURE, v_pressure);
	// initialValues.put(V_MAG, v_mag);
	// initialValues.put(V_ACC, v_acc);
	// initialValues.put(V_GYRO, v_gyro);
	// initialValues.put(V_PH_VALUE, ph_value);
	// initialValues.put(V_PH_TEMP, ph_temp);
	// initialValues.put(V_PH_MV, ph_temp);
	//
	// return db.insert(VALUES_DATASET, null, initialValues);
	// }
	//
	// public Cursor getValuesForDataSet(String rowId) throws SQLException {
	//
	// return db.query(VALUES_DATASET, new String[] { V_DATASET_ID, V_NAME,
	// V_NOTES, V_DATE, V_TIME, V_LOCATION, V_IR_AMB_TEMP,
	// V_IR_OBJ_TEMP, V_HUMIDITY, V_PRESSURE, V_MAG, V_ACC, V_GYRO,
	// VALUES_ID, V_PH_VALUE, V_PH_TEMP, V_PH_MV }, V_DATASET_ID + "="
	// + rowId, null, null, null, VALUES_ID + " DESC");
	//
	// }
	//
	// public Cursor getValuesForDataSetToAddExportTable(String rowId)
	// throws SQLException {
	//
	// return db.query(VALUES_DATASET, new String[] { V_DATASET_ID, V_NAME,
	// V_NOTES, V_DATE, V_TIME, V_LOCATION, V_IR_AMB_TEMP,
	// V_IR_OBJ_TEMP, V_HUMIDITY, V_PRESSURE, V_MAG, V_ACC, V_GYRO,
	// VALUES_ID, V_PH_VALUE, V_PH_TEMP, V_PH_MV }, V_DATASET_ID + "="
	// + rowId, null, null, null, null);
	//
	// }
	//
	// // ----update client details for id-----
	// public void updateNameAndNotesForValue(String valueId, String mName,
	// String mNotes) {
	//
	// ContentValues initialValues = new ContentValues();
	// initialValues.put(V_NAME, mName);
	// initialValues.put(V_NOTES, mNotes);
	//
	// db.update(VALUES_DATASET, initialValues, VALUES_ID + "=" + valueId,
	// null);
	// }
	//
	// // -----delete dataset values for id------
	// public boolean deleteValues(String rowId) {
	// return db.delete(VALUES_DATASET, VALUES_ID + "=" + rowId, null) > 0;
	// }

	//
	/********** Calibration pH table database methods ****************/
	// ----insert data into Calibration pH table----
	public long insertpHCalibration(String deviceAddress, String date,
			String ph_calibrate_slope, String ph_calibrate_intercept,
			String ph_calibrate_4, String ph_calibrate_7,
			String ph_calibrate_10, String ph_calibrate_no_values,
			String ph_calibrate_time) {
		ContentValues initialValues = new ContentValues();

		initialValues.put(PH_DEVICE_ADDRESS, deviceAddress);
		initialValues.put(PH_CALIBRATE_DATE, date);
		initialValues.put(PH_CALIBRATE_TIME, ph_calibrate_time);

		initialValues.put(PH_CALIBRATE_SLOPE, ph_calibrate_slope);
		initialValues.put(PH_CALIBRATE_INTERCEPT, ph_calibrate_intercept);
		initialValues.put(PH_CALIBRATE_4, ph_calibrate_4);
		initialValues.put(PH_CALIBRATE_7, ph_calibrate_7);
		initialValues.put(PH_CALIBRATE_10, ph_calibrate_10);
		initialValues.put(PH_CALIBRATE_NO_VALUES, ph_calibrate_no_values);

		return db.insert(PH_CALIBRATE_HISTORY, null, initialValues);
	}

	/****
	 * get the values from calibration pH table
	 * 
	 */

	public Cursor getLatestCalibrationHistoryID(String deviceAddress)
			throws SQLException {

		return db.query(PH_CALIBRATE_HISTORY, new String[] { PH_CALBRAT_ID },
				PH_DEVICE_ADDRESS + "='" + deviceAddress + "'", null, null,
				null, PH_CALBRAT_ID + " DESC");

	}

	public Cursor getLatestNumberOfValuesforID(String id) throws SQLException {

		return db.query(PH_CALIBRATE_HISTORY,
				new String[] { PH_CALIBRATE_NO_VALUES }, PH_CALBRAT_ID + "="
						+ id + "", null, null, null, PH_CALBRAT_ID + " DESC");

	}

	/****
	 * get the values from calibration pH table
	 * 
	 */

	public Cursor getCalibrationHistoryForDeviceAddress(String deviceAddress)
			throws SQLException {

		return db.query(PH_CALIBRATE_HISTORY, new String[] { PH_CALIBRATE_DATE,
				PH_CALIBRATE_TIME, PH_CALIBRATE_SLOPE, PH_CALIBRATE_INTERCEPT,
				PH_CALIBRATE_4, PH_CALIBRATE_7, PH_CALIBRATE_10,
				PH_CALIBRATE_NO_VALUES, PH_CALBRAT_ID }, PH_DEVICE_ADDRESS
				+ "='" + deviceAddress + "'", null, null, null, PH_CALBRAT_ID
				+ " DESC");

	}

	/****
	 * get the slope from calibration pH table
	 * 
	 */

	public Cursor getSlopeForPhCalib(String mId) throws SQLException {

		return db.query(PH_CALIBRATE_HISTORY,
				new String[] { PH_CALIBRATE_SLOPE }, PH_CALBRAT_ID + "='" + mId
						+ "'", null, null, null, null);

	}

	/****
	 * get the intercept from calibration pH table
	 * 
	 */

	public Cursor getInterceptForPhCalib(String mId) throws SQLException {

		return db.query(PH_CALIBRATE_HISTORY,
				new String[] { PH_CALIBRATE_INTERCEPT }, PH_CALBRAT_ID + "='"
						+ mId + "'", null, null, null, null);

	}

	// ----update no of values recording into calibration history for id-----
	public void updateNoOfValues(String mId, String noOfValues) {

		ContentValues initialValues = new ContentValues();

		initialValues.put(PH_CALIBRATE_NO_VALUES, noOfValues);

		db.update(PH_CALIBRATE_HISTORY, initialValues, PH_CALBRAT_ID + "="
				+ mId, null);
	}

	// -----delete calibration history of ph values for id------
	public boolean deleteCalibrationHistoryRow(String rowId) {
		return db.delete(PH_CALIBRATE_HISTORY, PH_CALBRAT_ID + "=" + rowId,
				null) > 0;
	}

	// insert export table

	public long insertExportValues(String e_d_name, String e_d_notes,
			String e_location, String e_date_time, String e_a_temp,
			String e_pressure, String e_mag_x, String e_mag_y, String e_mag_z,
			String e_o_temp, String e_hum, String e_gyro_x, String e_gyro_y,
			String e_gyro_z, String e_acc_x, String e_acc_y, String e_acc_z,
			String e_temp_ph, String e_ph, String e_mv) {
		ContentValues initialValues = new ContentValues();

		initialValues.put(E_D_NAME, e_d_name);
		initialValues.put(E_D_NOTES, e_d_notes);
		initialValues.put(E_LOCATION, e_location);
		initialValues.put(E_DATE_TIME, e_date_time);
		initialValues.put(E_TEMP, e_a_temp);
		initialValues.put(E_PRESSURE, e_pressure);
		initialValues.put(E_MAG_X, e_mag_x);
		initialValues.put(E_MAG_Y, e_mag_y);
		initialValues.put(E_MAG_Z, e_mag_z);
		initialValues.put(E_O_TEMP, e_o_temp);
		initialValues.put(E_HUM, e_hum);
		initialValues.put(E_GYRO_X, e_gyro_x);
		initialValues.put(E_GYRO_Y, e_gyro_y);
		initialValues.put(E_GYRO_Z, e_gyro_z);
		initialValues.put(E_ACC_X, e_acc_x);
		initialValues.put(E_ACC_Y, e_acc_y);
		initialValues.put(E_ACC_Z, e_acc_z);
		initialValues.put(E_TEMP_PH, e_temp_ph);
		initialValues.put(E_PH, e_ph);
		initialValues.put(E_MV, e_mv);

		return db.insert(EXPORT_VALUES, null, initialValues);
	}

	// delete all rows in exports records table//

	public boolean deleteExportValues() {

		db.delete(EXPORT_VALUES, null, null);
		return true;

	}

	// public static final String DEVICE_INFO = "device_info";
	// insert device info table

	public long insertDeviceInfo(String device_address, String device_name,
			String custom_name, String rssi, String forward_revision,
			String manufacturer_name, String hardware_revision,
			String serial_number, String model_number, String battery_level,
			String deviceType, String ph_value_mv) {
		ContentValues initialValues = new ContentValues();

		initialValues.put(DEVICE_ADDRESS, device_address);
		initialValues.put(DEVICE_NAME, device_name);
		initialValues.put(CUSTOM_NAME, custom_name);
		initialValues.put(RSSI, rssi);

		initialValues.put(FORWARD_REVISION, forward_revision);
		initialValues.put(MANUFACTURER_NAME, manufacturer_name);
		initialValues.put(HARDWARE_REVISION, hardware_revision);
		initialValues.put(SERIAL_NUMBER, serial_number);
		initialValues.put(MODEL_NUMBER, model_number);
		initialValues.put(BATTERY_LEVEL, battery_level);
		initialValues.put(DEVICE_TYPE, deviceType);
		initialValues.put(PH_VALUE_MV, ph_value_mv);

		return db.insert(DEVICE_INFO, null, initialValues);
	}

	// ----update device info device address-----
	public void updateDeviceInfo(String deviceAddress, String tag,
			String device_info) {

		ContentValues initialValues = new ContentValues();

		if (tag.equalsIgnoreCase(Constants.FORWARD_REVISION_TAG)) {

			initialValues.put(FORWARD_REVISION, device_info);

		} else if (tag.equalsIgnoreCase(Constants.MANUFACTURER_NAME_TAG)) {
			initialValues.put(MANUFACTURER_NAME, device_info);
		} else if (tag.equalsIgnoreCase(Constants.HARDWARE_REVISION_TAG)) {
			initialValues.put(HARDWARE_REVISION, device_info);
		} else if (tag.equalsIgnoreCase(Constants.SERIAL_NUMBER_TAG)) {
			initialValues.put(SERIAL_NUMBER, device_info);
		} else if (tag.equalsIgnoreCase(Constants.MODEL_NUMBER_TAG)) {
			initialValues.put(MODEL_NUMBER, device_info);
		} else if (tag.equalsIgnoreCase(Constants.BATTERY_LEVEL_TAG)) {
			initialValues.put(BATTERY_LEVEL, device_info);
		}

		else if (tag.equalsIgnoreCase(Constants.RSSI_TAG)) {
			initialValues.put(RSSI, device_info);
		} else if (tag.equalsIgnoreCase(Constants.DEVICE_CUSTOM_NAME)) {
			initialValues.put(CUSTOM_NAME, device_info);
		} else if (tag.equalsIgnoreCase(Constants.PH_MV_SHOW_TAG)) {
			initialValues.put(PH_VALUE_MV, device_info);
		}

		db.update(DEVICE_INFO, initialValues, DEVICE_ADDRESS + "='"
				+ deviceAddress + "'", null);
	}

	/****
	 * check whether device exists in table or not.
	 */

	public boolean isdeviceExists(String deviceAddress) {

		String Query = "Select * from " + DEVICE_INFO + " where "
				+ DEVICE_ADDRESS + " = '" + deviceAddress + "'";
		Cursor cursor = db.rawQuery(Query, null);
		if (cursor.getCount() <= 0) {
			return false;
		}
		return true;
	}

	/***
	 * 
	 * @return
	 * @throws SQLException
	 *             get device info for the device address
	 */

	public Cursor getDeviceInfoAddress(String deviceAddress)
			throws SQLException {

		return db.query(DEVICE_INFO, new String[] { CUSTOM_NAME, RSSI,
				FORWARD_REVISION, MANUFACTURER_NAME, HARDWARE_REVISION,
				SERIAL_NUMBER, MODEL_NUMBER, BATTERY_LEVEL }, DEVICE_ADDRESS
				+ " = '" + deviceAddress + "'", null, null, null, null);

	}

	/***
	 * 
	 * @return
	 * @throws SQLException
	 *             get device info for the device address
	 */

	public Cursor getDeviceInfo(String deviceAddress) throws SQLException {

		return db.query(DEVICE_INFO, new String[] { CUSTOM_NAME, RSSI,
				FORWARD_REVISION, MANUFACTURER_NAME, HARDWARE_REVISION,
				SERIAL_NUMBER, MODEL_NUMBER, BATTERY_LEVEL, DEVICE_TYPE },
				DEVICE_ADDRESS + " = '" + deviceAddress + "'", null, null,
				null, null);

	}

	public boolean getmV(String deviceAddress) throws SQLException {

		Cursor cursor = db.query(DEVICE_INFO, new String[] { PH_VALUE_MV },
				DEVICE_ADDRESS + " = '" + deviceAddress + "'", null, null,
				null, null);
		boolean mV_type = false;
		if (cursor.moveToFirst()) {
			do {
				String mV = cursor.getString(0);
				if (mV.equalsIgnoreCase("1")) {
					mV_type = true;
				}
			} while (cursor.moveToNext());
		}

		// return db.query(DEVICE_INFO, new String[] { PH_VALUE_MV },
		// DEVICE_ADDRESS + " = '" + deviceAddress + "'", null, null,
		// null, null);

		return mV_type;

	}

	public Cursor getDeviceInfoAddressAndNames() throws SQLException {

		return db.query(DEVICE_INFO,
				new String[] { DEVICE_ADDRESS, DEVICE_TYPE }, null, null, null,
				null, null);

	}

	public Cursor getDeviceTypeUsingDeviceAddress(String deviceAddress)
			throws SQLException {

		return db.query(DEVICE_INFO, new String[] { DEVICE_TYPE },
				DEVICE_ADDRESS + " = '" + deviceAddress + "'", null, null,
				null, null);

	}

	/***
	 * 
	 * @param calibrate_mag
	 * @param calibrate_gyro
	 *            insert calibration details.
	 * @return
	 */
	public long insertSensorTagCalibration(String calibrate_mag,
			String calibrate_gyro, String isCalibratedMag,
			String isCalibratedGyro, String deviceAddress) {
		ContentValues initialValues = new ContentValues();

		initialValues.put(CALIBRATE_MAG, calibrate_mag);
		initialValues.put(CALIBRATE_GYRO, calibrate_gyro);
		initialValues.put(IS_CALIBRATED_MAG, isCalibratedMag);
		initialValues.put(IS_CALIBRATED_GYRO, isCalibratedGyro);
		initialValues.put(CALIBRATE_DEVICE_ADDRESS, deviceAddress);

		return db.insert(SENSORTAG_CALIBRATION, null, initialValues);
	}

	/***
	 * 
	 * @param deviceAddress
	 * @param tag
	 * @param calibration
	 *            update calibration details
	 */

	public void updateCalibrationSensorTag(String deviceAddress, String tag,
			String calibration) {

		ContentValues initialValues = new ContentValues();

		if (tag.equalsIgnoreCase(Constants.CALIBRATE_MAG_TAG)) {

			initialValues.put(CALIBRATE_MAG, calibration);

		} else if (tag.equalsIgnoreCase(Constants.CALIBRATE_GYRO_TAG)) {
			initialValues.put(CALIBRATE_GYRO, calibration);
		}
		if (tag.equalsIgnoreCase(Constants.IS_MAG_CALIBRATED_TAG)) {

			initialValues.put(IS_CALIBRATED_MAG, calibration);

		} else if (tag.equalsIgnoreCase(Constants.IS_GYRO_CALIBRATED_TAG)) {
			initialValues.put(IS_CALIBRATED_GYRO, calibration);
		}

		db.update(SENSORTAG_CALIBRATION, initialValues,
				CALIBRATE_DEVICE_ADDRESS + "='" + deviceAddress + "'", null);
	}

	/****
	 * 
	 * @param deviceAddress
	 * @return
	 * @throws SQLException
	 *             get the sensorTag calibration magnetometer details
	 */

	public Cursor getSensorTagCalibrationMagneto(String deviceAddress)
			throws SQLException {

		return db.query(SENSORTAG_CALIBRATION, new String[] { CALIBRATE_MAG },
				CALIBRATE_DEVICE_ADDRESS + " = '" + deviceAddress + "'", null,
				null, null, null);

	}

	/****
	 * 
	 * @param deviceAddress
	 * @return
	 * @throws SQLException
	 *             get the sensorTag calibration gyroscope details
	 */

	public Cursor getSensorTagCalibrationGyro(String deviceAddress)
			throws SQLException {

		return db.query(SENSORTAG_CALIBRATION, new String[] { CALIBRATE_GYRO },
				CALIBRATE_DEVICE_ADDRESS + " = '" + deviceAddress + "'", null,
				null, null, null);

	}

	/****
	 * 
	 * @param deviceAddress
	 * @return
	 * @throws SQLException
	 *             get the sensorTag calibration gyroscope details
	 */

	public Cursor getSensorTagCalibrationMagDone(String deviceAddress)
			throws SQLException {

		return db.query(SENSORTAG_CALIBRATION,
				new String[] { IS_CALIBRATED_MAG }, CALIBRATE_DEVICE_ADDRESS
						+ " = '" + deviceAddress + "'", null, null, null, null);

	}

	public Cursor getSensorTagCalibrationGyroDone(String deviceAddress)
			throws SQLException {

		return db.query(SENSORTAG_CALIBRATION,
				new String[] { IS_CALIBRATED_GYRO }, CALIBRATE_DEVICE_ADDRESS
						+ " = '" + deviceAddress + "'", null, null, null, null);

	}

	public long insertPHCalibration(String deviceAddress, String slope,
			String intercept, String temperature) {
		ContentValues initialValues = new ContentValues();

		initialValues.put(PH_CALIB_DEVICE_ADDRESS, deviceAddress);
		initialValues.put(SLOPE, slope);
		initialValues.put(INTERCEPT, intercept);
		initialValues.put(TEMPERATURE, temperature);

		return db.insert(PH_CALIBRATION, null, initialValues);
	}

	public Cursor getpHSlope(String deviceAddress) throws SQLException {

		return db.query(PH_CALIBRATION, new String[] { SLOPE },
				PH_CALIB_DEVICE_ADDRESS + " = '" + deviceAddress + "'", null,
				null, null, null);

	}

	public Cursor getpHIntercept(String deviceAddress) throws SQLException {

		return db.query(PH_CALIBRATION, new String[] { INTERCEPT },
				PH_CALIB_DEVICE_ADDRESS + " = '" + deviceAddress + "'", null,
				null, null, null);

	}

	public Cursor getpHTemperature(String deviceAddress) throws SQLException {

		return db.query(PH_CALIBRATION, new String[] { TEMPERATURE },
				PH_CALIB_DEVICE_ADDRESS + " = '" + deviceAddress + "'", null,
				null, null, null);

	}

	public void updatePHCalibration(String deviceAddress, String tag,
			String calibrationType) {

		ContentValues initialValues = new ContentValues();

		if (tag.equalsIgnoreCase(Constants.SLOPE_TAG)) {

			initialValues.put(SLOPE, calibrationType);

		} else if (tag.equalsIgnoreCase(Constants.INTERCEPT_TAG)) {
			initialValues.put(INTERCEPT, calibrationType);
		}
		if (tag.equalsIgnoreCase(Constants.TEMPERATURE_TAG)) {

			initialValues.put(TEMPERATURE, calibrationType);

		}
		db.update(PH_CALIBRATION, initialValues, PH_CALIB_DEVICE_ADDRESS + "='"
				+ deviceAddress + "'", null);
	}

	/****
	 * check whether device exists in table or not.
	 */

	public boolean isPHDeviceExistsinDB(String deviceAddress) {

		String Query = "Select * from " + PH_CALIBRATION + " where "
				+ PH_CALIB_DEVICE_ADDRESS + " = '" + deviceAddress + "'";
		Cursor cursor = db.rawQuery(Query, null);
		if (cursor.getCount() <= 0) {
			return false;
		}
		return true;
	}

	/*****
	 * 
	 * @param recording_dataset_id
	 * @param recording_time
	 * @param recording_datapoint_name
	 * @param recording_datapoint_notes
	 * @param recording_datapoint_location
	 *            insert recording points
	 * @return
	 */

	public long insertRecording(String recording_dataset_id,
			String recording_time, String recording_datapoint_name,
			String recording_datapoint_notes,
			String recording_datapoint_location) {
		ContentValues initialValues = new ContentValues();

		initialValues.put(RECORDING_DATASET_ID, recording_dataset_id);
		initialValues.put(RECORDING_TIME, recording_time);
		initialValues.put(RECORDING_DATAPOINT_NAME, recording_datapoint_name);
		initialValues.put(RECORDING_DATAPOINT_NOTES, recording_datapoint_notes);
		initialValues.put(RECORDING_DATAPOINT_LOCATION,
				recording_datapoint_location);

		return db.insert(RECORDING, null, initialValues);
	}

	/****
	 * 
	 * @param record_values_time
	 * @param record_values_device_address
	 * @param record_values_device_name
	 * @param record_values_amb_temp
	 * @param record_values_obj_temp
	 * @param record_values_humidity
	 * @param record_values_pressure
	 * @param record_values_mag_x
	 * @param record_values_mag_y
	 * @param record_values_mag_z
	 * @param record_values_acc_x
	 * @param record_values_acc_y
	 * @param record_values_acc_z
	 * @param record_values_gyro_x
	 * @param record_values_gyro_y
	 * @param record_values_gyro_z
	 * @param record_values_ph_value
	 * @param record_values_ph_temp
	 * @param record_values_ph_mv
	 *            insert record values for sensors.
	 * @return
	 */

	public long insertRecordingVaues(String record_values_time,
			String record_values_device_address,
			String record_values_device_name, String record_values_amb_temp,
			String record_values_obj_temp, String record_values_humidity,
			String record_values_pressure, String record_values_mag_x,
			String record_values_mag_y, String record_values_mag_z,
			String record_values_acc_x, String record_values_acc_y,
			String record_values_acc_z, String record_values_gyro_x,
			String record_values_gyro_y, String record_values_gyro_z,
			String record_values_ph_value, String record_values_ph_temp,
			String record_values_ph_mv, String recording_values_dataset_id) {
		ContentValues initialValues = new ContentValues();

		initialValues.put(RECORD_VALUES_TIME, record_values_time);
		initialValues.put(RECORD_VALUES_DEVICE_ADDRESS,
				record_values_device_address);
		initialValues.put(RECORD_VALUES_DEVICE_NAME, record_values_device_name);
		initialValues.put(RECORD_VALUES_IR_AMB_TEMP, record_values_amb_temp);
		initialValues.put(RECORD_VALUES_IR_OBJ_TEMP, record_values_obj_temp);
		initialValues.put(RECORD_VALUES_HUMIDITY, record_values_humidity);
		initialValues.put(RECORD_VALUES_PRESSURE, record_values_pressure);
		initialValues.put(RECORD_VALUES_MAG_X, record_values_mag_x);
		initialValues.put(RECORD_VALUES_MAG_Y, record_values_mag_y);
		initialValues.put(RECORD_VALUES_MAG_Z, record_values_mag_z);
		initialValues.put(RECORD_VALUES_ACC_X, record_values_acc_x);
		initialValues.put(RECORD_VALUES_ACC_Y, record_values_acc_y);
		initialValues.put(RECORD_VALUES_ACC_Z, record_values_acc_z);

		initialValues.put(RECORD_VALUES_GYRO_X, record_values_gyro_x);
		initialValues.put(RECORD_VALUES_GYRO_Y, record_values_gyro_y);
		initialValues.put(RECORD_VALUES_GYRO_Z, record_values_gyro_z);
		initialValues.put(RECORD_VALUES_PH_VALUE, record_values_ph_value);
		initialValues.put(RECORD_VALUES_PH_TEMP, record_values_ph_temp);
		initialValues.put(RECORD_VALUES_PH_MV, record_values_ph_mv);
		initialValues.put(RECORDING_VALUES_DATASET_ID,
				recording_values_dataset_id);

		return db.insert(RECORDING_VALUES, null, initialValues);
	}

	/****
	 * 
	 * @param rowId
	 * @return
	 * @throws SQLException
	 *             get Values for dataset Id
	 */
	public Cursor getDPValuesForDataSet(String rowId) throws SQLException {

		return db.query(RECORDING, new String[] { RECORDING_DATASET_ID,
				RECORDING_TIME, RECORDING_DATAPOINT_NAME,
				RECORDING_DATAPOINT_NOTES, RECORDING_DATAPOINT_LOCATION,
				RECORDING_ID }, RECORDING_DATASET_ID + "=" + rowId, null, null,
				null, RECORDING_ID + " DESC");

		// return db.query(RECORDING, new String[] { RECORDING_DATASET_ID,
		// RECORDING_TIME, RECORDING_DATAPOINT_NAME,
		// RECORDING_DATAPOINT_NOTES, RECORDING_DATAPOINT_LOCATION,
		// RECORDING_ID }, RECORDING_DATASET_ID + "=" + rowId, null, null,
		// null, null);

	}

	public int ValuesCount(String rowId) throws SQLException {
		Cursor cursor = db.query(RECORDING, new String[] {
				RECORDING_DATASET_ID, RECORDING_TIME, RECORDING_DATAPOINT_NAME,
				RECORDING_DATAPOINT_NOTES, RECORDING_DATAPOINT_LOCATION,
				RECORDING_ID }, RECORDING_DATASET_ID + "=" + rowId, null, null,
				null, RECORDING_ID + " DESC");

		int count = cursor.getCount();
		return count;

	}

	public boolean deleteValues(String timeUnique) throws SQLException {

		return db.delete(RECORDING, RECORDING_TIME + "=" + timeUnique, null) > 0;

	}

	public Cursor getValuesForDataPoint(String time) throws SQLException {

		return db.query(RECORDING_VALUES, new String[] {
				RECORD_VALUES_DEVICE_ADDRESS, RECORD_VALUES_DEVICE_NAME,
				RECORD_VALUES_IR_AMB_TEMP, RECORD_VALUES_IR_OBJ_TEMP,
				RECORD_VALUES_HUMIDITY, RECORD_VALUES_PRESSURE,
				RECORD_VALUES_MAG_X, RECORD_VALUES_MAG_Y, RECORD_VALUES_MAG_Z,
				RECORD_VALUES_ACC_X, RECORD_VALUES_ACC_Y, RECORD_VALUES_ACC_Z,
				RECORD_VALUES_GYRO_X, RECORD_VALUES_GYRO_Y,
				RECORD_VALUES_GYRO_Z, RECORD_VALUES_PH_VALUE,
				RECORD_VALUES_PH_TEMP, RECORD_VALUES_PH_MV },
				RECORD_VALUES_TIME + "='" + time + "'", null, null, null, null);

	}

	public Cursor getDevicesList(String datasetId) throws SQLException {

		return db.query(RECORDING_VALUES,
				new String[] { RECORD_VALUES_DEVICE_ADDRESS },
				RECORDING_VALUES_DATASET_ID + "=" + datasetId, null, null,
				null, null);

	}

	public void updateNameAndNotesForValue(String valueId, String mName,
			String mNotes) {
		// TODO Auto-generated method stub
		ContentValues initialValues = new ContentValues();

		initialValues.put(RECORDING_DATAPOINT_NAME, mName);

		initialValues.put(RECORDING_DATAPOINT_NOTES, mNotes);

		// db.update(RECORDING, initialValues, RECORDING_ID + "=" + valueId,
		// null);

		db.update(RECORDING, initialValues, RECORDING_TIME + "=" + valueId,
				null);

	}

	public long insertAlarmDetailsForDevice(String ph_alarm_device_address,
			String ph_high_enabled, String ph_low_enabled,
			String ph_high_value, String ph_low_value, String is_beep_playing,
			String is_alarm_stopped) {
		ContentValues initialValues = new ContentValues();

		initialValues.put(PH_ALARM_DEVICE_ADDRESS, ph_alarm_device_address);
		initialValues.put(PH_HIGH_ENABLED, ph_high_enabled);
		initialValues.put(PH_LOW_ENABLED, ph_low_enabled);
		initialValues.put(PH_HIGH_VALUE, ph_high_value);
		initialValues.put(PH_LOW_VALUE, ph_low_value);
		initialValues.put(IS_BEEP_PLAYING, is_beep_playing);
		initialValues.put(IS_ALARM_STOPPED, is_alarm_stopped);

		return db.insert(PH_ALARM, null, initialValues);
	}

	public Cursor getAlarmDetailsForDeviceAddress(String deviceAddress)
			throws SQLException {

		return db.query(PH_ALARM, new String[] { PH_HIGH_ENABLED,
				PH_LOW_ENABLED, PH_HIGH_VALUE, PH_LOW_VALUE, IS_BEEP_PLAYING,
				IS_ALARM_STOPPED }, PH_ALARM_DEVICE_ADDRESS + "='"
				+ deviceAddress + "'", null, null, null, null);

	}

	public void updateAlarmDetailsForDevice(String deviceAddress,
			String ph_high_enabled, String ph_low_enabled,
			String ph_high_value, String ph_low_value, String is_beep_playing,
			String is_alarm_stopped) {
		// TODO Auto-generated method stub
		ContentValues initialValues = new ContentValues();

		initialValues.put(PH_HIGH_ENABLED, ph_high_enabled);

		initialValues.put(PH_LOW_ENABLED, ph_low_enabled);

		initialValues.put(PH_HIGH_VALUE, ph_high_value);

		initialValues.put(PH_LOW_VALUE, ph_low_value);
		initialValues.put(IS_BEEP_PLAYING, is_beep_playing);
		initialValues.put(IS_ALARM_STOPPED, is_alarm_stopped);

		db.update(PH_ALARM, initialValues, PH_ALARM_DEVICE_ADDRESS + "='"
				+ deviceAddress + "'", null);
	}

	public void updateAlarmBeepPlay(String deviceAddress,
			String is_beep_playing, String is_alarm_stopped) {
		// TODO Auto-generated method stub
		ContentValues initialValues = new ContentValues();

		initialValues.put(IS_BEEP_PLAYING, is_beep_playing);
		initialValues.put(IS_ALARM_STOPPED, is_alarm_stopped);

		db.update(PH_ALARM, initialValues, PH_ALARM_DEVICE_ADDRESS + "='"
				+ deviceAddress + "'", null);
	}

	public void updateIsAlarmStopped(String deviceAddress,
			String is_alarm_stopped) {
		// TODO Auto-generated method stub
		ContentValues initialValues = new ContentValues();

		initialValues.put(IS_ALARM_STOPPED, is_alarm_stopped);

		db.update(PH_ALARM, initialValues, PH_ALARM_DEVICE_ADDRESS + "='"
				+ deviceAddress + "'", null);
	}

	public void updateIsBeepPlaying(String deviceAddress, String is_beep_playing) {
		// TODO Auto-generated method stub
		ContentValues initialValues = new ContentValues();

		initialValues.put(IS_BEEP_PLAYING, is_beep_playing);

		db.update(PH_ALARM, initialValues, PH_ALARM_DEVICE_ADDRESS + "='"
				+ deviceAddress + "'", null);
	}

	public boolean isdeviceExistsForAlarm(String deviceAddress) {

		String Query = "Select * from " + PH_ALARM + " where "
				+ PH_ALARM_DEVICE_ADDRESS + " = '" + deviceAddress + "'";
		Cursor cursor = db.rawQuery(Query, null);
		if (cursor.getCount() <= 0) {
			return false;
		}
		return true;
	}

	public boolean deleteAlarmDetails() {

		db.delete(PH_ALARM, null, null);
		return true;

	}

}
