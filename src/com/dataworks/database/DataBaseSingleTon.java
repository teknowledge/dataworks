package com.dataworks.database;

import android.content.Context;

public class DataBaseSingleTon {

	private static DataBaseHelper db;

	public static Context sContext;

	public static synchronized DataBaseHelper getDB() {
		if (db == null)
			db = new DataBaseHelper(sContext);

		return db;
	}

}
