package com.dataworks.database;

import com.dataworks.data.AlarmData;

import android.content.Context;
import android.database.Cursor;

public class DevicesDataBaseManager {

	public DataBaseHelper db;
	public Context mContext;

	public DevicesDataBaseManager(Context context) {
		// TODO Auto-generated constructor stub

		this.mContext = context;

		DataBaseSingleTon.sContext = mContext;

		db = DataBaseSingleTon.getDB();
	}

	/**** to check whether the device exists or not for alarm ***/
	public boolean isDeviceExistsforAlarm(String deviceAddress) {

		boolean isExists;
		db.open();
		isExists = db.isdeviceExistsForAlarm(deviceAddress);
		db.close();

		return isExists;
	}

	/***** insert alarm details for pH device based on the address ****/
	public void insertAlarmDetailsForDeviceAddress(String deviceAddress,
			boolean isHighOn, boolean isLowOn, String highValue,
			String lowValue, boolean isBeepPlaying, boolean isAlarmStarted) {

		String high_enabled;
		String low_enabled;
		String is_beep_playing;
		String is_Alarm_stopped;

		if (isHighOn) {
			high_enabled = "1";

		} else {
			high_enabled = "0";
		}
		if (isLowOn) {
			low_enabled = "1";
		} else {
			low_enabled = "0";
		}
		if (isBeepPlaying) {
			is_beep_playing = "1";
		} else {
			is_beep_playing = "0";
		}
		if (isAlarmStarted) {
			is_Alarm_stopped = "1";
		} else {
			is_Alarm_stopped = "0";
		}

		db.open();
		db.insertAlarmDetailsForDevice(deviceAddress, high_enabled,
				low_enabled, highValue, lowValue, is_beep_playing,
				is_Alarm_stopped);
		db.close();
	}

	/***** update the alarm details for pH device based on the address ****/
	public void updateAlarmDetailsForDeviceAddress(String deviceAddress,
			boolean isHighOn, boolean isLowOn, String highValue,
			String lowValue, boolean isBeepPlaying, boolean isAlarmStarted) {

		String high_enabled;
		String low_enabled;
		String is_beep_playing;
		String is_Alarm_stopped;

		if (isHighOn) {
			high_enabled = "1";

		} else {
			high_enabled = "0";
		}
		if (isLowOn) {
			low_enabled = "1";
		} else {
			low_enabled = "0";
		}
		if (isBeepPlaying) {
			is_beep_playing = "1";
		} else {
			is_beep_playing = "0";
		}
		if (isAlarmStarted) {
			is_Alarm_stopped = "1";
		} else {
			is_Alarm_stopped = "0";
		}

		db.open();
		db.updateAlarmDetailsForDevice(deviceAddress, high_enabled,
				low_enabled, highValue, lowValue, is_beep_playing,
				is_Alarm_stopped);
		db.close();
	}

	/**** get the alaram details for the pH device based on the address ***/
	public AlarmData getAlarmDetailsForDevice(String deviceAddress) {

		AlarmData alarmData = new AlarmData();
		db.open();
		Cursor alarmDetails = db.getAlarmDetailsForDeviceAddress(deviceAddress);
		if (alarmDetails.moveToFirst()) {
			do {

				String isHighAlarmOn = alarmDetails.getString(0);
				String isLowAlarmOn = alarmDetails.getString(1);
				String highValue = alarmDetails.getString(2);
				String lowValue = alarmDetails.getString(3);
				String isBeepPlaying = alarmDetails.getString(4);
				String isAlarmStopped = alarmDetails.getString(5);

				if (isHighAlarmOn.equalsIgnoreCase("1")) {
					alarmData.setHighOn(true);
				} else {
					alarmData.setHighOn(false);
				}

				if (isLowAlarmOn.equalsIgnoreCase("1")) {
					alarmData.setLowOn(true);
				} else {
					alarmData.setLowOn(false);
				}

				alarmData.setHighValue(highValue);
				alarmData.setLowValue(lowValue);

				if (isBeepPlaying.equalsIgnoreCase("1")) {
					alarmData.setBeepPlaying(true);
				} else {
					alarmData.setBeepPlaying(false);
				}

				if (isAlarmStopped.equalsIgnoreCase("1")) {
					alarmData.setAlarmStopped(true);
				} else {
					alarmData.setAlarmStopped(false);
				}

			} while (alarmDetails.moveToNext());
		}
		alarmDetails.close();
		db.close();

		return alarmData;
	}

	/**** update the alarm beep play baesdon started or not ****/
	public void updateAlarmBeepPlay(String deviceAddress,
			boolean isBeepPlaying, boolean isAlarmStarted) {

		String is_beep_playing;
		String is_Alarm_stopped;
		if (isBeepPlaying) {
			is_beep_playing = "1";
		} else {
			is_beep_playing = "0";
		}
		if (isAlarmStarted) {
			is_Alarm_stopped = "1";
		} else {
			is_Alarm_stopped = "0";
		}

		db.open();
		db.updateAlarmBeepPlay(deviceAddress, is_beep_playing, is_Alarm_stopped);
		db.close();
	}

	/***** update whether beep is playing or not ***/
	public void updateisBeepPlaying(String deviceAddress, boolean isBeepPlaying) {

		String is_beep_playing;

		if (isBeepPlaying) {
			is_beep_playing = "1";
		} else {
			is_beep_playing = "0";
		}

		db.open();
		db.updateIsBeepPlaying(deviceAddress, is_beep_playing);
		db.close();
	}

	/**** delete the alarm details ****/
	public void deleteAlarmDetails() {
		db.open();
		db.deleteAlarmDetails();
		db.close();
	}
}
