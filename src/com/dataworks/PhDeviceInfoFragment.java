package com.dataworks;

import com.dataworks.database.DataBaseHelper;
import com.dataworks.R;
import com.dataworks.util.AddClickListener;
import com.dataworks.util.Constants;
import com.dataworks.util.Global;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

public class PhDeviceInfoFragment extends Fragment {

	public static PhDeviceInfoFragment pHdeviceFragment;

	public static PhDeviceInfoFragment newInstance() {
		pHdeviceFragment = new PhDeviceInfoFragment();
		return pHdeviceFragment;
	}

	public static PhDeviceInfoFragment getInstance() {

		return pHdeviceFragment;
	}

	public String mDeviceAddress;

	private TextView backBtn;
	private ToggleButton toggleBtn;
	private TextView doneBtn;
	private EditText device_name;
	private TextView rssi_ph;
	private TextView manufacturer_Name, serial_number, model_number,
			firmware_revision, hardware_revision, battery_level;

	private String deviceCustomName, Rssi, forwardRevision, manuFacturerName,
			hardWareRevision, serialNumber, modelNumber, batteryLevel;

	private DataBaseHelper db;
	private AddClickListener mClickListener;

	public void setmDeviceAddress(String deviceAddress) {
		mDeviceAddress = deviceAddress;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		if (activity instanceof AddClickListener) {
			mClickListener = (AddClickListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implement BluetoothClickListener");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View view = inflater.inflate(R.layout.ph_device_info, container, false);

		backBtn = (TextView) view.findViewById(R.id.backBtn);

		toggleBtn = (ToggleButton) view.findViewById(R.id.toggleBtn);

		doneBtn = (TextView) view.findViewById(R.id.done_info);

		rssi_ph = (TextView) view.findViewById(R.id.rssi_ph);

		manufacturer_Name = (TextView) view
				.findViewById(R.id.manufacturer_Name);

		serial_number = (TextView) view.findViewById(R.id.serial_number);

		model_number = (TextView) view.findViewById(R.id.model_number);

		firmware_revision = (TextView) view
				.findViewById(R.id.firmware_revision);

		hardware_revision = (TextView) view
				.findViewById(R.id.hardware_revision);

		battery_level = (TextView) view.findViewById(R.id.battery_level);

		device_name = (EditText) view.findViewById(R.id.device_name);

		return view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		getDeviceInfoFromDB(mDeviceAddress);

		device_name.setText(deviceCustomName);

		rssi_ph.setText(Rssi);

		manufacturer_Name.setText(manuFacturerName);

		serial_number.setText(serialNumber);

		model_number.setText(modelNumber);

		firmware_revision.setText(forwardRevision);

		hardware_revision.setText(hardWareRevision);

		battery_level.setText(batteryLevel + " %");

		device_name.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				device_name.setCursorVisible(true);
				doneBtn.setVisibility(View.VISIBLE);
				return false;
			}
		});

		device_name.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub
				device_name.setCursorVisible(true);
				doneBtn.setVisibility(View.VISIBLE);
			}
		});

		doneBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String deviceName = device_name.getText().toString();

				updateDeviceCustomName(mDeviceAddress, deviceName);

				doneBtn.setVisibility(View.GONE);

				device_name.setCursorVisible(false);

				try {
					Global.hideSoftKeyboard(getActivity());
				} catch (Exception e) {
					// TODO: handle exception
				}
				mClickListener.onpHDoneClick(mDeviceAddress);
			}
		});

		toggleBtn.setChecked(getmVType(mDeviceAddress));

		toggleBtn.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub

				String mV_type = "0";
				if (isChecked) {
					mV_type = "1";
				}

				db.open();
				db.updateDeviceInfo(mDeviceAddress, Constants.PH_MV_SHOW_TAG,
						mV_type);
				db.close();

			}
		});
		backBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				getActivity().onBackPressed();
			}
		});

	}

	public boolean getmVType(String deviceAddress) {

		db.open();
		boolean mV_type = db.getmV(deviceAddress);
		db.close();

		return mV_type;
	}

	public void getDeviceInfoFromDB(String deviceAddress) {
		db = new DataBaseHelper(getActivity());
		db.open();

		Cursor deviceInfo = db.getDeviceInfoAddress(deviceAddress);

		if (deviceInfo.moveToFirst()) {
			do {

				deviceCustomName = deviceInfo.getString(0);
				Rssi = deviceInfo.getString(1);
				forwardRevision = deviceInfo.getString(2);
				manuFacturerName = deviceInfo.getString(3);
				hardWareRevision = deviceInfo.getString(4);
				serialNumber = deviceInfo.getString(5);
				modelNumber = deviceInfo.getString(6);
				batteryLevel = deviceInfo.getString(7);

			} while (deviceInfo.moveToNext());
		}
		deviceInfo.close();
		db.close();
	}

	public void updateDeviceCustomName(String deviceAddress, String customName) {
		db = new DataBaseHelper(getActivity());
		db.open();
		db.updateDeviceInfo(deviceAddress, Constants.DEVICE_CUSTOM_NAME,
				customName);
		db.close();
	}
}
