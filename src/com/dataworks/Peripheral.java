package com.dataworks;

import java.util.List;
import com.dataworks.bluetooth.service.GattInfo;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class Peripheral {
	Context mContext;

	public BluetoothDevice mBleDevice = null;

	public BluetoothGatt mBluetoothGatt = null;

	public String address = null;
	public String name = null;

	static final String TAG = "BluetoothLeService";

	public final static String ACTION_GATT_CONNECTING = "com.dataworks.bluetooth.service.ACTION_GATT_CONNECTING";
	public final static String ACTION_GATT_CONNECTED = "com.dataworks.bluetooth.service.ACTION_GATT_CONNECTED";
	public final static String ACTION_GATT_DISCONNECTING = "com.dataworks.bluetooth.service.ACTION_GATT_DISCONNECTING";
	public final static String ACTION_GATT_DISCONNECTED = "com.dataworks.bluetooth.service.ACTION_GATT_DISCONNECTED";

	public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.dataworks.bluetooth.service.ACTION_GATT_SERVICES_DISCOVERED";
	public final static String ACTION_DATA_READ = "com.dataworks.bluetooth.service.ACTION_DATA_READ";
	public final static String ACTION_DATA_NOTIFY = "com.dataworks.bluetooth.service.ACTION_DATA_NOTIFY";
	public final static String ACTION_DATA_WRITE = "com.dataworks.bluetooth.service.ACTION_DATA_WRITE";
	public final static String EXTRA_DATA = "com.dataworks.bluetooth.service.EXTRA_DATA";
	public final static String EXTRA_UUID = "com.dataworks.bluetooth.service.EXTRA_UUID";
	public final static String EXTRA_STATUS = "com.dataworks.bluetooth.service.EXTRA_STATUS";
	public final static String EXTRA_ADDRESS = "com.dataworks.bluetooth.service.EXTRA_ADDRESS";

	// BLE

	private BluetoothAdapter mBtAdapter = null;

	private volatile boolean mBusy = false; // Write/read pending response

	private boolean isConnecting;

	public boolean isConnecting() {
		return isConnecting;
	}

	public void setConnecting(boolean isConnecting) {
		this.isConnecting = isConnecting;
	}

	public Peripheral(BluetoothDevice mBleDevice, Context context) {
		mContext = context;
		this.mBleDevice = mBleDevice;
		address = mBleDevice.getAddress();
		name = mBleDevice.getName();
	}

	int counter;

	/**
	 * GATT client callbacks
	 */
	private BluetoothGattCallback mGattCallbacks = new BluetoothGattCallback() {

		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {
			if (mBluetoothGatt == null) {
				// Log.e(TAG, "mBluetoothGatt not created!");
				return;
			}

			BluetoothDevice device = gatt.getDevice();

			String address = device.getAddress();
			// String address = device.getName();

			// Log.d(TAG, "onConnectionStateChange (" + address + ") " +
			// newState +
			// " status: " + status);

			try {
				switch (newState) {
				case BluetoothProfile.STATE_CONNECTED:
					broadcastUpdate(ACTION_GATT_CONNECTED, address, status);
					break;
				case BluetoothProfile.STATE_DISCONNECTED:
					broadcastUpdate(ACTION_GATT_DISCONNECTED, address, status);
					break;
				case BluetoothProfile.STATE_CONNECTING:
					broadcastUpdate(ACTION_GATT_CONNECTING, address, status);
				case BluetoothProfile.STATE_DISCONNECTING:
					broadcastUpdate(ACTION_GATT_DISCONNECTING, address, status);
				default:
					// Log.e(TAG, "New state not processed: " + newState);
					break;
				}
			} catch (NullPointerException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			BluetoothDevice device = gatt.getDevice();
			broadcastUpdate(ACTION_GATT_SERVICES_DISCOVERED,
					device.getAddress(), status);
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic) {
			BluetoothDevice device = gatt.getDevice();
			broadcastUpdate(ACTION_DATA_NOTIFY, characteristic,
					BluetoothGatt.GATT_SUCCESS, device.getAddress());

			counter = counter + 1;
			System.out.println("counter values in callback" + counter);

		}

		@Override
		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			BluetoothDevice device = gatt.getDevice();
			broadcastUpdate(ACTION_DATA_READ, characteristic, status,
					device.getAddress());
		}

		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
			BluetoothDevice device = gatt.getDevice();
			broadcastUpdate(ACTION_DATA_WRITE, characteristic, status,
					device.getAddress());
		}

		@Override
		public void onDescriptorRead(BluetoothGatt gatt,
				BluetoothGattDescriptor descriptor, int status) {
			mBusy = false;
		}

		@Override
		public void onDescriptorWrite(BluetoothGatt gatt,
				BluetoothGattDescriptor descriptor, int status) {
			// Log.i(TAG, "onDescriptorWrite: " +
			// descriptor.getUuid().toString());
			mBusy = false;
		}
	};

	private void broadcastUpdate(final String action, final String address,
			final int status) {
		final Intent intent = new Intent(action);
		intent.putExtra(EXTRA_ADDRESS, address);
		intent.putExtra(EXTRA_STATUS, status);
		mContext.sendBroadcast(intent);
		mBusy = false;
	}

	private void broadcastUpdate(final String action,
			final BluetoothGattCharacteristic characteristic, final int status,
			String address) {
		final Intent intent = new Intent(action);
		intent.putExtra(EXTRA_UUID, characteristic.getUuid().toString());
		intent.putExtra(EXTRA_DATA, characteristic.getValue());
		intent.putExtra(EXTRA_ADDRESS, address);
		intent.putExtra(EXTRA_STATUS, status);
		mContext.sendBroadcast(intent);
		mBusy = false;
	}

	private boolean checkGatt() {

		if (mBluetoothGatt == null) {
			// Log.w(TAG, "BluetoothGatt not initialized");
			return false;
		}

		if (mBusy) {
			// Log.w(TAG, "LeService busy");
			return false;
		}
		return true;

	}

	public boolean connect() {

		// Previously connected device. Try to reconnect.
		if (mBluetoothGatt != null) {
			// Log.d(TAG, "Re-use GATT connection");
			if (mBluetoothGatt.connect()) {

				return true;
			} else {
				// Log.w(TAG, "GATT re-connect failed.");
				return false;
			}
		}
		System.out.println(mBleDevice.getName() + ".......connect method");

		// We want to directly connect to the device, so we are setting the
		// autoConnect parameter to false.
		// Log.d(TAG, "Create a new GATT connection.");
		mBluetoothGatt = mBleDevice
				.connectGatt(mContext, false, mGattCallbacks);

		// Peripheral peripheral=new Peripheral(device,
		// mBluetoothGatt,address);
		//

		return true;
	}

	/**
	 * Disconnects an existing connection or cancel a pending connection. The
	 * disconnection result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 * callback.
	 */
	public void disconnect() {

		if (mBluetoothGatt != null) {

			// Log.w(TAG, "Attempt to disconnect in state: " +
			// connectionState);
			mBluetoothGatt.disconnect();
		}

	}

	//
	// GATT API
	//
	/**
	 * Request a read on a given {@code BluetoothGattCharacteristic}. The read
	 * result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
	 * callback.
	 * 
	 * @param characteristic
	 *            The characteristic to read from.
	 */
	public void readCharacteristic(BluetoothGattCharacteristic characteristic) {
		if (!checkGatt())
			return;
		mBusy = true;
		mBluetoothGatt.readCharacteristic(characteristic);
	}

	/***
	 * write characteristic uuid for ble device
	 * 
	 * @param characteristic
	 * @param b
	 * @return
	 */
	public boolean writeCharacteristic(
			BluetoothGattCharacteristic characteristic, byte b) {
		if (!checkGatt())
			return false;

		byte[] val = new byte[1];
		val[0] = b;
		characteristic.setValue(val);
		characteristic
				.setWriteType(BluetoothGattCharacteristic.WRITE_TYPE_DEFAULT);

		mBusy = true;

		boolean write = mBluetoothGatt.writeCharacteristic(characteristic);
		if (write) {
			Log.d("DataWorks", "writeCharacteristic() - uuid: "
					+ characteristic.getUuid() + "  value " + val[0]);
		}

		return write;
	}

	public boolean writeCharacteristic(
			BluetoothGattCharacteristic characteristic, boolean b) {
		if (!checkGatt())
			return false;

		byte[] val = new byte[1];

		val[0] = (byte) (b ? 1 : 0);
		characteristic.setValue(val);
		mBusy = true;
		return mBluetoothGatt.writeCharacteristic(characteristic);
	}

	public boolean writeCharacteristic(
			BluetoothGattCharacteristic characteristic) {
		if (!checkGatt())
			return false;

		mBusy = true;
		return mBluetoothGatt.writeCharacteristic(characteristic);
	}

	/**
	 * Retrieves the number of GATT services on the connected device. This
	 * should be invoked only after {@code BluetoothGatt#discoverServices()}
	 * completes successfully.
	 * 
	 * @return A {@code integer} number of supported services.
	 */
	public int getNumServices() {
		if (mBluetoothGatt == null)
			return 0;

		return mBluetoothGatt.getServices().size();
	}

	/**
	 * Retrieves a list of supported GATT services on the connected device. This
	 * should be invoked only after {@code BluetoothGatt#discoverServices()}
	 * completes successfully.
	 * 
	 * @return A {@code List} of supported services.
	 */
	public List<BluetoothGattService> getSupportedGattServices() {
		if (mBluetoothGatt == null)
			return null;

		return mBluetoothGatt.getServices();
	}

	/**
	 * Enables or disables notification on a give characteristic.
	 * 
	 * @param characteristic
	 *            Characteristic to act on.
	 * @param enabled
	 *            If true, enable notification. False otherwise.
	 */
	public boolean setCharacteristicNotification(
			BluetoothGattCharacteristic characteristic, boolean enable) {
		if (!checkGatt())
			return false;

		boolean ok = false;

		if (mBluetoothGatt != null) {

			if (mBluetoothGatt.setCharacteristicNotification(characteristic,
					enable)) {

				BluetoothGattDescriptor clientConfig = characteristic
						.getDescriptor(GattInfo.CLIENT_CHARACTERISTIC_CONFIG);
				if (clientConfig != null) {

					if (enable) {
						// Log.i(TAG, "Enable notification: " +
						// characteristic.getUuid().toString());
						ok = clientConfig
								.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
					} else {
						// Log.i(TAG, "Disable notification: " +
						// characteristic.getUuid().toString());
						ok = clientConfig
								.setValue(BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
					}

					if (ok) {
						mBusy = true;
						ok = mBluetoothGatt.writeDescriptor(clientConfig);
						// Log.i(TAG, "writeDescriptor: " +
						// characteristic.getUuid().toString());
					}
				}
			}
		}

		return ok;
	}

	public boolean isNotificationEnabled(
			BluetoothGattCharacteristic characteristic) {
		if (!checkGatt())
			return false;

		BluetoothGattDescriptor clientConfig = characteristic
				.getDescriptor(GattInfo.CLIENT_CHARACTERISTIC_CONFIG);
		if (clientConfig == null)
			return false;

		return clientConfig.getValue() == BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE;
	}

	/**
	 * After using a given BLE device, the app must call this method to ensure
	 * resources are released properly.
	 */
	public void close() {
		if (mBluetoothGatt != null) {
			// Log.i(TAG, "close");
			mBluetoothGatt.close();
			mBluetoothGatt = null;
		}
	}

	public boolean waitIdle(int timeout) {
		timeout /= 10;
		while (--timeout > 0) {
			if (mBusy)
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			else
				break;
		}

		return timeout > 0;
	}

}
