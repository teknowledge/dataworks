package com.dataworks;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ToggleButton;

import com.dataworks.database.DataBaseHelper;
import com.dataworks.database.DataBaseSingleTon;
import com.dataworks.R;
import com.dataworks.util.Global;

public class NewDataSetFragment extends Fragment {

	private EditText mName;
	private EditText mNotes;
	private TextView mCancel;
	private TextView mDone;
	private ToggleButton toggleLocation;
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");
	DataBaseHelper db;
	String dataset_location = "";

	public MainActivity mainActivity;

	public static NewDataSetFragment newInstance(MainActivity parent) {
		NewDataSetFragment newDataSetFragment = new NewDataSetFragment(parent);
		return newDataSetFragment;
	}

	public NewDataSetFragment(MainActivity parent) {
		this.mainActivity = parent;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		DataBaseSingleTon.sContext = getActivity();

		db = DataBaseSingleTon.getDB();

		View view = inflater.inflate(R.layout.new_dataset, container, false);

		mName = (EditText) view.findViewById(R.id.name_edit);
		mNotes = (EditText) view.findViewById(R.id.notes_edit);
		mCancel = (TextView) view.findViewById(R.id.cancel_dataset);
		mDone = (TextView) view.findViewById(R.id.done_btn);
		toggleLocation = (ToggleButton) view.findViewById(R.id.toggleBtn);

		mDone.setEnabled(true);

		mCancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
				try {
					Global.hideSoftKeyboard(getActivity());
				} catch (Exception e) {

				}
			}
		});

		toggleLocation
				.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView,
							boolean isChecked) {
						// TODO Auto-generated method stub
						if (isChecked) {
							AlertDialog.Builder builder = new AlertDialog.Builder(
									getActivity());
							builder.setTitle("Location Services");
							builder.setMessage("Location services are currently unavailable. We hope to make them available in an upcoming version. Your interest in using them has been noted.");
							builder.setPositiveButton("Done",
									new DialogInterface.OnClickListener() {
										public void onClick(
												DialogInterface dialog,
												int which) {
											Log.e("info", "OK");
											toggleLocation.setChecked(false);
										}
									});

							builder.show();
						}
					}
				});

		mDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					Global.hideSoftKeyboard(getActivity());
				} catch (Exception e) {

				}

				String d_name = mName.getText().toString();
				String d_notes = mNotes.getText().toString();

				if (!d_name.equalsIgnoreCase("")) {
					// get current date time with Calendar()
					Calendar cal = Calendar.getInstance();
					// System.out.println(dateFormat.format(cal.getTime()));
					String dateAndTime[] = dateFormat.format(cal.getTime())
							.split(" ");
					String date = dateAndTime[0];
					String time = dateAndTime[1];

					insertData(d_name, d_notes, date, time);

					getActivity().onBackPressed();

					mDone.setEnabled(false);

				} else {
					// Toast.makeText(getActivity(), "Enter name",
					// Toast.LENGTH_LONG).show();
					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					final Dialog d = builder.create();

					builder.setTitle("Oops!");
					builder.setMessage("Enter a name for the Dataset");
					builder.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									Log.e("info", "OK");
									d.dismiss();

								}
							});
					builder.show();
				}

			}
		});

		return view;
	}

	/***** insert dataset into db ***/
	public void insertData(String d_name, String d_notes, String date,
			String time) {

		db.open();
		db.insertDataSets(d_name, d_notes, date, time, dataset_location,
				System.currentTimeMillis());
		db.close();

		DataSetsFragment.newInstance(mainActivity).notifyDataChanged();

	}
}
