package com.dataworks;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import com.dataworks.data.BufferItem;
import com.dataworks.data.DataItem;
import com.dataworks.data.RegressionResult;
import com.dataworks.database.DataBaseHelper;
import com.dataworks.database.DataBaseSingleTon;
import com.dataworks.R;
import com.dataworks.util.AddClickListener;
import com.dataworks.util.Constants;
import com.dataworks.util.Global;

public class PhCaliberationFragment extends Fragment {
	private static PhCaliberationFragment pHCaliberationFragment;
	private boolean isDeviceConnected = true;
	private DecimalFormat decimal_2 = new DecimalFormat("0.00");

	private String mDeviceAddress;

	private float mtc_value;

	private String temp_type;

	MediaPlayer m = new MediaPlayer();

	public static PhCaliberationFragment newInstance() {
		if (pHCaliberationFragment == null)
			pHCaliberationFragment = new PhCaliberationFragment();
		return pHCaliberationFragment;
	}

	public static PhCaliberationFragment getInstance() {

		return pHCaliberationFragment;
	}

	public void setDeviceConnectionStatus(boolean isDeviceConnected) {
		this.isDeviceConnected = isDeviceConnected;
		new Handler().post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				checkDeviceConnection();
			}
		});

	}

	public void setDeviceAddress(String deviceAddress) {

		this.mDeviceAddress = deviceAddress;
	}

	private AddClickListener addClickListener;
	private ImageView calibration_start_stop_img;
	private ImageView ph_4_caliber;
	private ImageView ph_7_caliber;
	private ImageView ph_10_caliber;
	private TextView calibrating_push_tv;
	private TextView ph_caliber_value;
	private TextView cancel_ph_calib;

	LinearLayout ph_mv_text_layout;
	FrameLayout ph_error_text_layout;
	TextView ph_error_text;
	TextView mV_text;

	private TextView alarm_high_tv, alarm_low_tv;

	private TextView atc_tv, atc_title_tv;

	private TextView calib_history_tv;

	private LinearLayout atc_parent_layout;
	private FrameLayout picker_parent_layout;

	private boolean isStarted;
	private boolean pH4Caliber;
	private boolean pH7Caliber;
	private boolean pH10Caliber;

	public double idealmV = 59.16;
	ArrayList<BufferItem> bufferItems;
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

	public double mV;
	DataBaseHelper db;
	float unCorrectedpH;

	boolean isPickerShowing = false;
	TextView pickerDone;
	Button picker_shadow_dummy_layout;

	// StringPicker leftTempPicker, rightTempPicker;
	NumberPicker leftTempPicker, rightTempPicker;
	View view;
	private TextView deviceName_pH;
	private String deviceCustomName;

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		// return super.onCreateView(inflater, container, savedInstanceState);
		view = inflater.inflate(R.layout.ph_calibration, container, false);

		db = DataBaseSingleTon.getDB();

		calibration_start_stop_img = (ImageView) view
				.findViewById(R.id.calibration_start_stop_img);
		ph_4_caliber = (ImageView) view.findViewById(R.id.ph_4_caliber);
		ph_7_caliber = (ImageView) view.findViewById(R.id.ph_7_caliber);
		ph_10_caliber = (ImageView) view.findViewById(R.id.ph_10_caliber);
		calibrating_push_tv = (TextView) view
				.findViewById(R.id.calibrating_push_tv);
		cancel_ph_calib = (TextView) view.findViewById(R.id.cancel_ph_calib);

		ph_caliber_value = (TextView) view.findViewById(R.id.ph_caliber_value);

		calib_history_tv = (TextView) view.findViewById(R.id.calib_history_tv);

		alarm_high_tv = (TextView) view.findViewById(R.id.alarm_high_tv);

		alarm_low_tv = (TextView) view.findViewById(R.id.alarm_low_tv);

		deviceName_pH = (TextView) view.findViewById(R.id.deviceName_pH);

		ph_mv_text_layout = (LinearLayout) view
				.findViewById(R.id.ph_mv_text_layout);
		ph_error_text_layout = (FrameLayout) view
				.findViewById(R.id.ph_error_text_layout);
		ph_error_text = (TextView) view.findViewById(R.id.ph_error_text);
		mV_text = (TextView) view.findViewById(R.id.mV_text);

		atc_parent_layout = (LinearLayout) view
				.findViewById(R.id.atc_parent_layout);
		picker_parent_layout = (FrameLayout) view
				.findViewById(R.id.picker_parent_layout);

		pickerDone = (TextView) view.findViewById(R.id.picker_done_tv);
		picker_shadow_dummy_layout = (Button) view
				.findViewById(R.id.picker_shadow_dummy_layout);

		leftTempPicker = (NumberPicker) view
				.findViewById(R.id.left_temp_picker);
		rightTempPicker = (NumberPicker) view
				.findViewById(R.id.right_temp_picker);

		atc_tv = (TextView) view.findViewById(R.id.atc_tv);

		return view;
	}

	public void getDeviceInfoFromDB(String deviceAddress) {
		// db = new DataBaseHelper(getActivity());
		db.open();
		// ArrayList<Values> array = new ArrayList<Values>();

		Cursor deviceInfo = db.getDeviceInfoAddress(deviceAddress);

		if (deviceInfo.moveToFirst()) {
			do {

				deviceCustomName = deviceInfo.getString(0);

			} while (deviceInfo.moveToNext());
		}
		deviceInfo.close();
		db.close();
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		if (activity instanceof AddClickListener) {
			addClickListener = (AddClickListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implement AddClickListener");
		}
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		atc_title_tv = (TextView) view.findViewById(R.id.atc_title_tv);

		String mtc_temperature = getMTC_ATC_Temp(mDeviceAddress);

		atc_tv.setText(mtc_temperature + "ºC");

		getDeviceInfoFromDB(mDeviceAddress);
		deviceName_pH.setText(deviceCustomName);

		checkDeviceConnection();

		/*****
		 * Close the fragment on clicking cancel button in this fragment
		 */

		cancel_ph_calib.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

		atc_parent_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				isPickerShowing = true;

				if (temp_type.equalsIgnoreCase("MTC")) {
					Animation animation = AnimationUtils.loadAnimation(
							getActivity(), R.anim.slide_in_up);

					animation
							.setAnimationListener(new Animation.AnimationListener() {

								@Override
								public void onAnimationStart(Animation animation) {
									// TODO Auto-generated method stub
									System.out.println();
								}

								@Override
								public void onAnimationRepeat(
										Animation animation) {
									// TODO Auto-generated method stub

								}

								@Override
								public void onAnimationEnd(Animation animation) {
									// TODO Auto-generated method stub

									Animation fadeInAnimation = AnimationUtils
											.loadAnimation(getActivity(),
													android.R.anim.fade_in);

									picker_shadow_dummy_layout
											.setAnimation(fadeInAnimation);

									picker_shadow_dummy_layout
											.setVisibility(View.VISIBLE);

								}
							});

					picker_parent_layout.setAnimation(animation);
					picker_parent_layout.setVisibility(View.VISIBLE);
				}

			}
		});

		pickerDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				isPickerShowing = false;

				Animation fadeOutAnimation = AnimationUtils.loadAnimation(
						getActivity(), android.R.anim.fade_out);

				fadeOutAnimation
						.setAnimationListener(new Animation.AnimationListener() {

							@Override
							public void onAnimationStart(Animation animation) {
								// TODO Auto-generated method stub
								System.out.println();
							}

							@Override
							public void onAnimationRepeat(Animation animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationEnd(Animation animation) {
								// TODO Auto-generated method stub

								picker_shadow_dummy_layout
										.setVisibility(View.GONE);

							}
						});
				picker_shadow_dummy_layout.setAnimation(fadeOutAnimation);

				Animation animation = AnimationUtils.loadAnimation(
						getActivity(), R.anim.slide_out_down);
				animation
						.setAnimationListener(new Animation.AnimationListener() {

							@Override
							public void onAnimationStart(Animation animation) {
								// TODO Auto-generated method stub
								System.out.println();
							}

							@Override
							public void onAnimationRepeat(Animation animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationEnd(Animation animation) {
								// TODO Auto-generated method stub

								picker_parent_layout.setVisibility(View.GONE);

								String mTCValue = leftTempPicker.getValue()
										+ "." + rightTempPicker.getValue();

								if (isDeviceConnected) {
									atc_tv.setText(mTCValue + " ºC");
								}

								updateTempProbeValueFordevice(mDeviceAddress,
										mTCValue);

								// Global.getInstance().storeIntoPreference(
								// getActivity(), "mtc_value",
								// Float.parseFloat(mTCValue));

							}
						});

				picker_parent_layout.startAnimation(animation);
			}
		});

		picker_shadow_dummy_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				isPickerShowing = false;

				Animation fadeOutAnimation = AnimationUtils.loadAnimation(
						getActivity(), android.R.anim.fade_out);

				fadeOutAnimation
						.setAnimationListener(new Animation.AnimationListener() {

							@Override
							public void onAnimationStart(Animation animation) {
								// TODO Auto-generated method stub
								System.out.println();
							}

							@Override
							public void onAnimationRepeat(Animation animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationEnd(Animation animation) {
								// TODO Auto-generated method stub

								picker_shadow_dummy_layout
										.setVisibility(View.GONE);

							}
						});
				picker_shadow_dummy_layout.setAnimation(fadeOutAnimation);

				Animation animation = AnimationUtils.loadAnimation(
						getActivity(), R.anim.slide_out_down);
				animation
						.setAnimationListener(new Animation.AnimationListener() {

							@Override
							public void onAnimationStart(Animation animation) {
								// TODO Auto-generated method stub
								System.out.println();
							}

							@Override
							public void onAnimationRepeat(Animation animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationEnd(Animation animation) {
								// TODO Auto-generated method stub

								picker_parent_layout.setVisibility(View.GONE);

							}
						});

				picker_parent_layout.startAnimation(animation);
			}
		});

		calibration_start_stop_img.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if (!isStarted) {

					bufferItems = new ArrayList<BufferItem>();

					isStarted = true;
					calibration_start_stop_img
							.setImageResource(R.drawable.ph_caliber_stop);
					ph_4_caliber.setImageResource(R.drawable.ph_4_caliber);
					ph_7_caliber.setImageResource(R.drawable.ph_7_caliber);
					ph_10_caliber.setImageResource(R.drawable.ph_10_caliber);

					ph_4_caliber.setEnabled(true);
					ph_7_caliber.setEnabled(true);
					ph_10_caliber.setEnabled(true);

					checkPhCaliberButtons(false);

				} else {
					isStarted = false;
					calibration_start_stop_img
							.setImageResource(R.drawable.ph_caliber_start);
					ph_4_caliber
							.setImageResource(R.drawable.ph_4_caliber_pressed);
					ph_7_caliber
							.setImageResource(R.drawable.ph_7_caliber_pressed);
					ph_10_caliber
							.setImageResource(R.drawable.ph_10_caliber_pressed);

					ph_4_caliber.setEnabled(false);
					ph_7_caliber.setEnabled(false);
					ph_10_caliber.setEnabled(false);

					checkPhCaliberButtons(true);

					changepHCaliberBooleans();

				}

			}
		});

		ph_4_caliber.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isStarted && !pH4Caliber) {
					pH4Caliber = true;
					ph_4_caliber
							.setImageResource(R.drawable.ph_4_caliber_pressed);

					BufferItem buffer = new BufferItem();
					buffer.milliVolts = mV;
					buffer.temperature = mtc_value;
					buffer.nominalValue = 4.01;
					buffer.unCorrectedpH = unCorrectedpH;

					if (bufferItems == null)
						bufferItems = new ArrayList<>();
					bufferItems.add(buffer);

					checkPhCaliberButtons(false);
				}

			}
		});
		ph_7_caliber.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isStarted && !pH7Caliber) {
					pH7Caliber = true;
					ph_7_caliber
							.setImageResource(R.drawable.ph_7_caliber_pressed);
					BufferItem buffer = new BufferItem();
					buffer.milliVolts = mV;
					buffer.temperature = mtc_value;
					buffer.nominalValue = 7.00;
					buffer.unCorrectedpH = unCorrectedpH;
					bufferItems.add(buffer);

					checkPhCaliberButtons(false);
				}

			}
		});
		ph_10_caliber.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (isStarted && !pH10Caliber) {
					pH10Caliber = true;
					ph_10_caliber
							.setImageResource(R.drawable.ph_10_caliber_pressed);

					BufferItem buffer = new BufferItem();
					buffer.milliVolts = mV;
					buffer.temperature = mtc_value;
					buffer.nominalValue = 10.00;
					buffer.unCorrectedpH = unCorrectedpH;
					bufferItems.add(buffer);

					checkPhCaliberButtons(false);
				}

			}
		});

		calib_history_tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				addClickListener.onCalibrationHistory(mDeviceAddress);
			}
		});

		setPickerValues();

	}

	private void setPickerValues() {
		ArrayList<String> leftPickerValuesArray = new ArrayList<>();
		ArrayList<String> rightPickerValuesArray = new ArrayList<>();
		for (int i = 0; i < 91; i++) {
			leftPickerValuesArray.add("" + i);
		}

		for (int i = 0; i < 10; i++) {
			rightPickerValuesArray.add("" + i);
		}

		// leftTempPicker.setValues(leftPickerValuesArray);
		// leftTempPicker.setCurrent(0);
		// rightTempPicker.setValues(rightPickerValuesArray);
		// rightTempPicker.setCurrent(0);

		leftTempPicker.setMinValue(0);
		leftTempPicker.setMaxValue(90);
		rightTempPicker.setMinValue(0);
		rightTempPicker.setMaxValue(9);

		leftTempPicker.setWrapSelectorWheel(true);
		rightTempPicker.setWrapSelectorWheel(true);

	}

	private void checkPhCaliberButtons(boolean isSuccess) {
		if ((pH4Caliber && pH7Caliber && pH10Caliber)) {

			completeCalibration(bufferItems);
			// if(isSuccess)
			showCalibSuccessDialog();
			// else
			changeInstructionsText(3, "all");

		} else if (!pH4Caliber && pH7Caliber && pH10Caliber) {
			if (isSuccess) {
				completeCalibration(bufferItems);
				showCalibSuccessDialog();
				changeInstructionsText(4, "none");
			} else {
				changeInstructionsText(2, "7&10");
			}

		} else if (pH4Caliber && !pH7Caliber && pH10Caliber) {

			if (isSuccess) {
				completeCalibration(bufferItems);
				showCalibSuccessDialog();
				changeInstructionsText(4, "none");
			} else {
				changeInstructionsText(2, "4&10");
			}

		} else if (pH4Caliber && pH7Caliber && !pH10Caliber) {
			if (isSuccess) {
				completeCalibration(bufferItems);
				showCalibSuccessDialog();
				changeInstructionsText(4, "none");
			} else {
				changeInstructionsText(2, "4&7");
			}

		} else if (pH4Caliber && !pH7Caliber && !pH10Caliber) {
			if (isSuccess) {
				changeInstructionsText(5, "none");
			} else {
				changeInstructionsText(1, "4");
			}

		} else if (!pH4Caliber && pH7Caliber && !pH10Caliber) {
			if (isSuccess) {
				changeInstructionsText(5, "none");
			} else {
				changeInstructionsText(1, "7");
			}

		} else if (!pH4Caliber && !pH7Caliber && pH10Caliber) {
			if (isSuccess) {
				changeInstructionsText(5, "none");
			} else {
				changeInstructionsText(1, "10");
			}

		} else if (!pH4Caliber && !pH7Caliber && !pH10Caliber) {
			if (isSuccess) {
				changeInstructionsText(5, "none");
			} else {
				changeInstructionsText(0, "none");
			}

		}

		else {
			changeInstructionsText(0, "none");
		}
	}

	private void showCalibSuccessDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setMessage(
				"This electrode was successfully calibrated and is now ready to use.")
				.setCancelable(false)
				.setTitle("Calibration Successful")
				.setPositiveButton("Done",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								// do things
							}
						});
		AlertDialog alert = builder.create();
		alert.show();

		changepHCaliberBooleans();

	}

	private void changeInstructionsText(int count, String type) {
		if (count == 5) {
			calibrating_push_tv.setText("Calibration cancelled.");
		} else if (count == 4) {
			calibrating_push_tv.setText("2-point calibration complete.");
		} else if (count == 3) {
			calibrating_push_tv.setText("3-point calibration complete.");
		} else if (count == 2) {
			switch (type) {
			case "7&10":
				String s = "You have calibrated this electrode at pH 7.00 and 10.00. You may hit stop now to complete calibration, or place in the pH 4.01 buffer solution to complete a 3-point calibration(Recommended)";
				calibrating_push_tv.setText(s);
				break;
			case "4&10":
				String s1 = "You have calibrated this electrode at pH 4.01 and 10.00. You may hit stop now to complete calibration, or place in the pH 7.00 buffer solution to complete a 3-point calibration(Recommended)";
				calibrating_push_tv.setText(s1);
				break;
			case "4&7":
				String s2 = "You have calibrated this electrode at pH 4.01 and 7.00. You may hit stop now to complete calibration, or place in the pH 10.00 buffer solution to complete a 3-point calibration(Recommended)";
				calibrating_push_tv.setText(s2);
				break;
			}
		} else if (count == 1) {
			switch (type) {
			case "4":
				String s = "pH 4.01 calibration complete. You may now calibrate for a second buffer solution. Either 7.00 or 10.00. Rinse and dry your electrode before placing it in the solution.";
				calibrating_push_tv.setText(s);
				break;
			case "7":
				String s1 = "pH 7.00 calibration complete. You may now calibrate for a second buffer solution. Either 4.01 or 10.00. Rinse and dry your electrode before placing it in the solution.";
				calibrating_push_tv.setText(s1);
				break;
			case "10":
				String s2 = "pH 10.00 calibration complete. You may now calibrate for a second buffer solution. Either 4.01 or 7.00. Rinse and dry your electrode before placing it in the solution.";
				calibrating_push_tv.setText(s2);
				break;
			}
		} else {
			calibrating_push_tv
					.setText("You are ready to start calibrating. Rinse and clean the electrode, place it in one of the buffer solutions. Stir gently and once the pH value has been stabilised press the button for the relevant buffer.");
		}
	}

	private void changepHCaliberBooleans() {
		pH4Caliber = false;
		pH7Caliber = false;
		pH10Caliber = false;

		isStarted = false;

		calibration_start_stop_img
				.setImageResource(R.drawable.ph_caliber_start);

	}

	/*****
	 * purpose: Update pH values on every charateristic update changed from the
	 * device pH bluetooth
	 * 
	 */
	public void onCharacteristicUpdatedpH(final float value, final String v,
			final String deviceAddress) {

		new Handler().post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub

				if (mDeviceAddress.equalsIgnoreCase(deviceAddress)) {

					float pHValue = value;

					// ph_caliber_value.setText(value);

					mV = Double.parseDouble(v);

					String pHDecimal = decimal_2.format(pHValue);

					unCorrectedpH = caliberatedpH(Float.parseFloat(v), 1f, 0f);

					checkProbe(pHValue, ph_caliber_value, pHDecimal, v,
							deviceAddress, ph_mv_text_layout,
							ph_error_text_layout, ph_error_text, mV_text);

					// checkAlarm(value, alarm_high_tv, alarm_low_tv,
					// getActivity());
				}

			}
		});

	}

	public void onCharacteristicUpdatedpH_Temp(String deviceAddress,
			String tempType, String temp_value) {

		if (mDeviceAddress.equalsIgnoreCase(deviceAddress)) {
			mtc_value = Float.parseFloat(temp_value);

			temp_type = tempType;

			atc_tv.setText(mtc_value + " ºC");

			if (temp_type.equalsIgnoreCase("MTC")) {
				atc_title_tv.setText("MTC");
			} else {
				atc_title_tv.setText("ATC");
			}
		}

	}

	private void updateTempProbeValueFordevice(String deviceAddress,
			String temperature) {
		// TODO Auto-generated method stub

		// db = new DataBaseHelper(getActivity());
		db.open();
		db.updatePHCalibration(deviceAddress, Constants.TEMPERATURE_TAG,
				temperature);
		db.close();

	}

	public String getMTC_ATC_Temp(String deviceAddress) {
		String temperature = null;
		// db = new DataBaseHelper(getActivity());

		db.open();
		Cursor cursor_temperature = db.getpHTemperature(deviceAddress);
		if (cursor_temperature.moveToFirst()) {
			do {
				temperature = cursor_temperature.getString(0);
			} while (cursor_temperature.moveToNext());
		}

		cursor_temperature.close();
		db.close();

		return temperature;
	}

	public float caliberatedpH(float mV, float slope, float intercept) {
		// Converts the mV value to a calibrated pH value
		// slope = is slope adjustment determined by the calibration of the
		// electrode
		// zero = is the zero point adjustment determined by the calibration of
		// the electrode
		// sl25 = is the ideal slope at 25 degrees C
		// mV = is the measure mV value supplied by the pH electrode.

		float sl25 = 59.16f;
		float pH;

		// pH = slope * (sl25 * 7 - (mV - zero)) / sl25;
		pH = 7 - ((mV - intercept) / (slope * sl25));
		// NSLog(@"pH = %f, mV = %f, zero = %f, slope = %f",pH, mV, zero,
		// slope);

		return pH;
	}

	/*****
	 * Complete calibration method will do the calibration based on the selected
	 * type pHs
	 */

	public void completeCalibration(ArrayList<BufferItem> bufferItems) {

		// db = new DataBaseHelper(getActivity());

		RegressionResult results = doRegression(bufferItems);

		double slope = results.slope;
		double intercept = results.intercept;

		// Global.getInstance().storeIntoPreference(getActivity(), "slope",
		// (float) slope);
		// Global.getInstance().storeIntoPreference(getActivity(), "intercept",
		// (float) intercept);

		Global.getInstance().storeIntoPreference(getActivity(), "no_of_values",
				"0");

		Calendar cal2 = Calendar.getInstance();
		String dateAndTime2[] = dateFormat.format(cal2.getTime()).split(" ");
		String date2 = dateAndTime2[0];
		String time2 = dateAndTime2[1];

		String ph4 = "";
		String ph7 = "";
		String ph10 = "";

		for (int i = 0; i < bufferItems.size(); i++) {
			if (bufferItems.get(i).nominalValue > 4
					&& bufferItems.get(i).nominalValue < 5) {
				ph4 = bufferItems.get(i).temperature + ","
						+ bufferItems.get(i).milliVolts + ","
						+ bufferItems.get(i).unCorrectedpH;
			} else if (bufferItems.get(i).nominalValue == 7.00) {
				ph7 = bufferItems.get(i).temperature + ","
						+ bufferItems.get(i).milliVolts + ","
						+ bufferItems.get(i).unCorrectedpH;
			} else if (bufferItems.get(i).nominalValue == 10.00) {
				ph10 = bufferItems.get(i).temperature + ","
						+ bufferItems.get(i).milliVolts + ","
						+ bufferItems.get(i).unCorrectedpH;
			}

		}

		db.open();
		db.updatePHCalibration(mDeviceAddress, Constants.SLOPE_TAG, slope + "");
		db.updatePHCalibration(mDeviceAddress, Constants.INTERCEPT_TAG,
				intercept + "");

		db.insertpHCalibration(mDeviceAddress, date2, String.valueOf(slope),
				String.valueOf(intercept), ph4, ph7, ph10, "0", time2);

		db.close();

	}

	public RegressionResult doRegression(ArrayList<BufferItem> bufferItems) {

		ArrayList<DataItem> itemsList = new ArrayList<>();

		for (int i = 0; i < bufferItems.size(); i++) {
			DataItem items = new DataItem();
			items.xValue = (7 - bufferItems.get(i).nominalValue) * idealmV;
			items.yValue = bufferItems.get(i).milliVolts;

			itemsList.add(items);
		}

		LinearRegression regression = new LinearRegression();
		RegressionResult results = regression.CalculateRegression(itemsList);
		return results;

	}

	private void checkDeviceConnection() {

		if (!isDeviceConnected) {

			ph_mv_text_layout.setVisibility(View.GONE);
			ph_error_text_layout.setVisibility(View.VISIBLE);

			ph_error_text.setText("Instrument \ndisconnected");

			mV_text.setVisibility(View.GONE);

			atc_tv.setText("-  -");

		}
	}

	/*****
	 * 
	 * Check the alarm to notify the sound when the values are set and on
	 * 
	 * @param pH
	 */

	public void checkAlarm(float pH, TextView alarm_high, TextView alarm_low,
			Context context) {

		if (context != null) {

			boolean highpH = Global.getInstance().getBooleanType(context,
					"high_pH");
			boolean lowpH = Global.getInstance().getBooleanType(context,
					"low_pH");

			if (highpH) {

				float highValuePh = Global.getInstance().getPreferencefloatVal(
						context, "pH_high_value");
				if (highValuePh > pH) {
					alarm_high.setTextColor(Color.parseColor("#ff0000"));
					playBeep();
				} else {
					stopBeep();
					alarm_high.setTextColor(Color.parseColor("#000000"));
				}
			}
			if (lowpH) {
				float lowValuePh = Global.getInstance().getPreferencefloatVal(
						context, "pH_low_value");
				if (lowValuePh > pH) {
					alarm_low.setTextColor(Color.parseColor("#ff0000"));
					playBeep();
				} else {
					stopBeep();
					alarm_low.setTextColor(Color.parseColor("#000000"));
				}
			}
			if (!highpH && !lowpH) {
				stopBeep();
				alarm_high.setTextColor(Color.parseColor("#000000"));
				alarm_low.setTextColor(Color.parseColor("#000000"));
			}
		}
	}

	public void playBeep() {
		try {
			if (m.isPlaying()) {
				m.stop();
				m.release();
				m = new MediaPlayer();
			}

			AssetFileDescriptor descriptor = getActivity().getAssets().openFd(
					"alarm.mp3");
			m.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();

			m.prepare();
			m.setVolume(1f, 1f);
			m.setLooping(true);
			m.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void stopBeep() {
		if (m.isPlaying()) {
			m.stop();
			m.release();
			m = new MediaPlayer();
		}
	}

	/*****
	 * 
	 * Check the getting value from device value is valid or else check the
	 * probe
	 * 
	 * @param pH
	 * @param pH_tview
	 * @param pHDecimal
	 */
	public void checkProbe(float pH, TextView pH_tview, String pHDecimal,
			String mV, String deviceAddress, LinearLayout ph_mv_text_layout,
			FrameLayout ph_error_text_layout, TextView ph_error_text,
			TextView mV_text) {
		if (getActivity() != null) {
			boolean type_display = getmVType(deviceAddress);

			if (pH > 14 || pH < 0.0) {
				ph_mv_text_layout.setVisibility(View.GONE);
				ph_error_text_layout.setVisibility(View.VISIBLE);

				ph_error_text.setText("Check \nProbe");

				mV_text.setVisibility(View.GONE);

			} else {

				ph_error_text_layout.setVisibility(View.GONE);
				ph_mv_text_layout.setVisibility(View.VISIBLE);
				if (type_display) {
					pH_tview.setText(mV);

					mV_text.setVisibility(View.VISIBLE);

				} else {

					pH_tview.setText(pHDecimal);
					mV_text.setVisibility(View.GONE);

				}

			}
		}

	}

	public boolean getmVType(String deviceAddress) {

		db.open();
		boolean mV_type = db.getmV(deviceAddress);
		db.close();

		return mV_type;
	}
}
