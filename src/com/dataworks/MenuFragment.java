package com.dataworks;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dataworks.bluetooth.service.BleDeviceInfo;
import com.dataworks.bluetooth.service.BluetoothLeService;
import com.dataworks.database.DataBaseHelper;
import com.dataworks.R;
import com.dataworks.slidemenu.SlidingMenu;
import com.dataworks.util.BluetoothClickListener;
import com.dataworks.util.Constants;
import com.dataworks.util.CustomTimer;
import com.dataworks.util.CustomTimerCallback;
import com.dataworks.util.DeviceDisconnectListener;

public class MenuFragment extends Fragment {
	View menuInflateView;
	SlidingMenu slidingMenu;
	private TextView no_device_text;

	private boolean mBleSupported = true;
	public static BluetoothManager mBluetoothManager;
	private BluetoothAdapter mBtAdapter = null;
	private boolean mInitialised = false;
	SwipeRefreshLayout mSwipeRefreshLayout;
	private boolean mBtAdapterEnabled = false;

	private IntentFilter mFilter;
	private List<BleDeviceInfo> mDeviceInfoList;
	private BluetoothLeService mBluetoothLeService = null;
	private int mNumDevs = 0;
	private boolean mScanning = false;
	private int mConnIndex = NO_DEVICE;
	private static final int NO_DEVICE = -1;

	private String[] mDeviceFilter = null;

	// Requests to other activities
	private static final int REQ_ENABLE_BT = 0;
	private static final int REQ_DEVICE_ACT = 1;
	private BluetoothDevice mBluetoothDevice = null;
	private CustomTimer mConnectTimer = null;
	private final int CONNECT_TIMEOUT = 14; // Seconds
	DeviceListAdapter mDeviceAdapter = null;
	private ListView mDeviceListView = null;
	private CustomTimer mStatusTimer;
	private CustomTimer mScanTimer = null;
	private final int SCAN_TIMEOUT = 3; // Seconds
	private boolean mBusy;
	private Button mBtnScan = null;

	public static boolean mCaliberateMagValue;
	public static boolean mCaliberateGyroValue;

	private int checkedDevicePos = -1;

	/**
	 * This will Check the device is existed or not
	 * 
	 * if existed true else false
	 * */
	private boolean isDeviceFound = false;

	private List<BleDeviceInfo> deviceList = new ArrayList<BleDeviceInfo>();;

	private BluetoothClickListener mClickListener;

	private DeviceDisconnectListener mDeviceDisconnectListener;

	private String clickedDeviceName = "";

	private DataBaseHelper db;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		menuInflateView = inflater.inflate(R.layout.menu_fragment, container,
				false);

		db = new DataBaseHelper(getActivity());

		if (!getActivity().getPackageManager().hasSystemFeature(
				PackageManager.FEATURE_BLUETOOTH_LE)) {
			Toast.makeText(getActivity(), R.string.ble_not_supported,
					Toast.LENGTH_LONG).show();
			mBleSupported = false;
		}

		// Initializes a Bluetooth adapter. For API level 18 and above, get a
		// reference to BluetoothAdapter through BluetoothManager.
		mBluetoothManager = (BluetoothManager) getActivity().getSystemService(
				Context.BLUETOOTH_SERVICE);
		mBtAdapter = mBluetoothManager.getAdapter();

		// Checks if Bluetooth is supported on the device.
		if (mBtAdapter == null) {
			Toast.makeText(getActivity(), R.string.bt_not_supported,
					Toast.LENGTH_LONG).show();
			mBleSupported = false;
		}

		// Initialize device list container and device filter
		mDeviceInfoList = new ArrayList<BleDeviceInfo>();
		Resources res = getResources();
		mDeviceFilter = res.getStringArray(R.array.device_filter);

		// Register the BroadcastReceiver
		mFilter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
		mFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
		mFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);

		// slidingMenu = new SlidingMenu(getActivity());

		no_device_text = (TextView) menuInflateView
				.findViewById(R.id.no_device_text);

		mDeviceListView = (ListView) menuInflateView
				.findViewById(R.id.device_list);
		mDeviceListView.setClickable(true);
		mDeviceListView.setOnItemClickListener(mDeviceClickListener);

		mSwipeRefreshLayout = (SwipeRefreshLayout) menuInflateView
				.findViewById(R.id.activity_main_swipe_refresh_layout);

		mSwipeRefreshLayout.setOnRefreshListener(new OnRefreshListener() {

			@Override
			public void onRefresh() {
				// TODO Auto-generated method stub

				// onBtnScan();

				mBtAdapterEnabled = mBtAdapter.isEnabled();
				if (mBtAdapterEnabled) {

					/*****
					 * Do not scan when the device is connected and Scan for the
					 * device when it is not connected
					 ************/
					onBtnScan();

				} else {

					/************ show alert when blue tooth is off *********/
					mInitialised = false;
					mSwipeRefreshLayout.setRefreshing(false);
					// enableBluetooth();

					AlertDialog.Builder builder = new AlertDialog.Builder(
							getActivity());
					final Dialog d = builder.create();
					builder.setTitle("Bluetooth Unavailable");
					builder.setMessage("Your Bluetooth has been switched off. Go to the settings page to turn it back on");
					builder.setPositiveButton("Done",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {
									Log.e("info", "OK");
									d.dismiss();
								}
							});

					builder.show();
				}

			}

		});

		mBtnScan = (Button) menuInflateView.findViewById(R.id.btn_scan);
		mBtnScan.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				onBtnScan();
			}
		});

		onScanViewReady(menuInflateView);
		inItUI(menuInflateView);
		return menuInflateView;
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		if (activity instanceof BluetoothClickListener) {
			mClickListener = (BluetoothClickListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implement BluetoothClickListener");
		}

		if (activity instanceof DeviceDisconnectListener) {
			mDeviceDisconnectListener = (DeviceDisconnectListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implement DeviceDisconnectListener");
		}
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		// Use this check to determine whether BLE is supported on the device.
		// Then
		// you can selectively disable BLE-related features.

		// enableBluetooth();
	}

	@Override
	public void onDestroy() {
		// Log.e(TAG,"onDestroy");
		super.onDestroy();
		if (mBluetoothLeService != null) {
			if (mScanning)
				scanLeDevice(false);
			getActivity().unregisterReceiver(mReceiver);
			getActivity().unbindService(mServiceConnection);
			mBluetoothLeService.close();
			mBluetoothLeService = null;
		}

		mBtAdapter = null;

		// Clear cache
		File cache = getActivity().getCacheDir();
		String path = cache.getPath();
		try {
			Runtime.getRuntime().exec(String.format("rm -rf %s", path));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void inItUI(View contentView) {

	}

	ArrayList<String> scanedDeviceNames = new ArrayList<String>();

	public void onBtnScan() {
		scanedDeviceNames.clear();
		if (mScanning) {
			stopScan();
		} else {
			isDeviceFound = false;

			startScan();
		}
	}

	void notifyDataSetChanged() {

		if (mDeviceAdapter == null) {
			deviceList = getDeviceInfoList();
			mDeviceAdapter = new DeviceListAdapter(getActivity(), deviceList);
			mDeviceListView.setAdapter(mDeviceAdapter);
		}

		mDeviceAdapter.notifyDataSetChanged();
		if (deviceList.size() > 0) {

			no_device_text.setVisibility(View.GONE);
		} else {

			no_device_text.setVisibility(View.VISIBLE);

		}
	}

	private boolean deviceInfoExists(String address) {
		for (int i = 0; i < mDeviceInfoList.size(); i++) {

			if (mDeviceInfoList.get(i).getBluetoothDevice() != null) {
				if (mDeviceInfoList.get(i).getBluetoothDevice().getAddress()
						.equals(address)) {
					return true;
				}
			}

		}
		return false;
	}

	public void notifySetDataChanged() {
		if (mDeviceAdapter != null) {
			mDeviceAdapter.notifyDataSetChanged();
		}
	}

	List<BleDeviceInfo> getDeviceInfoList() {
		return mDeviceInfoList;
	}

	// Listener for device list
	private OnItemClickListener mDeviceClickListener = new OnItemClickListener() {
		public void onItemClick(AdapterView<?> parent, View view,
				final int pos, long id) {
			// Log.d(TAG,"item click");
			checkedDevicePos = pos;
			final BleDeviceInfo deviceInfo = (BleDeviceInfo) parent
					.getItemAtPosition(pos);
			deviceInfo.setDeviceDeselected(false);

			clickedDeviceName = deviceInfo.getBluetoothDevice().getAddress();
			if (deviceInfo.isConnecting())
				return;

			if (!deviceInfo.isDisconnected() && !deviceInfo.isDeviceFound()) {
				deviceList.remove(pos);
				mDeviceAdapter.notifyDataSetChanged();
			} else if (deviceInfo.isDisconnected() && deviceInfo.isDeselected()
					&& deviceInfo.isDeviceFound()) {

				deviceInfo.setConnecting(true);
				deviceInfo.setDeselected(false);
				deviceList.set(pos, deviceInfo);

				mConnectTimer = new CustomTimer(null, CONNECT_TIMEOUT,
						mPgConnectCallback, pos);

				onDeviceClick(pos);

				mDeviceAdapter.notifyDataSetChanged(); // Force disabling of
														// all
														// Connect buttons
			} else if (deviceInfo.isDisconnected() && deviceInfo.isDeselected()
					&& !deviceInfo.isDeviceFound()) {
				deviceList.remove(pos);
				mDeviceAdapter.notifyDataSetChanged();
			}

			else if (deviceInfo.isDisconnected()) {

				LayoutInflater layoutInflater = LayoutInflater
						.from(getActivity());
				View promptView = layoutInflater.inflate(
						R.layout.instrument_disconnected, null);

				final AlertDialog alertD = new AlertDialog.Builder(
						getActivity()).create();

				alertD.requestWindowFeature(Window.FEATURE_NO_TITLE);
				alertD.setView(promptView);
				alertD.show();
				alertD.getWindow().setBackgroundDrawable(
						new ColorDrawable(Color.TRANSPARENT));
				alertD.setCanceledOnTouchOutside(false);
				@SuppressWarnings("static-access")
				Display display = ((WindowManager) getActivity()
						.getSystemService(Context.WINDOW_SERVICE))
						.getDefaultDisplay();
				@SuppressWarnings("deprecation")
				Button deselect = (Button) promptView
						.findViewById(R.id.deselect);
				Button reconnect = (Button) promptView
						.findViewById(R.id.reconnect);

				deselect.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						deviceInfo.setDeselected(true);
						deviceList.set(pos, deviceInfo);
						mDeviceAdapter.notifyDataSetChanged();

						alertD.dismiss();
					}
				});

				reconnect.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						deviceInfo.setConnecting(true);

						if (deviceInfo.isDisconnected()
								&& !deviceInfo.isDeviceFound()) {
							deviceInfo.setDeselected(false);
							mDeviceAdapter.notifyDataSetChanged();
						}
						deviceList.set(pos, deviceInfo);

						mConnectTimer = new CustomTimer(null, CONNECT_TIMEOUT,
								mPgConnectCallback, pos);

						onDeviceClick(pos);
						mDeviceAdapter.notifyDataSetChanged();

						alertD.dismiss();

					}
				});

			}

			else if (!deviceInfo.isConnected()) {

				deviceInfo.setConnecting(true);
				deviceList.set(pos, deviceInfo);

				mConnectTimer = new CustomTimer(null, CONNECT_TIMEOUT,
						mPgConnectCallback, pos);

				onDeviceClick(pos);
				mDeviceAdapter.notifyDataSetChanged(); // Force disabling of
														// all
														// Connect buttons

			} else if (deviceInfo.isConnected()) {

				deviceInfo.setDeselected(true);
				deviceList.set(pos, deviceInfo);

				mConnectTimer = new CustomTimer(null, CONNECT_TIMEOUT,
						mPgConnectCallback, pos);

				onDeviceClick(pos);
				mDeviceAdapter.notifyDataSetChanged(); // Force disabling of
														// all
														// Connect buttons

			}

		}
	};
	// Listener for connect/disconnect expiration
	// Listener for connect/disconnect expiration
	private CustomTimerCallback mPgConnectCallback = new CustomTimerCallback() {
		public void onTimeout(int checkedPosition) {
			onConnectTimeout(checkedPosition);
			mBtnScan.setEnabled(true);

		}

		public void onTick(int i) {
			// refreshBusyIndicator();
		}
	};

	void onScanViewReady(View view) {
		// Initial state of widgets
		updateGuiState();

		enableBluetooth();
	}

	public void onConnectTimeout(final int checkedDevicePos) {
		getActivity().runOnUiThread(new Runnable() {
			public void run() {
				setError("Connection timed out");
				if (deviceList.size() > 0) {
					BleDeviceInfo deviceInfo = deviceList.get(checkedDevicePos);
					BluetoothDevice device = deviceInfo.getBluetoothDevice();

					Peripheral peripheral = null;
					if (device.getAddress() != null) {

						for (Peripheral pp : mBluetoothLeService
								.connectedPeripherals()) {

							if (pp.address.equalsIgnoreCase(device.getAddress())) {
								peripheral = pp;
								break;
							}
						}
					}

					peripheral.disconnect();

					deviceInfo.setConnecting(false);
					deviceInfo.setDeselected(true);
					deviceInfo.setDisconnected(true);
					deviceInfo.setDeviceFound(false);

					deviceList.set(checkedDevicePos, deviceInfo);

					mDeviceAdapter.notifyDataSetChanged();
				}

			}
		});

	}

	public void onDeviceClick(final int pos) {

		if (mScanning)
			stopScan();

		setBusy(true);

		onConnect(pos);

	}

	void setStatus(String txt) {

	}

	void setStatus(String txt, int duration) {
		setStatus(txt);
		mStatusTimer = new CustomTimer(null, duration, mClearStatusCallback, -1);
	}

	// Listener for connect/disconnect expiration
	private CustomTimerCallback mClearStatusCallback = new CustomTimerCallback() {
		public void onTimeout(int checkedPosition) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					setStatus("");
				}
			});
			mStatusTimer = null;
		}

		public void onTick(int i) {
		}
	};

	void onConnect(int pos) {

		mBluetoothDevice = mDeviceInfoList.get(pos).getBluetoothDevice();

		if (mNumDevs > 0) {

			int connState = mBluetoothManager.getConnectionState(
					mBluetoothDevice, BluetoothGatt.GATT);

			switch (connState) {
			case BluetoothGatt.STATE_CONNECTED:
				mBluetoothLeService.disconnect(mBluetoothDevice);
				break;
			case BluetoothGatt.STATE_DISCONNECTED:
				boolean ok = mBluetoothLeService.connect(mBluetoothDevice);
				if (!ok) {

				}
				break;
			default:
				setError("Device busy (connecting/disconnecting)");
				break;
			}
		}
	}

	private void stopScan() {
		mScanning = false;
		updateGui(false);
		scanLeDevice(false);
	}

	void setBusy(boolean f) {
		if (f != mBusy) {
			mBusy = f;
			if (!mBusy) {
				// stopTimers(false);
				stopScanTimer();
				mBtnScan.setEnabled(true); // Enable in case of connection
											// timeout
				mDeviceAdapter.notifyDataSetChanged(); // Force enabling of all
														// Connect buttons
			}
			// showBusyIndicator(f);
		}
	}

	private void stopScanTimer() {
		if (mScanTimer != null) {
			mScanTimer.stop();

			mScanTimer = null;
		}
	}

	private void stopTimers(boolean makeNull) {

		if (mConnectTimer != null) {
			mConnectTimer.stop();
			if (makeNull)
				mConnectTimer = null;
		}
	}

	public void enableBluetooth() {

		if (!mInitialised) {
			// Broadcast receiver
			getActivity().registerReceiver(mReceiver, mFilter);
			mBtAdapterEnabled = mBtAdapter.isEnabled();
			if (mBtAdapterEnabled) {
				// Start straight away
				startBluetoothLeService();
			} else {
				// Request BT adapter to be turned on
				Intent enableIntent = new Intent(
						BluetoothAdapter.ACTION_REQUEST_ENABLE);
				startActivityForResult(enableIntent, REQ_ENABLE_BT);
			}
			mInitialised = true;
		} else {

			notifyDataSetChanged();
		}

	}

	boolean onScanTimeOut = false;
	Handler scanDeviceHandler = new Handler();
	Runnable scanDeviceRunnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub

			if (!isDeviceFound) {

				for (int i = 0; i < deviceList.size(); i++) {
					BleDeviceInfo deviceInfo = findDeviceInfo(deviceList.get(i)
							.getBluetoothDevice());
					if (!deviceInfo.isConnected()) {
						deviceInfo.setDeviceFound(false);
					}
				}
				notifyDataSetChanged();
				scanDeviceHandler.removeCallbacks(scanDeviceRunnable);

			}
		}
	};

	private void startScan() {
		// Start device discovery
		if (mBleSupported) {

			notifyDataSetChanged();
			scanLeDevice(true);
			updateGui(mScanning);
			if (!mScanning) {
				setError("Device discovery start failed");

			}
		} else {
			setError("BLE not supported on this device");
		}

	}

	void updateGui(boolean scanning) {
		if (mBtnScan == null)
			return; // UI not ready

		setBusy(scanning);

		if (scanning) {
			// Indicate that scanning has started
			mScanTimer = new CustomTimer(null, SCAN_TIMEOUT, mPgScanCallback,
					-1);
			mBtnScan.setText("Stop");
			mBtnScan.setCompoundDrawablesWithIntrinsicBounds(0, 0,
					R.drawable.ic_action_cancel, 0);
			// mStatus.setTextAppearance(mContext, R.style.statusStyle_Busy);
			// mStatus.setText("Scanning...");
			// mEmptyMsg.setText(R.string.nodevice);
			updateGuiState();
		} else {
			// Indicate that scanning has stopped
			// mStatus.setTextAppearance(mContext, R.style.statusStyle_Success);
			mBtnScan.setText("Scan");
			mBtnScan.setCompoundDrawablesWithIntrinsicBounds(0, 0,
					R.drawable.ic_action_refresh, 0);
			// mEmptyMsg.setText(R.string.scan_advice);
			// mActivity.setProgressBarIndeterminateVisibility(false);
			mDeviceAdapter.notifyDataSetChanged();
		}
	}

	// Listener for progress timer expiration
	private CustomTimerCallback mPgScanCallback = new CustomTimerCallback() {
		public void onTimeout(int checkedPosition) {
			onScanTimeout();
		}

		public void onTick(int i) {
			// refreshBusyIndicator();
		}
	};

	public void onScanTimeout() {
		if (getActivity() != null) {
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					// onScanTimeOut = true;
					scanDeviceHandler.post(scanDeviceRunnable);
					stopScan();

					notifyDataSetChanged();
					// mSwipeRefreshLayout.setRefreshing(false);

				}
			});
		}

	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// GUI methods
	//
	public void updateGuiState() {
		boolean mBtEnabled = mBtAdapter.isEnabled();

		if (mBtEnabled) {
			if (mScanning) {
				// BLE Host connected
				if (mConnIndex != NO_DEVICE) {
					String txt = mBluetoothDevice.getName() + " connected";

				} else {

				}
			}
		} else {
			mDeviceInfoList.clear();
			notifyDataSetChanged();
		}
	}

	private boolean scanLeDevice(boolean enable) {

		if (enable) {

			mScanning = mBtAdapter.startLeScan(mLeScanCallback);

			System.out.println();
		} else {
			mScanning = false;

			mBtAdapter.stopLeScan(mLeScanCallback);
			mSwipeRefreshLayout.setRefreshing(false);

		}
		return mScanning;
	}

	void setError(String txt) {

		// "Turning BT adapter off and on again may fix Android BLE stack problems");
	}

	boolean checkDeviceFilter(String deviceName) {
		if (deviceName == null)
			return false;

		int n = mDeviceFilter.length;
		if (n > 0) {
			boolean found = false;
			for (int i = 0; i < n && !found; i++) {
				found = deviceName.equals(mDeviceFilter[i]);
			}
			return found;
		} else
			// Allow all devices if the device filter is empty
			return true;
	}

	private BleDeviceInfo createDeviceInfo(BluetoothDevice device, int rssi) {
		BleDeviceInfo deviceInfo = new BleDeviceInfo(device, rssi);

		return deviceInfo;
	}

	private void addDevice(BleDeviceInfo device) {
		mNumDevs++;

		mDeviceInfoList.add(device);

		if (mNumDevs > 1) {
			// mScanView.setStatus(mNumDevs + "devices");
		}

		else {
			// mScanView.setStatus("1 device");
		}

	}

	private void startBluetoothLeService() {
		boolean f;

		Intent bindIntent = new Intent(getActivity(), BluetoothLeService.class);
		getActivity().startService(bindIntent);
		f = getActivity().bindService(bindIntent, mServiceConnection,
				Context.BIND_AUTO_CREATE);
		if (!f) {
			// CustomToast.middleBottom(this,
			// "Bind to BluetoothLeService failed");
			// finish();
		}
	}

	// Activity result handling
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		switch (requestCode) {

		case REQ_ENABLE_BT:
			// When the request to enable Bluetooth returns
			if (resultCode == Activity.RESULT_OK) {

				// Toast.makeText(this, R.string.bt_on,
				// Toast.LENGTH_SHORT).show();
			} else {
				// User did not enable Bluetooth or an error occurred
				// Toast.makeText(this, R.string.bt_not_on,
				// Toast.LENGTH_SHORT).show();
				// getActivity().finish();
			}
			break;
		default:

			// Log.e(TAG, "Unknown request code");
			break;
		}
	}

	private void stopDeviceActivity() {
		// getActivity().finishActivity(REQ_DEVICE_ACT);
	}

	private void startDeviceActivity(BluetoothDevice bluetoothDevice) {

		mClickListener.onDeviceButtonClick(bluetoothDevice);
	}

	// ////////////////////////////////////////////////////////////////////////////////////////////////
	//
	// Broadcasted actions from Bluetooth adapter and BluetoothLeService
	//
	private BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();

			String name = intent
					.getStringExtra(BluetoothLeService.EXTRA_ADDRESS);

			Peripheral peripheral = null;
			if (name != null) {

				for (Peripheral pp : mBluetoothLeService.connectedPeripherals()) {

					if (pp.address.equalsIgnoreCase(name)) {
						peripheral = pp;
						break;
					}
				}
			}

			// List<BleDeviceInfo> deviceList = getDeviceInfoList();
			Map<String, Integer> deviceNames = new HashMap<String, Integer>();

			for (int i = 0; i < deviceList.size(); i++) {
				BleDeviceInfo deviceInfo = deviceList.get(i);
				BluetoothDevice device = deviceInfo.getBluetoothDevice();
				deviceNames.put(device.getAddress(), i);
			}

			if (BluetoothAdapter.ACTION_STATE_CHANGED.equals(action)) {
				// Bluetooth adapter state change
				switch (mBtAdapter.getState()) {
				case BluetoothAdapter.STATE_ON:
					mConnIndex = NO_DEVICE;
					startBluetoothLeService();
					break;
				case BluetoothAdapter.STATE_OFF:
					// Toast.makeText(context, R.string.app_closing,
					// Toast.LENGTH_LONG).show();
					// getActivity().finish();
					break;
				default:
					// Log.w(TAG, "Action STATE CHANGED not processed ");
					break;
				}

			} else if (BluetoothLeService.ACTION_GATT_CONNECTING.equals(action)) {
				int status = intent.getIntExtra(
						BluetoothLeService.EXTRA_STATUS,
						BluetoothGatt.GATT_SUCCESS);

				if (deviceNames.containsKey(name)) {

					int selectedDevicePosition = deviceNames.get(name);
					BleDeviceInfo deviceInfo = deviceList
							.get(selectedDevicePosition);
					BluetoothDevice device = deviceInfo.getBluetoothDevice();

					BleDeviceInfo bleDeviceInfo = new BleDeviceInfo(device);
					bleDeviceInfo.setConnecting(true);
					bleDeviceInfo.setIsconnected(false);
					bleDeviceInfo.setDisconnecting(false);
					bleDeviceInfo.setDisconnected(false);
					if (deviceInfo.isDeselected()) {
						bleDeviceInfo.setDeselected(true);
					} else {
						bleDeviceInfo.setDeselected(false);
					}

					bleDeviceInfo.setDeviceFound(true);

					deviceList.set(selectedDevicePosition, bleDeviceInfo);

				}

			}

			else if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {

				// int selectedDevicePosition = deviceNames.get(name);
				// BleDeviceInfo deviceInfo = deviceList
				// .get(selectedDevicePosition);

				// GATT connect
				int status = intent.getIntExtra(
						BluetoothLeService.EXTRA_STATUS,
						BluetoothGatt.GATT_SUCCESS);

				// System.out.println(name);
				if (status == BluetoothGatt.GATT_SUCCESS) {
					setBusy(false);
					stopTimers(false);

					mConnIndex = 1;
					BluetoothDevice device = null;
					if (deviceNames.containsKey(name)) {

						int selectedDevicePosition = deviceNames.get(name);
						BleDeviceInfo deviceInfo = deviceList
								.get(selectedDevicePosition);
						// System.out.println(" connected " + "  ....connected"
						// + deviceInfo.getBluetoothDevice().getName());

						if (deviceInfo.isDeviceDeselected()) {
							// System.out.println("is return");
							// peripheral.disconnect();
							return;
						}
						device = deviceInfo.getBluetoothDevice();
						mBluetoothLeService.setConnecting(device, true);
						BleDeviceInfo bleDeviceInfo = new BleDeviceInfo(device);
						bleDeviceInfo.setConnecting(false);
						bleDeviceInfo.setIsconnected(true);
						bleDeviceInfo.setDisconnecting(false);
						bleDeviceInfo.setDisconnected(false);
						if (deviceInfo.isDeselected()) {
							bleDeviceInfo.setDeselected(true);
						} else {
							bleDeviceInfo.setDeselected(false);
						}
						bleDeviceInfo.setDeviceFound(true);
						deviceList.set(selectedDevicePosition, bleDeviceInfo);

						startDeviceActivity(device);
					}

					// startDeviceActivity();
				} else {

					// System.out.println("0");

				}
			}

			else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
				// GATT disconnect
				int status = intent.getIntExtra(
						BluetoothLeService.EXTRA_STATUS,
						BluetoothGatt.GATT_SUCCESS);

				stopDeviceActivity();
				// if (status == BluetoothGatt.GATT_SUCCESS) {
				setBusy(false);
				stopTimers(false);

				if (deviceNames.containsKey(name)) {
					// System.out.println("device contains " + name);
					int selectedDevicePosition = deviceNames.get(name);
					BleDeviceInfo deviceInfo = deviceList
							.get(selectedDevicePosition);
					BluetoothDevice device = deviceInfo.getBluetoothDevice();

					BleDeviceInfo bleDeviceInfo = new BleDeviceInfo(device);
					bleDeviceInfo.setConnecting(false);
					bleDeviceInfo.setIsconnected(false);
					bleDeviceInfo.setDisconnecting(false);
					bleDeviceInfo.setDisconnected(true);
					if (deviceInfo.isDeselected()) {
						bleDeviceInfo.setDeselected(true);

						bleDeviceInfo.setDeviceDeselected(true);

						// System.out.println("deselectd " +
						// "      ........true");
					} else {
						bleDeviceInfo.setDeselected(false);
						// System.out
						// .println("deselectd " + "      ........false");
					}
					bleDeviceInfo.setDeviceFound(true);

					deviceList.set(selectedDevicePosition, bleDeviceInfo);
				}

				mConnIndex = NO_DEVICE;

				// System.out.println("device disconnected" + "disconnected");

				peripheral.close();

			} else {

				// System.out.println("unknown action");

				// Log.w(TAG,"Unknown action: " + action);
			}

			mDeviceAdapter.notifyDataSetChanged();
		}
	};

	private BleDeviceInfo findDeviceInfo(BluetoothDevice device) {
		for (int i = 0; i < mDeviceInfoList.size(); i++) {

			if (mDeviceInfoList.get(i).getBluetoothDevice() != null) {
				if (mDeviceInfoList.get(i).getBluetoothDevice().getAddress()
						.equals(device.getAddress())) {
					return mDeviceInfoList.get(i);
				}
			}

		}
		return null;
	}

	// Code to manage Service life cycle.
	private final ServiceConnection mServiceConnection = new ServiceConnection() {

		public void onServiceConnected(ComponentName componentName,
				IBinder service) {
			mBluetoothLeService = ((BluetoothLeService.LocalBinder) service)
					.getService();
			if (!mBluetoothLeService.initialize()) {
				Toast.makeText(getActivity(),
						"Unable to initialize BluetoothLeService",
						Toast.LENGTH_SHORT).show();
				// finish();
				return;
			}
			final int n = mBluetoothLeService.numConnectedDevices();
			if (n > 0) {
				getActivity().runOnUiThread(new Runnable() {
					public void run() {
						setError("Multiple connections!");
					}
				});
			} else {
				startScan();
				// Log.i(TAG, "BluetoothLeService connected");
			}
		}

		public void onServiceDisconnected(ComponentName componentName) {
			mBluetoothLeService = null;
			// Log.i(TAG, "BluetoothLeService disconnected");
		}
	};

	// Device scan callback.
	// NB! Nexus 4 and Nexus 7 (2012) only provide one scan result per scan
	private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {

		public void onLeScan(final BluetoothDevice device, final int rssi,
				byte[] scanRecord) {

			isDeviceFound = true;
			System.out.println();
			getActivity().runOnUiThread(new Runnable() {
				public void run() {
					// Filter devices

					String deviceName = device.getAddress();
					if (checkDeviceFilter(device.getName())) {
						scanedDeviceNames.add(deviceName);
						if (!deviceInfoExists(device.getAddress())) {
							// New device
							BleDeviceInfo deviceInfo = createDeviceInfo(device,
									rssi);
							String custom_name = "";

							if (device.getName().equalsIgnoreCase("SensorTag")) {
								custom_name = "Sensor Tag";
							} else if (device.getName().equalsIgnoreCase(
									"pH-Meter")) {
								custom_name = "pH";
							}

							if (isDeviceExistsinDB(device)) {
								updateRssi(device, rssi);
							} else {
								insertDeviceInfoDetails(device, rssi,
										custom_name, custom_name);
							}

							addDevice(deviceInfo);

						} else {
							// Already in list, update RSSI info
							BleDeviceInfo deviceInfo = findDeviceInfo(device);
							deviceInfo.updateRssi(rssi);
							if (deviceInfo.isDisconnected()) {
								deviceInfo.setDeselected(true);

								notifyDataSetChanged();

								// System.out
								// .println("notifydatasetchanged in else");

							}

						}

						for (int i = 0; i < deviceList.size(); i++) {

							String nameOfDevice = deviceList.get(i)
									.getBluetoothDevice().getAddress();

							// System.out.println("main list: "+nameOfDevice);
							BleDeviceInfo deviceInfo = findDeviceInfo(deviceList
									.get(i).getBluetoothDevice());
							if (scanedDeviceNames.contains(nameOfDevice)) {

								deviceInfo.setDeviceFound(true);

								// System.out.println(deviceInfo
								// .getBluetoothDevice().getAddress()
								// + " is found");

							} else {

								if (!deviceInfo.isConnected()) {
									deviceInfo.setDeviceFound(false);
								}

								// System.out.println(deviceInfo
								// .getBluetoothDevice().getAddress()
								// + " is not found");
							}

						}

					}

				}

			});

		}

	};

	/****
	 * 
	 * @param device
	 * @param rssi
	 * @param custom_name
	 *            insert device information into database.
	 */
	public void insertDeviceInfoDetails(BluetoothDevice device, int rssi,
			String custom_name, String deviceType) {
		db.open();
		db.insertDeviceInfo(device.getAddress(), device.getName(), custom_name,
				rssi + "", "", "", "", "", "", "", deviceType, "0");
		db.close();
	}

	/***
	 * 
	 * check whether device already exists in db.
	 */

	public boolean isDeviceExistsinDB(BluetoothDevice device) {

		boolean isExists;
		db.open();
		isExists = db.isdeviceExists(device.getAddress());
		db.close();

		return isExists;
	}

	/****
	 * 
	 * @author update rssi in db for particular device address
	 * 
	 */
	public void updateRssi(BluetoothDevice device, int rssi) {
		db.open();
		db.updateDeviceInfo(device.getAddress(), Constants.RSSI_TAG, rssi + "");
		db.close();
	}

	//
	// CLASS DeviceAdapter: handle device list
	//
	class DeviceListAdapter extends BaseAdapter {
		private List<BleDeviceInfo> mDevices;
		private LayoutInflater mInflater;

		public DeviceListAdapter(Context context, List<BleDeviceInfo> devices) {
			mInflater = LayoutInflater.from(context);
			mDevices = devices;
		}

		public int getCount() {
			return mDevices.size();
		}

		public Object getItem(int position) {
			return mDevices.get(position);
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(final int position, View convertView,
				ViewGroup parent) {
			ViewGroup vg;

			if (convertView != null) {
				vg = (ViewGroup) convertView;
			} else {
				vg = (ViewGroup) mInflater.inflate(R.layout.element_device,
						null);
			}

			System.out.println("devices list adapter getview");
			LinearLayout view_layout = (LinearLayout) vg
					.findViewById(R.id.view_layout);

			BleDeviceInfo deviceInfo = mDevices.get(position);
			BluetoothDevice device = deviceInfo.getBluetoothDevice();
			int rssi = deviceInfo.getRssi();

			TextView small_device_text = (TextView) vg
					.findViewById(R.id.small_device_text);

			// Disable connect button when connecting or connected
			ImageView bv = (ImageView) vg.findViewById(R.id.btnConnect);

			bv.setEnabled(false);

			String name = null;

			if (device != null) {
				String deviceName = device.getName();
				if (deviceName.equalsIgnoreCase("SensorTag")) {
					name = "TI BLE Sensor Tag";
					view_layout.setBackgroundResource(R.drawable.device_bg);
					bv.setBackgroundResource(R.drawable.white_button);
					// get device information details

					small_device_text.setText("Sensor Tag");

					small_device_text.setTextColor(Color.parseColor("#EFBBCB"));

				} else if (deviceName.equalsIgnoreCase("pH-Meter")) {
					name = "pH Meter";
					view_layout.setBackgroundResource(R.drawable.ph_device_bg);
					bv.setBackgroundResource(R.drawable.white_button);

					small_device_text.setText("pH");
					small_device_text.setTextColor(Color.parseColor("#C1E1BC"));
				}
			}

			((TextView) vg.findViewById(R.id.device_name)).setText(name);
			ProgressBar progress_bar = (ProgressBar) vg
					.findViewById(R.id.connecting_bar);
			progress_bar.setVisibility(View.GONE);

			if (!deviceInfo.isDisconnected() && !deviceInfo.isDeviceFound()) {
				view_layout.setBackgroundResource(R.drawable.grey_background);
				bv.setBackgroundResource(R.drawable.cancel);
				small_device_text.setTextColor(Color.parseColor("#DEE0E2"));
			} else if (deviceInfo.isConnecting() && deviceInfo.isDeviceFound()) {
				progress_bar.setVisibility(View.VISIBLE);
				bv.setBackgroundResource(R.drawable.white_button);
			} else if (deviceInfo.isConnecting() && !deviceInfo.isDeviceFound()) {
				progress_bar.setVisibility(View.VISIBLE);
				bv.setBackgroundResource(R.drawable.white_button);
				view_layout.setBackgroundResource(R.drawable.grey_background);
				small_device_text.setTextColor(Color.parseColor("#DEE0E2"));
			} else if (deviceInfo.isConnected()) {
				progress_bar.setVisibility(View.GONE);
				bv.setBackgroundResource(R.drawable.check_green);
				// System.out.println("is connected " + "connected"
				// + deviceInfo.getBluetoothDevice().getName());

			} else if (deviceInfo.isDisconnected() && deviceInfo.isDeselected()
					&& deviceInfo.isDeviceFound()) {
				bv.setBackgroundResource(R.drawable.white_button);
				// System.out.println("is disselected " + "disselected"
				// + deviceInfo.getBluetoothDevice().getName());
				notifyDeviceDisconnect(deviceInfo);
			} else if (deviceInfo.isDisconnected() && deviceInfo.isDeselected()
					&& !deviceInfo.isDeviceFound()) {
				view_layout.setBackgroundResource(R.drawable.grey_background);
				bv.setBackgroundResource(R.drawable.cancel);
				small_device_text.setTextColor(Color.parseColor("#DEE0E2"));
				notifyDeviceDisconnect(deviceInfo);
			} else if (deviceInfo.isDisconnected()
					&& !deviceInfo.isDeviceFound()) {
				view_layout.setBackgroundResource(R.drawable.grey_background);
				bv.setBackgroundResource(R.drawable.exclamatory);
				small_device_text.setTextColor(Color.parseColor("#DEE0E2"));
				notifyDeviceDisconnect(deviceInfo);
			} else if (deviceInfo.isDisconnected()) {
				bv.setBackgroundResource(R.drawable.exclamatory);
				notifyDeviceDisconnect(deviceInfo);
			}

			return vg;
		}
	}

	/***** will get called when device disconnected or disscelected *****/
	private void notifyDeviceDisconnect(BleDeviceInfo bleDeviceInfo) {
		if ((bleDeviceInfo.isDisconnected() && !bleDeviceInfo.isDeviceFound())
				|| (bleDeviceInfo.isDisconnected() && bleDeviceInfo
						.isDeselected())) { // Hide the device information
											// layout

			mDeviceDisconnectListener.onDeviceDeselect(bleDeviceInfo
					.getBluetoothDevice().getName(), bleDeviceInfo
					.getBluetoothDevice().getAddress());
		} else if (bleDeviceInfo.isDisconnected()) { // show Instrument
														// disconnected
			mDeviceDisconnectListener.onDeviceDisconnect(bleDeviceInfo
					.getBluetoothDevice().getName(), bleDeviceInfo
					.getBluetoothDevice().getAddress());
		}
	}

	public List<BleDeviceInfo> getBleDevices() {
		return deviceList;
	}
}