package com.dataworks;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.dataworks.database.DataBaseHelper;
import com.dataworks.util.AddClickListener;
import com.dataworks.util.Constants;
import com.dataworks.util.Global;

public class DeviceInfoFragment extends Fragment {

	public static DeviceInfoFragment termsFragment;

	public static DeviceInfoFragment newInstance() {
		termsFragment = new DeviceInfoFragment();
		return termsFragment;
	}

	public String mDeviceAddress;

	private TextView back, done_info;
	public static TextView rssi;
	private EditText device_name;
	private TextView st_manufacturer_name, battery_level, st_serial_number,
			st_model_number, st_firmware_revision, st_hardware_revision;

	private String deviceCustomName, Rssi, forwardRevision, manuFacturerName,
			hardWareRevision, serialNumber, modelNumber, batteryLevel;

	public static DeviceInfoFragment getInstance() {

		return termsFragment;
	}

	public void setmDeviceAddress(String deviceAddress) {
		mDeviceAddress = deviceAddress;
	}

	private DataBaseHelper db;
	private AddClickListener mClickListener;

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);

		if (activity instanceof AddClickListener) {
			mClickListener = (AddClickListener) activity;
		} else {
			throw new IllegalArgumentException(
					"Activity must implement BluetoothClickListener");
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		View view = inflater.inflate(R.layout.device_info_fragment, container,
				false);

		back = (TextView) view.findViewById(R.id.back);
		done_info = (TextView) view.findViewById(R.id.done_info);
		device_name = (EditText) view.findViewById(R.id.device_name);
		rssi = (TextView) view.findViewById(R.id.rssi);

		st_manufacturer_name = (TextView) view
				.findViewById(R.id.st_manufacturer_name);
		battery_level = (TextView) view.findViewById(R.id.battery_level);

		st_serial_number = (TextView) view.findViewById(R.id.st_serial_number);
		st_model_number = (TextView) view.findViewById(R.id.st_model_number);
		st_firmware_revision = (TextView) view
				.findViewById(R.id.st_firmware_revision);
		st_hardware_revision = (TextView) view
				.findViewById(R.id.st_hardware_revision);

		return view;
	}

	@Override
	public void onActivityCreated(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		getDeviceInfoFromDB(mDeviceAddress);

		device_name.setText(deviceCustomName);

		rssi.setText(Rssi);

		st_manufacturer_name.setText(manuFacturerName);

		st_serial_number.setText(serialNumber);

		st_model_number.setText(modelNumber);

		st_firmware_revision.setText(forwardRevision);

		st_hardware_revision.setText(hardWareRevision);

		battery_level.setText(batteryLevel);

		device_name.setOnFocusChangeListener(new OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				// TODO Auto-generated method stub

				device_name.setCursorVisible(true);
				done_info.setVisibility(View.VISIBLE);
			}
		});

		device_name.setOnTouchListener(new View.OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// TODO Auto-generated method stub
				device_name.setCursorVisible(true);
				done_info.setVisibility(View.VISIBLE);
				return false;
			}
		});

		done_info.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				String deviceName = device_name.getText().toString();
				updateDeviceCustomName(mDeviceAddress, deviceName);

				done_info.setVisibility(View.GONE);

				device_name.setCursorVisible(false);

				try {
					Global.hideSoftKeyboard(getActivity());
				} catch (Exception e) {
					// TODO: handle exception
				}

				mClickListener.onSensorDoneClick(mDeviceAddress);

			}
		});
		back.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getActivity().onBackPressed();
			}
		});

	}

	/***** get device info based on address ****/
	public void getDeviceInfoFromDB(String deviceAddress) {
		db = new DataBaseHelper(getActivity());
		db.open();

		Cursor deviceInfo = db.getDeviceInfoAddress(deviceAddress);

		if (deviceInfo.moveToFirst()) {
			do {

				deviceCustomName = deviceInfo.getString(0);
				Rssi = deviceInfo.getString(1);
				forwardRevision = deviceInfo.getString(2);
				manuFacturerName = deviceInfo.getString(3);
				hardWareRevision = deviceInfo.getString(4);
				serialNumber = deviceInfo.getString(5);
				modelNumber = deviceInfo.getString(6);
				batteryLevel = deviceInfo.getString(7);

			} while (deviceInfo.moveToNext());
		}
		deviceInfo.close();
		db.close();
	}

	/**** update device name customised in database ****/
	public void updateDeviceCustomName(String deviceAddress, String customName) {
		db = new DataBaseHelper(getActivity());
		db.open();
		db.updateDeviceInfo(deviceAddress, Constants.DEVICE_CUSTOM_NAME,
				customName);
		db.close();
	}

}
