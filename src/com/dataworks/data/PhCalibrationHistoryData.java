package com.dataworks.data;

public class PhCalibrationHistoryData {

	public String mId;
	public String getmId() {
		return mId;
	}
	public void setmId(String mId) {
		this.mId = mId;
	}
	public String date;
	public String time;
	public String slope;
	public String intercept;
	public String ph4;
	public String ph7;
	public String ph10;
	public String no_values;
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getSlope() {
		return slope;
	}
	public void setSlope(String slope) {
		this.slope = slope;
	}
	public String getIntercept() {
		return intercept;
	}
	public void setIntercept(String intercept) {
		this.intercept = intercept;
	}
	public String getPh4() {
		return ph4;
	}
	public void setPh4(String ph4) {
		this.ph4 = ph4;
	}
	public String getPh7() {
		return ph7;
	}
	public void setPh7(String ph7) {
		this.ph7 = ph7;
	}
	public String getPh10() {
		return ph10;
	}
	public void setPh10(String ph10) {
		this.ph10 = ph10;
	}
	public String getNo_values() {
		return no_values;
	}
	public void setNo_values(String no_values) {
		this.no_values = no_values;
	}
	
}
