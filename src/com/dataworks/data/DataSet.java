package com.dataworks.data;

import java.util.ArrayList;
import java.util.Collections;

public class DataSet {

	public String mId;

	public String mName;
	public String mNotes;
	public String mLocation;
	public String mDate;
	public String mTime;
	public String mDateAndTime;
	public Values mValues;

	public ArrayList<Values> valueList = new ArrayList<Values>();

	/*****
	 * 
	 * @param values
	 *            delete the selected datapoint from dataset based on
	 *            time(unique id)
	 */
	public void deleteValuePoint(Values values) {

		String datapointId = values.getmTime();

		for (int i = 0; i < valueList.size(); i++) {

			if (valueList.get(i).getmTime().equalsIgnoreCase(datapointId)) {
				this.valueList.remove(valueList.get(i));
			}

		}

	}

	/*****
	 * 
	 * @param values
	 *            update the name and notes selected datapoint from dataset
	 *            based on time(unique id)
	 */

	public void updateNameOrNotesOrBoth(Values values) {
		String datapointId = values.getmTime();

		for (int i = 0; i < valueList.size(); i++) {

			if (valueList.get(i).getmTime().equalsIgnoreCase(datapointId)) {
				this.valueList.set(i, values);
			}

		}
	}

	public ArrayList<Values> getValueList() {
		return valueList;
	}
	public void deleteTopValue(){
		valueList.remove((valueList.size()-1));
	}

	public void setValueList(Values values) {

		this.mValues = values;
		this.valueList.add(values);

	}
	
	public Values getValues(){
		return mValues;
	}

	public void setValuesDataList(ArrayList<Values> list) {
		this.valueList = list;
	}

	public String getmDateAndTime() {
		return mDateAndTime;
	}

	public void setmDateAndTime(String mDateAndTime) {
		this.mDateAndTime = mDateAndTime;
	}

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getmNotes() {
		return mNotes;
	}

	public void setmNotes(String mNotes) {
		this.mNotes = mNotes;
	}

	public String getmLocation() {
		return mLocation;
	}

	public void setmLocation(String mLocation) {
		this.mLocation = mLocation;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}

	public String getmTime() {
		return mTime;
	}

	public void setmTime(String mTime) {
		this.mTime = mTime;
	}

}
