package com.dataworks.data;

public class SensorTag {

	public String deviceName;
	public String deviceAddress;
	
	public boolean mDisconnected;



	public boolean ismDisconnected() {
		return mDisconnected;
	}

	public void setmDisconnected(boolean mDisconnected) {
		this.mDisconnected = mDisconnected;
	}

	public String magnetometerValue_x;
	public String magnetometerValue_y;
	public String magnetometerValue_z;
	
	public String accelerometerValue_x;
	public String accelerometerValue_y;
	public String accelerometerValue_z;
	
	public String gyroMeterValue_x;
	public String gyroMeterValue_y;
	public String gyroMeterValue_z;

	public String aTempValue;
	public String oTempValue;
	public String baroValue;
	public String humidity;
	
	public int progress_acc_x;
	public int progress_acc_y;
	public int progress_acc_z;
	
	public int progress_mag_x;
	public int progress_mag_y;
	public int progress_mag_z;
	
	public int progress_gyro_x;
	public int progress_gyro_y;
	public int progress_gyro_z;
	

	public int getProgress_mag_x() {
		return progress_mag_x;
	}

	public void setProgress_mag_x(int progress_mag_x) {
		this.progress_mag_x = progress_mag_x;
	}

	public int getProgress_mag_y() {
		return progress_mag_y;
	}

	public void setProgress_mag_y(int progress_mag_y) {
		this.progress_mag_y = progress_mag_y;
	}

	public int getProgress_mag_z() {
		return progress_mag_z;
	}

	public void setProgress_mag_z(int progress_mag_z) {
		this.progress_mag_z = progress_mag_z;
	}

	public int getProgress_gyro_x() {
		return progress_gyro_x;
	}

	public void setProgress_gyro_x(int progress_gyro_x) {
		this.progress_gyro_x = progress_gyro_x;
	}

	public int getProgress_gyro_y() {
		return progress_gyro_y;
	}

	public void setProgress_gyro_y(int progress_gyro_y) {
		this.progress_gyro_y = progress_gyro_y;
	}

	public int getProgress_gyro_z() {
		return progress_gyro_z;
	}

	public void setProgress_gyro_z(int progress_gyro_z) {
		this.progress_gyro_z = progress_gyro_z;
	}

	public int getProgress_acc_x() {
		return progress_acc_x;
	}

	public void setProgress_acc_x(int progress_acc_x) {
		this.progress_acc_x = progress_acc_x;
	}

	public int getProgress_acc_y() {
		return progress_acc_y;
	}

	public void setProgress_acc_y(int progress_acc_y) {
		this.progress_acc_y = progress_acc_y;
	}

	public int getProgress_acc_z() {
		return progress_acc_z;
	}

	public void setProgress_acc_z(int progress_acc_z) {
		this.progress_acc_z = progress_acc_z;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceAddress() {
		return deviceAddress;
	}

	public void setDeviceAddress(String deviceAddress) {
		this.deviceAddress = deviceAddress;
	}

	

	public String getMagnetometerValue_x() {
		return magnetometerValue_x;
	}

	public void setMagnetometerValue_x(String magnetometerValue_x) {
		this.magnetometerValue_x = magnetometerValue_x;
	}

	public String getMagnetometerValue_y() {
		return magnetometerValue_y;
	}

	public void setMagnetometerValue_y(String magnetometerValue_y) {
		this.magnetometerValue_y = magnetometerValue_y;
	}

	public String getMagnetometerValue_z() {
		return magnetometerValue_z;
	}

	public void setMagnetometerValue_z(String magnetometerValue_z) {
		this.magnetometerValue_z = magnetometerValue_z;
	}

	public String getAccelerometerValue_x() {
		return accelerometerValue_x;
	}

	public void setAccelerometerValue_x(String accelerometerValue_x) {
		this.accelerometerValue_x = accelerometerValue_x;
	}

	public String getAccelerometerValue_y() {
		return accelerometerValue_y;
	}

	public void setAccelerometerValue_y(String accelerometerValue_y) {
		this.accelerometerValue_y = accelerometerValue_y;
	}

	public String getAccelerometerValue_z() {
		return accelerometerValue_z;
	}

	public void setAccelerometerValue_z(String accelerometerValue_z) {
		this.accelerometerValue_z = accelerometerValue_z;
	}

	public String getGyroMeterValue_x() {
		return gyroMeterValue_x;
	}

	public void setGyroMeterValue_x(String gyroMeterValue_x) {
		this.gyroMeterValue_x = gyroMeterValue_x;
	}

	public String getGyroMeterValue_y() {
		return gyroMeterValue_y;
	}

	public void setGyroMeterValue_y(String gyroMeterValue_y) {
		this.gyroMeterValue_y = gyroMeterValue_y;
	}

	public String getGyroMeterValue_z() {
		return gyroMeterValue_z;
	}

	public void setGyroMeterValue_z(String gyroMeterValue_z) {
		this.gyroMeterValue_z = gyroMeterValue_z;
	}

	public String getaTempValue() {
		return aTempValue;
	}

	public void setaTempValue(String aTempValue) {
		this.aTempValue = aTempValue;
	}

	public String getoTempValue() {
		return oTempValue;
	}

	public void setoTempValue(String oTempValue) {
		this.oTempValue = oTempValue;
	}

	public String getBaroValue() {
		return baroValue;
	}

	public void setBaroValue(String baroValue) {
		this.baroValue = baroValue;
	}

	public String getHumidity() {
		return humidity;
	}

	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}

}
