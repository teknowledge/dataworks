package com.dataworks.data;

public class RegressionResult {

	
	public double slope;
	public double intercept;
	public double correlation;
}
