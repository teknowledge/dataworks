package com.dataworks.data;

public class AlarmData {

	public boolean isHighOn;
	public boolean isLowOn;
	
	public boolean isBeepPlaying;
	public boolean isAlarmStopped;
	
	public String highValue;
	public String lowValue;

	public boolean isHighOn() {
		return isHighOn;
	}

	public void setHighOn(boolean isHighOn) {
		this.isHighOn = isHighOn;
	}

	public boolean isLowOn() {
		return isLowOn;
	}

	public void setLowOn(boolean isLowOn) {
		this.isLowOn = isLowOn;
	}

	public boolean isBeepPlaying() {
		return isBeepPlaying;
	}

	public void setBeepPlaying(boolean isBeepPlaying) {
		this.isBeepPlaying = isBeepPlaying;
	}

	public boolean isAlarmStopped() {
		return isAlarmStopped;
	}

	public void setAlarmStopped(boolean isAlarmStopped) {
		this.isAlarmStopped = isAlarmStopped;
	}

	public String getHighValue() {
		return highValue;
	}

	public void setHighValue(String highValue) {
		this.highValue = highValue;
	}

	public String getLowValue() {
		return lowValue;
	}

	public void setLowValue(String lowValue) {
		this.lowValue = lowValue;
	}
	
	
}
