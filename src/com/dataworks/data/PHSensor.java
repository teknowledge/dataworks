package com.dataworks.data;

public class PHSensor {

	public String deviceName;
	public String deviceAddress;
	public boolean mDisconected;
	public boolean isTempProbeConnected;
	public String mV;
	

	/**
	 * @return the mV
	 */
	public String getmV() {
		return mV;
	}

	/**
	 * @param mV the mV to set
	 */
	public void setmV(String mV) {
		this.mV = mV;
	}

	/**
	 * @return the isTempProbeConnected
	 */
	public boolean isTempProbeConnected() {
		return isTempProbeConnected;
	}

	/**
	 * @param isTempProbeConnected the isTempProbeConnected to set
	 */
	public void setTempProbeConnected(boolean isTempProbeConnected) {
		this.isTempProbeConnected = isTempProbeConnected;
	}

	/**
	 * @return the mDisconected
	 */
	public boolean ismDisconected() {
		return mDisconected;
	}

	/**
	 * @param mDisconected the mDisconected to set
	 */
	public void setmDisconected(boolean mDisconected) {
		this.mDisconected = mDisconected;
	}

	public String pHValue;
	public String high_alarm;
	public String low_alarm;
	public String mAtcValue;
	public String mMtcValue;
	public String atc_text;
	
	/**
	 * @return the temp_value
	 */
	public String getTemp_value() {
		return temp_value;
	}

	/**
	 * @param temp_value the temp_value to set
	 */
	public void setTemp_value(String temp_value) {
		this.temp_value = temp_value;
	}

	public String temp_value;

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceAddress() {
		return deviceAddress;
	}

	public void setDeviceAddress(String deviceAddress) {
		this.deviceAddress = deviceAddress;
	}

	public String getpHValue() {
		return pHValue;
	}

	public void setpHValue(String pHValue) {
		this.pHValue = pHValue;
	}

	public String getHigh_alarm() {
		return high_alarm;
	}

	public void setHigh_alarm(String high_alarm) {
		this.high_alarm = high_alarm;
	}

	public String getLow_alarm() {
		return low_alarm;
	}

	public void setLow_alarm(String low_alarm) {
		this.low_alarm = low_alarm;
	}

	public String getmAtcValue() {
		return mAtcValue;
	}

	public void setmAtcValue(String mAtcValue) {
		this.mAtcValue = mAtcValue;
	}

	public String getmMtcValue() {
		return mMtcValue;
	}

	public void setmMtcValue(String mMtcValue) {
		this.mMtcValue = mMtcValue;
	}

	public String getAtc_text() {
		return atc_text;
	}

	public void setAtc_text(String atc_text) {
		this.atc_text = atc_text;
	}

}
