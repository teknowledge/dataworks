package com.dataworks.data;

import java.util.ArrayList;
import java.util.Comparator;

public class Values implements Comparable<Values> {

	public String mDataSetId;
	public String mName;
	public String mNotes;
	public String mDate;
	public String mTime;
	public String mLocation;

	public String mId;

	public ArrayList<SensorTag> sensorTagsList;
	public ArrayList<PHSensor> pHSensorsList;

	public ArrayList<SensorTag> getSensorTagsList() {
		return sensorTagsList;
	}

	public void setSensorTagsList(ArrayList<SensorTag> sensorTagsList) {
		this.sensorTagsList = sensorTagsList;
	}

	public ArrayList<PHSensor> getpHSensorsList() {
		return pHSensorsList;
	}

	public void setpHSensorsList(ArrayList<PHSensor> pHSensorsList) {
		this.pHSensorsList = pHSensorsList;
	}

	public String getmId() {
		return mId;
	}

	public void setmId(String mId) {
		this.mId = mId;
	}

	public String getmDataSetId() {
		return mDataSetId;
	}

	public void setmDataSetId(String mDataSetId) {
		this.mDataSetId = mDataSetId;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}

	public String getmNotes() {
		return mNotes;
	}

	public void setmNotes(String mNotes) {
		this.mNotes = mNotes;
	}

	public String getmDate() {
		return mDate;
	}

	public void setmDate(String mDate) {
		this.mDate = mDate;
	}

	public String getmTime() {
		return mTime;
	}

	public void setmTime(String mTime) {
		this.mTime = mTime;
	}

	public String getmLocation() {
		return mLocation;
	}

	public void setmLocation(String mLocation) {
		this.mLocation = mLocation;
	}

	@Override
	public int compareTo(Values another) {
		// TODO Auto-generated method stub

		long left = Long.parseLong(this.getmTime());
		long right = Long.parseLong(another.getmTime());

		final int BEFORE = 1;
		final int EQUAL = 0;
		final int AFTER = -1;

		// if (this == another) return 0;

		if (left > right) {
			return AFTER;
		} else if (left < right) {
			return BEFORE;
		} else {
			return EQUAL;
		}
	}

}
