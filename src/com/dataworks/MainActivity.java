package com.dataworks;

import static java.util.UUID.fromString;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.dataworks.adapter.PHSensorListInflater;
import com.dataworks.adapter.SensorTagAdapter;
import com.dataworks.bluetooth.service.BluetoothLeService;
import com.dataworks.bluetooth.service.GattInfo;
import com.dataworks.data.AlarmData;
import com.dataworks.data.DataSet;
import com.dataworks.data.PHSensor;
import com.dataworks.data.PhCalibrationHistoryData;
import com.dataworks.data.SensorTag;
import com.dataworks.data.Values;
import com.dataworks.database.DataBaseHelper;
import com.dataworks.database.DataBaseSingleTon;
import com.dataworks.database.DevicesDataBaseManager;
import com.dataworks.helper.PHThread;
import com.dataworks.slidemenu.SlidingMenu;
import com.dataworks.slidemenu.SlidingMenu.OnOpenedListener;
import com.dataworks.util.AddClickListener;
import com.dataworks.util.BluetoothClickListener;
import com.dataworks.util.Constants;
import com.dataworks.util.DeviceDisconnectListener;
import com.dataworks.util.Global;
import com.dataworks.util.NestedListView;
import com.dataworks.util.Point3D;
import com.dataworks.util.Utils;

public class MainActivity extends BaseFragmentActivity implements
		android.view.View.OnClickListener, BluetoothClickListener,
		AddClickListener, DeviceDisconnectListener {

	SlidingMenu slidingMenu;
	private ImageButton menu_button;
	private ImageView glow_animation_view;
	private ProgressBar connecting_status;
	private FrameLayout mDataSetsFragment;
	private FrameLayout mValuesFragment;
	private FrameLayout mAddDatasetFragment;
	private FrameLayout mValueDetailsFragment;
	private FrameLayout device_info_layout;
	private FrameLayout caliber_fragment;
	private FrameLayout dataset_info_frag;
	private FrameLayout pH_caliber_fragment;
	private FrameLayout pH_caliber_history;
	private FrameLayout pH_caliber_history_details;
	private FrameLayout pH_info_fragment;
	FragmentManager mFragmentManager;

	public static MainActivity mActivity;
	int sample_points;
	int sample_time_x;

	// BLE
	private BluetoothLeService mBtLeService = null;
	private BluetoothDevice mBluetoothDevice = null;
	private List<BluetoothGattService> mServiceList = null;
	private BluetoothGatt mBtGatt = null;
	private static final int GATT_TIMEOUT = 250; // milliseconds
	private boolean mServicesRdy = false;
	private boolean mIsReceiving = false;
	// SensorTagGatt
	private List<Sensor> mEnabledSensors = new ArrayList<Sensor>();
	private BluetoothGattService mOadService = null;
	private BluetoothGattService mConnControlService = null;

	private boolean mMagCalibrateRequest = true;
	private boolean mHeightCalibrateRequest = true;

	public static boolean isCaliberateMag;
	public static boolean isInsideCaliber;

	public static boolean isCaliberGyro;
	public static boolean isInsideCaliberGyro;

	private String mFwRev;

	// House-keeping
	private DecimalFormat decimal = new DecimalFormat("0.0;-0.0");
	private DecimalFormat pH_decimal = new DecimalFormat("00.00;-00.00");
	private DecimalFormat decimal_2 = new DecimalFormat("0.00");
	private DecimalFormat temp_decimal = new DecimalFormat("0.00");
	private DecimalFormat temp_decimal1 = new DecimalFormat("0.0");

	private TextView mDatasets, mValues, dataset_title;

	private static final double PA_PER_METER = 12.0;

	private ImageButton record_button;
	private TextView sample_time_text, sample_text;
	public static View line_instrument;
	private String dataSetId, dataSetName;

	private float pH = 0.0f;
	private float temp = 0.0f;
	private String pH_data;
	private String pH_temp_data;

	private DataBaseHelper db;
	DateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

	public boolean isStopped;
	private int delayInMSec;
	private int numberOfPoints;
	private boolean isRecordPressed, isStarted, isRecordSwitched;
	private boolean OffToggleWhileIRRunning;
	public static boolean startCaiber, stopCaliber;
	private int i = 0;
	private Animation animAlpha;

	private boolean intervalToggle;

	private MenuFragment menuFragment;

	NumberPicker leftTempPicker, rightTempPicker;

	private FrameLayout picker_parent_layout;
	private Button picker_shadow_dummy_layout;
	TextView pickerDone;

	private String pH_mV_Value;

	public Utils utils = new Utils();

	public MediaPlayer m = new MediaPlayer();

	private ArrayList<SensorTag> sensorTagsList = new ArrayList<SensorTag>();
	private ArrayList<PHSensor> pHSensorsList = new ArrayList<PHSensor>();

	private NestedListView sensorTag_listView;

	private LinearLayout pH_listView_parentLayout;

	private SensorTagAdapter mSensorAdapter;

	private PHSensorListInflater mPHListInflater;

	private DevicesDataBaseManager mDatabaseManager = new DevicesDataBaseManager(
			MainActivity.this);
	private Context mContext;

	boolean samplesPressedValue;
	boolean instructionsPressedValue;
	boolean timePressedValue;
	private boolean IRDialogOpened;

	public DataSet curDataSet;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		DataBaseSingleTon.sContext = mContext;

		db = DataBaseSingleTon.getDB();

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		mActivity = new MainActivity();
		slidingMenu = getSlidingMenu();
		// slidingMenu.setMode(SlidingMenu.LEFT_RIGHT);
		slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
		slidingMenu.setShadowDrawable(R.drawable.shadow);

		slidingMenu.setMenu(R.layout.menu_frame);

		setContentView(R.layout.fragment_main);

		mFragmentManager = getSupportFragmentManager();

		mDatasets = (TextView) findViewById(R.id.datasets);
		mValues = (TextView) findViewById(R.id.values);
		dataset_title = (TextView) findViewById(R.id.dataset_title);
		sample_time_text = (TextView) findViewById(R.id.sample_time_text);
		sample_text = (TextView) findViewById(R.id.sample_text);
		glow_animation_view = (ImageView) findViewById(R.id.animatedImage);

		sensorTag_listView = (NestedListView) findViewById(R.id.sensorTag_listView);

		pH_listView_parentLayout = (LinearLayout) findViewById(R.id.ph_listview_parent_layout);

		record_button = (ImageButton) findViewById(R.id.record_button);
		connecting_status = (ProgressBar) findViewById(R.id.connecting_status);

		mDataSetsFragment = (FrameLayout) findViewById(R.id.datasets_layout);
		mValuesFragment = (FrameLayout) findViewById(R.id.values_layout);
		mAddDatasetFragment = (FrameLayout) findViewById(R.id.add_dataset_layout);
		mValueDetailsFragment = (FrameLayout) findViewById(R.id.values_details_layout);
		device_info_layout = (FrameLayout) findViewById(R.id.device_info_layout);
		dataset_info_frag = (FrameLayout) findViewById(R.id.dataset_info_frag);
		caliber_fragment = (FrameLayout) findViewById(R.id.caliber_fragment);
		pH_caliber_fragment = (FrameLayout) findViewById(R.id.pH_caliber_fragment);
		pH_caliber_history = (FrameLayout) findViewById(R.id.pH_caliber_history);
		pH_caliber_history_details = (FrameLayout) findViewById(R.id.pH_caliber_history_details);
		pH_info_fragment = (FrameLayout) findViewById(R.id.pH_info_fragment);
		leftTempPicker = (NumberPicker) findViewById(R.id.left_temp_picker);
		rightTempPicker = (NumberPicker) findViewById(R.id.right_temp_picker);
		picker_parent_layout = (FrameLayout) findViewById(R.id.picker_parent_layout);
		picker_shadow_dummy_layout = (Button) findViewById(R.id.picker_shadow_dummy_layout);
		pickerDone = (TextView) findViewById(R.id.picker_done_tv);

		animAlpha = AnimationUtils.loadAnimation(this, R.anim.anim_alpha);

		initializeUI();

		menuFragment = (MenuFragment) mFragment;

	}

	/****
	 * initializing Ui elements and creating dataset empty on first launch
	 */
	public void initializeUI() {

		dataSetName = Global.getInstance().getPreferenceVal(
				getApplicationContext(), "datasetName");

		dataSetId = Global.getInstance().getPreferenceVal(
				getApplicationContext(), "datasetId");

		if (dataSetName.equalsIgnoreCase("")) {
			// dataset_title.setText("New DataSet");

			Calendar cal = Calendar.getInstance();
			// System.out.println(dateFormat.format(cal.getTime()));
			String dateAndTime[] = dateFormat.format(cal.getTime()).split(" ");
			String date = dateAndTime[0];
			String time = dateAndTime[1];

			insertData("New Dataset", "", date, time);

			Global.getInstance().storeIntoPreference(MainActivity.this,
					"datasetName", "New Dataset");

			Global.getInstance().storeIntoPreference(MainActivity.this,
					"datasetId", "1");

			curDataSet = new DataSet();
			curDataSet.setmDate(date);
			curDataSet.setmTime(time);
			curDataSet.setmName("New DataSet");
			curDataSet.setmId("1");
			curDataSet.setmDateAndTime(String.valueOf(System
					.currentTimeMillis()));

			curDataSet.setmNotes("");

		} else {
			dataset_title.setText(dataSetName);

			new GetValuesForDataSet().execute();

		}

		menu_button = (ImageButton) findViewById(R.id.menu_button);
		menu_button.setOnClickListener(this);

		mDatasets.setOnClickListener(this);
		mValues.setOnClickListener(this);
		record_button.setOnClickListener(this);

		record_button.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub

				if (!IRDialogOpened) {
					openDialog();
				}

				return false;

			}
		});

		/****
		 * Long click on datasets will open datasets details screen
		 */

		mDatasets.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub
				if (!Global.getInstance()
						.getPreferenceVal(MainActivity.this, "datasetId")
						.equalsIgnoreCase("")) {

					onDataSetInfo(curDataSet, "from_main");
				}

				return true;
			}
		});

		/****
		 * Long click on values will take to the values details screen
		 */
		mValues.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				// TODO Auto-generated method stub

				if (!Global.getInstance()
						.getPreferenceVal(MainActivity.this, "datasetId")
						.equalsIgnoreCase("")) {

					String dataSetId = Global.getInstance().getPreferenceVal(
							MainActivity.this, "datasetId");
					Values values = getValuesForDataSetPoint(dataSetId);

					if (values.getmId() != null) {
						onDetailClick(values, "from_main");
					} else {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								MainActivity.this);
						builder.setTitle("No Values");
						builder.setMessage("Oops! There are no datapoints available.");
						builder.setPositiveButton("Ok",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int which) {
										Log.e("info", "OK");

									}
								});

						builder.show();
					}

				}

				return true;
			}
		});

		setPickerValues();

		pickerDone.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				final int reqPosition = (int) pickerDone.getTag();

				Animation fadeOutAnimation = AnimationUtils.loadAnimation(
						MainActivity.this, android.R.anim.fade_out);

				fadeOutAnimation
						.setAnimationListener(new Animation.AnimationListener() {

							@Override
							public void onAnimationStart(Animation animation) {
								// TODO Auto-generated method stub
								System.out.println();
							}

							@Override
							public void onAnimationRepeat(Animation animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationEnd(Animation animation) {
								// TODO Auto-generated method stub

								picker_shadow_dummy_layout
										.setVisibility(View.GONE);

							}
						});
				picker_shadow_dummy_layout.setAnimation(fadeOutAnimation);

				// picker_shadow_dummy_layout.setVisibility(View.GONE);

				Animation animation = AnimationUtils.loadAnimation(
						MainActivity.this, R.anim.slide_out_down);
				animation
						.setAnimationListener(new Animation.AnimationListener() {

							@Override
							public void onAnimationStart(Animation animation) {
								// TODO Auto-generated method stub
								System.out.println();
							}

							@Override
							public void onAnimationRepeat(Animation animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationEnd(Animation animation) {
								// TODO Auto-generated method stub

								picker_parent_layout.setVisibility(View.GONE);

								String mTCValue = leftTempPicker.getValue()
										+ "." + rightTempPicker.getValue();

								pH_temp_data = mTCValue;

								String deviceAddress = pHSensorsList.get(
										reqPosition).getDeviceAddress();

								updateTempProbeValueFordevice(deviceAddress,
										mTCValue);

								PHSensor pHSensor = pHSensorsList
										.get(reqPosition);

								pHSensor.setTemp_value(mTCValue);

								pHSensorsList.set(reqPosition, pHSensor);

								if (!pHSensor.ismDisconected()) {
									View pHSensorView = pH_listView_parentLayout
											.getChildAt(reqPosition);

									if (pHSensorView != null) {
										TextView temperature_type = (TextView) pHSensorView
												.findViewById(R.id.temperature_type);

										temperature_type.setText("MTC");

										TextView mtc_temp_value = (TextView) pHSensorView
												.findViewById(R.id.mtc_temp_value);
										mtc_temp_value.setText(pHSensorsList
												.get(reqPosition)
												.getTemp_value()
												+ "ºC");

									}
								}

							}
						});

				picker_parent_layout.startAnimation(animation);

			}
		});

		picker_shadow_dummy_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Animation fadeOutAnimation = AnimationUtils.loadAnimation(
						MainActivity.this, android.R.anim.fade_out);

				fadeOutAnimation
						.setAnimationListener(new Animation.AnimationListener() {

							@Override
							public void onAnimationStart(Animation animation) {
								// TODO Auto-generated method stub
								System.out.println();
							}

							@Override
							public void onAnimationRepeat(Animation animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationEnd(Animation animation) {
								// TODO Auto-generated method stub

								picker_shadow_dummy_layout
										.setVisibility(View.GONE);

							}
						});
				picker_shadow_dummy_layout.setAnimation(fadeOutAnimation);

				Animation animation = AnimationUtils.loadAnimation(
						MainActivity.this, R.anim.slide_out_down);
				animation
						.setAnimationListener(new Animation.AnimationListener() {

							@Override
							public void onAnimationStart(Animation animation) {
								// TODO Auto-generated method stub
								System.out.println();
							}

							@Override
							public void onAnimationRepeat(Animation animation) {
								// TODO Auto-generated method stub

							}

							@Override
							public void onAnimationEnd(Animation animation) {
								// TODO Auto-generated method stub

								picker_parent_layout.setVisibility(View.GONE);

							}
						});

				picker_parent_layout.startAnimation(animation);
			}
		});

		/****
		 * when slide gets opened, hide the dialogs menu
		 */
		slidingMenu.setOnOpenedListener(new OnOpenedListener() {

			@Override
			public void onOpened() {
				// TODO Auto-generated method stub

				for (int i = 0; i < sensorTagsList.size(); i++) {

					View view = getSensorTagView(i);

					if (view != null) {
						RelativeLayout info_inflate_scale_layout = (RelativeLayout) view
								.findViewById(R.id.info_inflate_scale_layout);
						info_inflate_scale_layout.setVisibility(View.GONE);
					}
				}

				for (int i = 0; i < pHSensorsList.size(); i++) {

					View view = pH_listView_parentLayout.getChildAt(i);

					if (view != null) {
						RelativeLayout pH_info_caliber_layout = (RelativeLayout) view
								.findViewById(R.id.pH_info_caliber_layout);
						pH_info_caliber_layout.setVisibility(View.GONE);
					}
				}

			}
		});

	}

	/****
	 * open the interval recording dailog when pressed on record button or
	 * sensor tag physical button
	 */

	private void openDialog() {

		// TODO Auto-generated method stub

		IRDialogOpened = true;

		final Dialog d = new Dialog(MainActivity.this);

		d.requestWindowFeature(Window.FEATURE_NO_TITLE);
		d.setContentView(R.layout.interval_record);
		d.show();
		d.getWindow().setBackgroundDrawable(
				new ColorDrawable(Color.TRANSPARENT));
		d.setCanceledOnTouchOutside(false);
		@SuppressWarnings("static-access")
		Display display = ((WindowManager) getSystemService(Context.WINDOW_SERVICE))
				.getDefaultDisplay();
		@SuppressWarnings("deprecation")
		int width = display.getWidth();
		@SuppressWarnings("deprecation")
		int height = display.getHeight();

		TextView cancel = (TextView) d.findViewById(R.id.cancel);
		TextView done = (TextView) d.findViewById(R.id.done);
		final TextView instructions = (TextView) d
				.findViewById(R.id.instructions);
		final TextView samples = (TextView) d.findViewById(R.id.samples);
		final TextView time = (TextView) d.findViewById(R.id.time);

		final EditText sample_time = (EditText) d
				.findViewById(R.id.sample_time);
		final EditText num_points = (EditText) d.findViewById(R.id.num_points);

		final TextView total_time = (TextView) d.findViewById(R.id.total_time);

		sample_time.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

				if (!s.toString().equalsIgnoreCase("0")
						&& !s.toString().equalsIgnoreCase("")
						&& !num_points.getText().toString()
								.equalsIgnoreCase("")
						&& !num_points.getText().toString()
								.equalsIgnoreCase("0")) {

					int sampleSec = Integer.parseInt(s.toString());

					int numberSec = Integer.parseInt(num_points.getText()
							.toString());

					String totalTime = Utils.splitToComponentTimes(sampleSec
							* numberSec);

					total_time.setText(totalTime);
				} else if (!s.toString().equalsIgnoreCase("0")
						&& !s.toString().equalsIgnoreCase("")
						&& (num_points.getText().toString()
								.equalsIgnoreCase("") || num_points.getText()
								.toString().equalsIgnoreCase("0"))) {
					total_time.setText("Recording will continue until stopped");
				}

				else {
					total_time.setText("Recording time not set");
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});
		num_points.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				// TODO Auto-generated method stub

				if (!s.toString().equalsIgnoreCase("0")
						&& !s.toString().equalsIgnoreCase("")
						&& !sample_time.getText().toString()
								.equalsIgnoreCase("")
						&& !sample_time.getText().toString()
								.equalsIgnoreCase("0")) {

					int sampleSec = Integer.parseInt(sample_time.getText()
							.toString());

					int numberSec = Integer.parseInt(s.toString());

					String totalTime = Utils.splitToComponentTimes(sampleSec
							* numberSec);

					total_time.setText(totalTime);
				} else if ((num_points.getText().toString()
						.equalsIgnoreCase("0") || num_points.getText()
						.toString().equalsIgnoreCase(""))
						&& (!s.toString().equalsIgnoreCase("") || !s.toString()
								.equalsIgnoreCase("0"))) {
					total_time.setText("Recording will continue until stopped");
				}

			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

		ToggleButton toggleBtn = (ToggleButton) d.findViewById(R.id.toggleBtn);

		Global.getInstance().storeBooleanType(MainActivity.this,
				"instructions", true);

		String timesInInterval = Global.getInstance().getPreferenceVal(
				MainActivity.this, "interval_time");
		if (!timesInInterval.equalsIgnoreCase("")
				&& !timesInInterval.equalsIgnoreCase("0")) {
			sample_time.setText(timesInInterval);
		}

		String pointsInterval = Global.getInstance().getPreferenceVal(
				MainActivity.this, "interval_points");

		num_points.setText(pointsInterval);

		toggleBtn.setChecked(Global.getInstance().getBooleanType(
				MainActivity.this, "interval_toggle"));

		toggleBtn.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView,
					boolean isChecked) {
				// TODO Auto-generated method stub

				intervalToggle = isChecked;

				Global.getInstance().storeBooleanType(MainActivity.this,
						"interval_toggle", intervalToggle);
			}
		});

		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				d.dismiss();
				IRDialogOpened = false;
			}
		});

		d.setOnCancelListener(new OnCancelListener() {

			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub

				IRDialogOpened = false;
			}
		});

		done.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				IRDialogOpened = false;

				boolean isToggled = Global.getInstance().getBooleanType(
						MainActivity.this, "interval_toggle");

				Global.getInstance().storeBooleanType(MainActivity.this,
						"samples_pressed", samplesPressedValue);
				Global.getInstance().storeBooleanType(MainActivity.this,
						"time_pressed", timePressedValue);
				Global.getInstance().storeBooleanType(MainActivity.this,
						"instructions", instructionsPressedValue);

				if (isToggled) {

					if ((!sample_time.getText().toString().equalsIgnoreCase(""))) {

						if (!sample_time.getText().toString()
								.equalsIgnoreCase("0")) {

							delayInMSec = Integer.parseInt(sample_time
									.getText().toString()) * 1000;

							if (!num_points.getText().toString()
									.equalsIgnoreCase("")
									&& !num_points.getText().toString()
											.equalsIgnoreCase("0")) {
								numberOfPoints = Integer.parseInt(num_points
										.getText().toString());
							} else {
								numberOfPoints = 0;
							}

							String prevTime = Global.getInstance()
									.getPreferenceVal(MainActivity.this,
											"interval_time");

							String prevPoints = Global.getInstance()
									.getPreferenceVal(MainActivity.this,
											"interval_points");

							Global.getInstance().storeIntoPreference(
									MainActivity.this, "interval_time",
									sample_time.getText().toString());
							Global.getInstance().storeIntoPreference(
									MainActivity.this, "interval_points",
									numberOfPoints + "");

							if (intervalRunning) {

								/**********
								 * When interval recording is going. Do nothing
								 *********/
								System.out.println();

								handler1.post(runnable1);
								sample_time_x = Integer.parseInt(Global
										.getInstance().getPreferenceVal(
												MainActivity.this,
												"interval_time"));
								record_button
										.setImageResource(R.drawable.red_connecting);

								if ((!prevTime.equalsIgnoreCase(sample_time
										.getText().toString()))
										|| (!prevPoints
												.equalsIgnoreCase(num_points
														.getText().toString()))) {
									isStopped = true;
									isStarted = false;
									sample_time_text.setVisibility(View.GONE);
									sample_text.setVisibility(View.GONE);
									record_button
											.setImageResource(R.drawable.start_record);

									for (int i = 0; i < sensorTagsList.size(); i++) {

										View view = getSensorTagView(i);

										if (view != null) {

											ImageView switch_device = (ImageView) view
													.findViewById(R.id.switch_device);
											ImageView record_device = (ImageView) view
													.findViewById(R.id.record_device);

											record_device
													.setImageResource(R.drawable.start_red_button);
											switch_device
													.setImageResource(R.drawable.switch_red_button);
										}
									}

									if (mSensorAdapter != null) {
										mSensorAdapter
												.setInteralRecordingStarted(false);
									}

									handler.removeCallbacks(runnable);
									handler1.removeCallbacks(runnable1);
									;
									intervalRunning = false;

									glow_animation_view.clearAnimation();
									glow_animation_view
											.setVisibility(View.GONE);
								}

							} else {
								/*********
								 * once IR values are set, then change the Views
								 * of the record and start and switch buttons
								 * and ready to recording with intervals
								 *********/

								record_button
										.setImageResource(R.drawable.start_record);

								if (mSensorAdapter != null) {
									mSensorAdapter
											.setInteralRecordingActivated(true);

								}

								for (int i = 0; i < sensorTagsList.size(); i++) {

									View view = getSensorTagView(i);

									if (view != null) {

										ImageView switch_device = (ImageView) view
												.findViewById(R.id.switch_device);
										ImageView record_device = (ImageView) view
												.findViewById(R.id.record_device);
										switch_device
												.setImageResource(R.drawable.switch_red_button);

										record_device
												.setImageResource(R.drawable.start_red_button);
									}
								}

								mDatasets.setTextColor(Color
										.parseColor("#f8622b"));
								mValues.setTextColor(Color
										.parseColor("#f8622b"));
							}

							isRecordPressed = true;
							isRecordSwitched = true;

							d.dismiss();

						}

						else {
							Toast.makeText(MainActivity.this,
									"Please enter sample time",
									Toast.LENGTH_LONG).show();
						}

					} else {
						Toast.makeText(MainActivity.this,
								"Please enter sample time", Toast.LENGTH_LONG)
								.show();
					}
				} else {

					// dimiss the alert interval recording (IR)
					d.dismiss();

					/**********
					 * When disable the toggle button, Change the functions and
					 * view of the Record and Swich and start buttons and Keep
					 * IR off only
					 *******/
					if (!isRecordSwitched) {
						// do nothing
					} else {
						if (isStarted) {
							// do disable the action for switch button
							isStarted = false;

							handler.removeCallbacks(runnable);
							handler1.removeCallbacks(runnable1);

							sample_time_text.setVisibility(View.GONE);
							sample_text.setVisibility(View.GONE);
							record_button
									.setImageResource(R.drawable.button_record);

							if (mSensorAdapter != null) {
								mSensorAdapter
										.setInteralRecordingActivated(false);
							}
							if (mSensorAdapter != null) {
								mSensorAdapter
										.setInteralRecordingStarted(false);
							}

							for (int i = 0; i < sensorTagsList.size(); i++) {

								View view = getSensorTagView(i);

								if (view != null) {

									ImageView switch_device = (ImageView) view
											.findViewById(R.id.switch_device);
									ImageView record_device = (ImageView) view
											.findViewById(R.id.record_device);
									switch_device
											.setImageResource(R.drawable.record_button);
									record_device
											.setImageResource(R.drawable.switch_button);
								}
							}

							if (CaliberationFragment.getInstance() != null) {
								CaliberationFragment.record_device
										.setImageResource(R.drawable.record_button);
								CaliberationFragment.switch_device
										.setImageResource(R.drawable.switch_button);

								startCaiber = false;
								stopCaliber = false;
							}

							mDatasets.setTextColor(Color.parseColor("#1284FF"));
							mValues.setTextColor(Color.parseColor("#1284FF"));

							intervalRunning = false;

							isRecordPressed = false;

							glow_animation_view.clearAnimation();

							glow_animation_view.setVisibility(View.GONE);

						} else {
							// when it is not started and record button
							// pressed..
							if (isRecordPressed) {
								record_button
										.setImageResource(R.drawable.button_record);

								if (mSensorAdapter != null) {
									mSensorAdapter
											.setInteralRecordingActivated(false);
								}

								for (int i = 0; i < sensorTagsList.size(); i++) {

									View view = getSensorTagView(i);

									if (view != null) {

										ImageView switch_device = (ImageView) view
												.findViewById(R.id.switch_device);
										ImageView record_device = (ImageView) view
												.findViewById(R.id.record_device);
										switch_device
												.setImageResource(R.drawable.record_button);
										record_device
												.setImageResource(R.drawable.switch_button);
									}
								}

								if (CaliberationFragment.getInstance() != null) {
									CaliberationFragment.record_device
											.setImageResource(R.drawable.record_button);
									CaliberationFragment.switch_device
											.setImageResource(R.drawable.switch_button);

									startCaiber = false;
									stopCaliber = false;
								}

								mDatasets.setTextColor(Color
										.parseColor("#1284FF"));
								mValues.setTextColor(Color
										.parseColor("#1284FF"));

								handler.removeCallbacks(runnable);
								intervalRunning = false;

								isRecordPressed = false;

							}
						}
					}
				}

			}
		});

		if (Global.getInstance().getBooleanType(MainActivity.this,
				"samples_pressed")) {

			samples.setBackgroundResource(R.drawable.samples_pressed);
			instructions.setBackgroundResource(R.drawable.instruction_ir);
			time.setBackgroundResource(R.drawable.time_ir);

		}
		if (Global.getInstance().getBooleanType(MainActivity.this,
				"time_pressed")) {

			samples.setBackgroundResource(R.drawable.samples_ir);
			instructions.setBackgroundResource(R.drawable.instruction_ir);
			time.setBackgroundResource(R.drawable.time_pressed);

		}

		instructions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				samplesPressedValue = false;
				instructionsPressedValue = true;
				timePressedValue = false;

				samples.setBackgroundResource(R.drawable.samples_ir);
				instructions
						.setBackgroundResource(R.drawable.instruction_pressed);
				time.setBackgroundResource(R.drawable.time_ir);
			}
		});
		samples.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				samplesPressedValue = true;
				instructionsPressedValue = false;
				timePressedValue = false;

				samples.setBackgroundResource(R.drawable.samples_pressed);
				instructions.setBackgroundResource(R.drawable.instruction_ir);
				time.setBackgroundResource(R.drawable.time_ir);

			}
		});
		time.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				samplesPressedValue = false;
				instructionsPressedValue = false;
				timePressedValue = true;

				samples.setBackgroundResource(R.drawable.samples_ir);
				instructions.setBackgroundResource(R.drawable.instruction_ir);
				time.setBackgroundResource(R.drawable.time_pressed);

			}
		});

	}

	boolean samples_pressed;
	boolean time_pressed;
	boolean instructionsPressed;
	boolean intervalRunning;
	Handler handler = new Handler();
	Runnable runnable = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub

			glow_animation_view.setVisibility(View.VISIBLE);
			glow_animation_view.startAnimation(animAlpha);

			if (sensorTagsList.size() > 0 || pHSensorsList.size() > 0) {
				if (numberOfPoints == 0) {
					// when number of points are zero
					intervalRunning = true;

					samples_pressed = Global.getInstance().getBooleanType(
							MainActivity.this, "samples_pressed");
					time_pressed = Global.getInstance().getBooleanType(
							MainActivity.this, "time_pressed");
					instructionsPressed = Global.getInstance().getBooleanType(
							MainActivity.this, "instructions");

					insertValuesForDataset("blue");
					// System.out.println(delayInMSec);

					isStopped = false;
					sample_points++;

					String collectedText = "Collected";
					String secondsText = "Seconds";

					handler.postDelayed(runnable, delayInMSec);

					if (time_pressed) {

						/******** remove callbacks before start handler again ********/
						handler1.removeCallbacks(runnable1);

						sample_time_x = Integer.parseInt(Global.getInstance()
								.getPreferenceVal(MainActivity.this,
										"interval_time"));
						sample_time_text.setText(String.valueOf(sample_time_x));
						sample_text.setText(secondsText);

						handler1.post(runnable1);

					} else if (samples_pressed) {
						record_button
								.setImageResource(R.drawable.red_connecting);
						sample_time_text.setText(String.valueOf(sample_points));
						sample_text.setText(collectedText);

					} else if (instructionsPressed) {

						sample_text.setText("");
						sample_time_text.setText("");

					}
				}

				else if (i != numberOfPoints) {

					/**** when two values of sample and time are valid ******/

					intervalRunning = true;

					samples_pressed = Global.getInstance().getBooleanType(
							MainActivity.this, "samples_pressed");
					time_pressed = Global.getInstance().getBooleanType(
							MainActivity.this, "time_pressed");
					instructionsPressed = Global.getInstance().getBooleanType(
							MainActivity.this, "instructions");
					i++;

					insertValuesForDataset("blue");
					// System.out.println(delayInMSec);

					isStopped = false;
					sample_points--;

					if (i != numberOfPoints) {
						handler.postDelayed(runnable, delayInMSec);

						if (time_pressed) {

							/******** remove callbacks before start handler again ********/
							handler1.removeCallbacks(runnable1);

							sample_time_x = Integer
									.parseInt(Global.getInstance()
											.getPreferenceVal(
													MainActivity.this,
													"interval_time"));
							sample_time_text.setText(String
									.valueOf(sample_time_x));
							sample_text.setText("Seconds");

							handler1.post(runnable1);

						} else if (samples_pressed) {
							record_button
									.setImageResource(R.drawable.red_connecting);
							sample_time_text.setText(String
									.valueOf(sample_points));
							sample_text.setText("Remaining");

						} else if (instructionsPressed) {

							sample_text.setText("");
							sample_time_text.setText("");

						}
					}

					else {
						isStopped = true;
						isStarted = false;
						sample_time_text.setVisibility(View.GONE);
						sample_text.setVisibility(View.GONE);
						record_button.setImageResource(R.drawable.start_record);

						for (int i = 0; i < sensorTagsList.size(); i++) {

							View view = getSensorTagView(i);

							if (view != null) {

								ImageView switch_device = (ImageView) view
										.findViewById(R.id.switch_device);
								ImageView record_device = (ImageView) view
										.findViewById(R.id.record_device);

								record_device
										.setImageResource(R.drawable.start_red_button);
								switch_device
										.setImageResource(R.drawable.switch_red_button);
							}
						}

						if (mSensorAdapter != null) {
							mSensorAdapter.setInteralRecordingStarted(false);
						}

						handler.removeCallbacks(runnable);
						intervalRunning = false;

						glow_animation_view.clearAnimation();

						glow_animation_view.setVisibility(View.GONE);

					}

				}
			} else {

				/**** when all the devices are disconnected or not connected... *****/
				isStopped = true;
				isStarted = false;
				sample_time_text.setVisibility(View.GONE);
				sample_text.setVisibility(View.GONE);
				record_button.setImageResource(R.drawable.start_record);

				for (int i = 0; i < sensorTagsList.size(); i++) {

					View view = getSensorTagView(i);

					if (view != null) {

						ImageView switch_device = (ImageView) view
								.findViewById(R.id.switch_device);
						ImageView record_device = (ImageView) view
								.findViewById(R.id.record_device);

						record_device
								.setImageResource(R.drawable.start_red_button);
						switch_device
								.setImageResource(R.drawable.switch_red_button);
					}
				}

				if (mSensorAdapter != null) {
					mSensorAdapter.setInteralRecordingStarted(false);
				}

				handler.removeCallbacks(runnable);
				intervalRunning = false;

				glow_animation_view.clearAnimation();

				glow_animation_view.setVisibility(View.GONE);
			}

		}
	};

	Handler handler1 = new Handler();
	Runnable runnable1 = new Runnable() {

		@Override
		public void run() {
			// TODO Auto-generated method stub

			if (sensorTagsList.size() > 0 || pHSensorsList.size() > 0) {

				samples_pressed = Global.getInstance().getBooleanType(
						MainActivity.this, "samples_pressed");
				time_pressed = Global.getInstance().getBooleanType(
						MainActivity.this, "time_pressed");
				instructionsPressed = Global.getInstance().getBooleanType(
						MainActivity.this, "instructions");

				if (time_pressed) {

					System.out.println("sample_time_x" + sample_time_x);
					sample_time_text.setText(String.valueOf(sample_time_x));
					sample_text.setText("Seconds");
					if (sample_time_x != 1) {
						handler1.postDelayed(runnable1, 1000);
						sample_time_x--;
					} else {
						handler1.removeCallbacks(runnable1);

					}
				} else if (samples_pressed) {

					record_button.setImageResource(R.drawable.red_connecting);
					sample_time_text.setText(String.valueOf(sample_points));

					sample_time_text.setVisibility(View.VISIBLE);
					sample_text.setVisibility(View.VISIBLE);

					if (numberOfPoints == 0) {
						sample_text.setText("Collected");
					} else {
						sample_text.setText("Remaining");
					}

				} else if (instructionsPressed) {

					sample_text.setText("");
					sample_time_text.setText("");
					record_button.setImageResource(R.drawable.stop_record);

				}
			} else {
				isStopped = true;
				isStarted = false;
				sample_time_text.setVisibility(View.GONE);
				sample_text.setVisibility(View.GONE);
				record_button.setImageResource(R.drawable.start_record);

				for (int i = 0; i < sensorTagsList.size(); i++) {

					View view = getSensorTagView(i);

					if (view != null) {

						ImageView switch_device = (ImageView) view
								.findViewById(R.id.switch_device);
						ImageView record_device = (ImageView) view
								.findViewById(R.id.record_device);

						record_device
								.setImageResource(R.drawable.start_red_button);
						switch_device
								.setImageResource(R.drawable.switch_red_button);
					}
				}

				if (mSensorAdapter != null) {
					mSensorAdapter.setInteralRecordingStarted(false);
				}

				handler.removeCallbacks(runnable);
				intervalRunning = false;

				glow_animation_view.clearAnimation();

				glow_animation_view.setVisibility(View.GONE);
			}

		}
	};

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.menu_button:
			slidingMenu = getSlidingMenu();

			slidingMenu.showMenu();
			break;

		case R.id.datasets:
			openDataSetFragment();

			break;
		case R.id.values:
			openValuesFragment(
					Global.getInstance().getPreferenceVal(
							getApplicationContext(), "datasetName"),
					Global.getInstance().getPreferenceVal(
							getApplicationContext(), "datasetId"), curDataSet);
			break;
		case R.id.record_button:

			if (connecting_status.getVisibility() == View.GONE) {
				if (menuFragment != null) {

					// Both ph and sensor tag disconnected and deselected
					if (sensorTagsList.size() > 0 || pHSensorsList.size() > 0) {

						/***********
						 * when IR (Interval recording)value is activated and
						 * not started to record values
						 **********/
						if (isRecordPressed && !isStarted) {

							sample_points = Integer.parseInt(Global
									.getInstance().getPreferenceVal(
											MainActivity.this,
											"interval_points"));

							isStarted = true;
							i = 0;
							handler.post(runnable);

							samples_pressed = Global.getInstance()
									.getBooleanType(MainActivity.this,
											"samples_pressed");
							time_pressed = Global.getInstance().getBooleanType(
									MainActivity.this, "time_pressed");

							if (mSensorAdapter != null) {
								mSensorAdapter.setInteralRecordingStarted(true);
							}

							for (int i = 0; i < sensorTagsList.size(); i++) {

								View view = getSensorTagView(i);

								if (view != null) {

									ImageView switch_device = (ImageView) view
											.findViewById(R.id.switch_device);
									ImageView record_device = (ImageView) view
											.findViewById(R.id.record_device);
									switch_device
											.setImageResource(R.drawable.switch_black_button);
									record_device
											.setImageResource(R.drawable.stop_red_button);
								}
							}

							/********* when Samples pressed in IR(Interval Recording ********/

							if (samples_pressed) {
								record_button
										.setImageResource(R.drawable.red_connecting);

								sample_time_text.setVisibility(View.VISIBLE);
								sample_text.setVisibility(View.VISIBLE);
								sample_time_text.setText(String
										.valueOf(sample_points));
								sample_text.setText("Remaining");

								/********* when Time pressed in IR(Interval Recording ********/
							} else if (time_pressed) {
								record_button
										.setImageResource(R.drawable.red_connecting);

								sample_time_text.setVisibility(View.VISIBLE);
								sample_text.setVisibility(View.VISIBLE);
								sample_time_text.setText(String
										.valueOf(sample_time_x));
								sample_text.setText("Seconds");

							} else {
								record_button
										.setImageResource(R.drawable.stop_record);

							}

							/********
							 * stop the recording if the recording is started
							 * already
							 ********/

						} else if (isStarted)

						{

							isStarted = false;
							sample_time_text.setVisibility(View.GONE);
							sample_text.setVisibility(View.GONE);
							record_button
									.setImageResource(R.drawable.start_record);

							for (int i = 0; i < sensorTagsList.size(); i++) {

								View view = getSensorTagView(i);

								if (view != null) {

									ImageView switch_device = (ImageView) view
											.findViewById(R.id.switch_device);
									ImageView record_device = (ImageView) view
											.findViewById(R.id.record_device);
									switch_device
											.setImageResource(R.drawable.switch_red_button);
									record_device
											.setImageResource(R.drawable.start_red_button);
								}
							}

							if (mSensorAdapter != null) {
								mSensorAdapter
										.setInteralRecordingStarted(false);
							}

							glow_animation_view.clearAnimation();
							glow_animation_view.setVisibility(View.GONE);

							handler.removeCallbacks(runnable);
							handler1.removeCallbacks(runnable1);
							intervalRunning = false;

							/******** if IR is not set initially *********/

						} else if (!isRecordPressed) {

							insertValuesForDataset("red");
						}

					}
				}
			}

			break;

		}
	}

	/*******
	 * 
	 * insert dataset values of particular sensorTag and pH-Meter to the
	 * database bsed on connection state
	 * 
	 * @param color
	 */

	public void insertValuesForDataset(final String color) {

		dataSetId = Global.getInstance().getPreferenceVal(
				getApplicationContext(), "datasetId");

		if (!dataSetId.equalsIgnoreCase("")) {

			if (menuFragment != null) {

				if (sensorTagsList.size() > 0 || pHSensorsList.size() > 0) {

					connecting_status.setVisibility(View.VISIBLE);

					if (color.equalsIgnoreCase("blue")) {

						record_button
								.setImageResource(R.drawable.red_connecting);
						sample_time_text.setVisibility(View.GONE);
						sample_text.setVisibility(View.GONE);

					} else if (color.equalsIgnoreCase("red")) {
						record_button
								.setImageResource(R.drawable.blue_recording);
					}

					long time_milliseconds = System.currentTimeMillis();

					String time = String.valueOf(time_milliseconds);

					db.open();

					db.insertRecording(dataSetId, time, "", "", "");

					Values values = new Values();
					values.setmDataSetId(dataSetId);
					values.setmTime(time);
					values.setmName("");
					values.setmNotes("");
					values.setmLocation("");

					ArrayList<SensorTag> senTagsList = new ArrayList<SensorTag>();
					ArrayList<PHSensor> phSenTagsList = new ArrayList<PHSensor>();

					for (int i = 0; i < sensorTagsList.size(); i++) {

						SensorTag sensorTag = sensorTagsList.get(i);
						String deviceAddress = sensorTag.getDeviceAddress();
						String deviceName = "SensorTag";
						String amb_temp = sensorTag.getaTempValue();
						String obj_temp = sensorTag.getoTempValue();
						String humidity = sensorTag.getHumidity();
						String pressure = sensorTag.getBaroValue();
						String mag_X = sensorTag.getMagnetometerValue_x();
						String mag_Y = sensorTag.getMagnetometerValue_y();
						String mag_Z = sensorTag.getMagnetometerValue_z();
						String acc_X = sensorTag.getAccelerometerValue_x();
						String acc_Y = sensorTag.getAccelerometerValue_y();
						String acc_Z = sensorTag.getAccelerometerValue_z();
						String gyro_X = sensorTag.getGyroMeterValue_x();
						String gyro_Y = sensorTag.getGyroMeterValue_y();
						String gyro_Z = sensorTag.getGyroMeterValue_z();

						boolean disconnected = sensorTag.ismDisconnected();

						if (!disconnected) {
							db.insertRecordingVaues(time, deviceAddress,
									deviceName, amb_temp, obj_temp, humidity,
									pressure, mag_X, mag_Y, mag_Z, acc_X,
									acc_Y, acc_Z, gyro_X, gyro_Y, gyro_Z, "",
									"", "", dataSetId);

							senTagsList.add(sensorTag);

						}

					}
					for (int i = 0; i < pHSensorsList.size(); i++) {

						PHSensor pHsensor = pHSensorsList.get(i);

						String deviceAddress = pHsensor.getDeviceAddress();
						String deviceName = "pH-Meter";
						String pH_value = pHsensor.getpHValue();
						String temp_value = pHsensor.getTemp_value();
						String mV = pHsensor.getmV();

						boolean disconnected = pHsensor.ismDisconected();
						if (!disconnected) {
							db.insertRecordingVaues(time, deviceAddress,
									deviceName, "", "", "", "", "", "", "", "",
									"", "", "", "", "", pH_value, temp_value,
									mV, dataSetId);

							phSenTagsList.add(pHsensor);
						}

						String LatestId = getRecentCalibrationHistoryId(deviceAddress);
						if (LatestId != null) {
							String numberOfValuesRecorded = getNumberOfValuesforId(LatestId);
							int recordedValues = Integer
									.parseInt(numberOfValuesRecorded);
							recordedValues++;
							updateNumberOfValuesForDevice(recordedValues,
									LatestId);
						}

					}

					values.setSensorTagsList(senTagsList);
					values.setpHSensorsList(phSenTagsList);

					curDataSet.setValueList(values);

					if (ValuesFragment.getInstance() != null) {

						ValuesFragment.getInstance().notifyDataSetChanged();

					}

					db.updateDataSetsTimeAndDate(dataSetId,
							System.currentTimeMillis());
					db.close();

					/****
					 * once after recording change the drawables of record
					 * button
					 ****/

					Handler handler = new Handler();
					handler.postDelayed(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							connecting_status.setVisibility(View.GONE);

							if (color.equalsIgnoreCase("red")) {
								record_button
										.setImageResource(R.drawable.button_record);
							} else if (color.equalsIgnoreCase("blue")) {

								if (sample_points != 0) {
									if (intervalRunning)
										record_button
												.setImageResource(R.drawable.red_connecting);

								} else {
									record_button
											.setImageResource(R.drawable.start_record);
								}
								if (!samples_pressed && !time_pressed) {

									if (isStopped) {
										record_button
												.setImageResource(R.drawable.start_record);
									} else {
										if (intervalRunning)
											record_button
													.setImageResource(R.drawable.stop_record);
									}

								} else {

									if (isStarted) {
										sample_time_text
												.setVisibility(View.VISIBLE);
										sample_text.setVisibility(View.VISIBLE);
									}

								}

							}

						}
					}, 250);

				} else {
					// do nothing
				}

			}

		}

	}

	/***
	 * open dataset fragment method
	 */
	public void openDataSetFragment() {
		FragmentTransaction transaction = mFragmentManager.beginTransaction();
		// transaction.setCustomAnimations(R.anim.slide_in_up,
		// R.anim.slide_none);
		transaction.add(R.id.datasets_layout,
				DataSetsFragment.newInstance(this), "datasets");

		Fragment frag = mFragmentManager.findFragmentByTag("datasets");
		if (frag != null) {
			return;
		}

		transaction.addToBackStack("datasets");
		transaction.commit();
		Animation animation = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.slide_in_up);
		mDataSetsFragment.setAnimation(animation);
		mDataSetsFragment.setVisibility(View.VISIBLE);
	}

	/*** open values fragment with dataset ****/

	public void openValuesFragment(String name, String id, DataSet dataset) {
		FragmentTransaction transaction = mFragmentManager.beginTransaction();

		transaction.add(R.id.values_layout, new ValuesFragment(name, id,
				dataset, this), "values");
		transaction.addToBackStack("values");
		transaction.commit();
		Animation animation = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.slide_in_up);
		mValuesFragment.setAnimation(animation);
		mValuesFragment.setVisibility(View.VISIBLE);
	}

	/***** open the calibration history for ph *****/

	public void pHCalibrationHistory(String deviceAddress) {
		FragmentTransaction transaction = mFragmentManager.beginTransaction();

		transaction.add(R.id.pH_caliber_history,
				CalibertionHistoryFragment.newInstance(),
				"pH_caliberation_history");
		transaction.addToBackStack("pH_caliberation_history");
		CalibertionHistoryFragment.getInstance()
				.setDeviceAddress(deviceAddress);

		transaction.commit();
		Animation animation = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.slide_in_up);
		pH_caliber_history.setAnimation(animation);
		pH_caliber_history.setVisibility(View.VISIBLE);
	}

	/***** ph calibration history details ***/

	public void pHCalibrationHistoryDetails(
			PhCalibrationHistoryData calibrationHistoryData) {
		FragmentTransaction transaction = mFragmentManager.beginTransaction();

		transaction.add(R.id.pH_caliber_history_details,
				PhCalibrationHistoryDetails.newInstance(),
				"pH_caliberation_history_details");
		transaction.addToBackStack("pH_caliberation_history_details");
		transaction.commit();
		Animation animation = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.slide_in_up);
		pH_caliber_history_details.setAnimation(animation);
		pH_caliber_history_details.setVisibility(View.VISIBLE);

		PhCalibrationHistoryDetails.getInstance().setpHCalibrationHistoryData(
				calibrationHistoryData);
	}

	/****** override method for opening values details fragment *****/
	@Override
	public void onDetailClick(Values mValues, String from_where) {
		// TODO Auto-generated method stub
		FragmentTransaction transaction = mFragmentManager.beginTransaction();
		ValueDetailsFragment detailsFragment = ValueDetailsFragment
				.newInstance(this);

		detailsFragment.setData(mValues, from_where);
		transaction.add(R.id.values_details_layout, detailsFragment,
				"valuesDetails");
		transaction.addToBackStack("valuesDetails");
		transaction.commit();
		Animation animation = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.slide_in_up);
		mValueDetailsFragment.setAnimation(animation);
		mValueDetailsFragment.setVisibility(View.VISIBLE);
	}

	@Override
	protected void onResume() {
		// Log.d(TAG, "onResume");
		super.onResume();
		// if (!mIsReceiving) {
		// registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
		// mIsReceiving = true;
		// }
	}

	@Override
	protected void onPause() {
		// Log.d(TAG, "onPause");
		super.onPause();
		// if (mIsReceiving) {
		// unregisterReceiver(mGattUpdateReceiver);
		// mIsReceiving = false;
		// }
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();

		if (mIsReceiving) {
			unregisterReceiver(mGattUpdateReceiver);
			mIsReceiving = false;
		}

		Global.getInstance().storeBooleanType(MainActivity.this,
				"interval_toggle", false);

		Global.getInstance().storeIntoPreference(MainActivity.this,
				"interval_time", "0");

		Global.getInstance().storeIntoPreference(MainActivity.this,
				"interval_points", "0");

		Global.getInstance().storeIntoPreference(MainActivity.this,
				"mtc_value", 0.0f);

		mDatabaseManager.deleteAlarmDetails();
	}

	private static IntentFilter makeGattUpdateIntentFilter() {
		final IntentFilter fi = new IntentFilter();
		fi.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
		fi.addAction(BluetoothLeService.ACTION_DATA_NOTIFY);
		fi.addAction(BluetoothLeService.ACTION_DATA_WRITE);
		fi.addAction(BluetoothLeService.ACTION_DATA_READ);
		return fi;
	}

	@Override
	public void onDeviceButtonClick(BluetoothDevice mBtDevice) {
		// TODO Auto-generated method stub

		if (!mIsReceiving) {
			registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
			mIsReceiving = true;
		}

		showReadingValues(mBtDevice);

	}

	public void showReadingValues(BluetoothDevice mBtDevice) {
		// BLE
		mBtLeService = BluetoothLeService.getInstance();
		mBluetoothDevice = mBtDevice;
		mServiceList = new ArrayList<BluetoothGattService>();

		// Determine type of SensorTagGatt
		// String deviceName = mBluetoothDevice.getName();

		// GATT database
		Resources res = getResources();
		XmlResourceParser xpp = res.getXml(R.xml.gatt_uuid);
		new GattInfo(xpp);

		// Initialize sensor list
		updateSensorList();

		onViewInflated(mBtDevice);

	}

	//
	// Application implementation
	//
	private void updateSensorList() {
		mEnabledSensors.clear();

		for (int i = 0; i < Sensor.SENSOR_LIST.length; i++) {
			Sensor sensor = Sensor.SENSOR_LIST[i];

			mEnabledSensors.add(sensor);
		}

	}

	void onViewInflated(BluetoothDevice device) {
		// Log.d(TAG, "Gatt view ready");

		Peripheral peripheral = mBtLeService.getPeripheral(device);

		BluetoothGatt gatt = peripheral.mBluetoothGatt;

		String deviceName = device.getName();
		String deviceAddress = device.getAddress();

		if (deviceName.equalsIgnoreCase("SensorTag")) {

			boolean isAlreadyAdded = false;
			int AddedDeviceposition = -1;

			for (int i = 0; i < sensorTagsList.size(); i++) {
				if (deviceAddress.equalsIgnoreCase(sensorTagsList.get(i)
						.getDeviceAddress())) {

					AddedDeviceposition = i;
					isAlreadyAdded = true;
				}
			}

			// add sensor tag to the list
			if (!isAlreadyAdded) {
				String deviceCustomName = getDeviceCustomNameForAddress(deviceAddress);

				SensorTag sensorTag = new SensorTag();
				sensorTag.setDeviceAddress(deviceAddress);
				sensorTag.setDeviceName(deviceCustomName);
				sensorTagsList.add(sensorTag);
				if (mSensorAdapter == null) {
					mSensorAdapter = new SensorTagAdapter(MainActivity.this,
							R.layout.sensortag_inflate_list, sensorTagsList);

					sensorTag_listView.setAdapter(mSensorAdapter);

				} else {
					mSensorAdapter.notifyDataSetChanged();
				}

			} else {
				SensorTag sensorTag = sensorTagsList.get(AddedDeviceposition);
				sensorTag.setmDisconnected(false);
				sensorTagsList.set(AddedDeviceposition, sensorTag);
				mSensorAdapter.notifyDataSetChanged();
			}

			insertCalibrationSensorTag(deviceAddress);

		} else if (deviceName.equalsIgnoreCase("pH-Meter")) {
			// pH_layout.setVisibility(View.VISIBLE);

			boolean isPHAlreadyExists = isPHDeviceExistsinDB(deviceAddress);

			if (!isPHAlreadyExists) {
				insertSlopeAndInterceptAndTemp(deviceAddress, "0.0", "0.0",
						"0.0");
			}

			boolean ispHAlreadyAdded = false;
			int AddedpHDeviceposition = -1;

			for (int i = 0; i < pHSensorsList.size(); i++) {
				if (deviceAddress.equalsIgnoreCase(pHSensorsList.get(i)
						.getDeviceAddress())) {

					AddedpHDeviceposition = i;
					ispHAlreadyAdded = true;
				}
			}
			// add ph meter to the list
			if (!ispHAlreadyAdded) {
				String deviceCustomName = getDeviceCustomNameForAddress(deviceAddress);
				PHSensor pHSensor = new PHSensor();
				pHSensor.setDeviceAddress(deviceAddress);
				pHSensor.setDeviceName(deviceCustomName);

				pHSensorsList.add(pHSensor);

				if (mPHListInflater == null) {
					mPHListInflater = new PHSensorListInflater(
							MainActivity.this, pH_listView_parentLayout);
					pH_listView_parentLayout.removeAllViews();
					mPHListInflater.notifyList(pHSensorsList,
							R.layout.ph_sensor_inflate);
				} else {
					pH_listView_parentLayout.removeAllViews();
					mPHListInflater.notifyList(pHSensorsList,
							R.layout.ph_sensor_inflate);
				}

			} else {
				PHSensor pHSensor = pHSensorsList.get(AddedpHDeviceposition);
				pHSensor.setmDisconected(false);
				pHSensorsList.set(AddedpHDeviceposition, pHSensor);

				pH_listView_parentLayout.removeAllViews();
				mPHListInflater.notifyList(pHSensorsList,
						R.layout.ph_sensor_inflate);
			}

		}

		mServicesRdy = false;
		// Start service discovery
		if (!mServicesRdy && gatt != null) {
			if (peripheral.getNumServices() == 0)
				discoverServices(peripheral);
			else {
				displayServices(peripheral);
				enableDataCollection(true, peripheral);
			}
		}
	}

	/**** get device custom name for address of the device *****/
	private String getDeviceCustomNameForAddress(String deviceAddress) {
		// TODO Auto-generated method stub
		String deviceCustomName = null;

		db.open();

		Cursor deviceInfo = db.getDeviceInfoAddress(deviceAddress);

		if (deviceInfo.moveToFirst()) {
			do {

				deviceCustomName = deviceInfo.getString(0);

			} while (deviceInfo.moveToNext());
		}
		deviceInfo.close();
		db.close();

		return deviceCustomName;
	}

	/***
	 * 
	 * check whether device already exists in db.
	 */

	public boolean isPHDeviceExistsinDB(String deviceAddress) {

		boolean isExists;
		db.open();
		isExists = db.isPHDeviceExistsinDB(deviceAddress);
		db.close();

		return isExists;
	}

	/***** insert slope and intercept and temp for particular pH meter *****/
	public void insertSlopeAndInterceptAndTemp(String deviceAddress,
			String slope, String intercept, String temperature) {
		db.open();
		db.insertPHCalibration(deviceAddress, slope, intercept, temperature);
		db.close();

	}

	/**** insert calibration sensor Tag values ***/
	public void insertCalibrationSensorTag(String deviceAddress) {
		// db = new DataBaseHelper(MainActivity.this);
		db.open();
		db.insertSensorTagCalibration("0", "0", "0", "0", deviceAddress);
		db.close();
	}

	/**** update sensor tag calibration in database ****/
	public void updateCalibrationForSensorTag(String deviceAddress, String tag,
			String calibration) {
		// db = new DataBaseHelper(MainActivity.this);
		db.open();
		db.updateCalibrationSensorTag(deviceAddress, tag, calibration);
		db.close();
	}

	private void discoverServices(Peripheral peripheral) {
		if (peripheral.mBluetoothGatt.discoverServices()) {
			mServiceList.clear();

		} else {
			// setError("Service discovery start failed");
		}
	}

	private void displayServices(Peripheral peripheral) {
		mServicesRdy = true;

		try {
			mServiceList = peripheral.getSupportedGattServices();
		} catch (Exception e) {
			e.printStackTrace();
			mServicesRdy = false;
		}

		// Characteristics descriptor readout done
		if (!mServicesRdy) {
			// setError("Failed to read services");
		}
	}

	private void enableDataCollection(final boolean enable,
			final Peripheral peripheral) {

		List<Sensor> Sensors = new ArrayList<>();
		Sensors.addAll(mEnabledSensors);

		enableSensors(enable, peripheral, Sensors);
		enableNotifications(enable, peripheral, Sensors);
		getFirmwareRevison(peripheral);
		getManufacturerName(peripheral);
		getModelNumber(peripheral);
		getHardWareRevision(peripheral);
		getSerialNumber(peripheral);

		if (peripheral.mBleDevice.getName().equalsIgnoreCase("pH-Meter")) {
			getBatteryLevel(peripheral);
		}

	}

	int enableSensor = 0;

	/**** enable the sensors available and write characteristics on the UUID's *****/

	private void enableSensors(boolean f, Peripheral peripheral,
			List<Sensor> mEnabledSensors) {
		final boolean enable = f;
		enableSensor++;

		for (Sensor sensor : mEnabledSensors) {
			UUID servUuid = sensor.getService();
			UUID confUuid = sensor.getConfig();

			// Skip keys
			if (confUuid == null)
				break;

			String deviceName = peripheral.mBleDevice.getName();
			if (deviceName.equalsIgnoreCase("SensorTag")) {
				if (confUuid.equals(SensorTagGatt.UUID_BAR_CONF) && enable) {
					calibrateBarometer(peripheral);
				}
			}

			// }

			BluetoothGattService serv = null;

			if (peripheral.mBluetoothGatt != null) {
				serv = peripheral.mBluetoothGatt.getService(servUuid);
			}
			if (serv != null) {
				BluetoothGattCharacteristic charac = serv
						.getCharacteristic(confUuid);
				UUID pH_period = fromString("623001e4-9a90-11e3-a5e2-0800200c9a66");
				byte value;
				if (pH_period.equals(confUuid)) {

					value = 11;

				} else {

					value = enable ? sensor.getEnableSensorCode()
							: Sensor.DISABLE_SENSOR_CODE;

					// value = enable ? sensor.getEnableSensorCodeForSensorTag()
					// : Sensor.DISABLE_SENSOR_CODE;
				}

				if (peripheral.writeCharacteristic(charac, value)) {
					peripheral.waitIdle(GATT_TIMEOUT);
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					// setError("Sensor config failed: "
					// + serv.getUuid().toString());
					// Toast.makeText(MainActivity.this,
					// "Sensor config failed: "
					// + serv.getUuid().toString(),
					// Toast.LENGTH_LONG).show();
					// if (enableSensor == 5) {
					// enableSensors(true, peripheral);
					// }

					break;
				}

			}
		}
	}

	int enableNotification = 0;

	/****** enable notifications of the sensors available ****/

	private void enableNotifications(boolean f, Peripheral peripheral,
			List<Sensor> mEnabledSensors) {
		final boolean enable = f;

		for (Sensor sensor : mEnabledSensors) {
			UUID servUuid = sensor.getService();
			UUID dataUuid = sensor.getData();

			BluetoothGattService serv = null;

			if (peripheral.mBluetoothGatt != null) {
				serv = peripheral.mBluetoothGatt.getService(servUuid);
			}

			if (serv != null) {

				BluetoothGattCharacteristic charac = serv
						.getCharacteristic(dataUuid);

				if (peripheral.setCharacteristicNotification(charac, enable)) {
					peripheral.waitIdle(GATT_TIMEOUT);
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} else {
					// setError("Sensor notification failed: "
					// + serv.getUuid().toString());

					// Toast.makeText(MainActivity.this,
					// "Sensor notification failed: "
					// + serv.getUuid().toString(),
					// Toast.LENGTH_LONG).show();

					// if (enableNotification == 5) {
					// enableNotifications(true, peripheral);
					// }

					break;
				}

			}
		}
	}

	private void checkOad() {
		// Check if OAD is supported (needs OAD and Connection Control service)
		mOadService = null;
		mConnControlService = null;

		for (int i = 0; i < mServiceList.size()
				&& (mOadService == null || mConnControlService == null); i++) {
			BluetoothGattService srv = mServiceList.get(i);
			if (srv.getUuid().equals(GattInfo.OAD_SERVICE_UUID)) {
				mOadService = srv;
			}
			if (srv.getUuid().equals(GattInfo.CC_SERVICE_UUID)) {
				mConnControlService = srv;
			}
		}
	}

	/*
	 * Calibrating the barometer includes
	 * 
	 * 1. Write calibration code to configuration characteristic. 2. Read
	 * calibration values from sensor, either with notifications or a normal
	 * read. 3. Use calibration values in formulas when interpreting sensor
	 * values.
	 */

	private void calibrateBarometer(Peripheral peripheral) {
		// if (mIsSensorTag2)
		// return;

		UUID servUuid = Sensor.BAROMETER.getService();
		UUID configUuid = Sensor.BAROMETER.getConfig();

		BluetoothGattService serv = null;

		if (peripheral.mBluetoothGatt != null) {
			serv = peripheral.mBluetoothGatt.getService(servUuid);
		}

		if (serv != null) {
			BluetoothGattCharacteristic config = serv
					.getCharacteristic(configUuid);

			// Write the calibration code to the configuration registers
			peripheral
					.writeCharacteristic(config, Sensor.CALIBRATE_SENSOR_CODE);
			peripheral.waitIdle(GATT_TIMEOUT);
			BluetoothGattCharacteristic calibrationCharacteristic = serv
					.getCharacteristic(SensorTagGatt.UUID_BAR_CALI);
			peripheral.readCharacteristic(calibrationCharacteristic);
			peripheral.waitIdle(GATT_TIMEOUT);
		}

	}

	/***** get the firmware revision ofthe particualr device ****/
	private void getFirmwareRevison(Peripheral peripheral) {
		UUID servUuid = SensorTagGatt.UUID_DEVINFO_SERV;
		UUID charUuid = SensorTagGatt.UUID_DEVINFO_FWREV;

		BluetoothGattService serv = null;

		if (peripheral.mBluetoothGatt != null) {
			serv = peripheral.mBluetoothGatt.getService(servUuid);
		}

		if (serv != null) {
			BluetoothGattCharacteristic charFwrev = serv
					.getCharacteristic(charUuid);

			// Write the calibration code to the configuration registers
			peripheral.readCharacteristic(charFwrev);
			peripheral.waitIdle(GATT_TIMEOUT);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	/***** get the manufacturer name of the particualr device ****/
	private void getManufacturerName(Peripheral peripheral) {
		UUID servUuid = SensorTagGatt.UUID_DEVINFO_SERV;
		UUID charUuid = SensorTagGatt.UUID_MANUFACTURER;

		BluetoothGattService serv = null;

		if (peripheral.mBluetoothGatt != null) {
			serv = peripheral.mBluetoothGatt.getService(servUuid);
		}

		if (serv != null) {
			BluetoothGattCharacteristic charFwrev = serv
					.getCharacteristic(charUuid);

			peripheral.readCharacteristic(charFwrev);
			peripheral.waitIdle(GATT_TIMEOUT);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	/***** get the model number of the particualr device ****/
	private void getModelNumber(Peripheral peripheral) {
		UUID servUuid = SensorTagGatt.UUID_DEVINFO_SERV;
		UUID charUuid = SensorTagGatt.UUID_MODEL_NUMBER_DEVICE;

		BluetoothGattService serv = null;

		if (peripheral.mBluetoothGatt != null) {
			serv = peripheral.mBluetoothGatt.getService(servUuid);
		}

		if (serv != null) {
			BluetoothGattCharacteristic charFwrev = serv
					.getCharacteristic(charUuid);

			peripheral.readCharacteristic(charFwrev);
			peripheral.waitIdle(GATT_TIMEOUT);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	/***** get the hardware revision of the particualr device ****/
	private void getHardWareRevision(Peripheral peripheral) {
		UUID servUuid = SensorTagGatt.UUID_DEVINFO_SERV;
		UUID charUuid = SensorTagGatt.UUID_HARDWARE_DIV_DEVICE;

		BluetoothGattService serv = null;

		if (peripheral.mBluetoothGatt != null) {
			serv = peripheral.mBluetoothGatt.getService(servUuid);
		}

		if (serv != null) {
			BluetoothGattCharacteristic charFwrev = serv
					.getCharacteristic(charUuid);

			peripheral.readCharacteristic(charFwrev);
			peripheral.waitIdle(GATT_TIMEOUT);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	/***** get the serial number of the particualr device ****/
	private void getSerialNumber(Peripheral peripheral) {
		UUID servUuid = SensorTagGatt.UUID_DEVINFO_SERV;
		UUID charUuid = SensorTagGatt.UUID_SERIAL_NUMBER_DEVICE;

		BluetoothGattService serv = null;

		if (peripheral.mBluetoothGatt != null) {
			serv = peripheral.mBluetoothGatt.getService(servUuid);
		}

		if (serv != null) {
			BluetoothGattCharacteristic charFwrev = serv
					.getCharacteristic(charUuid);

			peripheral.readCharacteristic(charFwrev);
			peripheral.waitIdle(GATT_TIMEOUT);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	/***** get the battery level of the particualr device ****/
	private void getBatteryLevel(Peripheral peripheral) {
		UUID servUuid = SensorTagGatt.UUID_BATTERY_SERV;
		UUID charUuid = SensorTagGatt.UUID_BATTERY_CHAR;

		BluetoothGattService serv = null;

		if (peripheral.mBluetoothGatt != null) {
			serv = peripheral.mBluetoothGatt.getService(servUuid);
		}

		if (serv != null) {
			BluetoothGattCharacteristic charFwrev = serv
					.getCharacteristic(charUuid);

			peripheral.readCharacteristic(charFwrev);
			peripheral.waitIdle(GATT_TIMEOUT);
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

	/***** calibrate the magneto meter ****/
	void calibrateMagnetometer() {
		MagnetometerCalibrationCoefficients.INSTANCE.val.x = 0.0;
		MagnetometerCalibrationCoefficients.INSTANCE.val.y = 0.0;
		MagnetometerCalibrationCoefficients.INSTANCE.val.z = 0.0;

		mMagCalibrateRequest = true;
	}

	void calibrateHeight() {
		mHeightCalibrateRequest = true;
	}

	Peripheral peripheral = null;
	Handler enableDataHandler = null;

	boolean isProcessing = false;
	private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();
			int status = intent.getIntExtra(BluetoothLeService.EXTRA_STATUS,
					BluetoothGatt.GATT_SUCCESS);

			String deviceAddress = intent
					.getStringExtra(BluetoothLeService.EXTRA_ADDRESS);

			peripheral = null;

			final ArrayList<Peripheral> processingPeripherals = new ArrayList<Peripheral>();

			for (int i = 0; i < mBtLeService.connectedPeripherals().size(); i++) {
				Peripheral peripheral = mBtLeService.connectedPeripherals()
						.get(i);

				if (peripheral.isConnecting())
					processingPeripherals.add(peripheral);
			}

			for (Peripheral pp : mBtLeService.connectedPeripherals()) {

				if (pp.address.equalsIgnoreCase(deviceAddress)) {
					peripheral = pp;
					break;
				}
			}

			if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED
					.equals(action)) {
				if (status == BluetoothGatt.GATT_SUCCESS) {

					enableDataHandler = new Handler() {

						public void handleMessage(android.os.Message msg) {

							isProcessing = false;
							try {
								processingPeripherals.remove(msg.what);
							} catch (Exception e) {
							}
							// System.out.println("processingPeripherals : "
							// + processingPeripherals.size()
							// + " in handler");

							for (int j = 0; j < processingPeripherals.size(); j++) {
								Peripheral peripheral = processingPeripherals
										.get(j);

								if (peripheral.isConnecting()) {
									isProcessing = true;
									System.out
											.println("processingPeripherals : "
													+ processingPeripherals
															.size()
													+ " in handler");
									getDataCollection(peripheral, true, j);
									break;
								}
							}
						};
					};

					// System.out.println("processingPeripherals : "
					// + processingPeripherals.size());
					if (processingPeripherals.size() > 0) {

						if (processingPeripherals.get(0).isConnecting()
								&& !isProcessing) {
							// System.out.println("processingPeripherals : "
							// + processingPeripherals.size());
							isProcessing = true;
							getDataCollection(processingPeripherals.get(0),
									true, 0);
						}
					}

				} else {
					Toast.makeText(getApplication(),
							"Service discovery failed", Toast.LENGTH_LONG)
							.show();
					return;
				}
			} else if (BluetoothLeService.ACTION_DATA_NOTIFY.equals(action)) {
				// Notification
				byte[] value = intent
						.getByteArrayExtra(BluetoothLeService.EXTRA_DATA);
				String uuidStr = intent
						.getStringExtra(BluetoothLeService.EXTRA_UUID);
				onCharacteristicChanged(uuidStr, value,
						peripheral.mBleDevice.getAddress());
			} else if (BluetoothLeService.ACTION_DATA_WRITE.equals(action)) {
				// Data written
				String uuidStr = intent
						.getStringExtra(BluetoothLeService.EXTRA_UUID);
				onCharacteristicWrite(uuidStr, status);
			} else if (BluetoothLeService.ACTION_DATA_READ.equals(action)) {
				// Data read
				String uuidStr = intent
						.getStringExtra(BluetoothLeService.EXTRA_UUID);
				byte[] value = intent
						.getByteArrayExtra(BluetoothLeService.EXTRA_DATA);
				onCharacteristicsRead(uuidStr, value, status,
						peripheral.mBleDevice.getAddress());

			}

			if (status != BluetoothGatt.GATT_SUCCESS) {
				// setError("GATT error code: " + status);
			}
		}
	};
	private int counter;

	private void getDataCollection(final Peripheral peripheral,
			final boolean enable, final int position) {
		Thread thread = new Thread() {

			public void run() {
				displayServices(peripheral);
				enableDataCollection(enable, peripheral);

				mBtLeService.setConnecting(peripheral.mBleDevice, false);

				enableDataHandler.sendEmptyMessage(position);
			};
		};

		thread.start();
	}

	private void onCharacteristicWrite(String uuidStr, int status) {
		// Log.d(TAG, "onCharacteristicWrite: " + uuidStr);
	}

	private void onCharacteristicChanged(String uuidStr, byte[] value,
			String deviceAddress) {

		if (mHeightCalibrateRequest) {
			if (uuidStr.equals(SensorTagGatt.UUID_BAR_DATA.toString())) {
				Point3D v = Sensor.BAROMETER.convert(value);

				BarometerCalibrationCoefficients.INSTANCE.heightCalibration = v.x;
				mHeightCalibrateRequest = false;
				// Toast.makeText(this, "Height measurement calibrated",
				// Toast.LENGTH_SHORT).show();
			}
		}

		if (uuidStr.equals(SensorTagGatt.UUID_MAG_DATA.toString())) {

			String calibrationMag = getSensorTagMagCalibration(deviceAddress);

			String calibration_mag_done = getSensorTagCalibrationMagDone(deviceAddress);

			if (calibration_mag_done.equalsIgnoreCase("0")) {
				if (calibrationMag.equalsIgnoreCase("1")) {
					Point3D v = Sensor.MAGNETOMETER.convert(value);

					MagnetometerCalibrationCoefficients.INSTANCE.val = v;

					updateCalibrationForSensorTag(deviceAddress,
							Constants.IS_MAG_CALIBRATED_TAG, "1");
				}
			}

		}
		if (uuidStr.equals(SensorTagGatt.UUID_GYR_DATA.toString())) {

			String calibrationGyro = getSensorTagGyroCalibration(deviceAddress);

			String calibration_gyro_done = getSensorTagCalibrationGyroDone(deviceAddress);

			if (calibration_gyro_done.equalsIgnoreCase("0")) {
				if (calibrationGyro.equalsIgnoreCase("1")) {
					Point3D v = Sensor.GYROSCOPE.convert(value);

					GyroScopeCaliberation.INSTANCE.val = v;

					updateCalibrationForSensorTag(deviceAddress,
							Constants.IS_GYRO_CALIBRATED_TAG, "1");

				}
			}

		}

		onCharacteristicChangedUpdate(uuidStr, value, deviceAddress);
		if (CaliberationFragment.getInstance() != null) {

			CaliberationFragment.getInstance().onCharacteristicChangedUpdate(
					uuidStr, value, deviceAddress);
		}
		// }
	}

	/***** get the sensor tag magneto meter calibraton *****/
	private String getSensorTagMagCalibration(String deviceAddress) {
		String calibraion_mag = null;

		db.open();
		Cursor CalibrationSensorTag = db
				.getSensorTagCalibrationMagneto(deviceAddress);
		if (CalibrationSensorTag.moveToFirst()) {
			do {

				calibraion_mag = CalibrationSensorTag.getString(0);

			} while (CalibrationSensorTag.moveToNext());
		}
		CalibrationSensorTag.close();
		db.close();
		return calibraion_mag;
	}

	/***** get the sensor tag gyroscope calibraton *****/
	private String getSensorTagGyroCalibration(String deviceAddress) {
		String calibraion_gyro = null;

		db.open();
		Cursor CalibrationSensorTag = db
				.getSensorTagCalibrationGyro(deviceAddress);
		if (CalibrationSensorTag.moveToFirst()) {
			do {

				calibraion_gyro = CalibrationSensorTag.getString(0);

			} while (CalibrationSensorTag.moveToNext());
		}
		CalibrationSensorTag.close();
		db.close();
		return calibraion_gyro;
	}

	/***** get the sensor tag magneto meter calibraton done *****/
	private String getSensorTagCalibrationMagDone(String deviceAddress) {
		String calibraion_mag_done = null;

		db.open();
		Cursor CalibrationSensorTag = db
				.getSensorTagCalibrationMagDone(deviceAddress);
		if (CalibrationSensorTag.moveToFirst()) {
			do {

				calibraion_mag_done = CalibrationSensorTag.getString(0);

			} while (CalibrationSensorTag.moveToNext());
		}
		CalibrationSensorTag.close();
		db.close();
		return calibraion_mag_done;
	}

	/***** get the sensor tag gyroscope calibraton done *****/
	private String getSensorTagCalibrationGyroDone(String deviceAddress) {
		String calibraion_gyro_done = null;

		db.open();
		Cursor CalibrationSensorTag = db
				.getSensorTagCalibrationGyroDone(deviceAddress);
		if (CalibrationSensorTag.moveToFirst()) {
			do {

				calibraion_gyro_done = CalibrationSensorTag.getString(0);

			} while (CalibrationSensorTag.moveToNext());
		}
		CalibrationSensorTag.close();
		db.close();
		return calibraion_gyro_done;
	}

	private void onCharacteristicsRead(String uuidStr, byte[] value,
			int status, String deviceAddress) {
		// Log.i(TAG, "onCharacteristicsRead: " + uuidStr);

		if (value == null)
			return;

		if (uuidStr.equals(SensorTagGatt.UUID_DEVINFO_FWREV.toString())) {

			String firmwareRevision = new String(value);

			db.open();
			db.updateDeviceInfo(deviceAddress, Constants.FORWARD_REVISION_TAG,
					firmwareRevision);
			db.close();

		}

		if (uuidStr.equals(SensorTagGatt.UUID_MANUFACTURER.toString())) {
			String manufacurerName = new String(value);

			db.open();
			db.updateDeviceInfo(deviceAddress, Constants.MANUFACTURER_NAME_TAG,
					manufacurerName);
			db.close();

		}
		if (uuidStr.equals(SensorTagGatt.UUID_HARDWARE_DIV_DEVICE.toString())) {
			String hardware_revision = new String(value);

			db.open();
			db.updateDeviceInfo(deviceAddress, Constants.HARDWARE_REVISION_TAG,
					hardware_revision);
			db.close();

		}
		if (uuidStr.equals(SensorTagGatt.UUID_SERIAL_NUMBER_DEVICE.toString())) {
			String serial_number = new String(value);

			db.open();
			db.updateDeviceInfo(deviceAddress, Constants.SERIAL_NUMBER_TAG,
					serial_number);
			db.close();

		}
		if (uuidStr.equals(SensorTagGatt.UUID_MODEL_NUMBER_DEVICE.toString())) {
			String model_number = new String(value);

			db.open();
			db.updateDeviceInfo(deviceAddress, Constants.MODEL_NUMBER_TAG,
					model_number);
			db.close();

		}

		if (uuidStr.equals(SensorTagGatt.UUID_BATTERY_CHAR.toString())) {

			int batteryLevel = binaryToInteger(value);

			db.open();
			db.updateDeviceInfo(deviceAddress, Constants.BATTERY_LEVEL_TAG,
					batteryLevel + "");
			db.close();

		}

		if (uuidStr.equals(SensorTagGatt.UUID_BAR_CALI.toString())) {
			// Sanity check
			if (value.length != 16)
				return;

			// Barometer calibration values are read.
			List<Integer> cal = new ArrayList<Integer>();
			for (int offset = 0; offset < 8; offset += 2) {
				Integer lowerByte = (int) value[offset] & 0xFF;
				Integer upperByte = (int) value[offset + 1] & 0xFF;
				cal.add((upperByte << 8) + lowerByte);
			}

			for (int offset = 8; offset < 16; offset += 2) {
				Integer lowerByte = (int) value[offset] & 0xFF;
				Integer upperByte = (int) value[offset + 1];
				cal.add((upperByte << 8) + lowerByte);
			}

			BarometerCalibrationCoefficients.INSTANCE.barometerCalibrationCoefficients = cal;
		}
	}

	/**
	 * Handle changes in sensor values
	 * */
	public void onCharacteristicChangedUpdate(String uuidStr, byte[] rawValue,
			String deviceAddress) {
		Point3D v;
		String msg;
		float mV = 0f;

		if (uuidStr.equals(SensorTagGatt.UUID_ACC_DATA.toString())) {
			v = Sensor.ACCELEROMETER.convert(rawValue);

			SensorTagRecordingView view = new SensorTagRecordingView(
					deviceAddress, sensorTag_listView, sensorTagsList);
			view.updateAccValues(v);

		}

		if (uuidStr.equals(SensorTagGatt.UUID_MAG_DATA.toString())) {
			v = Sensor.MAGNETOMETER.convert(rawValue);

			SensorTagRecordingView view = new SensorTagRecordingView(
					deviceAddress, sensorTag_listView, sensorTagsList);
			view.updateMagValues(v);

		}

		if (uuidStr.equals(SensorTagGatt.UUID_OPT_DATA.toString())) {
			v = Sensor.LUXOMETER.convert(rawValue);
			msg = decimal.format(v.x);

		}

		if (uuidStr.equals(SensorTagGatt.UUID_GYR_DATA.toString())) {
			v = Sensor.GYROSCOPE.convert(rawValue);

			SensorTagRecordingView view = new SensorTagRecordingView(
					deviceAddress, sensorTag_listView, sensorTagsList);
			view.updateGyroValues(v);

		}

		if (uuidStr.equals(SensorTagGatt.UUID_IRT_DATA.toString())) {
			v = Sensor.IR_TEMPERATURE.convert(rawValue);

			SensorTagRecordingView view = new SensorTagRecordingView(
					deviceAddress, sensorTag_listView, sensorTagsList);
			view.updateIRtemperature(v);

		}

		if (uuidStr.equals(SensorTagGatt.UUID_HUM_DATA.toString())) {
			v = Sensor.HUMIDITY.convert(rawValue);

			SensorTagRecordingView view = new SensorTagRecordingView(
					deviceAddress, sensorTag_listView, sensorTagsList);
			view.updateHumidityValues(v);

		}

		if (uuidStr.equals(SensorTagGatt.UUID_BAR_DATA.toString())) {

			v = Sensor.BAROMETER.convert(rawValue);

			SensorTagRecordingView view = new SensorTagRecordingView(
					deviceAddress, sensorTag_listView, sensorTagsList);
			view.updateBaroValue(v);
		}

		if (uuidStr.equals(SensorTagGatt.UUID_PH_MEASURE.toString())) {

			v = Sensor.PH_MEASUREMENT.convert(rawValue);

			msg = pH_decimal.format(v.x);

			mV = Float.parseFloat(msg);

			new PHThread(MainActivity.this, pH_listView_parentLayout,
					deviceAddress, mV, mV, decimal_2, msg, msg, pHSensorsList);

		}

		if (uuidStr.equals(SensorTagGatt.UUID_PH_TEMP_MEASURE.toString())) {
			v = Sensor.PH_TEMP_MEASUREMENT.convert(rawValue);

			counter = counter + 1;
			// System.out.println("counter values in display method............"
			// + counter);

			int raw = (int) v.x;

			// System.out.println("raw values......." + raw);

			float resistance = utils.convertRawtoResistance(raw);
			float temperature = utils
					.convertResistancetoTemperature(resistance);

			if (raw > 8000 || raw < 100) {
				/*** pH temperature probe disconnected ***/

				float mtc_value = 0.0f;
				String mtc_temperature = getMTC_ATC_Temp(deviceAddress);

				if (mtc_temperature != null) {
					mtc_value = Float.parseFloat(mtc_temperature);
				} else {
					mtc_value = 0.0f;
				}

				pH_temp_data = temp_decimal.format(mtc_value);

				String pH_temp = temp_decimal1.format(mtc_value);

				int reqpHPosition = -1;
				for (int i = 0; i < pHSensorsList.size(); i++) {
					if (deviceAddress.equalsIgnoreCase(pHSensorsList.get(i)
							.getDeviceAddress())) {

						reqpHPosition = i;
						break;
					}
				}
				if (reqpHPosition != -1) {
					PHSensor pHSensor = pHSensorsList.get(reqpHPosition);
					pHSensor.setDeviceAddress(deviceAddress);
					pHSensor.setTempProbeConnected(false);
					pHSensor.setTemp_value(pH_temp);

					pHSensorsList.set(reqpHPosition, pHSensor);

					// View pHSensorView = getPHSensorView(reqpHPosition);
					View pHSensorView = pH_listView_parentLayout
							.getChildAt(reqpHPosition);

					if (pHSensorView != null) {
						TextView temperature_type = (TextView) pHSensorView
								.findViewById(R.id.temperature_type);

						temperature_type.setText("MTC");

						TextView mtc_temp_value = (TextView) pHSensorView
								.findViewById(R.id.mtc_temp_value);
						mtc_temp_value.setText(pHSensorsList.get(reqpHPosition)
								.getTemp_value() + "ºC");

						System.out.println("temp probe value is ....."
								+ counter
								+ "th notifify..."
								+ pHSensorsList.get(reqpHPosition)
										.getTemp_value());

					}
				}

				if (PhCaliberationFragment.getInstance() != null) {

					PhCaliberationFragment.getInstance()
							.onCharacteristicUpdatedpH_Temp(
									deviceAddress,
									"MTC",
									pHSensorsList.get(reqpHPosition)
											.getTemp_value());
				}

			} else {
				/*** pH temperature probe connected ***/

				pH_temp_data = temp_decimal.format(temperature);

				String pH_temp = temp_decimal1.format(temperature);

				updateTempProbeValueFordevice(deviceAddress, pH_temp);

				int reqpHPosition = -1;
				for (int i = 0; i < pHSensorsList.size(); i++) {
					if (deviceAddress.equalsIgnoreCase(pHSensorsList.get(i)
							.getDeviceAddress())) {

						reqpHPosition = i;
						break;
					}
				}

				if (reqpHPosition != -1) {

					PHSensor pHSensor = pHSensorsList.get(reqpHPosition);
					pHSensor.setDeviceAddress(deviceAddress);
					pHSensor.setTempProbeConnected(true);
					pHSensor.setTemp_value(pH_temp);

					pHSensorsList.set(reqpHPosition, pHSensor);

					// View pHSensorView = getPHSensorView(reqpHPosition);
					View pHSensorView = pH_listView_parentLayout
							.getChildAt(reqpHPosition);

					if (pHSensorView != null) {
						TextView temperature_type = (TextView) pHSensorView
								.findViewById(R.id.temperature_type);

						temperature_type.setText("ATC");

						TextView mtc_temp_value = (TextView) pHSensorView
								.findViewById(R.id.mtc_temp_value);
						mtc_temp_value.setText(pHSensorsList.get(reqpHPosition)
								.getTemp_value() + "ºC");

					}

					if (PhCaliberationFragment.getInstance() != null) {

						PhCaliberationFragment.getInstance()
								.onCharacteristicUpdatedpH_Temp(
										deviceAddress,
										"ATC",
										pHSensorsList.get(reqpHPosition)
												.getTemp_value());
					}

				}
			}
		}

		if (uuidStr.equals(SensorTagGatt.UUID_KEY_DATA.toString())) {
			int keys = rawValue[0];
			SimpleKeysStatus s;
			final int imgBtn;
			s = Sensor.SIMPLE_KEYS.convertKeys((byte) (keys & 3));

			SensorTagRecordingView sT_view = new SensorTagRecordingView(
					deviceAddress, sensorTag_listView, sensorTagsList);

			View keysView = sT_view.getSensorTagKeysView();

			ImageView record_device_sT = null;
			ImageView switch_device_sT = null;

			if (keysView != null) {
				record_device_sT = (ImageView) keysView
						.findViewById(R.id.record_device);
				switch_device_sT = (ImageView) keysView
						.findViewById(R.id.switch_device);
			}

			switch (s) {

			/******* Click on switch button on main screen ********/
			case OFF_ON:

				/********* if toggle button in IR(Interval recording) is off. **********/

				if (!isRecordSwitched) {

					if (!IRDialogOpened) {
						openDialog();
					}

				} else {
					if (isStarted) {
						// do nothing and disable the action for switch button
					} else {
						if (isRecordPressed) {
							record_button
									.setImageResource(R.drawable.button_record);

							switch_device_sT.setPressed(true);
							switch_device_sT
									.setImageResource(R.drawable.switch_button_background);

							new Handler().postDelayed(new Runnable() {

								@Override
								public void run() {
									// TODO Auto-generated method stub
									for (int i = 0; i < sensorTagsList.size(); i++) {

										View view = getSensorTagView(i);

										if (view != null) {

											ImageView switch_device = (ImageView) view
													.findViewById(R.id.switch_device);
											ImageView record_device = (ImageView) view
													.findViewById(R.id.record_device);
											switch_device
													.setImageResource(R.drawable.switch_button);
											record_device
													.setImageResource(R.drawable.record_button);
										}
									}
								}
							}, 10);

							if (mSensorAdapter != null) {
								mSensorAdapter
										.setInteralRecordingActivated(false);
							}

							if (CaliberationFragment.getInstance() != null) {
								CaliberationFragment.record_device
										.setImageResource(R.drawable.record_button);
								CaliberationFragment.switch_device
										.setImageResource(R.drawable.switch_button);

								startCaiber = false;
								stopCaliber = false;
							}

							Global.getInstance()
									.storeBooleanType(MainActivity.this,
											"interval_toggle", false);

							mDatasets.setTextColor(Color.parseColor("#1284FF"));
							mValues.setTextColor(Color.parseColor("#1284FF"));

							handler.removeCallbacks(runnable);
							intervalRunning = false;

							isRecordPressed = false;
						} else {
							record_button
									.setImageResource(R.drawable.start_record);

							switch_device_sT.setPressed(true);
							switch_device_sT
									.setImageResource(R.drawable.switch_red_background);

							new Handler().post(new Runnable() {

								@Override
								public void run() {
									// TODO Auto-generated method stub
									for (int i = 0; i < sensorTagsList.size(); i++) {

										View view = getSensorTagView(i);

										if (view != null) {

											ImageView switch_device = (ImageView) view
													.findViewById(R.id.switch_device);
											ImageView record_device = (ImageView) view
													.findViewById(R.id.record_device);
											switch_device
													.setImageResource(R.drawable.switch_red_button);
											record_device
													.setImageResource(R.drawable.start_red_button);
										}
									}

								}
							});

							if (mSensorAdapter != null) {
								mSensorAdapter
										.setInteralRecordingActivated(true);
							}

							if (CaliberationFragment.getInstance() != null) {
								CaliberationFragment.record_device
										.setImageResource(R.drawable.start_red_button);
								CaliberationFragment.switch_device
										.setImageResource(R.drawable.switch_red_button);

								startCaiber = true;
								stopCaliber = true;
							}

							mDatasets.setTextColor(Color.parseColor("#f8622b"));
							mValues.setTextColor(Color.parseColor("#f8622b"));

							isRecordPressed = true;

							Global.getInstance().storeBooleanType(
									MainActivity.this, "interval_toggle", true);

						}
					}
				}

				break;

			/********** when clicking on record button *********/
			case ON_OFF:

				/***********
				 * when IR (Interval recording)value is activated and not
				 * started to record values
				 **********/

				if (connecting_status.getVisibility() == View.VISIBLE)
					return;

				if (isRecordPressed && !isStarted) {

					sample_points = Integer.parseInt(Global.getInstance()
							.getPreferenceVal(MainActivity.this,
									"interval_points"));

					isStarted = true;
					i = 0;
					handler.post(runnable);

					samples_pressed = Global.getInstance().getBooleanType(
							MainActivity.this, "samples_pressed");
					time_pressed = Global.getInstance().getBooleanType(
							MainActivity.this, "time_pressed");

					record_device_sT.setPressed(true);
					record_device_sT
							.setImageResource(R.drawable.stop_red_background);

					new Handler().post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub
							for (int i = 0; i < sensorTagsList.size(); i++) {

								View view = getSensorTagView(i);

								if (view != null) {

									ImageView switch_device = (ImageView) view
											.findViewById(R.id.switch_device);
									ImageView record_device = (ImageView) view
											.findViewById(R.id.record_device);
									switch_device
											.setImageResource(R.drawable.switch_black_button);
									record_device
											.setImageResource(R.drawable.stop_red_button);
								}
							}
						}
					});

					if (mSensorAdapter != null) {
						mSensorAdapter.setInteralRecordingStarted(true);
					}

					// ********* change the view of record and switch
					// buttons based on the main activity buttons record and
					// switch ********//

					if (CaliberationFragment.getInstance() != null) {
						CaliberationFragment.record_device
								.setImageResource(R.drawable.stop_red_button);
						CaliberationFragment.switch_device
								.setImageResource(R.drawable.switch_black_button);
						startCaiber = true;
						stopCaliber = false;
					}

					/********* when Samples pressed in IR(Interval Recording ********/

					if (samples_pressed) {
						record_button
								.setImageResource(R.drawable.red_connecting);

						sample_time_text.setVisibility(View.VISIBLE);
						sample_text.setVisibility(View.VISIBLE);
						sample_time_text.setText(String.valueOf(sample_points));
						sample_text.setText("Remaining");

						/********* when Time pressed in IR(Interval Recording ********/
					} else if (time_pressed) {
						record_button
								.setImageResource(R.drawable.red_connecting);

						sample_time_text.setVisibility(View.VISIBLE);
						sample_text.setVisibility(View.VISIBLE);
						sample_time_text.setText(String.valueOf(sample_time_x));
						sample_text.setText("Seconds");

					} else {
						record_button.setImageResource(R.drawable.stop_record);

					}

					/******** stop the recording if the recording is started already ********/

				} else if (isStarted)

				{

					isStarted = false;
					sample_time_text.setVisibility(View.GONE);
					sample_text.setVisibility(View.GONE);
					record_button.setImageResource(R.drawable.start_record);

					record_device_sT.setPressed(true);
					record_device_sT
							.setImageResource(R.drawable.start_red_button_pressed);

					new Handler().post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							for (int i = 0; i < sensorTagsList.size(); i++) {

								View view = getSensorTagView(i);

								if (view != null) {

									ImageView switch_device = (ImageView) view
											.findViewById(R.id.switch_device);
									ImageView record_device = (ImageView) view
											.findViewById(R.id.record_device);
									switch_device
											.setImageResource(R.drawable.switch_red_button);
									record_device
											.setImageResource(R.drawable.start_red_button);

								}
							}
						}
					});

					if (mSensorAdapter != null) {
						mSensorAdapter.setInteralRecordingStarted(false);
					}

					glow_animation_view.clearAnimation();
					glow_animation_view.setVisibility(View.GONE);

					if (CaliberationFragment.getInstance() != null) {
						CaliberationFragment.record_device
								.setImageResource(R.drawable.start_red_button);
						CaliberationFragment.switch_device
								.setImageResource(R.drawable.switch_red_button);

						startCaiber = true;
						stopCaliber = true;
					}

					handler.removeCallbacks(runnable);
					handler1.removeCallbacks(runnable1);
					intervalRunning = false;

					/******** if IR is not set initially *********/

				} else if (!isRecordPressed) {

					record_device_sT.setPressed(true);
					record_device_sT
							.setImageResource(R.drawable.record_button_background);

					new Handler().post(new Runnable() {

						@Override
						public void run() {
							// TODO Auto-generated method stub

							// record_device_sT.setPressed(false);

							for (int i = 0; i < sensorTagsList.size(); i++) {

								View view = getSensorTagView(i);

								if (view != null) {

									ImageView switch_device = (ImageView) view
											.findViewById(R.id.switch_device);
									ImageView record_device = (ImageView) view
											.findViewById(R.id.record_device);
									switch_device
											.setImageResource(R.drawable.switch_button);
									record_device
											.setImageResource(R.drawable.record_button);

									insertValuesForDataset("red");

								}
							}
						}
					});

				}

				break;
			case ON_ON:

				break;
			default:

				break;
			}

		}
	}

	private void updateTempProbeValueFordevice(String deviceAddress,
			String temperature) {
		// TODO Auto-generated method stub

		// db = new DataBaseHelper(MainActivity.this);
		db.open();
		db.updatePHCalibration(deviceAddress, Constants.TEMPERATURE_TAG,
				temperature);
		db.close();

	}

	// /*****
	// *
	// * Check the getting value from device value is valid or else check the
	// * probe
	// *
	// * @param pH
	// * @param pH_tview
	// * @param pHDecimal
	// */

	int checkcounter;

	public void checkProbe(final float pH, final TextView pH_tview,
			final String pHDecimal, final String mV,
			final String deviceAddress, final TextView mV_text,
			final boolean type_display1, LinearLayout ph_mv_text_layout,
			FrameLayout ph_error_text_layout, TextView ph_error_text) {

		boolean type_display = getmVType(deviceAddress);

		// checkcounter = checkcounter+1;
		counter = counter + 1;
		// System.out.println("check how many times..." + counter);

		for (int i = 0; i < pHSensorsList.size(); i++) {
			if (deviceAddress.equalsIgnoreCase(pHSensorsList.get(i)
					.getDeviceAddress())) {

				PHSensor sensor = pHSensorsList.get(i);

				if (sensor.ismDisconected()) {
					return;
				}
			}
		}

		if (pH > 14 || pH < 0.0) {

			ph_mv_text_layout.setVisibility(View.GONE);
			ph_error_text_layout.setVisibility(View.VISIBLE);

			ph_error_text.setText("Check \nProbe");

			mV_text.setVisibility(View.GONE);

		} else {
			ph_mv_text_layout.setVisibility(View.VISIBLE);
			ph_error_text_layout.setVisibility(View.GONE);
			if (type_display) {

				pH_tview.setText(mV);

				mV_text.setVisibility(View.VISIBLE);

			} else {
				System.out.println("pH value is ....." + counter
						+ "th notifify..." + pHDecimal);
				pH_tview.setText(pHDecimal);

				mV_text.setVisibility(View.GONE);

			}

		}
	}

	public boolean getmVType(String deviceAddress) {

		db.open();
		boolean mV_type = db.getmV(deviceAddress);
		db.close();

		return mV_type;
	}

	//
	// }

	// /*****
	// *
	// * Check the alarm to notify the sound when the values are set and on
	// *
	// * @param pH
	// */
	//
	public void checkAlarm(float pH, final TextView alarm_high,
			final TextView alarm_low, final ImageView alarm_image,
			Context context, final String deviceAddress, AlarmData alarmData1) {

		if (context != null) {

			AlarmData alarmData = mDatabaseManager
					.getAlarmDetailsForDevice(deviceAddress);

			boolean highpH = alarmData.isHighOn();
			boolean lowpH = alarmData.isLowOn();

			boolean isBeepPlaying = alarmData.isBeepPlaying();
			boolean isAlarmStopped = alarmData.isAlarmStopped();

			String high_pH_value = alarmData.getHighValue();
			String low_pH_value = alarmData.getLowValue();

			boolean isHighActive = false, isLowActive = false;

			if (highpH) {

				float highValuePh = Float.parseFloat(high_pH_value);

				if (highValuePh <= pH) {
					alarm_high.setTextColor(Color.parseColor("#ff0000"));
					alarm_image.setImageResource(R.drawable.alarm_red);

					if (!isBeepPlaying && !isAlarmStopped) {
						playBeep(deviceAddress);
					}

					isHighActive = true;
				} else {
					alarm_high.setTextColor(Color.parseColor("#000000"));

					if (isHighActive) {
						stopBeep(deviceAddress);
					}

				}
			}
			if (lowpH) {

				float lowValuePh = Float.parseFloat(low_pH_value);

				if (lowValuePh >= pH) {
					alarm_low.setTextColor(Color.parseColor("#ff0000"));
					alarm_image.setImageResource(R.drawable.alarm_red);
					if (!isBeepPlaying && !isAlarmStopped) {
						playBeep(deviceAddress);
					}
					isLowActive = true;
				} else {

					alarm_low.setTextColor(Color.parseColor("#000000"));

					if (isLowActive) {
						stopBeep(deviceAddress);
					}
				}
			}
			if (!highpH && !lowpH) {
				alarm_image.post(new Runnable() {

					@Override
					public void run() {
						// TODO Auto-generated method stub
						stopBeep(deviceAddress);
						alarm_high.setTextColor(Color.parseColor("#000000"));
						alarm_low.setTextColor(Color.parseColor("#000000"));
						alarm_image.setImageResource(R.drawable.alarm);
					}
				});

			}
		}

	}

	/***** play beep sound for the alarm activation *****/

	public void playBeep(final String deviceAddress) {
		try {

			m = null;
			m = new MediaPlayer();
			m.setLooping(false);
			AssetFileDescriptor descriptor = getAssets().openFd("alarm.mp3");
			m.setDataSource(descriptor.getFileDescriptor(),
					descriptor.getStartOffset(), descriptor.getLength());
			descriptor.close();

			m.prepare();
			m.setVolume(1f, 1f);
			m.setLooping(false);
			m.start();

			// isBeepPlaying = true;

			Thread thread = new Thread() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					super.run();
					mDatabaseManager.updateisBeepPlaying(deviceAddress, true);
				}
			};
			thread.start();
			// mDatabaseManager.updateisBeepPlaying(deviceAddress, true);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void stopBeep(final String deviceAddress) {
		if (m.isPlaying()) {
			m.stop();
			m.release();

			m = null;
			m = new MediaPlayer();
			m.setLooping(false);

			Thread thread = new Thread() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					super.run();
					mDatabaseManager.updateAlarmBeepPlay(deviceAddress, false,
							true);
				}
			};
			thread.start();
			// mDatabaseManager.updateAlarmBeepPlay(deviceAddress, false, true);
		}
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		// super.onBackPressed();
		try {
			Global.hideSoftKeyboard(this);
		} catch (Exception e) {
		}

		FragmentManager fragmentManager = mFragmentManager;
		int stackCount = fragmentManager.getBackStackEntryCount();
		if (stackCount == 0) {

			moveTaskToBack(true);

		} else
			removeCurrentFragment();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		// TODO Auto-generated method stub
		int stackCount = mFragmentManager.getBackStackEntryCount();
		if (stackCount == 0) {
			getSlidingMenu().setSlidingEnabled(true);
		} else {
			getSlidingMenu().setSlidingEnabled(false);
		}

		return super.dispatchTouchEvent(ev);
	}

	/**** when back button pressed, remove the fragment from the back stack *****/
	private void removeCurrentFragment() {
		final String currentFragmentTag = getSupportFragmentManager()
				.getBackStackEntryAt(
						getSupportFragmentManager().getBackStackEntryCount() - 1)
				.getName();

		if (currentFragmentTag.equalsIgnoreCase("datasets")) {
			Animation animation = AnimationUtils.loadAnimation(
					getApplicationContext(), R.anim.slide_out_down);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub

					mDataSetsFragment.setVisibility(View.GONE);

					getSupportFragmentManager().popBackStack();
					FragmentTransaction transaction = mFragmentManager
							.beginTransaction();
					Fragment currentFragment = mFragmentManager
							.findFragmentByTag(currentFragmentTag);
					if (currentFragment != null) {
						transaction.remove(currentFragment);
					}

					transaction.commit();

				}
			});

			dataset_title.setText(Global.getInstance().getPreferenceVal(
					getApplicationContext(), "datasetName"));
			mDataSetsFragment.startAnimation(animation);

		} else if (currentFragmentTag.equalsIgnoreCase("values")) {
			Animation animation = AnimationUtils.loadAnimation(
					getApplicationContext(), R.anim.slide_out_down);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub

					mValuesFragment.setVisibility(View.GONE);
					// Collections.reverse(curDataSet.getValueList());

					getSupportFragmentManager().popBackStack();
					FragmentTransaction transaction = mFragmentManager
							.beginTransaction();
					Fragment currentFragment = mFragmentManager
							.findFragmentByTag(currentFragmentTag);
					if (currentFragment != null)
						transaction.remove(currentFragment);
					transaction.commit();
				}
			});

			mValuesFragment.startAnimation(animation);
		} else if (currentFragmentTag.equalsIgnoreCase("new_dataset")) {
			Animation animation = AnimationUtils.loadAnimation(
					getApplicationContext(), R.anim.slide_out_down);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub

					mAddDatasetFragment.setVisibility(View.GONE);

					getSupportFragmentManager().popBackStack();
					FragmentTransaction transaction = mFragmentManager
							.beginTransaction();
					Fragment currentFragment = mFragmentManager
							.findFragmentByTag(currentFragmentTag);
					if (currentFragment != null)
						transaction.remove(currentFragment);
					transaction.commit();
				}
			});

			mAddDatasetFragment.startAnimation(animation);

		} else if (currentFragmentTag.equalsIgnoreCase("valuesDetails")) {

			Animation animation = AnimationUtils.loadAnimation(
					getApplicationContext(), R.anim.slide_out_down);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub

					mValueDetailsFragment.setVisibility(View.GONE);

					getSupportFragmentManager().popBackStack();
					FragmentTransaction transaction = mFragmentManager
							.beginTransaction();
					Fragment currentFragment = mFragmentManager
							.findFragmentByTag(currentFragmentTag);
					if (currentFragment != null)
						transaction.remove(currentFragment);
					transaction.commit();
				}
			});

			mValueDetailsFragment.startAnimation(animation);

		} else if (currentFragmentTag.equalsIgnoreCase("deviceInfo")) {
			Animation animation = AnimationUtils.loadAnimation(
					getApplicationContext(), R.anim.slide_out_down);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					System.out.println();
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub

					device_info_layout.setVisibility(View.GONE);

					getSupportFragmentManager().popBackStack();
					FragmentTransaction transaction = mFragmentManager
							.beginTransaction();
					Fragment currentFragment = mFragmentManager
							.findFragmentByTag(currentFragmentTag);
					if (currentFragment != null)
						transaction.remove(currentFragment);
					transaction.commit();

				}
			});

			device_info_layout.startAnimation(animation);
		} else if (currentFragmentTag.equalsIgnoreCase("dataset_info")) {
			Animation animation = AnimationUtils.loadAnimation(
					getApplicationContext(), R.anim.slide_out_down);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					System.out.println();
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub

					dataset_info_frag.setVisibility(View.GONE);

					getSupportFragmentManager().popBackStack();
					FragmentTransaction transaction = mFragmentManager
							.beginTransaction();
					Fragment currentFragment = mFragmentManager
							.findFragmentByTag(currentFragmentTag);
					if (currentFragment != null)
						transaction.remove(currentFragment);
					transaction.commit();

				}
			});

			dataset_title.setText(Global.getInstance().getPreferenceVal(
					getApplicationContext(), "datasetName"));

			dataset_info_frag.startAnimation(animation);

		}

		else if (currentFragmentTag.equalsIgnoreCase("caliberation")) {
			Animation animation = AnimationUtils.loadAnimation(
					getApplicationContext(), R.anim.slide_out_down);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					System.out.println();
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub

					caliber_fragment.setVisibility(View.GONE);

					getSupportFragmentManager().popBackStack();
					FragmentTransaction transaction = mFragmentManager
							.beginTransaction();
					Fragment currentFragment = mFragmentManager
							.findFragmentByTag(currentFragmentTag);
					if (currentFragment != null)
						transaction.remove(currentFragment);
					transaction.commit();
				}
			});

			caliber_fragment.startAnimation(animation);
		}

		else if (currentFragmentTag.equalsIgnoreCase("pH_caliberation")) {
			Animation animation = AnimationUtils.loadAnimation(
					getApplicationContext(), R.anim.slide_out_down);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					System.out.println();
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub

					pH_caliber_fragment.setVisibility(View.GONE);

					getSupportFragmentManager().popBackStack();
					FragmentTransaction transaction = mFragmentManager
							.beginTransaction();
					Fragment currentFragment = mFragmentManager
							.findFragmentByTag(currentFragmentTag);
					if (currentFragment != null)
						transaction.remove(currentFragment);
					transaction.commit();
				}
			});

			pH_caliber_fragment.startAnimation(animation);
		} else if (currentFragmentTag
				.equalsIgnoreCase("pH_caliberation_history")) {
			Animation animation = AnimationUtils.loadAnimation(
					getApplicationContext(), R.anim.slide_out_down);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					System.out.println();
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub

					pH_caliber_history.setVisibility(View.GONE);

					getSupportFragmentManager().popBackStack();
					FragmentTransaction transaction = mFragmentManager
							.beginTransaction();
					Fragment currentFragment = mFragmentManager
							.findFragmentByTag(currentFragmentTag);
					if (currentFragment != null)
						transaction.remove(currentFragment);
					transaction.commit();
				}
			});

			pH_caliber_history.startAnimation(animation);
		} else if (currentFragmentTag
				.equalsIgnoreCase("pH_caliberation_history_details")) {
			Animation animation = AnimationUtils.loadAnimation(
					getApplicationContext(), R.anim.slide_out_down);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					System.out.println();
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub

					pH_caliber_history_details.setVisibility(View.GONE);

					getSupportFragmentManager().popBackStack();
					FragmentTransaction transaction = mFragmentManager
							.beginTransaction();
					Fragment currentFragment = mFragmentManager
							.findFragmentByTag(currentFragmentTag);
					if (currentFragment != null)
						transaction.remove(currentFragment);
					transaction.commit();
				}
			});

			pH_caliber_history_details.startAnimation(animation);
		} else if (currentFragmentTag.equalsIgnoreCase("pH_device_info")) {
			Animation animation = AnimationUtils.loadAnimation(
					getApplicationContext(), R.anim.slide_out_down);
			animation.setAnimationListener(new Animation.AnimationListener() {

				@Override
				public void onAnimationStart(Animation animation) {
					// TODO Auto-generated method stub
					System.out.println();
				}

				@Override
				public void onAnimationRepeat(Animation animation) {
					// TODO Auto-generated method stub

				}

				@Override
				public void onAnimationEnd(Animation animation) {
					// TODO Auto-generated method stub

					if (!Global
							.getInstance()
							.getPreferenceVal(getApplicationContext(),
									"PH_DEVICE_NAME").equalsIgnoreCase("")) {

					}

					pH_info_fragment.setVisibility(View.GONE);

					getSupportFragmentManager().popBackStack();
					FragmentTransaction transaction = mFragmentManager
							.beginTransaction();
					Fragment currentFragment = mFragmentManager
							.findFragmentByTag(currentFragmentTag);
					if (currentFragment != null)
						transaction.remove(currentFragment);
					transaction.commit();
				}
			});

			pH_info_fragment.startAnimation(animation);
		}
	}

	@Override
	public void onAddButtonClick() {
		// TODO Auto-generated method stub
		AddDataSetFragment();
	}

	// create dataset fragment
	public void AddDataSetFragment() {
		FragmentTransaction transaction = mFragmentManager.beginTransaction();

		transaction.add(R.id.add_dataset_layout,
				NewDataSetFragment.newInstance(this), "new_dataset");
		transaction.addToBackStack("new_dataset");
		transaction.commit();
		Animation animation = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.slide_in_up);
		mAddDatasetFragment.setAnimation(animation);
		mAddDatasetFragment.setVisibility(View.VISIBLE);
	}

	@Override
	public void onDataSetInfo(DataSet dataset, String fromWhere) {
		// TODO Auto-generated method stub

		FragmentTransaction transaction = mFragmentManager.beginTransaction();
		DataSetInfoFragment fragment = DataSetInfoFragment.newInstance(this);
		fragment.setData(dataset, fromWhere);
		if (fragment.isAdded()) {
			return;
		}
		transaction.add(R.id.dataset_info_frag, fragment, "dataset_info");
		transaction.addToBackStack("dataset_info");
		transaction.commit();
		Animation animation = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.slide_in_up);
		dataset_info_frag.setAnimation(animation);
		dataset_info_frag.setVisibility(View.VISIBLE);
	}

	@Override
	public void onViewValues(String name, String id, DataSet dataset,
			String viewValues) {
		// TODO Auto-generated method stub
		openValuesFragment(name, id, dataset);
	}

	/************* retrieving datasets from database and adding into the array **************/

	public DataSet getLatestDataSetFromDb(String id) {
		// db = new DataBaseHelper(MainActivity.this);
		db.open();

		Cursor dataSets = db.getDataSetsForId(id);
		// DataSet dataSet = new DataSet();
		if (dataSets.moveToFirst()) {
			do {

				String mName = dataSets.getString(0);
				String mNotes = dataSets.getString(1);
				String mDate = dataSets.getString(2);
				String mTime = dataSets.getString(3);
				String mLocation = dataSets.getString(4);
				String mId = dataSets.getString(5);
				String mDateAndTime = dataSets.getString(6);

				curDataSet.setmName(mName);
				curDataSet.setmNotes(mNotes);
				curDataSet.setmDate(mDate);
				curDataSet.setmTime(mTime);
				curDataSet.setmLocation(mLocation);
				curDataSet.setmId(mId);
				curDataSet.setmDateAndTime(mDateAndTime);

			} while (dataSets.moveToNext());
		}
		dataSets.close();
		db.close();
		return curDataSet;
	}

	/***** get the values for dataset point ***/
	public Values getValuesForDataSetPoint(String id) {

		db.open();

		Values values = new Values();
		boolean firstTimeValue = false;

		ArrayList<SensorTag> sensorsList = null;
		ArrayList<PHSensor> pHSensorsList = null;

		Cursor dataSetValues = db.getDPValuesForDataSet(id);

		if (dataSetValues.moveToFirst()) {
			do {
				if (!firstTimeValue) {
					firstTimeValue = true;
					sensorsList = new ArrayList<SensorTag>();
					pHSensorsList = new ArrayList<PHSensor>();

					String mDataSetId = dataSetValues.getString(0);
					String time = dataSetValues.getString(1);
					String data_point_name = dataSetValues.getString(2);
					String data_point_notes = dataSetValues.getString(3);
					String data_point_location = dataSetValues.getString(4);
					String mId = dataSetValues.getString(5);

					values.setmDataSetId(mDataSetId);
					values.setmTime(time);
					values.setmName(data_point_name);
					values.setmNotes(data_point_notes);
					values.setmLocation(data_point_location);
					values.setmId(mId);
					Cursor data = db.getValuesForDataPoint(time);
					if (data.moveToFirst()) {
						do {
							String deviceAddress = data.getString(0);
							String deviceName = data.getString(1);

							if (deviceName.equalsIgnoreCase("SensorTag")) {
								String ir_temp = data.getString(2);
								String object_temp = data.getString(3);
								String humidity = data.getString(4);
								String pressure = data.getString(5);
								String mag_x = data.getString(6);
								String mag_y = data.getString(7);
								String mag_z = data.getString(8);

								String acc_x = data.getString(9);
								String acc_y = data.getString(10);
								String acc_z = data.getString(11);
								String gyro_x = data.getString(12);
								String gyro_y = data.getString(13);
								String gyro_z = data.getString(14);

								SensorTag sensorTag = new SensorTag();
								sensorTag.setaTempValue(ir_temp);
								sensorTag.setDeviceAddress(deviceAddress);
								sensorTag.setoTempValue(object_temp);
								sensorTag.setHumidity(humidity);
								sensorTag.setBaroValue(pressure);
								sensorTag.setMagnetometerValue_x(mag_x);
								sensorTag.setMagnetometerValue_y(mag_y);
								sensorTag.setMagnetometerValue_z(mag_z);
								sensorTag.setAccelerometerValue_x(acc_x);
								sensorTag.setAccelerometerValue_y(acc_y);
								sensorTag.setAccelerometerValue_z(acc_z);
								sensorTag.setGyroMeterValue_x(gyro_x);
								sensorTag.setGyroMeterValue_y(gyro_y);
								sensorTag.setGyroMeterValue_z(gyro_z);

								sensorsList.add(sensorTag);

								values.setSensorTagsList(sensorsList);
							} else if (deviceName.equalsIgnoreCase("pH-Meter")) {

								String pH_value = data.getString(15);
								String pH_temp = data.getString(16);

								PHSensor pHsensor = new PHSensor();
								pHsensor.setDeviceAddress(deviceAddress);
								pHsensor.setpHValue(pH_value);
								pHsensor.setTemp_value(pH_temp);

								pHSensorsList.add(pHsensor);

								values.setpHSensorsList(pHSensorsList);
							}

						} while (data.moveToNext());
					}
					data.close();
				}

				break;
			} while (dataSetValues.moveToNext());

		}
		dataSetValues.close();
		db.close();
		return values;

	}

	/***** when the device gets disconnected *****/
	@Override
	public void onDeviceDisconnect(String deviceName, String deviceAddress) {
		// TODO Auto-generated method stub
		if (deviceName.equalsIgnoreCase("SensorTag")) {
			// disconnected_text_layout.setVisibility(View.VISIBLE);
			// mSensorTagValuesLayout.setVisibility(View.GONE);

			int reqPosition = -1;

			for (int i = 0; i < sensorTagsList.size(); i++) {
				if (deviceAddress.equalsIgnoreCase(sensorTagsList.get(i)
						.getDeviceAddress())) {

					reqPosition = i;
					break;
				}
			}

			if (reqPosition != -1) {
				SensorTag sensorTag = sensorTagsList.get(reqPosition);
				sensorTag.setmDisconnected(true);

				sensorTagsList.set(reqPosition, sensorTag);
				mSensorAdapter.notifyDataSetChanged();
			}

			insertCalibrationSensorTag(deviceAddress);

			if (CaliberationFragment.getInstance() != null) {
				CaliberationFragment.getInstance().setDeviceConnectionStatus(
						deviceAddress, false);
			}

		} else if (deviceName.equalsIgnoreCase("pH-Meter")) {

			int reqPosition = -1;

			for (int i = 0; i < pHSensorsList.size(); i++) {
				if (deviceAddress.equalsIgnoreCase(pHSensorsList.get(i)
						.getDeviceAddress())) {

					reqPosition = i;
					break;
				}
			}

			if (reqPosition != -1) {
				PHSensor pHSensor = pHSensorsList.get(reqPosition);
				pHSensor.setmDisconected(true);

				View pHSensorView = pH_listView_parentLayout
						.getChildAt(reqPosition);

				if (pHSensorView != null) {

					TextView pHValue = (TextView) pHSensorView
							.findViewById(R.id.pH_value);

					LinearLayout ph_mv_text_layout = (LinearLayout) pHSensorView
							.findViewById(R.id.ph_mv_text_layout);
					FrameLayout ph_error_text_layout = (FrameLayout) pHSensorView
							.findViewById(R.id.ph_error_text_layout);
					TextView ph_error_text = (TextView) pHSensorView
							.findViewById(R.id.ph_error_text);

					ph_mv_text_layout.setVisibility(View.GONE);
					ph_error_text_layout.setVisibility(View.VISIBLE);
					ph_error_text.setText("Instrument \ndisconnected");

					TextView mtc_temp_value = (TextView) pHSensorView
							.findViewById(R.id.mtc_temp_value);
					mtc_temp_value.setText("-  -");

				}

			}

			if (PhCaliberationFragment.getInstance() != null) {
				PhCaliberationFragment.getInstance().setDeviceConnectionStatus(
						false);
			}

		}
	}

	/**** when the device gets selected *****/
	@Override
	public void onDeviceDeselect(String deviceName, String deviceAddress) {
		// TODO Auto-generated method stub
		if (deviceName.equalsIgnoreCase("SensorTag")) {

			int reqPosition = -1;

			for (int i = 0; i < sensorTagsList.size(); i++) {
				if (deviceAddress.equalsIgnoreCase(sensorTagsList.get(i)
						.getDeviceAddress())) {

					reqPosition = i;
					break;
				}
			}

			if (reqPosition != -1) {
				sensorTagsList.remove(reqPosition);

				mSensorAdapter.notifyDataSetChanged();
			}

			insertCalibrationSensorTag(deviceAddress);

		} else if (deviceName.equalsIgnoreCase("pH-Meter")) {
			// pH_layout.setVisibility(View.GONE);

			int reqpHPosition = -1;
			for (int i = 0; i < pHSensorsList.size(); i++) {
				if (deviceAddress.equalsIgnoreCase(pHSensorsList.get(i)
						.getDeviceAddress())) {

					reqpHPosition = i;
					break;
				}
			}

			if (reqpHPosition != -1) {
				pHSensorsList.remove(reqpHPosition);

				// mPHAdapter.notifyDataSetChanged();
				pH_listView_parentLayout.removeAllViews();
				mPHListInflater.notifyList(pHSensorsList,
						R.layout.ph_sensor_inflate);
			}

		}
	}

	@Override
	public void onCalibrationHistory(String deviceAddress) {
		// TODO Auto-generated method stub
		pHCalibrationHistory(deviceAddress);
	}

	@Override
	public void onCalibrationHistoryItemClick(
			PhCalibrationHistoryData calibrationHistoryData) {
		// TODO Auto-generated method stub
		pHCalibrationHistoryDetails(calibrationHistoryData);
	}

	/*** set the values for picker for temp picker ******/
	private void setPickerValues() {
		ArrayList<String> leftPickerValuesArray = new ArrayList<>();
		ArrayList<String> rightPickerValuesArray = new ArrayList<>();
		for (int i = 0; i < 91; i++) {
			leftPickerValuesArray.add("" + i);
		}

		for (int i = 0; i < 10; i++) {
			rightPickerValuesArray.add("" + i);
		}

		leftTempPicker.setMinValue(0);
		leftTempPicker.setMaxValue(90);
		rightTempPicker.setMinValue(0);
		rightTempPicker.setMaxValue(9);

		leftTempPicker.setWrapSelectorWheel(true);
		rightTempPicker.setWrapSelectorWheel(true);

	}

	/***** update the values for ph device after calibaration ****/
	public void updateNumberOfValuesForDevice(int numOfValues, String id) {

		String noOfValues = String.valueOf(numOfValues);

		db.updateNoOfValues(id, noOfValues);
	}

	/***** get no of values stored after calibration for pH meter *****/
	public String getNumberOfValuesforId(String latestId) {

		String numbOfValues = null;

		Cursor calibrationRecordedValues = db
				.getLatestNumberOfValuesforID(latestId);

		if (calibrationRecordedValues.moveToFirst()) {
			do {
				numbOfValues = calibrationRecordedValues.getString(0);
			} while (calibrationRecordedValues.moveToNext());
		}
		calibrationRecordedValues.close();

		return numbOfValues;

	}

	/****
	 * 
	 * purpose: get recent calibration history to store recording no of valuess
	 * 
	 * @return
	 */

	private String getRecentCalibrationHistoryId(String deviceAddress) {

		String mId = null;

		boolean firstCalibrationHistoryId = false;

		Cursor calibrationHistory = db
				.getLatestCalibrationHistoryID(deviceAddress);

		if (calibrationHistory.moveToFirst()) {
			do {
				if (!firstCalibrationHistoryId) {

					mId = calibrationHistory.getString(0);

					firstCalibrationHistoryId = true;
				}

			} while (calibrationHistory.moveToNext());
		}
		calibrationHistory.close();

		return mId;
	}

	/*********** conversions to get the values from pH sensor in pHs and mVs ********/
	public Integer binaryToInteger(byte[] data) {

		// char[] hexChars = new char[2];
		int result = 0;
		//

		for (int i = 0; i < data.length; i++) {
			result = result | ((data[i] & 0xff) << i * 8);
		}

		return result;
	}

	/*****
	 * open device info fragment to display the device inormation
	 */

	@Override
	public void openDeviceInfoFragment(String deviceAddress) {
		// TODO Auto-generated method stub
		FragmentTransaction transaction = mFragmentManager.beginTransaction();
		// transaction.setCustomAnimations(R.anim.slide_in_up,
		// R.anim.slide_none);
		transaction.add(R.id.device_info_layout,
				DeviceInfoFragment.newInstance(), "deviceInfo");
		transaction.addToBackStack("deviceInfo");
		DeviceInfoFragment.getInstance().setmDeviceAddress(deviceAddress);
		transaction.commit();
		Animation animation = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.slide_in_up);
		device_info_layout.setAnimation(animation);
		device_info_layout.setVisibility(View.VISIBLE);
	}

	@Override
	public void pHDeviceInfoFragment(String deviceAddress) {
		// TODO Auto-generated method stub
		FragmentTransaction transaction = mFragmentManager.beginTransaction();

		transaction.add(R.id.pH_info_fragment,
				PhDeviceInfoFragment.newInstance(), "pH_device_info");
		transaction.addToBackStack("pH_device_info");
		PhDeviceInfoFragment.getInstance().setmDeviceAddress(deviceAddress);
		transaction.commit();
		Animation animation = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.slide_in_up);
		pH_info_fragment.setAnimation(animation);
		pH_info_fragment.setVisibility(View.VISIBLE);
	}

	/****** open the sensor Tag calibration fragment ****/
	@Override
	public void openSensorTagCalibrationFragment(String deviceAddress) {
		// TODO Auto-generated method stub

		FragmentTransaction transaction = mFragmentManager.beginTransaction();

		transaction.add(R.id.caliber_fragment,
				CaliberationFragment.newInstance(), "caliberation");
		transaction.addToBackStack("caliberation");

		CaliberationFragment.getInstance().setDeviceAddress(deviceAddress);
		transaction.commit();
		Animation animation = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.slide_in_up);
		caliber_fragment.setAnimation(animation);
		caliber_fragment.setVisibility(View.VISIBLE);

		if (CaliberationFragment.getInstance() != null) {
			int reqPosition = -1;

			for (int i = 0; i < sensorTagsList.size(); i++) {
				if (deviceAddress.equalsIgnoreCase(sensorTagsList.get(i)
						.getDeviceAddress())) {

					reqPosition = i;
					break;
				}
			}

			if (reqPosition != -1) {
				SensorTag sensorTag = sensorTagsList.get(reqPosition);

				boolean status = sensorTag.ismDisconnected();
				String address = sensorTag.getDeviceAddress();

				CaliberationFragment.getInstance().setDeviceConnectionStatus(
						address, !status);

			}
		}

	}

	/**** open ph device calibration fragment *****/
	@Override
	public void openpHSensorCalibrationFragment(String deviceAddress) {
		// TODO Auto-generated method stub

		FragmentTransaction transaction = mFragmentManager.beginTransaction();

		transaction.add(R.id.pH_caliber_fragment,
				PhCaliberationFragment.newInstance(), "pH_caliberation");
		transaction.addToBackStack("pH_caliberation");

		PhCaliberationFragment.getInstance().setDeviceAddress(deviceAddress);
		transaction.commit();
		Animation animation = AnimationUtils.loadAnimation(
				getApplicationContext(), R.anim.slide_in_up);
		pH_caliber_fragment.setAnimation(animation);
		pH_caliber_fragment.setVisibility(View.VISIBLE);

		int reqpHPosition = -1;
		for (int i = 0; i < pHSensorsList.size(); i++) {
			if (deviceAddress.equalsIgnoreCase(pHSensorsList.get(i)
					.getDeviceAddress())) {

				reqpHPosition = i;
				break;
			}
		}
		if (reqpHPosition != -1) {
			PHSensor pHSensor = pHSensorsList.get(reqpHPosition);
			boolean status = pHSensor.ismDisconected();
			String address = pHSensor.getDeviceAddress();
			if (PhCaliberationFragment.getInstance() != null) {

				if (status) {
					PhCaliberationFragment.getInstance()
							.setDeviceConnectionStatus(false);
				} else {
					PhCaliberationFragment.getInstance()
							.setDeviceConnectionStatus(true);
				}

			}
		}

	}

	/***** temp picker layout for selecting temp values ****/
	public void tempPickerLayout(int reqPosition) {

		Animation animation = AnimationUtils.loadAnimation(MainActivity.this,
				R.anim.slide_in_up);

		animation.setAnimationListener(new Animation.AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
				System.out.println();
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onAnimationEnd(Animation animation) {
				// TODO Auto-generated method stub

				Animation fadeInAnimation = AnimationUtils.loadAnimation(
						MainActivity.this, android.R.anim.fade_in);

				picker_shadow_dummy_layout.setAnimation(fadeInAnimation);

				picker_shadow_dummy_layout.setVisibility(View.VISIBLE);

			}
		});

		picker_parent_layout.setAnimation(animation);
		picker_parent_layout.setVisibility(View.VISIBLE);
		pickerDone.setTag(reqPosition);

	}

	/***** get the temperature for particular pH device ***/
	public String getMTC_ATC_Temp(String deviceAddress) {
		String temperature = null;

		db.open();
		Cursor cursor_temperature = db.getpHTemperature(deviceAddress);
		if (cursor_temperature.moveToFirst()) {
			do {
				temperature = cursor_temperature.getString(0);
			} while (cursor_temperature.moveToNext());
		}

		cursor_temperature.close();
		db.close();

		return temperature;
	}

	/****** get the slope value for pH address *****/
	public String getSlopeForDevice(String deviceAddress) {
		String slope = null;
		db.open();
		String LatestId = getRecentCalibrationHistoryId(deviceAddress);
		if (LatestId == null) {
			return null;
		}

		Cursor slope_cursor = db.getSlopeForPhCalib(LatestId);
		if (slope_cursor.moveToFirst()) {
			do {
				slope = slope_cursor.getString(0);
			} while (slope_cursor.moveToNext());
		}

		slope_cursor.close();
		db.close();

		return slope;
	}

	/***** return the intercept value for pH device ****/
	public String getInterceptForDevice(String deviceAddress) {
		String intercept = null;
		db.open();
		String LatestId = getRecentCalibrationHistoryId(deviceAddress);
		if (LatestId == null) {
			return null;
		}

		Cursor intercept_cursor = db.getInterceptForPhCalib(LatestId);
		if (intercept_cursor.moveToFirst()) {
			do {
				intercept = intercept_cursor.getString(0);
			} while (intercept_cursor.moveToNext());
		}

		intercept_cursor.close();
		db.close();
		return intercept;
	}

	@Override
	public void onSensorDoneClick(String deviceAddress) {
		// TODO Auto-generated method stub

		int AddedDeviceposition = -1;

		for (int i = 0; i < sensorTagsList.size(); i++) {
			if (deviceAddress.equalsIgnoreCase(sensorTagsList.get(i)
					.getDeviceAddress())) {

				AddedDeviceposition = i;

			}
		}

		if (AddedDeviceposition != -1) {

			String deviceCustomName = getDeviceCustomNameForAddress(deviceAddress);
			SensorTag sensorTag = sensorTagsList.get(AddedDeviceposition);
			sensorTag.setDeviceName(deviceCustomName);
			sensorTagsList.set(AddedDeviceposition, sensorTag);
			mSensorAdapter.notifyDataSetChanged();
		}

	}

	@Override
	public void onpHDoneClick(String deviceAddress) {
		// TODO Auto-generated method stub

		int AddedpHDeviceposition = -1;

		for (int i = 0; i < pHSensorsList.size(); i++) {
			if (deviceAddress.equalsIgnoreCase(pHSensorsList.get(i)
					.getDeviceAddress())) {

				AddedpHDeviceposition = i;
			}
		}

		if (AddedpHDeviceposition != -1) {
			String deviceCustomName = getDeviceCustomNameForAddress(deviceAddress);
			PHSensor pHSensor = pHSensorsList.get(AddedpHDeviceposition);
			pHSensor.setDeviceName(deviceCustomName);

			pHSensorsList.set(AddedpHDeviceposition, pHSensor);

			pH_listView_parentLayout.removeAllViews();
			mPHListInflater.notifyList(pHSensorsList,
					R.layout.ph_sensor_inflate);
		}

	}

	/**** get the sensor Tag list view item for each to update the values *****/
	public View getSensorTagView(int reqPosition) {
		int wantedPosition = reqPosition; // Whatever position you're
		// looking for
		int firstPosition = sensorTag_listView.getFirstVisiblePosition()
				- sensorTag_listView.getHeaderViewsCount(); // This is the
		// same as child
		// #0
		int wantedChild = wantedPosition - firstPosition;
		// Say, first visible position is 8, you want position 10,
		// wantedChild will now be 2
		// So that means your view is child #2 in the ViewGroup:
		if (wantedChild < 0
				|| wantedChild >= sensorTag_listView.getChildCount()) {
			// Log.w(TAG,
			// "Unable to get view for desired position, because it's not being displayed on screen.");
			return null;
		}
		// Could also check if wantedPosition is between
		// listView.getFirstVisiblePosition() and
		// listView.getLastVisiblePosition() instead.
		View wantedView = sensorTag_listView.getChildAt(wantedChild);

		return wantedView;
	}

	/**** insert details for dataset ****/
	public void insertData(String d_name, String d_notes, String date,
			String time) {

		db.open();
		db.insertDataSets(d_name, d_notes, date, time, "",
				System.currentTimeMillis());
		db.close();

	}

	/**** get values for particular dataset *****/
	public ArrayList<Values> getValuesForDataSet(String id) {

		db.open();
		ArrayList<Values> array = new ArrayList<Values>();

		ArrayList<SensorTag> sensorsList = null;
		ArrayList<PHSensor> pHSensorsList = null;

		Cursor dataSetValues = db.getDPValuesForDataSet(id);
		if (dataSetValues.moveToFirst()) {
			do {

				sensorsList = new ArrayList<SensorTag>();
				pHSensorsList = new ArrayList<PHSensor>();

				String mDataSetId = dataSetValues.getString(0);
				String time = dataSetValues.getString(1);
				String data_point_name = dataSetValues.getString(2);
				String data_point_notes = dataSetValues.getString(3);
				String data_point_location = dataSetValues.getString(4);
				String mId = dataSetValues.getString(5);

				Values values = new Values();
				values.setmDataSetId(mDataSetId);
				values.setmTime(time);
				values.setmName(data_point_name);
				values.setmNotes(data_point_notes);
				values.setmLocation(data_point_location);
				values.setmId(mId);
				Cursor data = db.getValuesForDataPoint(time);
				if (data.moveToFirst()) {
					do {
						String deviceAddress = data.getString(0);
						String deviceName = data.getString(1);

						if (deviceName.equalsIgnoreCase("SensorTag")) {
							String ir_temp = data.getString(2);
							String object_temp = data.getString(3);
							String humidity = data.getString(4);
							String pressure = data.getString(5);
							String mag_x = data.getString(6);
							String mag_y = data.getString(7);
							String mag_z = data.getString(8);

							String acc_x = data.getString(9);
							String acc_y = data.getString(10);
							String acc_z = data.getString(11);
							String gyro_x = data.getString(12);
							String gyro_y = data.getString(13);
							String gyro_z = data.getString(14);

							SensorTag sensorTag = new SensorTag();
							sensorTag.setDeviceAddress(deviceAddress);
							sensorTag.setDeviceName(deviceName);
							sensorTag.setaTempValue(ir_temp);
							sensorTag.setoTempValue(object_temp);
							sensorTag.setHumidity(humidity);
							sensorTag.setBaroValue(pressure);
							sensorTag.setMagnetometerValue_x(mag_x);
							sensorTag.setMagnetometerValue_y(mag_y);
							sensorTag.setMagnetometerValue_z(mag_z);
							sensorTag.setAccelerometerValue_x(acc_x);
							sensorTag.setAccelerometerValue_y(acc_y);
							sensorTag.setAccelerometerValue_z(acc_z);
							sensorTag.setGyroMeterValue_x(gyro_x);
							sensorTag.setGyroMeterValue_y(gyro_y);
							sensorTag.setGyroMeterValue_z(gyro_z);

							sensorsList.add(sensorTag);

							values.setSensorTagsList(sensorsList);
						} else if (deviceName.equalsIgnoreCase("pH-Meter")) {

							String pH_value = data.getString(15);
							String pH_temp = data.getString(16);
							String pH_mV = data.getString(17);

							PHSensor pHsensor = new PHSensor();
							pHsensor.setDeviceAddress(deviceAddress);
							pHsensor.setDeviceName(deviceName);
							pHsensor.setpHValue(pH_value);
							pHsensor.setTemp_value(pH_temp);
							pHsensor.setmV(pH_mV);

							pHSensorsList.add(pHsensor);

							values.setpHSensorsList(pHSensorsList);
						}

					} while (data.moveToNext());
				}

				data.close();

				array.add(values);

			} while (dataSetValues.moveToNext());
		}
		dataSetValues.close();
		db.close();
		return array;

	}

	/******
	 * get the values for dataset and add it to the dataset when the app
	 * launched everytime
	 *****/
	private class GetValuesForDataSet extends
			AsyncTask<String, ArrayList<Values>, ArrayList<Values>> {

		private ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();

			// if (number > 200) {
			progressDialog = new ProgressDialog(MainActivity.this);

			progressDialog.setMessage("Loading values...");
			progressDialog.setCancelable(false);
			progressDialog.show();
			// }

		}

		@Override
		protected ArrayList<Values> doInBackground(String... params) {
			// TODO Auto-generated method stub

			ArrayList<Values> valuesList = getValuesForDataSet(dataSetId);

			return valuesList;
		}

		@Override
		protected void onPostExecute(ArrayList<Values> valuesList) {
			// TODO Auto-generated method stub
			super.onPostExecute(valuesList);

			curDataSet = new DataSet();

			getLatestDataSetFromDb(dataSetId);

			curDataSet.setValuesDataList(valuesList);

			if (progressDialog != null && progressDialog.isShowing()) {
				try {
					progressDialog.dismiss();

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

	}
}
