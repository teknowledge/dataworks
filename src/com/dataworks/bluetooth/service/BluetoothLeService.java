package com.dataworks.bluetooth.service;

import java.util.ArrayList;
import java.util.List;

import com.dataworks.Peripheral;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

// import android.util.Log;

/**
 * Service for managing connection and data communication with a GATT server
 * hosted on a given Bluetooth LE device.
 */
public class BluetoothLeService extends Service {
	static final String TAG = "BluetoothLeService";

	public final static String ACTION_GATT_CONNECTING = "com.dataworks.bluetooth.service.ACTION_GATT_CONNECTING";
	public final static String ACTION_GATT_CONNECTED = "com.dataworks.bluetooth.service.ACTION_GATT_CONNECTED";
	public final static String ACTION_GATT_DISCONNECTING = "com.dataworks.bluetooth.service.ACTION_GATT_DISCONNECTING";
	public final static String ACTION_GATT_DISCONNECTED = "com.dataworks.bluetooth.service.ACTION_GATT_DISCONNECTED";

	public final static String ACTION_GATT_SERVICES_DISCOVERED = "com.dataworks.bluetooth.service.ACTION_GATT_SERVICES_DISCOVERED";
	public final static String ACTION_DATA_READ = "com.dataworks.bluetooth.service.ACTION_DATA_READ";
	public final static String ACTION_DATA_NOTIFY = "com.dataworks.bluetooth.service.ACTION_DATA_NOTIFY";
	public final static String ACTION_DATA_WRITE = "com.dataworks.bluetooth.service.ACTION_DATA_WRITE";
	public final static String EXTRA_DATA = "com.dataworks.bluetooth.service.EXTRA_DATA";
	public final static String EXTRA_UUID = "com.dataworks.bluetooth.service.EXTRA_UUID";
	public final static String EXTRA_STATUS = "com.dataworks.bluetooth.service.EXTRA_STATUS";
	public final static String EXTRA_ADDRESS = "com.dataworks.bluetooth.service.EXTRA_ADDRESS";

	// BLE
	private BluetoothManager mBluetoothManager = null;
	private BluetoothAdapter mBtAdapter = null;
	private BluetoothGatt mBluetoothGatt = null;
	private static BluetoothLeService mThis = null;
	private volatile boolean mBusy = false; // Write/read pending response
	private String mBluetoothDeviceAddress;
	private ArrayList<Peripheral> peripherals = null;

	/**
	 * Manage the BLE service
	 */
	public class LocalBinder extends Binder {
		public BluetoothLeService getService() {
			return BluetoothLeService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		return binder;
	}

	@Override
	public boolean onUnbind(Intent intent) {
		// After using a given device, you should make sure that
		// BluetoothGatt.close() is called
		// such that resources are cleaned up properly. In this particular
		// example,
		// close() is
		// invoked when the UI is disconnected from the Service.
		close();
		return super.onUnbind(intent);
	}

	private final IBinder binder = new LocalBinder();

	/**
	 * Initializes a reference to the local Bluetooth adapter.
	 * 
	 * @return Return true if the initialization is successful.
	 */
	public boolean initialize() {
		// For API level 18 and above, get a reference to BluetoothAdapter
		// through
		// BluetoothManager.

		if (peripherals == null) {
			peripherals = new ArrayList<Peripheral>();

		}
		mThis = this;
		if (mBluetoothManager == null) {
			mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
			if (mBluetoothManager == null) {
				// Log.e(TAG, "Unable to initialize BluetoothManager.");
				return false;
			}
		}

		mBtAdapter = mBluetoothManager.getAdapter();
		if (mBtAdapter == null) {
			// Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
			return false;
		}

		return true;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// Log.i(TAG, "Received start id " + startId + ": " + intent);
		// We want this service to continue running until it is explicitly
		// stopped, so return sticky.
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (mBluetoothGatt != null) {
			mBluetoothGatt.close();
			mBluetoothGatt = null;
		}
	}

	/**
	 * Connects to the GATT server hosted on the Bluetooth LE device.
	 * 
	 * @param address
	 *            The device address of the destination device.
	 * 
	 * @return Return true if the connection is initiated successfully. The
	 *         connection result is reported asynchronously through the
	 *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 *         callback.
	 */
	public boolean connect(final BluetoothDevice device) {

		boolean connected = false;

		Peripheral peripheral = getPeripheral(device);

		if (peripheral == null) {
			peripheral = new Peripheral(device, this);
			peripheral.connect();
			peripherals.add(peripheral);
		} else {
			peripheral.connect();
		}

		return connected;
	}

	public void setConnecting(BluetoothDevice device, boolean isConnecting) {

		for (int i = 0; i < peripherals.size(); i++) {
			Peripheral peripheral = peripherals.get(i);

			if (peripheral.mBleDevice.getAddress().equalsIgnoreCase(
					device.getAddress())) {
				peripheral.setConnecting(isConnecting);
				peripherals.set(i, peripheral);
				break;
			}
		}
	}

	public Peripheral getPeripheral(BluetoothDevice device) {
		Peripheral peripheral = null;

		for (Peripheral pp : peripherals) {
			if (pp.mBleDevice.getAddress()
					.equalsIgnoreCase(device.getAddress())) {
				peripheral = pp;
				break;
			}

		}

		return peripheral;

	}

	public void removePeripheral(Peripheral peripheral) {
		peripherals.remove(peripheral);
	}

	/**
	 * Disconnects an existing connection or cancel a pending connection. The
	 * disconnection result is reported asynchronously through the
	 * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
	 * callback.
	 */
	public void disconnect(BluetoothDevice device) {
		Peripheral peripheral = getPeripheral(device);
		peripheral.disconnect();

		System.out.println(device.getName() + "    ....disconnect method");

	}

	/**
	 * After using a given BLE device, the app must call this method to ensure
	 * resources are released properly.
	 */
	public void close() {
		if (mBluetoothGatt != null) {
			// Log.i(TAG, "close");
			mBluetoothGatt.close();
			mBluetoothGatt = null;
		}
	}

	public int numConnectedDevices() {
		int n = 0;

		if (mBluetoothGatt != null) {
			List<BluetoothDevice> devList;
			devList = mBluetoothManager
					.getConnectedDevices(BluetoothProfile.GATT);
			n = devList.size();
		}
		return n;
	}

	//
	// Utility functions
	//
	public BluetoothGatt getBtGatt(BluetoothDevice device) {
		Peripheral peripheral = getPeripheral(device);
		return peripheral.mBluetoothGatt;
	}

	public static BluetoothManager getBtManager() {
		return mThis.mBluetoothManager;
	}

	public static BluetoothLeService getInstance() {
		return mThis;
	}

	public ArrayList<Peripheral> connectedPeripherals() {

		return peripherals;

	}

}
