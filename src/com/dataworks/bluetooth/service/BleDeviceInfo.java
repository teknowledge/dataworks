package com.dataworks.bluetooth.service;

import android.bluetooth.BluetoothDevice;

public class BleDeviceInfo {
	// Data
	private BluetoothDevice mBtDevice;
	private int mRssi;
	private boolean isConnecting;
	private boolean isConnected;
	private boolean isDisconnecting;
	private boolean isDisconnected;
	private boolean isDeselected;
	private boolean isDeviceFound;

	private boolean isDeviceDeselected;

	public boolean isDeviceDeselected() {
		return isDeviceDeselected;
	}

	public void setDeviceDeselected(boolean isDeviceDeselected) {
		this.isDeviceDeselected = isDeviceDeselected;
	}

	public boolean isDeviceFound() {
		return isDeviceFound;
	}

	public void setDeviceFound(boolean isDeviceFound) {
		this.isDeviceFound = isDeviceFound;
	}

	public boolean isDeselected() {
		return isDeselected;
	}

	public void setDeselected(boolean isDeselected) {
		this.isDeselected = isDeselected;
	}

	public boolean isConnecting() {
		return isConnecting;
	}

	public void setConnecting(boolean isConnecting) {
		this.isConnecting = isConnecting;
	}

	public boolean isConnected() {
		return isConnected;
	}

	public void setIsconnected(boolean isConnected) {
		this.isConnected = isConnected;
	}

	public boolean isDisconnecting() {
		return isDisconnecting;
	}

	public void setDisconnecting(boolean isDisconnecting) {
		this.isDisconnecting = isDisconnecting;
	}

	public boolean isDisconnected() {
		return isDisconnected;
	}

	public void setDisconnected(boolean isDisconnected) {
		this.isDisconnected = isDisconnected;
	}

	public BleDeviceInfo(BluetoothDevice device, int rssi) {
		mBtDevice = device;
		mRssi = rssi;
	}

	public BleDeviceInfo(BluetoothDevice device) {
		mBtDevice = device;

	}

	public BluetoothDevice getBluetoothDevice() {
		return mBtDevice;
	}

	public int getRssi() {
		return mRssi;
	}

	public void updateRssi(int rssiValue) {
		mRssi = rssiValue;
	}

}
